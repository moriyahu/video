//
//  SignUpAddPhotoViewController.m
//  Video
//
//  Created by steve on 2020/4/3.
//  Copyright © 2020 steve. All rights reserved.
//

#import "SignUpAddPhotoViewController.h"
#import "SelectPhotoManager.h"
#import "SignUpAddInfoViewController.h"

#import <MJExtension/MJExtension.h>


@interface SignUpAddPhotoViewController () {
    UIButton *logoBt;
    
}

@property (nonatomic, strong) SelectPhotoManager *photoManager;

@end

@implementation SignUpAddPhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
    [self setdata];
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = YES;
}

- (void)setupUI {
    
    self.view.backgroundColor = BGDarkBlueColor;
    
    UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(10), StatusBarHeight + ZoomSize(5), ZoomSize(40), ZoomSize(30))];
    [self.view addSubview:backBt];
    [backBt setImage:GetImageByName(@"common_back") forState:UIControlStateNormal];
    [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    backBt.hidden = YES;
    
    UILabel *titleLb = [[UILabel alloc] init];
    titleLb.text = @"Let's build your profile";
    titleLb.textAlignment = NSTextAlignmentCenter;
    titleLb.textColor = WhiteColor;
    titleLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightSemibold];
    [self.view addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(StatusBarHeight + ZoomSize(50));
        make.centerX.equalTo(self.view);
        make.width.mas_equalTo(ZoomSize(300));
        make.height.mas_equalTo(ZoomSize(30));
    }];
    
    
    UILabel *tipLb = [[UILabel alloc] init];
    tipLb.text = @"Please add at least one personal photo of \n yourself";
    tipLb.textAlignment = NSTextAlignmentCenter;
    tipLb.textColor = WhiteColor;
    tipLb.font = [UIFont systemFontOfSize:ZoomSize(16)];
    tipLb.numberOfLines = 0;
    [self.view addSubview:tipLb];
    [tipLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(titleLb.mas_bottom).offset(ZoomSize(15));
        make.left.mas_equalTo(ZoomSize(23));
        make.right.mas_equalTo(ZoomSize(-23));
        make.height.mas_equalTo(72);
    }];
    
    
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_Height -  ZoomSize(445) - BottomSafeAreaHeight, SCREEN_Width, ZoomSize(445) + BottomSafeAreaHeight )];
    [self.view addSubview:contentView];
    contentView.backgroundColor = UIColor.whiteColor;
    
    UIBezierPath *contentViewMaskPath = [UIBezierPath bezierPathWithRoundedRect:contentView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(ZoomSize(36.5),ZoomSize(36.5))]; //设置部分圆角
    CAShapeLayer *contentViewMaskLayer = [[CAShapeLayer alloc] init];
    contentViewMaskLayer.frame = contentView.bounds;
    contentViewMaskLayer.path = contentViewMaskPath.CGPath;
    contentView.layer.mask = contentViewMaskLayer;
    
    
    logoBt = [[UIButton alloc] init]; //logo
    [contentView addSubview:logoBt];
    [logoBt addTarget:self action:@selector(addPhotoAction) forControlEvents:UIControlEventTouchUpInside];
    [logoBt setImage:GetImageByName(@"signup_add") forState:UIControlStateNormal];
    [logoBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(ZoomSize(90));
        make.centerX.equalTo(self.view);
        make.width.mas_equalTo(ZoomSize(118));
        make.height.mas_equalTo(ZoomSize(118));
    }];
    logoBt.layer.cornerRadius = ZoomSize(59);
    logoBt.layer.masksToBounds = YES;
    logoBt.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    [logoBt.imageView setContentMode:UIViewContentModeScaleAspectFill];
    
    
    UIButton *addPhotoButton = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(38), ZoomSize(300), SCREEN_Width - ZoomSize(76), ZoomSize(48))];
    [contentView addSubview:addPhotoButton];
    [addPhotoButton addTarget:self action:@selector(addPhotoAction) forControlEvents:UIControlEventTouchUpInside];
    addPhotoButton.layer.cornerRadius = ZoomSize(24);
    addPhotoButton.layer.masksToBounds = YES;
    
    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.frame = addPhotoButton.bounds;
    gl.startPoint = CGPointMake(0.56, -0.77);
    gl.endPoint = CGPointMake(0.46, 1.66);
    gl.colors = @[(__bridge id)[UIColor colorWithRed:238/255.0 green:48/255.0 blue:202/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:137/255.0 green:36/255.0 blue:244/255.0 alpha:1.0].CGColor];
    gl.locations = @[@(0), @(1.0f)];
    [addPhotoButton.layer addSublayer:gl];
    
    UILabel *addPhotoLb = [[UILabel alloc] initWithFrame:addPhotoButton.bounds];
    [addPhotoButton addSubview:addPhotoLb];
    addPhotoLb.text = @"Add a Photo";
    addPhotoLb.textAlignment = NSTextAlignmentCenter;
    addPhotoLb.textColor = WhiteColor;
    addPhotoLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightSemibold];
    
}

- (void)setdata {
    _photoManager = [[SelectPhotoManager alloc] init];
}

#pragma function

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)addPhotoAction {
    
    [_photoManager startSelectPhotoWithImageName:@"Choose Photo"];
    __weak __typeof (self) weakSelf = self;
    
    _photoManager.successHandle = ^(SelectPhotoManager * _Nonnull manager, UIImage * _Nonnull originalImage, UIImage * _Nonnull resizeImage) {
        __strong __typeof(self) strongSelf = weakSelf;
        [strongSelf ->logoBt setImage:resizeImage forState:UIControlStateNormal];
        
        NSData *originalImageData = UIImageJPEGRepresentation([DataTool compressSizeImage:originalImage], 0.25);
        NSData *resizeImageData = UIImageJPEGRepresentation([DataTool compressSizeImage:resizeImage], 0.25);
        
        NSMutableArray *dataArray = [NSMutableArray arrayWithObjects:originalImageData, resizeImageData, nil];
        [[NetTool shareInstance] putDatas:dataArray fileType:@"jpg" needShowHUD:YES callback:^(id  _Nullable obj) {
            NSMutableArray *getArray = obj;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([DataTool isEmptyString:[UserInfo shareInstant].user.nickName] || [DataTool isEmptyString:[UserInfo shareInstant].user.birthday] || [DataTool isEmptyString:[UserInfo shareInstant].user.sex] ) {
                    
                    SignUpAddInfoViewController *vc = [[SignUpAddInfoViewController alloc] init];
                    vc.originalImageString = getArray[0];
                    vc.compressImageString = getArray[1];
                    [strongSelf.navigationController pushViewController:vc animated:vc];
                    
                } else {
                    
                    NSDictionary *paramsDic = @{ @"headPortraitUrl":getArray[0], @"headThumbnailUrl":getArray[1]};
                    
                    [[NetTool shareInstance] startRequest:API_User_Update method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
                        
                        NSDictionary *resultDic = obj;
                        UserProfileDataModel *userProfileDataModel = [UserProfileDataModel mj_objectWithKeyValues:resultDic];
                        
                        [[UserInfo shareInstant] checkUserDataModel:userProfileDataModel callBackInfo:^(id  _Nullable obj) {
                            [[UserInfo shareInstant] userLogin];
                        }];
                        
                    }];
                    
                }
                
            });
            
        }];
    };
    
}



@end

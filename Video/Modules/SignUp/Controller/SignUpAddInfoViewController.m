//
//  SignUpAddInfoViewController.m
//  Video
//
//  Created by steve on 2020/4/7.
//  Copyright © 2020 steve. All rights reserved.
//

#import "SignUpAddInfoViewController.h"
#import "JKPickView.h"
#import "AgePickView.h"

#import <MJExtension/MJExtension.h>
#import <CoreLocation/CoreLocation.h>

@interface SignUpAddInfoViewController () <CLLocationManagerDelegate>{
    
    UILabel *infoTipLb;
    
    UITextField *nameTF;
    UIButton *birthdayBt;
    NSString *getBirthdayString;
    
    UIButton *genderBt;
    NSString *getGenderString;
    UIButton *continueBt;
    
    CLLocationManager *locationmanager;
    NSString *locationString;
}

@end

@implementation SignUpAddInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self setdata];
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = YES;
}

- (void)setupUI {
    
    self.view.backgroundColor = BGDarkBlueColor;
    
    UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(10), StatusBarHeight + ZoomSize(5), ZoomSize(40), ZoomSize(30))];
    [self.view addSubview:backBt];
    [backBt setImage:GetImageByName(@"common_back") forState:UIControlStateNormal];
    [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *titleLb = [[UILabel alloc] init];
    titleLb.text = @"Introduce yourself";
    titleLb.textAlignment = NSTextAlignmentCenter;
    titleLb.textColor = WhiteColor;
    titleLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightSemibold];
    [self.view addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(StatusBarHeight + ZoomSize(50));
        make.centerX.equalTo(self.view);
        make.width.mas_equalTo(ZoomSize(300));
        make.height.mas_equalTo(ZoomSize(30));
    }];
    
    UILabel *tipLb = [[UILabel alloc] init];
    tipLb.text = @"Fill in the detail to increase the chance of \n a successful match";
    tipLb.textAlignment = NSTextAlignmentCenter;
    tipLb.textColor = WhiteColor;
    tipLb.font = [UIFont systemFontOfSize:ZoomSize(16)];
    tipLb.numberOfLines = 0;
    [self.view addSubview:tipLb];
    [tipLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(titleLb.mas_bottom).offset(ZoomSize(15));
        make.left.mas_equalTo(ZoomSize(23));
        make.right.mas_equalTo(ZoomSize(-23));
        make.height.mas_equalTo(72);
    }];
    
    
    infoTipLb = [[UILabel alloc] init];
    tipLb.text = @"Fill in the detail to increase the chance of \n a successful match";
    tipLb.textAlignment = NSTextAlignmentCenter;
    tipLb.textColor = WhiteColor;
    tipLb.font = [UIFont systemFontOfSize:ZoomSize(16)];
    tipLb.numberOfLines = 0;
    [self.view addSubview:tipLb];
    [tipLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(tipLb.mas_bottom).offset(ZoomSize(15));
        make.left.mas_equalTo(ZoomSize(23));
        make.right.mas_equalTo(ZoomSize(-23));
        make.height.mas_equalTo(72);
    }];
    
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_Height -  ZoomSize(445) - BottomSafeAreaHeight, SCREEN_Width, ZoomSize(445) + BottomSafeAreaHeight )];
    [self.view addSubview:contentView];
    contentView.backgroundColor = UIColor.whiteColor;
    
    UIBezierPath *contentViewMaskPath = [UIBezierPath bezierPathWithRoundedRect:contentView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(ZoomSize(36.5),ZoomSize(36.5))]; //设置部分圆角
    CAShapeLayer *contentViewMaskLayer = [[CAShapeLayer alloc] init];
    contentViewMaskLayer.frame = contentView.bounds;
    contentViewMaskLayer.path = contentViewMaskPath.CGPath;
    contentView.layer.mask = contentViewMaskLayer;
    
#pragma nameView
    UIView *nameView = [self createFuntionViewWithFrame:CGRectMake(0, ZoomSize(30), SCREEN_Width, ZoomSize(105)) andTitleString:@"Name"];
    [contentView addSubview:nameView];
    
    nameTF = [[UITextField alloc] initWithFrame:CGRectMake(ZoomSize(16), ZoomSize(42), SCREEN_Width - ZoomSize(32), ZoomSize(48))];
    [nameTF addTarget:self action:@selector(nameTFChange:) forControlEvents:UIControlEventEditingChanged];
    [nameView addSubview:nameTF];
    nameTF.backgroundColor = LightGrayColor;
    nameTF.layer.cornerRadius = ZoomSize(16);
    nameTF.layer.masksToBounds = YES;
    nameTF.font = [UIFont systemFontOfSize:ZoomSize(16)];
    nameTF.tintColor = BGDarkBlueColor;
    nameTF.textColor = BGDarkBlueColor;
    nameTF.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ZoomSize(13), 0)];
    nameTF.leftViewMode = UITextFieldViewModeAlways;
    
    NSMutableDictionary *attrs = [NSMutableDictionary dictionary]; //设置placeholder效果
    attrs[NSForegroundColorAttributeName] = TextGrayColor;
    NSAttributedString *attrStr = [[NSAttributedString alloc] initWithString:@"Add your first name" attributes:attrs];
    nameTF.attributedPlaceholder = attrStr;
    
#pragma birthdayView
    UIView *birthdayView = [self createFuntionViewWithFrame:CGRectMake(0, CGRectGetMaxY(nameView.frame), SCREEN_Width, ZoomSize(105)) andTitleString:@"Birthday"];
    [contentView addSubview:birthdayView];
    
    birthdayBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(16), ZoomSize(42), SCREEN_Width - ZoomSize(32), ZoomSize(48))];
    [birthdayView addSubview:birthdayBt];
    [birthdayBt addTarget:self action:@selector(brithdayAction) forControlEvents:UIControlEventTouchUpInside];
    birthdayBt.backgroundColor = LightGrayColor;
    birthdayBt.layer.cornerRadius = ZoomSize(16);
    [birthdayBt setTitle:@"YYYY-MM-DD" forState:UIControlStateNormal];
    [birthdayBt setTitleColor:TextGrayColor forState:UIControlStateNormal];
    [birthdayBt setImage:GetImageByName(@"common_arrow_right_blue") forState:UIControlStateNormal];
    birthdayBt.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft; //设置居左
    birthdayBt.titleEdgeInsets = UIEdgeInsetsMake(0, ZoomSize(5), 0, 0);
    birthdayBt.imageEdgeInsets = UIEdgeInsetsMake(0, SCREEN_Width - ZoomSize(55), 0, 0); //设置图片文字距离
    
#pragma genderView
    UIView *genderView = [self createFuntionViewWithFrame:CGRectMake(0, CGRectGetMaxY(birthdayView.frame), SCREEN_Width, ZoomSize(105)) andTitleString:@"Gender"];
    [contentView addSubview:genderView];
    
    genderBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(16), ZoomSize(42), SCREEN_Width - ZoomSize(32), ZoomSize(48))];
    [genderView addSubview:genderBt];
    [genderBt addTarget:self action:@selector(genderAction) forControlEvents:UIControlEventTouchUpInside];
    genderBt.backgroundColor = LightGrayColor;
    genderBt.layer.cornerRadius = ZoomSize(16);
    [genderBt setTitle:@"I dentify as a" forState:UIControlStateNormal];
    [genderBt setTitleColor:TextGrayColor forState:UIControlStateNormal];
    [genderBt setImage:GetImageByName(@"common_arrow_right_blue") forState:UIControlStateNormal];
    genderBt.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft; //设置居左
    genderBt.titleEdgeInsets = UIEdgeInsetsMake(0, ZoomSize(5), 0, 0);
    genderBt.imageEdgeInsets = UIEdgeInsetsMake(0, SCREEN_Width - ZoomSize(55), 0, 0); //设置图片文字距离
    
    continueBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(38), CGRectGetMaxY(genderView.frame) + ZoomSize(20), SCREEN_Width - ZoomSize(76), ZoomSize(48))];
    [continueBt addTarget:self action:@selector(continueAction) forControlEvents:UIControlEventTouchUpInside];
    continueBt.layer.cornerRadius = ZoomSize(24);
    continueBt.layer.masksToBounds = YES;
    [continueBt setTitle:@"Continue" forState:UIControlStateNormal];
    [continueBt setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    continueBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightSemibold];
    [continueBt setBackgroundImage:GetImageByName(@"btBG_disable") forState:UIControlStateNormal];
    continueBt.enabled = NO;
    
    [contentView addSubview:continueBt];
    
//#warning 缺失敏感词判断
    
}

- (void)setdata {
    
    locationString = [[NSLocale currentLocale] localizedStringForCountryCode:[[NSLocale currentLocale] objectForKey:NSLocaleCountryCode] ];
    
//    [self getLocaleCountry]; //获取当前数据，防止经纬度获取不到的问题
//    if ([CLLocationManager locationServicesEnabled] && [CLLocationManager authorizationStatus] > 2) {
//        locationmanager = [[CLLocationManager alloc]init];
//        locationmanager.delegate = self;
//        locationmanager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
//        [locationmanager startUpdatingLocation];
//    }
    
}

#pragma function

- (void) setContinueButton {
    
    if (![DataTool isEmptyString:nameTF.text] && ![DataTool isEmptyString:getBirthdayString] && ![DataTool isEmptyString:getGenderString]) {
        [continueBt setBackgroundImage:GetImageByName(@"btBG_enable") forState:UIControlStateNormal];
        continueBt.enabled = YES;
    } else {
        [continueBt setBackgroundImage:GetImageByName(@"btBG_disable") forState:UIControlStateNormal];
        continueBt.enabled = NO;
    }
    
}

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIView *) createFuntionViewWithFrame:(CGRect) frame andTitleString:(NSString *) titleString {
    UIView *getView = [[UIView alloc] initWithFrame:frame];
    
    UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(16), ZoomSize(5), ZoomSize(200), ZoomSize(22))];
    [getView addSubview:titleLb];
    titleLb.text = titleString;
    titleLb.textColor = BGDarkBlueColor;
    titleLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightSemibold];
    
    return getView;
}

- (void)genderAction {
    [self.view endEditing:YES];
    
    NSMutableArray *dataArray = [NSMutableArray arrayWithObjects:@"MALE", @"FEMALE", nil];
    NSMutableArray *showArray = [NSMutableArray arrayWithObjects:@"Male", @"Female", nil];
    
    JKPickView *pickview = [[JKPickView alloc] initWithFrame:self.view.frame];
    [pickview.dataArray addObjectsFromArray: showArray];
    pickview.titleString = @"Gender";
    [pickview show];
    
    pickview.back = ^(NSString *callBackString) {
        
        [self->genderBt setTitle:callBackString forState:UIControlStateNormal];
        [self->genderBt setTitleColor:BGDarkBlueColor forState:UIControlStateNormal];
        
        NSUInteger index = [showArray indexOfObject:callBackString];
        self->getGenderString = dataArray[index];
        
        [self setContinueButton];
    };
    
}

- (void)brithdayAction {
    [self.view endEditing:YES];
    
    AgePickView *pickview = [[AgePickView alloc] initWithFrame:self.view.frame];
    pickview.titleString = @"Birthday";
    [pickview show];
    
    pickview.back = ^(NSString *callBackString) {
        self->getBirthdayString = callBackString;
        [self->birthdayBt setTitle:self->getBirthdayString forState:UIControlStateNormal];
        [self->birthdayBt setTitleColor:BGDarkBlueColor forState:UIControlStateNormal];
        
        [self setContinueButton];
    };
}

- (void) nameTFChange :(UITextField *) textField {
    
    [self setContinueButton];
    if (textField.text.length > 20) {
        textField.text = [textField.text substringToIndex:20];
    }
}




- (void)continueAction {
    
    NSDictionary *paramsDic = @{@"nickName":nameTF.text, @"birthday":getBirthdayString, @"sex":getGenderString, @"headPortraitUrl":_originalImageString, @"headThumbnailUrl":_compressImageString, @"country": locationString };
    
    [[NetTool shareInstance] startRequest:API_User_Update method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
        
        NSDictionary *resultDic = obj;
        UserProfileDataModel *userProfileDataModel = [UserProfileDataModel mj_objectWithKeyValues:resultDic];
        
        [[UserInfo shareInstant] checkUserDataModel:userProfileDataModel callBackInfo:^(id  _Nullable obj) {
            [[UserInfo shareInstant] userLogin];
        }];

    }];
    
}


#pragma location Delegate
//-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
//    
//    CLLocation *currentLocation = [locations lastObject];
//    CLGeocoder *geoCoder = [[CLGeocoder alloc]init];
//    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"en",nil] forKey:@"AppleLanguages"]; // en
//    
//    [geoCoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) { //反地理编码
//        
//        if (placemarks.count > 0) {
//            CLPlacemark *placeMark = placemarks[0];
//            self->locationString = [NSString stringWithFormat:@"%@",placeMark.country];
//        }
//    }];
//}


@end

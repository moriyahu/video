//
//  SignUpAddInfoViewController.h
//  Video
//
//  Created by steve on 2020/4/7.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SignUpAddInfoViewController : UIViewController

@property (nonatomic, strong) NSString *originalImageString;
@property (nonatomic, strong) NSString *compressImageString;

@end

NS_ASSUME_NONNULL_END

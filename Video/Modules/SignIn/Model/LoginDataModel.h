//
//  LoginDataModel.h
//  Video
//
//  Created by steve on 2020/3/25.
//  Copyright © 2020 steve. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LoginDataModel : NSObject

@property (nonatomic, strong) NSString *email; //required
@property (nonatomic, strong) NSString *type; //required / REGISTER, LOGIN, RESET, CLOSE
//@property (nonatomic, strong) NSString *telephone;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *code;

@end

NS_ASSUME_NONNULL_END

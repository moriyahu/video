//
//  LoginViewController.m
//  Video
//
//  Created by steve on 2020/3/20.
//  Copyright © 2020 steve. All rights reserved.
//

#import "LoginViewController.h"
#import "UserProfileDataModel.h"
//#import "LoginDataModel.h"
#import "MJExtension.h"

#import "WebViewController.h"
#import "SignUpAddPhotoViewController.h"

#import <AuthenticationServices/AuthenticationServices.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface LoginViewController ()<UITextViewDelegate, ASAuthorizationControllerDelegate,ASAuthorizationControllerPresentationContextProviding>

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self setData];
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = YES;
}

- (void)setupUI {
    self.view.backgroundColor = UIColor.lightGrayColor;
    
    UIImageView *logoIV = [[UIImageView alloc] init]; //logo
    [self.view addSubview:logoIV];
    logoIV.image = GetImageByName(@"launch_background");
    logoIV.contentMode = UIViewContentModeScaleAspectFill;
    logoIV.backgroundColor = UIColor.whiteColor;
    [logoIV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
    }];
    
    
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_Height -  ZoomSize(277) - BottomSafeAreaHeight, SCREEN_Width, ZoomSize(277) + BottomSafeAreaHeight )];
    [self.view addSubview:contentView];
    contentView.backgroundColor = UIColor.whiteColor;
    
    UIBezierPath *contentViewMaskPath = [UIBezierPath bezierPathWithRoundedRect:contentView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(ZoomSize(36.5),ZoomSize(36.5))]; //设置部分圆角
    CAShapeLayer *contentViewMaskLayer = [[CAShapeLayer alloc] init];
    contentViewMaskLayer.frame = contentView.bounds;
    contentViewMaskLayer.path = contentViewMaskPath.CGPath;
    contentView.layer.mask = contentViewMaskLayer;
    
    if (@available(iOS 13.0, *)) {//13.0的新属性
        UIButton *appleIDSignInButton = [[UIButton alloc] init]; //tipLb
        [contentView addSubview:appleIDSignInButton];
        [appleIDSignInButton addTarget:self action:@selector(userAppIDLogin:) forControlEvents:UIControlEventTouchUpInside];
        
        appleIDSignInButton.backgroundColor = BlackColor;
        [appleIDSignInButton setTitle:@"Continue with Apple" forState:UIControlStateNormal];
        [appleIDSignInButton setTitleColor:WhiteColor forState:UIControlStateNormal];
        [appleIDSignInButton setImage:GetImageByName(@"apple") forState:UIControlStateNormal];
        appleIDSignInButton.imageEdgeInsets = UIEdgeInsetsMake(0, ZoomSize(-10), 0, 0);
        appleIDSignInButton.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightSemibold];
        [appleIDSignInButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(ZoomSize(43));
            make.left.mas_equalTo(ZoomSize(28));
            make.right.mas_equalTo(ZoomSize(-28));
            make.height.mas_equalTo(ZoomSize(48));
        }];
        appleIDSignInButton.layer.cornerRadius = ZoomSize(24);
    }
    
    
    UIButton *fbButton = [[UIButton alloc] init];
    [contentView addSubview:fbButton];
    [fbButton addTarget:self action:@selector(facebookLoginAction) forControlEvents:UIControlEventTouchUpInside];
    
    fbButton.backgroundColor = BGLightBlueColor;
    [fbButton setTitle:@"Continue with Facebook" forState:UIControlStateNormal];
    [fbButton setTitleColor:WhiteColor forState:UIControlStateNormal];
    [fbButton setImage:GetImageByName(@"facebook") forState:UIControlStateNormal];
    fbButton.imageEdgeInsets = UIEdgeInsetsMake(0, ZoomSize(-10), 0, 0);
    fbButton.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightSemibold];
    [fbButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(ZoomSize(111));
        make.left.mas_equalTo(ZoomSize(28));
        make.right.mas_equalTo(ZoomSize(-28));
        make.height.mas_equalTo(ZoomSize(48));
    }];
    fbButton.layer.cornerRadius = ZoomSize(24);
    
    
    NSString *content = @"By signing up you agree to our \n Terms of Service and Privacy Policy";
    UITextView *contentTextView = [[UITextView alloc] init];
    [contentView addSubview:contentTextView];
    contentTextView.attributedText = [self getContentLabelAttributedText:content];
    contentTextView.backgroundColor = UIColor.clearColor;
    contentTextView.linkTextAttributes = @{NSForegroundColorAttributeName:BGDarkBlueColor};
    
    contentTextView.textAlignment = NSTextAlignmentCenter;
    contentTextView.delegate = self;
    contentTextView.editable = NO;
    contentTextView.scrollEnabled = NO;
    [contentTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(fbButton.mas_bottom).mas_offset(ZoomSize(50));
        make.centerX.equalTo(self.view);
        make.width.mas_equalTo(ZoomSize(300));
        make.height.mas_equalTo(ZoomSize(60));
    }];
}

- (void)setData {
}

- (NSAttributedString *)getContentLabelAttributedText:(NSString *)text {
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:text attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:ZoomSize(16)],NSForegroundColorAttributeName:TextGrayColor, NSParagraphStyleAttributeName: paragraphStyle}];
    
    NSDictionary *attribtDic1 = @{NSLinkAttributeName: @"Terms://", NSUnderlineStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
    [attrStr addAttributes:attribtDic1 range:NSMakeRange(text.length - 36, 18)];
    
    NSDictionary *attribtDic2 = @{NSLinkAttributeName: @"Privacy://", NSUnderlineStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
    [attrStr addAttributes:attribtDic2 range:NSMakeRange(text.length - 14, 14)];
    
    return attrStr;
}

//#pragma mark - 授权成功的回调
- (void)authorizationController:(ASAuthorizationController *)controller didCompleteWithAuthorization:(ASAuthorization *)authorization  API_AVAILABLE(ios(13.0)) {
    
    if ([authorization.credential isKindOfClass:[ASAuthorizationAppleIDCredential class]]) {
        ASAuthorizationAppleIDCredential *credential = (ASAuthorizationAppleIDCredential *)authorization.credential; // 用户登录使用ASAuthorizationAppleIDCredential
        NSString *getIdentityTokenString = [[NSString alloc]initWithData:credential.identityToken encoding:NSUTF8StringEncoding];
        NSString *getEmailString = credential.email;
        if ([DataTool isEmptyString:getEmailString]) { //由于可能隐藏，这里使用存储
            getEmailString = [DataTool getContentFromKeyChain:@"appleSignInEmail"];
        } else {
            [DataTool saveContentToKeyChain:getEmailString forKey:@"appleSignInEmail" service:@"DeviceUUID"];
        }
        
        if ([DataTool isEmptyString:getEmailString]) { //如果还是为空，这里再次判断
            getEmailString = @"";
        }
        
        NSDictionary *paramsDic = @{@"deviceCode":[DataTool getDeviceUUID], @"email":getEmailString, @"identityToken":getIdentityTokenString, @"userId":credential.user };
        
        [[NetTool shareInstance] startRequest:API_Login_Apple method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
            
    
            NSDictionary *resultDic = obj;
            UserProfileDataModel *userProfileDataModel = [UserProfileDataModel mj_objectWithKeyValues:resultDic];
            
            [[UserInfo shareInstant] checkUserDataModel:userProfileDataModel callBackInfo:^(id  _Nullable obj) {
                
                if ([DataTool isEmptyString:userProfileDataModel.headPortraitUrl] && [DataTool isEmptyString:userProfileDataModel.headThumbnailUrl]  && [DataTool isEmptyString:userProfileDataModel.o_avatar] ) {
                     [DataTool showUploadPhotoPage];
                     return;
                } else {
                    [[UserInfo shareInstant] userLogin];
                }
                
            }];
            
            
            
        }];
        
    } else {
        [DataTool showHUDWithString:@"please retry later"];
    }
}

- (ASPresentationAnchor)presentationAnchorForAuthorizationController:(ASAuthorizationController *)controller API_AVAILABLE(ios(13.0)){
    return self.view.window;
}

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange interaction:(UITextItemInteraction)interaction {
    WebViewController *ActivityVC = [[WebViewController alloc] init];
    if ([[URL scheme] isEqualToString:@"Terms"]) {
        ActivityVC.url = Terms_Url;
    } else if ([[URL scheme] isEqualToString:@"Privacy"]) {
        ActivityVC.url = Privacy_Url;
    }
    [[DataTool getCurrentVC] presentViewController:ActivityVC animated:YES completion:nil];

    return YES;
}

#pragma function
- (void)userAppIDLogin:(ASAuthorizationAppleIDButton *)button  API_AVAILABLE(ios(13.0)){
    
    
    ASAuthorizationAppleIDProvider *appleIdProvider = [[ASAuthorizationAppleIDProvider alloc] init]; // 创建新的AppleID 授权请求
    ASAuthorizationAppleIDRequest *request = appleIdProvider.createRequest; // 在用户授权期间请求的联系信息
    request.requestedScopes = @[ASAuthorizationScopeEmail,ASAuthorizationScopeFullName];

    ASAuthorizationController *controller = [[ASAuthorizationController alloc] initWithAuthorizationRequests:@[request]];
    controller.delegate = self;
    controller.presentationContextProvider = self;
    [controller performRequests];
    
}

- (void)facebookLoginAction {

    [DataTool startShowHUD];

//    [loginManager logOut];
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    [loginManager logOut];
    
    [loginManager logInWithPermissions: @[@"public_profile"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        
        if (result.isCancelled) {
            [DataTool dismissHUD];
            return;
        }
        
        if ([DataTool isEmptyString:result.token.tokenString]) {
            [DataTool dismissHUD];
            return;
        }
        
        NSDictionary *paramsDic = @{@"deviceCode":[DataTool getDeviceUUID], @"accessToken":result.token.tokenString };
        [[NetTool shareInstance] startRequest:API_Login_Facebook method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
            NSLog(@"obj %@", obj);
            
            NSDictionary *resultDic = obj;
            UserProfileDataModel *userProfileDataModel = [UserProfileDataModel mj_objectWithKeyValues:resultDic];
            
            [[UserInfo shareInstant] checkUserDataModel:userProfileDataModel callBackInfo:^(id  _Nullable obj) {
               if ([DataTool isEmptyString:userProfileDataModel.headPortraitUrl] && [DataTool isEmptyString:userProfileDataModel.headThumbnailUrl] && [DataTool isEmptyString:userProfileDataModel.o_avatar] ) {
                    [DataTool showUploadPhotoPage];
                    return;
               } else {
                   [[UserInfo shareInstant] userLogin];
               }
                
            }];
            

        }];
    }];
}


@end

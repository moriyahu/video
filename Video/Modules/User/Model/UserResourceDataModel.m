//
//  UserResourceDataModel.m
//  Video
//
//  Created by steve on 2020/4/23.
//  Copyright © 2020 steve. All rights reserved.
//

#import "UserResourceDataModel.h"

@implementation UserResourceDataModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"resource_id":@"id"};
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.createTime forKey:@"createTime"];
    [aCoder encodeObject:self.resource_id forKey:@"resource_id"];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeInt:self.sort forKey:@"sort"];
    [aCoder encodeInt:self.state forKey:@"state"];
    
    [aCoder encodeObject:self.type forKey:@"type"];
    [aCoder encodeObject:self.updateTime forKey:@"updateTime"];
    [aCoder encodeObject:self.userId forKey:@"userId"];
    [aCoder encodeObject:self.userType forKey:@"userType"];
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super init]) {
        self.createTime = [aDecoder decodeObjectForKey:@"createTime"];
        self.resource_id = [aDecoder decodeObjectForKey:@"resource_id"];
        self.name = [aDecoder decodeObjectForKey:@"name"];
        self.sort = [aDecoder decodeIntForKey:@"sort"];
        self.state = [aDecoder decodeIntForKey:@"state"];
        
        self.type = [aDecoder decodeObjectForKey:@"type"];
        self.updateTime = [aDecoder decodeObjectForKey:@"updateTime"];
        self.userId = [aDecoder decodeObjectForKey:@"userId"];
        self.userType = [aDecoder decodeObjectForKey:@"userType"];
    }
    return self;
}


@end

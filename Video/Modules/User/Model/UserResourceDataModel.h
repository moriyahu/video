//
//  UserResourceDataModel.h
//  Video
//
//  Created by steve on 2020/4/23.
//  Copyright © 2020 steve. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserResourceDataModel : NSObject

@property (nonatomic, strong) NSString *createTime;
@property (nonatomic, strong) NSString *resource_id;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) int sort;
@property (nonatomic, assign) int state; //state 0.待审核 1.通过 2.拒绝
@property (nonatomic, strong) NSString *type;

@property (nonatomic, strong) NSString *updateTime;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *userType;


//createTime = "2020-04-23 15:22:11";
//id = 459791151164719104;
//name = "https://videoproject4test.oss-cn-hangzhou.aliyuncs.com/User/20200422064551609-2485.jpg";
//sort = 1;
//state = 0;
//type = PIC;
//updateTime = "2020-04-23 15:22:11";
//userId = 459789866260660224;
//userType = USER;

//{
////            createTime = "2020-05-20T14:35:43";
////            id = 469875059861266432;
////            name = "https://videoproject4test.oss-cn-hangzhou.aliyuncs.com/User/20200520023506456-2740.jpg";
////            sort = 0;
////            state = 0;
////            type = PIC;
////            updateTime = "2020-05-20T14:35:43";
////            userId = 469869386465320960;
////            userType = USER;
////        }
    

@end

NS_ASSUME_NONNULL_END

//
//  UserProfileDataModel.h
//  Video
//
//  Created by steve on 2020/3/23.
//  Copyright © 2020 steve. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserResourceDataModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserProfileDataModel : NSObject

@property (nonatomic, strong) NSString *aboutMe;
@property (nonatomic, strong) NSString *aboutMe_status;
@property (nonatomic, strong) NSString *avatar_status;
@property (nonatomic, strong) NSString *balance;
@property (nonatomic, strong) NSString *birthday;
//coinPermin = 0; //通话费用，每分钟多少代币

@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, assign) BOOL enableAddTimes;

@property (nonatomic, strong) NSString *headPortraitUrl;
@property (nonatomic, strong) NSString *headThumbnailUrl;
@property (nonatomic, strong) NSString *nickName;
@property (nonatomic, strong) NSString *nickName_status;

@property (nonatomic, strong) NSString *o_aboutMe; //过去的数据
@property (nonatomic, strong) NSString *o_avatar;
@property (nonatomic, strong) NSString *o_nickName;
@property (nonatomic, strong) NSString *onlineState;

@property (nonatomic, strong) NSArray *photos;
@property (nonatomic, strong) NSString *sex;
@property (nonatomic, strong) NSString *sig;
@property (nonatomic, strong) NSString *times;
@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSString *type;

@property (nonatomic, strong) NSArray *videos;
@property (nonatomic, strong) NSString *vipEndtime;
@property (nonatomic, strong) NSString *vipState;
@property (nonatomic, strong) NSString *userId; // id


#pragma 用于im信息
@property (nonatomic, strong) NSString *roomId; //
@property (nonatomic, strong) NSString *messageText; //

#pragma 用于socket显示礼物
@property (nonatomic, strong) NSString *giftName; //
@property (nonatomic, strong) NSString *giftPic; //
@property (nonatomic, strong) NSString *sendTime; //


@end

NS_ASSUME_NONNULL_END

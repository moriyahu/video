//
//  UserInfo.m
//  Video
//
//  Created by steve on 2020/3/23.
//  Copyright © 2020 steve. All rights reserved.
//


#import "SocketTool.h"
#import "UserInfo.h"

#import "TUIBubbleMessageCellData.h"
#import "TUITextMessageCellData.h"
#import "TUIKitConfig.h"
#import "TUIKit.h"

#import "ChatViewController.h"
#import "PurchaseTool.h"
#import "UserCallInView.h"
#import "UserMatchPopView.h"
#import "UserCallOutView.h"
#import "UserLikeOtherPopView.h"
#import "OtherLikeMePopView.h"
#import "TRTCVideoCallViewController.h"

#import <MJExtension.h>
#import <EBCustomBannerView.h>

static id _shareInstance = nil;

@interface UserInfo() {
}

//@property (nonatomic ,strong) UserProfileDataModel *videoCallIn_profileModel;
@property (nonatomic ,strong) EBCustomBannerView *callIn_CustomBannerView;
@property (nonatomic ,strong) UserCallOutView *callOutView;
@property (nonatomic ,strong) UserMatchPopView *matchPopView;
@property (nonatomic ,strong) TRTCVideoCallViewController *trtcVC;


@end

@implementation UserInfo

+ (instancetype)shareInstant {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shareInstance = [[UserInfo alloc] init];
    });
    return _shareInstance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self loadUserinfoFromSadeBox];
    }
    return self;
}

- (void)checkUserDataModel: (id) obj callBackInfo : (CallBack) callback {
    
    UserProfileDataModel *userProfileInfoModel = obj;
    [[UserInfo shareInstant] setupWithUserProfileInfoModel:userProfileInfoModel]; //设置信息，并保存信息
    [[UserInfo shareInstant] saveUserInfoToSadeBox];
    
    if (userProfileInfoModel.token == nil) { //非空检查
        [self userLogout];
        return;
    }
    
    //    if ([DataTool isEmptyString:userProfileInfoModel.headPortraitUrl] || [DataTool isEmptyString:userProfileInfoModel.headThumbnailUrl] || [userProfileInfoModel.avatar_status isEqualToString:@"AUDIT_REJECT"] ) {
    //        [DataTool showUploadPhotoPage];
    //        return;
    //    }
    
    //    if ([DataTool isEmptyString:userProfileInfoModel.headThumbnailUrl] ) {
    //        [DataTool showUploadPhotoPage];
    //        return;
    //    }
    
    callback(obj);
}

- (void)setupWithUserProfileInfoModel: (UserProfileDataModel *) profile {
    [UserInfo shareInstant].user = profile;
}

- (void)loadUserinfoFromSadeBox {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *userData = [userDefaults objectForKey:User];
    UserProfileDataModel *userProfile = [NSKeyedUnarchiver unarchiveObjectWithData:userData];
    
    
    self.user = userProfile;
}

- (void)saveUserInfoToSadeBox {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *profileData = [NSKeyedArchiver archivedDataWithRootObject: self.user];
    [userDefaults setObject:profileData forKey:User];
    [userDefaults synchronize];
}

- (void)cleancurrentUserinfo {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults]; //用户退出，切换时必须删除的数据
    NSString *pathBundle = [[NSBundle mainBundle]pathForResource:@"UserDefault" ofType:@"plist"];
    NSArray *userDefaultArray = [NSArray arrayWithContentsOfFile:pathBundle];
    
    for(NSString* key in userDefaultArray){
        [userDefaults removeObjectForKey:key];
    }
    [userDefaults synchronize];
}

- (void)deleteAllInfo {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictionary = [userDefaults dictionaryRepresentation];
    for(NSString* key in [dictionary allKeys]){
        [userDefaults removeObjectForKey:key];
    }
    [userDefaults synchronize];
}

- (void)userLogin {
    
    [self setUpTim]; //设置Tim
    [[SocketTool shareInstance] openSocket]; //建立socket
    [[PurchaseTool shareInstance] addTranObserver]; //添加支付监听
    [DataTool showHomePage];
}

- (void)userLogout {
    
    [[TIMManager sharedInstance] logout:^{
        NSLog(@"TIMManager logout success");
    } fail:^(int code, NSString *msg) {
        NSLog(@"TIMManager logout fail ");
    }];
    
    [self cleancurrentUserinfo];
    
    [SocketTool shareInstance].reConnectTime = 5;
    [[SocketTool shareInstance] closeSocket];
    
    [DataTool showLoginPage];
}

- (void) setUpTim {
    
    TIMLoginParam *loginParam = [[TIMLoginParam alloc] init];
    loginParam.identifier = [UserInfo shareInstant].user.userId;
    loginParam.userSig = [UserInfo shareInstant].user.sig;
//        loginParam.identifier = TestUserId;
//        loginParam.userSig = TestUserSig;
    
    loginParam.appidAt3rd = TXIMSDK_APP_KEY;
    
    [[TIMManager sharedInstance] login:loginParam succ:^{
        
        if (self->_timLoginSuccBlock) {
            self->_timLoginSuccBlock(@"Succ");
        }
        
        TUIKitConfig *config = [TUIKitConfig defaultConfig];
        config.defaultAvatarImage = [UIImage imageNamed:@"message_vido"];
        
        [TUIBubbleMessageCellData setOutgoingBubble:[UIImage imageNamed:@"message_bubble_send"]];
        [TUIBubbleMessageCellData setOutgoingHighlightedBubble:[UIImage imageNamed:@"message_bubble_send"]];
        [TUIBubbleMessageCellData setIncommingBubble:[UIImage imageNamed:@"message_bubble_get"]];
        [TUIBubbleMessageCellData setIncommingHighlightedBubble:[UIImage imageNamed:@"message_bubble_get"]];
        
        [TUITextMessageCellData setIncommingNameFont:[UIFont systemFontOfSize:16]];
        [TUITextMessageCellData setIncommingTextColor:BGDarkBlueColor];
        [TUITextMessageCellData setOutgoingTextFont:[UIFont systemFontOfSize:16]];
        [TUITextMessageCellData setOutgoingTextColor:WhiteColor];
        
        // 设置发送头像大小；设置接收的方法类似
        [TUIMessageCellLayout incommingMessageLayout].avatarSize = CGSizeMake(0, 0);
        [TUIMessageCellLayout incommingMessageLayout].avatarInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        [TUIMessageCellLayout incommingMessageLayout].bubbleInsets = UIEdgeInsetsMake(9, 15, 5, 10);
        
        [TUIMessageCellLayout outgoingMessageLayout].avatarSize = CGSizeMake(0, 0);
        [TUIMessageCellLayout outgoingMessageLayout].avatarInsets = UIEdgeInsetsMake(0, 0, 0, 10);
        [TUIMessageCellLayout outgoingMessageLayout].bubbleInsets = UIEdgeInsetsMake(9, 10, 5, 15);
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onNewMessage:) name:TUIKitNotification_TIMMessageListener object:nil];
        
        NSLog(@"TIMManager login succ ");
    } fail:^(int code, NSString *msg) {
        NSLog(@"TIMManager login fail %d", code);
    }];
    
}

- (void)onNewMessage:(NSNotification *)notification {
    NSArray *msgs = notification.object;
    for (TIMMessage *msg in msgs) {
        for (int i = 0; i < msg.elemCount; ++i) {
            TIMElem *elem = [msg getElem:i];
            
            if([elem isKindOfClass:[TIMCustomElem class]]){
                
                TIMCustomElem *dataElem = (TIMCustomElem *)elem;

                NSString *getDicString = [dataElem.data mj_JSONString];
                
                UserProfileDataModel *otherUserModel = [UserProfileDataModel mj_objectWithKeyValues:getDicString]; //对方信息
                
                if ([otherUserModel.messageText isEqualToString:@"[direct_VideoCall]"]) { //已经产生关系，直接拨打
                    
                    [self showCallInViewWithProfileModel:otherUserModel getThoughClickBlock:^{
                        [[UserInfo shareInstant] sendImMessageToOther:otherUserModel.userId messgeTextString:@"[VideoCall_Accept]" roomId:otherUserModel.roomId];
                        [self showTrtcPage:otherUserModel roomId:otherUserModel.roomId callType:@"CallIn"];
                        
                    } hungUpClickBlock:^{
                        [[UserInfo shareInstant] sendImMessageToOther:otherUserModel.userId messgeTextString:@"[VideoCall_Denny]" roomId:@""];
                        [self->_trtcVC.navigationController popViewControllerAnimated:YES];
                    }];
                    
                } else if ([otherUserModel.messageText isEqualToString:@"[match_VideoCall]"]) { //出现match，弹出提示
                    [self showMatchPopView:otherUserModel showType:@"CallIn" roomId:otherUserModel.roomId];
                    
                } else if ([otherUserModel.messageText isEqualToString:@"[VideoCall_Cancel]"]) { //拨打方放弃。 出现cancel, 接听者取消等待
                    [self->_callIn_CustomBannerView hide];
                    [_matchPopView dismiss];
                    [self->_trtcVC.navigationController popViewControllerAnimated:YES];
                    //                    参数 onlineState OFFLINE-离线 ONLINE-在线
                } else if ([otherUserModel.messageText isEqualToString:@"[VideoCall_Accept]"]) { //接听方接受。出现accept, 拨打方进入房间
                    [self showTrtcPage:otherUserModel roomId:otherUserModel.roomId callType:@"CallOut"];
                    
                    [_callOutView dismiss];
                    [_matchPopView dismiss];
                    
                } else if ([otherUserModel.messageText isEqualToString:@"[VideoCall_Denny]"]) { //接听方拒绝。出现denny, 拨打者取消等待
                    [_callOutView dismiss];
                    [_matchPopView dismiss];
                    //                    参数 onlineState OFFLINE-离线 ONLINE-在线
                    
                }
                
                
            }
            
        }
    }
    
}

- (void)showChatViewControllerWithType:(TIMConversationType)type receiver:(NSString*)receiverId receiverProfileModel:(UserProfileDataModel *)profileModel {
    
    TIMConversation *conv = [[TIMManager sharedInstance] getConversation:type receiver:receiverId];
    ChatViewController *vc = [[ChatViewController alloc] initWithConversation:conv];
    vc.type = type;
    vc.receiverId = receiverId;
    vc.profileModel = profileModel;
    [[DataTool getCurrentVC].navigationController pushViewController:vc animated:YES];
}

- (void) showCallOutView:(UserProfileDataModel *)profileModel {
    
    
    self->_callOutView = [[UserCallOutView alloc] initWithFrame:[DataTool getCurrentVC].view.frame ];
    self->_callOutView.profileModel = profileModel;
    [self->_callOutView show];
    
}

- (void) showMatchPopView:(UserProfileDataModel *)profileModel showType:(NSString *)showType roomId:(NSString *)roomId{
    _matchPopView = [[UserMatchPopView alloc] initWithFrame:[DataTool getCurrentVC].view.frame ];
    _matchPopView.dataModel = profileModel;
    _matchPopView.showType = showType; //CallOut, CallIn, LikeMe
    _matchPopView.roomId = roomId;
    [_matchPopView show];
}

- (void) showCallInViewWithProfileModel:(UserProfileDataModel *)profileModel getThoughClickBlock:(ClickBlock) getThoughClickBlock hungUpClickBlock:(ClickBlock) hungUpClickBlock {
    UserCallInView *view = [[UserCallInView alloc] initWithFrame:CGRectMake(ZoomSize(16), StatusBarHeight + ZoomSize(10), ZoomSize(343), ZoomSize(114))];
    view.profileModel = profileModel;
    
    _callIn_CustomBannerView = [EBCustomBannerView customView:view block:^(EBCustomBannerViewMaker *make) {
        make.portraitMode = EBCustomViewAppearModeTop;
        make.stayDuration = 30.0 ;
    }];
    [_callIn_CustomBannerView show];
    
    view.getThoughBlock = ^{
        [self->_callIn_CustomBannerView hide];
        if (getThoughClickBlock) {
            getThoughClickBlock();
        }
    };
    
    view.hungUpBlock  = ^{
        [self->_callIn_CustomBannerView hide];
        if (hungUpClickBlock) {
            hungUpClickBlock();
        }
    };
}

- (void) showUserLikeOtherPopView:(UserProfileDataModel *) profileDataModel  callback:(CallBack) callback {
    
    UserLikeOtherPopView *view = [[UserLikeOtherPopView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, SCREEN_Height)];
    view.dataModel = profileDataModel;
    view.closeBlock = ^{
        callback(@"close");
    };
    
    [view show];
}

- (void) showOtherLikeMePopView:(UserProfileDataModel *) profileDataModel  {
    
    OtherLikeMePopView *view = [[OtherLikeMePopView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, SCREEN_Height)];
    view.dataModel = profileDataModel;
    [view show];
}

// messageTextString

// [match_VideoCall] spark中产生关系 需要弹出match界面 需要传 otherUserId, roomId
// [direct_VideoCall] 已经产生关系，直接拨出 需要传 otherUserId, roomId

// [VideoCall_Accept] 收到对方接收 ，进入trtc界面 需要传 otherUserId
// [VideoCall_Cancel] 收到对方取消 ，退出拨出界面 需要传 otherUserId
// [VideoCall_Denny] 收到对方拒绝 ，退出拨出界面 需要传 otherUserId

- (void) sendImMessageToOther:(NSString *)otherUserId messgeTextString:(NSString *) messageTextString roomId:(NSString *)roomId {
    
    NSString *headImageUrlString = @"";
    if (![DataTool isEmptyString:[UserInfo shareInstant].user.o_avatar]) {
        headImageUrlString = [UserInfo shareInstant].user.o_avatar;
    }
    if (![DataTool isEmptyString:[UserInfo shareInstant].user.headThumbnailUrl]) {
        headImageUrlString = [UserInfo shareInstant].user.headThumbnailUrl;
    }
    if (![DataTool isEmptyString:[UserInfo shareInstant].user.headPortraitUrl]) {
        headImageUrlString = [UserInfo shareInstant].user.headPortraitUrl;
    }
    
    NSDictionary *userInfoDic = @{ @"messageText":messageTextString, @"id":self.user.userId, @"birthday":self.user.birthday, @"nickName":self.user.nickName, @"headPortraitUrl": headImageUrlString, @"country":self.user.country, @"onlineState": self.user.onlineState, @"verified": [NSString stringWithFormat:@"%lu", (unsigned long)self.user.videos.count], @"roomId": roomId};
    NSData *userInfoData =  [userInfoDic mj_JSONData];
    V2TIMMessage *msg = [[V2TIMManager sharedInstance] createCustomMessage:userInfoData];
    
    [[V2TIMManager sharedInstance] sendMessage:msg receiver:otherUserId groupID:nil priority:V2TIM_PRIORITY_DEFAULT
                                onlineUserOnly:NO offlinePushInfo:nil progress:^(uint32_t progress) {
    } succ:^{
        NSLog(@"msg succ");
        
    } fail:^(int code, NSString *msg) {
        NSLog(@"msg fail %@", msg);
        
    }];
    
}


- (void) sendTextMessageToOther:(NSString *)otherUserId messgeTextString:(NSString *) messageTextString {
    
    V2TIMMessage *msg = [[V2TIMManager sharedInstance] createTextMessage:messageTextString];
    
    [[V2TIMManager sharedInstance] sendMessage:msg receiver:otherUserId groupID:nil priority:V2TIM_PRIORITY_DEFAULT
                                onlineUserOnly:NO offlinePushInfo:nil progress:^(uint32_t progress) {
    } succ:^{
        NSLog(@"msg succ");
        
    } fail:^(int code, NSString *msg) {
        NSLog(@"msg fail %@", msg);
        
    }];
    
}


//- (void) sendImMessageToUser:(NSString *)userId messgeTextString:( NSString *) messageTextString {
//    
//}

- (void) showTrtcPage:(UserProfileDataModel *)profileModel roomId:(NSString *)roomId callType:(NSString *)callTypeString {
    
    TRTCVideoCallViewController *trtcVC = [[TRTCVideoCallViewController alloc] init];
    trtcVC.profileModel = profileModel;
    trtcVC.roomId = roomId;
    trtcVC.callType = callTypeString;
    [[DataTool getCurrentVC].navigationController pushViewController:trtcVC animated:YES];
}


@end

//
//  UserReportTypeDataModel.h
//  Video
//
//  Created by steve on 2020/4/24.
//  Copyright © 2020 steve. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserReportTypeDataModel : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *value;

@end

NS_ASSUME_NONNULL_END

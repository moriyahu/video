//
//  UserProfileDataModel.m
//  Video
//
//  Created by steve on 2020/3/23.
//  Copyright © 2020 steve. All rights reserved.
//

#import "UserProfileDataModel.h"

@implementation UserProfileDataModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"userId":@"id"};
}

+ (NSDictionary *)mj_objectClassInArray {
    return @{
        @"photos":[UserResourceDataModel class],
        @"videos":[UserResourceDataModel class]
    };
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    
    [aCoder encodeObject:self.aboutMe forKey:@"aboutMe"];
    [aCoder encodeObject:self.aboutMe_status forKey:@"aboutMe_status"];
    //    [aCoder encodeObject:self.authState forKey:@"authState"];
    [aCoder encodeObject:self.avatar_status forKey:@"avatar_status"];
    [aCoder encodeObject:self.balance forKey:@"balance"];
    [aCoder encodeObject:self.birthday forKey:@"birthday"];
    
    [aCoder encodeObject:self.country forKey:@"country"];
    [aCoder encodeObject:self.email forKey:@"email"];
    [aCoder encodeBool:self.enableAddTimes forKey:@"enableAddTimes"];
    
    [aCoder encodeObject:self.headPortraitUrl forKey:@"headPortraitUrl"];
    [aCoder encodeObject:self.headThumbnailUrl forKey:@"headThumbnailUrl"];
    [aCoder encodeObject:self.nickName forKey:@"nickName"];
    [aCoder encodeObject:self.nickName_status forKey:@"nickName_status"];
    
    [aCoder encodeObject:self.o_aboutMe forKey:@"o_aboutMe"];
    [aCoder encodeObject:self.o_avatar forKey:@"o_avatar"];
    [aCoder encodeObject:self.o_nickName forKey:@"o_nickName"];
    [aCoder encodeObject:self.onlineState forKey:@"onlineState"];
    
    [aCoder encodeObject:self.photos forKey:@"photos"];
    [aCoder encodeObject:self.sex forKey:@"sex"];
    [aCoder encodeObject:self.sig forKey:@"sig"];
    [aCoder encodeObject:self.times forKey:@"times"];
    [aCoder encodeObject:self.token forKey:@"token"];
    [aCoder encodeObject:self.type forKey:@"type"];
    
    
    [aCoder encodeObject:self.videos forKey:@"videos"];
    [aCoder encodeObject:self.vipEndtime forKey:@"vipEndtime"];
    [aCoder encodeObject:self.vipState forKey:@"vipState"];
    
    [aCoder encodeObject:self.userId forKey:@"userId"];
    
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    
    if (self = [super init]) {
        self.aboutMe = [aDecoder decodeObjectForKey:@"aboutMe"];
        self.aboutMe_status = [aDecoder decodeObjectForKey:@"aboutMe_status"];
        self.avatar_status = [aDecoder decodeObjectForKey:@"avatar_status"];
        self.balance = [aDecoder decodeObjectForKey:@"balance"];
        self.birthday = [aDecoder decodeObjectForKey:@"birthday"];
        
        self.country = [aDecoder decodeObjectForKey:@"country"];
        self.email = [aDecoder decodeObjectForKey:@"email"];
        self.enableAddTimes = [aDecoder decodeBoolForKey:@"enableAddTimes"];
        
        self.headPortraitUrl = [aDecoder decodeObjectForKey:@"headPortraitUrl"];
        self.headThumbnailUrl = [aDecoder decodeObjectForKey:@"headThumbnailUrl"];
        self.nickName = [aDecoder decodeObjectForKey:@"nickName"];
        self.nickName_status = [aDecoder decodeObjectForKey:@"nickName_status"];
        
        self.o_aboutMe = [aDecoder decodeObjectForKey:@"o_aboutMe"];
        self.o_avatar = [aDecoder decodeObjectForKey:@"o_avatar"];
        self.o_nickName = [aDecoder decodeObjectForKey:@"o_nickName"];
        self.onlineState = [aDecoder decodeObjectForKey:@"onlineState"];
        
        self.photos = [aDecoder decodeObjectForKey:@"photos"];
        self.sex = [aDecoder decodeObjectForKey:@"sex"];
        self.sig = [aDecoder decodeObjectForKey:@"sig"];
        self.times = [aDecoder decodeObjectForKey:@"times"];
        self.token = [aDecoder decodeObjectForKey:@"token"];
        self.type = [aDecoder decodeObjectForKey:@"type"];
        
        self.videos = [aDecoder decodeObjectForKey:@"videos"];
        self.vipEndtime = [aDecoder decodeObjectForKey:@"vipEndtime"];
        self.vipState = [aDecoder decodeObjectForKey:@"vipState"];
        
        self.userId = [aDecoder decodeObjectForKey:@"userId"];
    }
    return self;
}


@end

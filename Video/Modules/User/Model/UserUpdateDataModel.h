//
//  UserUpdateDataModel.h
//  Video
//
//  Created by steve on 2020/4/29.
//  Copyright © 2020 steve. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserUpdateDataModel : NSObject

@property (nonatomic, strong) NSString *aboutMe;
@property (nonatomic, strong) NSString *birthday;
@property (nonatomic, strong) NSString *nickName;

@end

NS_ASSUME_NONNULL_END

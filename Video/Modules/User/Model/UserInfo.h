//
//  UserInfo.h
//  Video
//
//  Created by steve on 2020/3/23.
//  Copyright © 2020 steve. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserProfileDataModel.h"
#import <TIMComm.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserInfo : NSObject

@property (nonatomic, strong) UserProfileDataModel *user;

+ (instancetype) shareInstant;

typedef void(^CallBack)(id _Nullable obj);

@property (nonatomic, copy) CallBack timLoginSuccBlock;

- (void)checkUserDataModel:(id) obj callBackInfo:(CallBack) callback;
- (void)setupWithUserProfileInfoModel: (UserProfileDataModel *) profile;

- (void)loadUserinfoFromSadeBox; //获取沙盒数据
- (void)saveUserInfoToSadeBox; //存储用户数据
//- (void)cleancurrentUserinfo; //清理当前用户数据
- (void)deleteAllInfo; //清除全部数据

- (void)userLogin;
- (void)userLogout;

- (void) sendTextMessageToOther:(NSString *)otherUserId messgeTextString:(NSString *) messageTextString;
- (void) sendImMessageToOther:(NSString *)otherUserId messgeTextString:( NSString *) messageTextString roomId:(NSString *)roomId;

- (void)showChatViewControllerWithType:(TIMConversationType)type receiver:(NSString*)receiverId receiverProfileModel:(UserProfileDataModel *)profileModel;

- (void)showCallOutView:(UserProfileDataModel *) profile;
- (void)showMatchPopView:(UserProfileDataModel *)profileModel showType:(NSString *)showType roomId:(NSString *)roomId;
- (void)showUserLikeOtherPopView:(UserProfileDataModel *) profileDataModel  callback:(CallBack) callback;
- (void)showOtherLikeMePopView:(UserProfileDataModel *) profileDataModel;
- (void)showTrtcPage:(UserProfileDataModel *)profileModel roomId:(NSString *)roomId callType:(NSString *)callTypeString;


@end

NS_ASSUME_NONNULL_END

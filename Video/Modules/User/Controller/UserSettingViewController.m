//
//  UserSettingViewController.m
//  Video
//
//  Created by steve on 2020/4/10.
//  Copyright © 2020 steve. All rights reserved.
//

#import "UserSettingViewController.h"

#import "BlockListViewController.h"
#import "FeedBackViewController.h"
#import "WebViewController.h"

@interface UserSettingViewController () {
    UISwitch *pushSwitch;
    UISwitch *callSwitch;
    NSDictionary *getInfoDic;
}

@property (nonatomic, strong) UIView *navView;
@property (nonatomic, strong) UIScrollView *contentScrollView;

@end

@implementation UserSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self setdata];
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.tabBarController.tabBar.hidden = YES;
}

- (void)setupUI {
    self.view.backgroundColor = BGDarkBlueColor;
    [self.view addSubview:self.navView];
    [self.view addSubview:self.contentScrollView];
}

- (void)setdata {
    getInfoDic = [[NSUserDefaults standardUserDefaults] objectForKey:PermissionsSwitchDic];
    [self refleshSettigWitdhDic:getInfoDic];
    
    NSDictionary *paramsDic = @{}; //API_User_PictureAudit
    [[NetTool shareInstance] startRequest:API_Setting_get method:PostMethod params:paramsDic needShowHUD:NO callback:^(id  _Nullable obj) {
        
        self->getInfoDic = obj;
        [self refleshSettigWitdhDic:self->getInfoDic];

        [[NSUserDefaults standardUserDefaults] setObject:self->getInfoDic forKey:PermissionsSwitchDic];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }];
}

#pragma mark - Function
- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)switchAction {
    NSDictionary *paramsDic = @{@"enableCallIn":[NSString stringWithFormat:@"%d",callSwitch.isOn], @"enablePush":[NSString stringWithFormat:@"%d",pushSwitch.isOn]};
    
    [[NetTool shareInstance] startRequest:API_Settings method:PostMethod params:paramsDic needShowHUD:NO callback:^(id  _Nullable obj) {
        
        self->getInfoDic = paramsDic;
        [[NSUserDefaults standardUserDefaults] setObject:self->getInfoDic forKey:PermissionsSwitchDic];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }];
    
}

- (void)refleshSettigWitdhDic: (NSDictionary *)getDic {
    if (getDic == nil) {
        return;
    }
    
    if ([getDic[@"enableCallIn"] boolValue]) {
        self->callSwitch.on = YES;
    }
    
    if ([getDic[@"enablePush"] boolValue]) {
        self->pushSwitch.on = YES;
    }
}

- (UIButton *) createFunctionBtWithFrame:(CGRect) frame btEnabled:(BOOL) btEnabled  titleString:(NSString *) titleString tipString:(NSString *) tipString imageString:(NSString *) imageString btnTag:(int) btnTag  {
    
    UIButton *funcBt = [[UIButton alloc] initWithFrame:frame];
    [funcBt setTitle:titleString forState:UIControlStateNormal];
    [funcBt setTitleColor:BGDarkBlueColor forState:UIControlStateNormal];
    funcBt.backgroundColor = LightGrayColor;
    funcBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(18)];
    funcBt.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    funcBt.titleEdgeInsets = UIEdgeInsetsMake(0, ZoomSize(13), 0, 0);
    funcBt.enabled = btEnabled;
    funcBt.layer.cornerRadius = ZoomSize(16);
    funcBt.layer.masksToBounds = YES;
    funcBt.tag = btnTag;
    [funcBt addTarget:self action:@selector(switchFunctionAction:) forControlEvents:UIControlEventTouchUpInside];
    
    if (![DataTool isEmptyString:tipString]) {
        UILabel *infoLb = [[UILabel alloc] initWithFrame:CGRectMake(frame.size.width - ZoomSize(112), 0, ZoomSize(100), frame.size.height)];
        infoLb.text = tipString;
        infoLb.textAlignment = NSTextAlignmentRight;
        infoLb.textColor =TextBlackColor;
        infoLb.font = [UIFont systemFontOfSize:ZoomSize(16)];
        [funcBt addSubview:infoLb];
    }
    
    if (![DataTool isEmptyString:imageString]) {
        UIImageView *contactImagView = [[UIImageView alloc] initWithFrame:CGRectMake(frame.size.width - ZoomSize(20), (frame.size.height - ZoomSize(18))/2.0 , ZoomSize(10), ZoomSize(18))];
        contactImagView.image = [UIImage imageNamed:imageString];
        [funcBt addSubview:contactImagView];
    }
    
    return funcBt;
}

- (void)switchFunctionAction:(UIButton *) sender {
    
    switch (sender.tag) {
            
        case 120: {
            NSLog(@"Block List");
            BlockListViewController *vc = [[BlockListViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case 130: {
            FeedBackViewController *vc = [[FeedBackViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case 140: {
            WebViewController *ActivityVC = [[WebViewController alloc] init];
            ActivityVC.url = @"http://www.baidu.com";
            [[DataTool getCurrentVC] presentViewController:ActivityVC animated:YES completion:nil];
            break;
        }
        case 150: {
            WebViewController *ActivityVC = [[WebViewController alloc] init];
            ActivityVC.url = @"http://www.baidu.com";
            [[DataTool getCurrentVC] presentViewController:ActivityVC animated:YES completion:nil];
            break;
        }
        case 160: {
            NSLog(@"Version");
            break;
        }
        case 170: {
            
            UIAlertController *infoAlert = [UIAlertController alertControllerWithTitle:nil message:@"Are you sure to exit?" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *sureAlert = [UIAlertAction actionWithTitle:@"sure" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [[UserInfo shareInstant] userLogout];
            }];
            
            UIAlertAction *cancelAlert = [UIAlertAction actionWithTitle:@"cancel" style:UIAlertActionStyleCancel handler:nil];
//            [sureAlert setValue:Color_TextGray forKey:@"titleTextColor"];
//            [cancelAlert setValue:Color_Red forKey:@"titleTextColor"];
            [infoAlert addAction:sureAlert];
            [infoAlert addAction:cancelAlert];
            [self.navigationController presentViewController:infoAlert animated:YES completion:^{ }];
            
            break;
        }
            
        default: break;
    }
}

#pragma Lazy

- (UIView *)navView {
    if (!_navView) {
        _navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];;
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(10), StatusBarHeight + ZoomSize(5), ZoomSize(40), ZoomSize(30))];
        [_navView addSubview:backBt];
        [backBt setImage:GetImageByName(@"common_back") forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(60), StatusBarHeight, ZoomSize(160), ZoomSize(30))];
        titleLb.text = @"Setting";
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightSemibold];
        [_navView addSubview:titleLb];
        
    }
    return _navView;
}

- (UIScrollView *)contentScrollView {
    if (!_contentScrollView) {
        _contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, NavigationHeight + ZoomSize(17), SCREEN_Width, SCREEN_Height - NavigationHeight - ZoomSize(17))];
        _contentScrollView.bounces = YES ;
        _contentScrollView.showsHorizontalScrollIndicator = NO ;
        _contentScrollView.showsVerticalScrollIndicator = NO ;
        _contentScrollView.alwaysBounceVertical = YES;// 垂直
        _contentScrollView.contentSize = CGSizeMake(SCREEN_Width, ZoomSize(680));
        
        UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, ZoomSize(0), SCREEN_Width, ZoomSize(720) )];
        [_contentScrollView addSubview:contentView];
        contentView.backgroundColor = UIColor.whiteColor;
        
        UIBezierPath *contentViewMaskPath = [UIBezierPath bezierPathWithRoundedRect:contentView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(ZoomSize(36.5),ZoomSize(36.5))]; //设置部分圆角
        CAShapeLayer *contentViewMaskLayer = [[CAShapeLayer alloc] init];
        contentViewMaskLayer.frame = contentView.bounds;
        contentViewMaskLayer.path = contentViewMaskPath.CGPath;
        contentView.layer.mask = contentViewMaskLayer;
        
#pragma pushNotiBt
        UIButton *pushNotiBt = [self createFunctionBtWithFrame:CGRectMake(ZoomSize(16), ZoomSize(22), SCREEN_Width - ZoomSize(32), ZoomSize(48)) btEnabled:YES titleString:@"Push Notification" tipString:nil imageString:nil btnTag:100];
        [contentView addSubview:pushNotiBt];
        
        pushSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(44) - 57, (ZoomSize(48) - 32) /2, 57,32)];
        [pushSwitch addTarget:self action:@selector(switchAction) forControlEvents:UIControlEventValueChanged];
        pushSwitch.on = NO;
        pushSwitch.tintColor = RGB(165, 160, 170);
        [pushNotiBt addSubview:pushSwitch];
        
#pragma incomeCallBt
        UIButton *incomeCallBt = [self createFunctionBtWithFrame:CGRectMake(ZoomSize(16), ZoomSize(90), SCREEN_Width - ZoomSize(32), ZoomSize(48)) btEnabled:YES titleString:@"Incoming Call" tipString:nil imageString:nil btnTag:110];
        [contentView addSubview:incomeCallBt];
        
        callSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(44) - 57, (ZoomSize(48) - 32) /2, 57,32)];
        [callSwitch addTarget:self action:@selector(switchAction) forControlEvents:UIControlEventValueChanged];
        callSwitch.on = NO;
        callSwitch.tintColor = RGB(165, 160, 170);
        [incomeCallBt addSubview:callSwitch];
        
        
#pragma BlockList
        UIButton *blockListBt = [self createFunctionBtWithFrame:CGRectMake(ZoomSize(16), ZoomSize(183), SCREEN_Width - ZoomSize(32), ZoomSize(48)) btEnabled:YES titleString:@"Block List" tipString:nil imageString:@"common_arrow_right_blue" btnTag:120];
        [contentView addSubview:blockListBt];
        
#pragma FeedbackBt
        UIButton *feedBackBt = [self createFunctionBtWithFrame:CGRectMake(ZoomSize(16), ZoomSize(251), SCREEN_Width - ZoomSize(32), ZoomSize(48)) btEnabled:YES titleString:@"Feedback" tipString:nil imageString:@"common_arrow_right_blue" btnTag:130];
        [contentView addSubview:feedBackBt];
        
#pragma privacyPolicyBt
        UIButton *privacyPolicyBt = [self createFunctionBtWithFrame:CGRectMake(ZoomSize(16), ZoomSize(344), SCREEN_Width - ZoomSize(32), ZoomSize(48)) btEnabled:YES titleString:@"Privacy Policy" tipString:nil imageString:@"common_arrow_right_blue" btnTag:140];
        [contentView addSubview:privacyPolicyBt];
        
#pragma termOfUseBt
        UIButton *termOfUseBt = [self createFunctionBtWithFrame:CGRectMake(ZoomSize(16), ZoomSize(412), SCREEN_Width - ZoomSize(32), ZoomSize(48)) btEnabled:YES titleString:@"Term Of Use" tipString:nil imageString:@"common_arrow_right_blue" btnTag:150];
        [contentView addSubview:termOfUseBt];
        
#pragma versionBt
        UIButton *versionBt = [self createFunctionBtWithFrame:CGRectMake(ZoomSize(16), ZoomSize(480), SCREEN_Width - ZoomSize(32), ZoomSize(48)) btEnabled:YES titleString:@"Version" tipString:[DataTool getVersionString] imageString:nil btnTag:160];
        [contentView addSubview:versionBt];
        
#pragma logOutBt
        UIButton *logOutBt = [self createFunctionBtWithFrame:CGRectMake(ZoomSize(100), ZoomSize(560), SCREEN_Width - ZoomSize(200), ZoomSize(48)) btEnabled:YES titleString:@"Log Out" tipString:nil imageString:nil btnTag:170];
        logOutBt.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        logOutBt.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        logOutBt.backgroundColor = UIColor.clearColor;
        [logOutBt setTitleColor:TextRedColor forState:UIControlStateNormal];
        logOutBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(16)];
        [contentView addSubview:logOutBt];
        
    }
    return _contentScrollView;
}


@end

//
//  UserProfileViewController.m
//  Video
//
//  Created by steve on 2020/4/22.
//  Copyright © 2020 steve. All rights reserved.
//

#import "UserProfileViewController.h"
#import "PhotoBannerView.h"
#import "JKPickView.h"

@interface UserProfileViewController () {
    UILabel *titleLb;
}

@property (nonatomic, strong) UIView *navBgView;
@property (nonatomic, strong) UIScrollView *contentScrollView;

@property (nonatomic, strong) PhotoBannerView *photoBannerView;
@property (nonatomic, strong) UIView *userInfoView;

@property (nonatomic, strong) NSMutableArray *photoArray;

@end

@implementation UserProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
    [self setdata];
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.tabBarController.tabBar.hidden = YES;
}

- (void)setupUI {
    self.view.backgroundColor = BGDarkBlueColor;
    [self.view addSubview:self.navBgView];
    [self.view addSubview:self.contentScrollView];
    
    UIButton *messageBt = [[UIButton alloc] initWithFrame:CGRectMake(0, SCREEN_Height - ZoomSize(97) - BottomSafeAreaHeight, SCREEN_Width/2, ZoomSize(97))];
    [messageBt setImage:GetImageByName(@"userprofile_message") forState:UIControlStateNormal];
    [self.view addSubview:messageBt];
    [messageBt addTarget:self action:@selector(messageAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *videoBt = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_Width/2, SCREEN_Height - ZoomSize(97) - BottomSafeAreaHeight, SCREEN_Width/2, ZoomSize(97))];
    [videoBt setImage:GetImageByName(@"userprofile_video") forState:UIControlStateNormal];
    [self.view addSubview:videoBt];
    [videoBt addTarget:self action:@selector(videoAction) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)setdata {
    
}

- (void)setProfileModel:(UserProfileDataModel *)profileModel {
    _profileModel = profileModel;
}

#pragma mark - Function

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
    
    //    [[UserInfo shareInstant] exitTRTCRoom];
    
}

- (void)moreAction {
    NSLog(@"more");
    [DataTool reportBlockUserName:_profileModel.nickName andBlockUserId:_profileModel.userId reportCallback:^(id  _Nullable obj) {
        NSLog(@"reportSuccess");
    } blockCallback:^(id  _Nullable obj) {
        NSLog(@"blockSuccess");
    }];
}

- (void)messageAction {
    
    NSLog(@"messageAction");
//    [[UserInfo shareInstant] showChatViewControllerWithType:TIM_C2C receiver:_profileModel.userId];
    [[UserInfo shareInstant] showChatViewControllerWithType:TIM_C2C receiver:_profileModel.userId receiverProfileModel:_profileModel];
}

- (void)videoAction {
    
}

#pragma mark - lazy

- (UIView *)navBgView {
    if (!_navBgView) {
        _navBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];
        [self.view addSubview:_navBgView];
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(10), StatusBarHeight + ZoomSize(5), ZoomSize(40), ZoomSize(30))];
        [_navBgView addSubview:backBt];
        [backBt setImage:GetImageByName(@"common_back") forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        titleLb = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_Width/2 - ZoomSize(100), StatusBarHeight, ZoomSize(200), ZoomSize(30))];
        [_navBgView addSubview:titleLb];
        titleLb.text = _profileModel.nickName;
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightSemibold];
        
        UIButton *moreBt = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(60), StatusBarHeight, ZoomSize(60), ZoomSize(30))];
        [_navBgView addSubview:moreBt];
        [moreBt setImage:GetImageByName(@"profile_more") forState:UIControlStateNormal];
        [moreBt addTarget:self action:@selector(moreAction) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _navBgView;
}

- (UIScrollView *)contentScrollView {
    if (!_contentScrollView) {
        _contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, NavigationHeight, SCREEN_Width, SCREEN_Height - NavigationHeight)];
        _contentScrollView.bounces = YES ;
        _contentScrollView.showsHorizontalScrollIndicator = NO ;
        _contentScrollView.showsVerticalScrollIndicator = NO ;
        
        UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, ZoomSize(17), SCREEN_Width, ZoomSize(1320) )];
        [_contentScrollView addSubview:contentView];
        contentView.backgroundColor = UIColor.whiteColor;
        
        UIBezierPath *contentViewMaskPath = [UIBezierPath bezierPathWithRoundedRect:contentView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(ZoomSize(36.5),ZoomSize(36.5))]; //设置部分圆角
        CAShapeLayer *contentViewMaskLayer = [[CAShapeLayer alloc] init];
        contentViewMaskLayer.frame = contentView.bounds;
        contentViewMaskLayer.path = contentViewMaskPath.CGPath;
        contentView.layer.mask = contentViewMaskLayer;
        
        [contentView addSubview:self.photoBannerView];
        [contentView addSubview:self.userInfoView];
        
        _contentScrollView.contentSize = CGSizeMake( SCREEN_Width , ZoomSize(800));
    }
    
    return _contentScrollView;
}

- (PhotoBannerView *)photoBannerView {
    if (!_photoBannerView) {
        
        _photoArray = [NSMutableArray array];
        
        if (_profileModel.videos.count > 0) { //添加视频
            UserResourceDataModel *videoDataModel = _profileModel.videos.firstObject;
            [_photoArray addObject:videoDataModel];
        }
        
        if ( [DataTool getStateCodeFromString:_profileModel.avatar_status]  != 2) {
            
            UserResourceDataModel *headPortraitDataModel = [[UserResourceDataModel alloc] init]; //添加头像
            
            if (![DataTool isEmptyString:_profileModel.headPortraitUrl]) {
                headPortraitDataModel.name = _profileModel.headPortraitUrl;
            } else {
                
                headPortraitDataModel.name = @"";
                
                if (![DataTool isEmptyString:_profileModel.headThumbnailUrl]) {
                    headPortraitDataModel.name = _profileModel.headThumbnailUrl;
                }
                if (![DataTool isEmptyString:_profileModel.o_avatar]) {
                    headPortraitDataModel.name = _profileModel.o_avatar;
                }
            }
            headPortraitDataModel.type = @"PIC";
            [_photoArray addObject:headPortraitDataModel];
        }
        
        
        if (_profileModel.photos.count > 0) { //添加相册
            for (UserResourceDataModel *dataModel in _profileModel.photos) {
                [_photoArray addObject:dataModel];
            }
        }
        
        _photoBannerView = [[PhotoBannerView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, ZoomSize(500)) photoArray:_photoArray];
    }
    return _photoBannerView;
}

- (UIView *)userInfoView {
    if (!_userInfoView) {
        _userInfoView = [[UIView alloc] initWithFrame:CGRectMake(0, ZoomSize(520), SCREEN_Width, ZoomSize(200))];
        
        UILabel *nameLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(16), ZoomSize(10), ZoomSize(160), ZoomSize(30))];
        [_userInfoView addSubview:nameLb];
        nameLb.text = [NSString stringWithFormat:@"%@,%ld",  _profileModel.nickName, [DataTool getUserAgeFromString:_profileModel.birthday]];
        nameLb.textAlignment = NSTextAlignmentLeft;
        nameLb.textColor = BlackColor;
        nameLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightSemibold];
        
        UIImageView *onlineLogoIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(140), ZoomSize(12), ZoomSize(28), ZoomSize(28))];
        onlineLogoIV.image = GetImageByName(@"common_online");
        [_userInfoView addSubview:onlineLogoIV];
        onlineLogoIV.hidden = YES;
        if ([_profileModel.onlineState isEqualToString:@"ONLINE"]) {
            onlineLogoIV.hidden = NO;
        }
        
        UIImageView *verifyLogoIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(170), ZoomSize(12), ZoomSize(28), ZoomSize(28))];
        verifyLogoIV.image = GetImageByName(@"common_validation");
        [_userInfoView addSubview:verifyLogoIV];
        verifyLogoIV.hidden = YES;
        if (_profileModel.videos.count > 0) {
            verifyLogoIV.hidden = NO;
        }
        
        UIImageView *nationIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(16), ZoomSize(50), ZoomSize(20), ZoomSize(16))];
        nationIV.contentMode = UIViewContentModeScaleAspectFit;
        nationIV.image = [DataTool getImageForCountryName:_profileModel.country];
        [_userInfoView addSubview:nationIV];
        
        UILabel *nationLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(45), ZoomSize(50), ZoomSize(150), ZoomSize(16))];
        [_userInfoView addSubview:nationLb];
        nationLb.text = _profileModel.country;
        nationLb.textColor = BGDarkBlueColor;
        nationLb.font = [UIFont systemFontOfSize:ZoomSize(14)];
        
        UILabel *aboutLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(16), ZoomSize(100), SCREEN_Width - ZoomSize(32), ZoomSize(50))];
        [_userInfoView addSubview:aboutLb];
        aboutLb.text = _profileModel.aboutMe;
        aboutLb.textAlignment = NSTextAlignmentLeft;
        aboutLb.textColor = TextGrayColor;
        aboutLb.font = [UIFont systemFontOfSize:ZoomSize(16)];
        aboutLb.numberOfLines = 0;
        
    }
    
    return _userInfoView;
}

@end

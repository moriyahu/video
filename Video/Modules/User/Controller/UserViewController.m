//
//  UserViewController.m
//  Video
//
//  Created by steve on 2020/4/8.
//  Copyright © 2020 steve. All rights reserved.
//

#import <SDWebImage.h>

#import "UserViewController.h"
#import "UserSettingViewController.h"
#import "UserEditProfileViewController.h"

#import "VipInfoView.h"
#import "UserBuyVipView.h"
#import "UserTipUploadVideoView.h"

@interface UserViewController () {
    UIImageView *userVipLogoIV;
    UIImageView *logoIV;
    UILabel *userCoinsLb;
    int vipInfoIndex;
}

@property (nonatomic, strong) UIView *userInfoView;
@property (nonatomic, strong) UIScrollView *contentScrollView;

@property (nonatomic, strong) UIButton *videoBt;
@property (nonatomic, strong) UIButton *coinBt;
@property (nonatomic, strong) VipInfoView *vipInfoView;
@property (nonatomic, strong) UIButton *getVipBt;

@end

@implementation UserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setData) name:NeedRefleshProfilePage object:nil]; //编辑用户设置
}

- (void)viewWillAppear:(BOOL)animated {
    [self setData];
    self.navigationController.navigationBar.hidden = YES;
    self.navigationController.tabBarController.tabBar.hidden = NO;
    [self.vipInfoView createTimer];
}

- (void) viewWillDisappear:(BOOL)animated {
    [self.vipInfoView stopTimer];
}

- (void)setupUI {
    self.view.backgroundColor = BGDarkBlueColor;
    [self.view addSubview:self.userInfoView];
    [self.view addSubview:self.contentScrollView];
}

- (void)setData {
    
    if (![DataTool isEmptyString:[UserInfo shareInstant].user.o_avatar]) {
        [logoIV sd_setImageWithURL:[NSURL URLWithString:[UserInfo shareInstant].user.o_avatar] placeholderImage:GetImageByName(@"message_user_nopho")];
    }
    if (![DataTool isEmptyString:[UserInfo shareInstant].user.headThumbnailUrl]) {
        [logoIV sd_setImageWithURL:[NSURL URLWithString:[UserInfo shareInstant].user.headThumbnailUrl] placeholderImage:GetImageByName(@"message_user_nopho")];
    }
    if (![DataTool isEmptyString:[UserInfo shareInstant].user.headPortraitUrl]) {
        [logoIV sd_setImageWithURL:[NSURL URLWithString:[UserInfo shareInstant].user.headThumbnailUrl] placeholderImage:GetImageByName(@"message_user_nopho")];
    }
    
    if ([UserInfo shareInstant].user.videos.count > 0) {
        _videoBt.hidden = YES;
        _coinBt.frame = CGRectMake(ZoomSize(16), ZoomSize(26), SCREEN_Width - ZoomSize(32), ZoomSize(68));
        _vipInfoView.frame = CGRectMake(0, ZoomSize(106), SCREEN_Width, ZoomSize(220));
        _getVipBt.frame = CGRectMake(ZoomSize(38), ZoomSize(344), SCREEN_Width - ZoomSize(76), ZoomSize(48));
    }
    
    if ([[UserInfo shareInstant].user.vipState isEqualToString:@"VIP"]) {
        userVipLogoIV.hidden = NO;
        _getVipBt.hidden = YES;
    }
    
    userCoinsLb.text = [UserInfo shareInstant].user.balance;
}

#pragma function
- (void)settingAction {
    UserSettingViewController *vc = [[UserSettingViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)userInfoActon {
    NSLog(@"userInfoActon");
//    [DataTool showUserProfilePage:[UserInfo shareInstant].user ];
    [self editAction];
}

- (void)editAction {
    UserEditProfileViewController *vc = [[UserEditProfileViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)uploadAction {
    [DataTool showUploadVideoPage];
}

- (void)coinAction {
    NSLog(@"coinAction");
    [DataTool showGetCoinsView];
}

- (void)vipAction {
    [DataTool showGetVipViewWithIndex:self->vipInfoIndex];
}

#pragma lazy
- (UIView *)userInfoView {
    if (!_userInfoView) {
        _userInfoView = [[UIView alloc] initWithFrame:CGRectMake(0, StatusBarHeight , SCREEN_Width, ZoomSize(172))];
        
        UIButton *settingBt = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(45), 0, ZoomSize(30), ZoomSize(30))];
        [_userInfoView addSubview:settingBt];
        [settingBt addTarget:self action:@selector(settingAction) forControlEvents:UIControlEventTouchUpInside];
        [settingBt setImage:GetImageByName(@"profile_setting") forState:UIControlStateNormal];
        
        userVipLogoIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(80), ZoomSize(23), ZoomSize(59), ZoomSize(31))]; //logo
        [_userInfoView addSubview:userVipLogoIV];
        userVipLogoIV.image = GetImageByName(@"me_vip");
        userVipLogoIV.contentMode = UIViewContentModeScaleAspectFill;
        userVipLogoIV.hidden = YES;
        
        UIButton *userInfoBt = [[UIButton alloc] initWithFrame:CGRectMake((SCREEN_Width - ZoomSize(108))/2.0, ZoomSize(23), ZoomSize(108), ZoomSize(108))]; //logo
        [_userInfoView addSubview:userInfoBt];
        [userInfoBt addTarget:self action:@selector(userInfoActon) forControlEvents:UIControlEventTouchUpInside];
        
        logoIV = [[UIImageView alloc] initWithFrame:userInfoBt.bounds]; //logo
        [userInfoBt addSubview:logoIV];
        logoIV.layer.cornerRadius = ZoomSize(54);
        logoIV.layer.masksToBounds = YES;
        logoIV.layer.borderColor = WhiteColor.CGColor;
        logoIV.layer.borderWidth = ZoomSize(2);
        //        logoIV.image = GetImageByName(@"launch_background");
        logoIV.contentMode = UIViewContentModeScaleAspectFill;
        
        UIButton *editLogoBt = [[UIButton alloc] initWithFrame:CGRectMake((SCREEN_Width)/2.0 + ZoomSize(22), ZoomSize(99), ZoomSize(32), ZoomSize(32))];
        [_userInfoView addSubview:editLogoBt];
        [editLogoBt addTarget:self action:@selector(editAction) forControlEvents:UIControlEventTouchUpInside];
        editLogoBt.layer.cornerRadius = ZoomSize(16);
        editLogoBt.layer.masksToBounds = YES;
        [editLogoBt setImage: GetImageByName(@"profile_edit") forState:UIControlStateNormal];
        
        UILabel *editLogoTipLb = [[UILabel alloc] initWithFrame:CGRectMake(0, ZoomSize(139), SCREEN_Width, ZoomSize(18))];
        [_userInfoView addSubview:editLogoTipLb];
        editLogoTipLb.text = @"Click and edit your profile";
        editLogoTipLb.textAlignment = NSTextAlignmentCenter;
        editLogoTipLb.textColor = TextGrayColor;
        editLogoTipLb.font = [UIFont systemFontOfSize:ZoomSize(14)];
    }
    
    return _userInfoView;
}

- (UIScrollView *)contentScrollView {
    if (!_contentScrollView) {
        _contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, StatusBarHeight + ZoomSize(172), SCREEN_Width, SCREEN_Height - TabBarHeight - StatusBarHeight  - ZoomSize(172))];
        _contentScrollView.bounces = YES ;
        _contentScrollView.showsHorizontalScrollIndicator = NO ;
        _contentScrollView.showsVerticalScrollIndicator = NO ;
        _contentScrollView.alwaysBounceVertical = YES;// 垂直
        _contentScrollView.contentSize = CGSizeMake(SCREEN_Width, ZoomSize(530));
        
        UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, ZoomSize(0), SCREEN_Width, ZoomSize(530) )];
        [_contentScrollView addSubview:contentView];
        contentView.backgroundColor = UIColor.whiteColor;
        
        UIBezierPath *contentViewMaskPath = [UIBezierPath bezierPathWithRoundedRect:contentView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(ZoomSize(36.5),ZoomSize(36.5))]; //设置部分圆角
        CAShapeLayer *contentViewMaskLayer = [[CAShapeLayer alloc] init];
        contentViewMaskLayer.frame = contentView.bounds;
        contentViewMaskLayer.path = contentViewMaskPath.CGPath;
        contentView.layer.mask = contentViewMaskLayer;
        
        [contentView addSubview:self.videoBt];
        [contentView addSubview:self.coinBt];
        [contentView addSubview:self.vipInfoView];
        [contentView addSubview:self.getVipBt];
        
    }
    return _contentScrollView;
}

- (UIButton *)videoBt {
    if (!_videoBt) {
        _videoBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(16), ZoomSize(26), SCREEN_Width - ZoomSize(32), ZoomSize(68))];
        _videoBt.layer.cornerRadius = ZoomSize(34);
        _videoBt.layer.masksToBounds = YES;
        [_videoBt addTarget:self action:@selector(uploadAction) forControlEvents:UIControlEventTouchUpInside];
        
        [_videoBt setBackgroundImage:GetImageByName(@"profile_getLiveVideo_bg") forState:UIControlStateNormal];
        [_videoBt setTitle:@"Create a Live Video" forState:UIControlStateNormal];
        _videoBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(18)];
        [_videoBt setTitleColor:BGDarkBlueColor forState:UIControlStateNormal];
        _videoBt.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft; //设置居左
        _videoBt.titleEdgeInsets = UIEdgeInsetsMake(ZoomSize(-30), ZoomSize(78), 0, 0);
        
        UILabel *tipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(78), ZoomSize(30), SCREEN_Width - ZoomSize(150), ZoomSize(34))];
        tipLb.text = @"You are only live video away from \nyour dream match";
        tipLb.textAlignment = NSTextAlignmentLeft;
        tipLb.textColor = TextGrayColor;
        tipLb.font = [UIFont systemFontOfSize:ZoomSize(14)];
        tipLb.numberOfLines = 0;
        [_videoBt addSubview:tipLb];
        
    }
    return _videoBt;
}

- (UIButton *)coinBt {
    if (!_coinBt) {
        _coinBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(16), ZoomSize(120), SCREEN_Width - ZoomSize(32), ZoomSize(68))];
        _coinBt.layer.cornerRadius = ZoomSize(34);
        _coinBt.layer.masksToBounds = YES;
        [_coinBt addTarget:self action:@selector(coinAction) forControlEvents:UIControlEventTouchUpInside];
        [_coinBt setBackgroundImage:GetImageByName(@"profile_getcoin_bg") forState:UIControlStateNormal];
        [_coinBt setTitle:@"Get Coins" forState:UIControlStateNormal];
        _coinBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(18)];
        [_coinBt setTitleColor:BGDarkBlueColor forState:UIControlStateNormal];
        
        _coinBt.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft; //设置居左
        _coinBt.titleEdgeInsets = UIEdgeInsetsMake(0, ZoomSize(78), 0, 0);
        
        
        userCoinsLb = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(160), ZoomSize(23), ZoomSize(90), ZoomSize(22))];
//        userCoinsLb.text = [UserInfo shareInstant].user.balance;
        userCoinsLb.textAlignment = NSTextAlignmentRight;
        userCoinsLb.textColor = BGDarkBlueColor;
        userCoinsLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightSemibold];
        [_coinBt addSubview:userCoinsLb];
        
    }
    return _coinBt;
}

- (VipInfoView *)vipInfoView {
    if (!_vipInfoView ) {
        _vipInfoView = [[VipInfoView alloc] initWithFrame:CGRectMake(0, ZoomSize(210), SCREEN_Width, ZoomSize(220)) photoArray:VipImageArray currentIndex:0];
        _vipInfoView.canPop = YES;
//        _vipInfoView.interval = 3;
        _vipInfoView.scrollAction = ^(int scrollIndex) {
            self->vipInfoIndex = scrollIndex;
        };
    }
    return  _vipInfoView;
}

- (UIButton *)getVipBt {
    if (!_getVipBt) {
        _getVipBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(38), ZoomSize(440), SCREEN_Width - ZoomSize(76), ZoomSize(48))];
        [_getVipBt addTarget:self action:@selector(vipAction) forControlEvents:UIControlEventTouchUpInside];
        _getVipBt.layer.cornerRadius = ZoomSize(24);
        _getVipBt.layer.masksToBounds = YES;
        
        CAGradientLayer *gl = [CAGradientLayer layer];
        gl.frame = _getVipBt.bounds;
        gl.startPoint = CGPointMake(0.56, -0.77);
        gl.endPoint = CGPointMake(0.46, 1.66);
        gl.colors = @[(__bridge id)[UIColor colorWithRed:238/255.0 green:48/255.0 blue:202/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:137/255.0 green:36/255.0 blue:244/255.0 alpha:1.0].CGColor];
        gl.locations = @[@(0), @(1.0f)];
        [_getVipBt.layer addSublayer:gl];
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:_getVipBt.bounds];
        [_getVipBt addSubview:titleLb];
        titleLb.text = @"Get Vip";
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightSemibold];
        
    }
    return _getVipBt;
}

@end

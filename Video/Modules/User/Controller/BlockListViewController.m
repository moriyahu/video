//
//  BlockListViewController.m
//  Video
//
//  Created by steve on 2020/4/26.
//  Copyright © 2020 steve. All rights reserved.
//

#import "MJExtension.h"
#import "MJRefresh.h"
#import "BlockListViewController.h"
#import "BlockListTableViewCell.h"
#import "UITableView+PlaceHolderView.h"
#import "ListPlaceHoderView.h"

static NSString * const reuseIdentifier = @"BlockListTableViewCell";

@interface BlockListViewController ()<UITableViewDelegate, UITableViewDataSource> {
    int pageIndex;
    bool hasNextPage;
}

@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * array;

@property (nonatomic, strong) UIView *navView;
@property (nonatomic, strong) UIScrollView *contentScrollView;

@end

@implementation BlockListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
    [self setData];
}

- (void)setupUI {
    self.view.backgroundColor = BGDarkBlueColor;
    [self.view addSubview:self.navView];
    [self.view addSubview:self.contentScrollView];
}

- (void)setData {
    self.array = [NSMutableArray array];
    [self getHeaderData];
}

#pragma mark - Function
- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getHeaderData {
    
    self->pageIndex = 1;
    [self requestData];
}

- (void)getFooterData {
    
    if (hasNextPage) {
        [self requestData];
    } else {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    }
    
}

- (void)requestData {
    
    NSDictionary *paramsDic = @{@"pageSize": [NSNumber numberWithInt:5], @"pageNum": [NSNumber numberWithInt:pageIndex]};

    [[NetTool shareInstance] startRequest:API_Block_Page method:PostMethod params:paramsDic needShowHUD:NO callback:^(id  _Nullable obj) {

        if (self->pageIndex == 1) {
            [self.array removeAllObjects];
            [self.tableView reloadData];
        }

        NSDictionary *resultDic = obj;
        NSArray *listArray = resultDic[@"list"];
        NSArray *userArray = [UserProfileDataModel mj_objectArrayWithKeyValuesArray:listArray];
        [self.array addObjectsFromArray:userArray];
        self->hasNextPage = [resultDic[@"hasNextPage"] boolValue];
        
        
//        self->pageIndex = [resultDic[@"pageNum"] intValue];
        self->pageIndex ++;

        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [self.tableView reloadData];
    }];
    
}

- (void)reloadListData {
    [self.tableView reloadData];
}

#pragma mark - delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.array.count;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BlockListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if(cell == nil) {
        cell = [[BlockListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }
    
    UserProfileDataModel *userModel = self.array[indexPath.row];
    cell.profileModel = userModel;
    
    cell.unblockBlock = ^{
        NSLog(@"unblockBlock");
        
         NSDictionary *paramsDic = @{@"beBlockUserId":userModel.userId, @"blockUserId":[UserInfo shareInstant].user.userId};
        [[NetTool shareInstance] startRequest:API_Block_Del method:PostMethod params:paramsDic needShowHUD:NO callback:^(id  _Nullable obj) {
            [self.array removeObjectAtIndex:indexPath.row];
            [self.tableView reloadData];
        }];
        
    };
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSLog(@"didSelectRowAtIndexPath");
}


#pragma Lazy
- (UIView *)navView {
    if (!_navView) {
        _navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];;
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(10), StatusBarHeight + ZoomSize(5), ZoomSize(40), ZoomSize(30))];
        [_navView addSubview:backBt];
        [backBt setImage:GetImageByName(@"common_back") forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(60), StatusBarHeight, ZoomSize(160), ZoomSize(30))];
        titleLb.text = @"Block List";
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightSemibold];
        [_navView addSubview:titleLb];
        
    }
    return _navView;
}

- (UIScrollView *)contentScrollView {
    if (!_contentScrollView) {
        _contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, NavigationHeight + ZoomSize(17), SCREEN_Width, SCREEN_Height - NavigationHeight - ZoomSize(17))];
        _contentScrollView.bounces = YES ;
        _contentScrollView.showsHorizontalScrollIndicator = NO ;
        _contentScrollView.showsVerticalScrollIndicator = NO ;
        
        UIView *contentView = [[UIView alloc] initWithFrame:_contentScrollView.bounds];
        [_contentScrollView addSubview:contentView];
        contentView.backgroundColor = UIColor.whiteColor;
        
        UIBezierPath *contentViewMaskPath = [UIBezierPath bezierPathWithRoundedRect:contentView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(ZoomSize(36.5),ZoomSize(36.5))]; //设置部分圆角
        CAShapeLayer *contentViewMaskLayer = [[CAShapeLayer alloc] init];
        contentViewMaskLayer.frame = contentView.bounds;
        contentViewMaskLayer.path = contentViewMaskPath.CGPath;
        contentView.layer.mask = contentViewMaskLayer;
        
        [contentView addSubview:self.tableView];
        
    }
    return _contentScrollView;
}


- (UITableView *)tableView {
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:self.contentScrollView.bounds style:UITableViewStylePlain];
        
        [_tableView registerNib:[UINib nibWithNibName:reuseIdentifier bundle:nil] forCellReuseIdentifier:reuseIdentifier];
        _tableView.separatorStyle = UITableViewCellEditingStyleNone;
        
        _tableView.rowHeight = ZoomSize(90);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        _tableView.mj_header = [MJRefreshStateHeader headerWithRefreshingBlock:^{
            [self getHeaderData];
        }];
        
        _tableView.mj_footer = [MJRefreshBackStateFooter footerWithRefreshingBlock:^{
            
            [self getFooterData];
        }];
        
        _tableView.enablePlaceHolderView = YES;
        _tableView.yh_PlaceHolderView = [[ListPlaceHoderView alloc] initWithFrame:_tableView.frame tipImgName:@"common_empty" tipsInfo:@"No users"];
    }
    return _tableView;
}


@end

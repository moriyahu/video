//
//  CreateLiveVideoViewController.m
//  Video
//
//  Created by steve on 2020/5/1.
//  Copyright © 2020 steve. All rights reserved.
//

#import "CreateLiveVideoViewController.h"
#import "NTUIPageControl.h"
#import "CircleScaleView.h"
#import "VideoPlayerView.h"

#import <GPUImage/GPUImage.h>
#import <AssetsLibrary/ALAssetsLibrary.h>
#import <MJExtension/MJExtension.h>

@interface CreateLiveVideoViewController ()<UIScrollViewDelegate> {
    
    NSString *pathToMovie;// 视频地址
    
    UIView *filterMaskView; //视频视图
    GPUImageVideoCamera *videoCamera; //摄像头
    GPUImageOutput<GPUImageInput> *filter; //输出
    GPUImageView *filterView; //输出视图
    GPUImageMovieWriter *movieWriter; //写入
    
    VideoPlayerView *playerView; //视频播放
    
    UIButton *startRecordBt;
    CircleScaleView *circleView;
    UIButton *restartRecordBt;
    UIButton *ensureBt;
}

@property (nonatomic, strong) UIView *navView;
@property (nonatomic, strong) UIView *contentView;

@property (nonatomic, strong) UIScrollView *infoScrollView;
@property (nonatomic, strong) NTUIPageControl *pageControl;
@property(strong ,nonatomic) NSTimer *bannerTimer;

@property (nonatomic, strong) NSArray *infoArray;
@property (nonatomic, assign) int currentIndex;

@end

@implementation CreateLiveVideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
    [self setdata];
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.tabBarController.tabBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [circleView stopAnimation];
}

- (void)setupUI {
    self.view.backgroundColor = BGDarkBlueColor;
    [self.view addSubview:self.navView];
    [self.view addSubview:self.contentView];
}

- (void)setdata {
    [self creatTimer];
}

- (void)creatTimer {
    
    self.bannerTimer = [NSTimer scheduledTimerWithTimeInterval:3
                                                        target:self
                                                      selector:@selector(changeScrollOffset)
                                                      userInfo:nil
                                                       repeats:YES];
    // 调整timer 的优先级
    NSRunLoop *mainLoop = [NSRunLoop currentRunLoop];
    [mainLoop addTimer:self.bannerTimer forMode:NSRunLoopCommonModes];
}

- (void)changeScrollOffset {
    
    if (_currentIndex == _infoArray.count - 1 ) {
        _currentIndex = 0;
        [_infoScrollView setContentOffset:CGPointMake(SCREEN_Width * -1 , 0) animated:NO];
    } else {
        _currentIndex ++;
    }
    
    [_infoScrollView setContentOffset:CGPointMake(SCREEN_Width  * _currentIndex , 0) animated:YES];
    _pageControl.currentPage = _currentIndex;
}


- (void)stopTimer {
    [self.bannerTimer invalidate];
    self.bannerTimer = nil;
}


#pragma mark - Function
- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)startRecordAction {
    startRecordBt.hidden = YES;
    circleView.hidden = NO;
    restartRecordBt.hidden = YES;
    ensureBt.hidden = YES;
    [circleView startAnimation];
    
    [self performSelector:@selector(startRecordVideo) withObject:nil afterDelay:4];
    [self performSelector:@selector(finishiRecordVideo) withObject:nil afterDelay:19];
}

- (void)stopRecordAction {
    
    playerView.hidden = NO;
    filterMaskView.hidden = YES;
    
    startRecordBt.hidden = YES;
    circleView.hidden = YES;
    restartRecordBt.hidden = NO;
    ensureBt.hidden = NO;
}

- (void)ensureAction {
    NSLog(@"ensureAction");
    
    [DataTool startShowHUD];
    
    [playerView pause];
    
    AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:pathToMovie]];
    [self compressVideoWithAsset:asset callback:^(NSURL *fileUrl, NSString *errorStr) {
        NSData *compressVideoData = [NSData dataWithContentsOfURL:fileUrl];
//        NSLog(@"fileUrl %@   compressResizeImageData %lu", fileUrl, (unsigned long)compressResizeImageData.length);
        
//            NSData *compressResizeImageData = [NSData dataWithContentsOfURL:(nonnull NSURL *)]
            NSMutableArray *dataArray = [NSMutableArray arrayWithObjects:compressVideoData,  nil];
        [[NetTool shareInstance] putDatas:dataArray fileType:@"Mp4" needShowHUD:YES callback:^(id  _Nullable obj) {
                NSMutableArray *getArray = obj;
        
                dispatch_async(dispatch_get_main_queue(), ^{
        
//                    NSMutableArray *videoArray = [NSMutableArray arrayWithObject:getArray[0]];
                    NSDictionary *paramsDic = @{ @"videoType":@"ORDINARY", @"videoUrl":getArray[0]}; //API_User_PictureAudit
                    
                    
                    [[NetTool shareInstance] startRequest:API_Video_Audit_Save method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
    
                        
                        NSDictionary *resultDic = obj;
                        UserProfileDataModel *userProfileDataModel = [UserProfileDataModel mj_objectWithKeyValues:resultDic];
        
                        [[UserInfo shareInstant] checkUserDataModel:userProfileDataModel callBackInfo:^(id  _Nullable obj) {
                            
                            [self backAction];
                        }];
        
                    }];
                });
            }];
        
    }];
    
    
}

- (void)restartRecordAction {
    
    [playerView pause];
    playerView.hidden = YES;
    filterMaskView.hidden = NO;
    
    startRecordBt.hidden = NO;
    circleView.hidden = YES;
    restartRecordBt.hidden = YES;
    ensureBt.hidden = YES;
}

- (void)startRecordVideo {
    
    pathToMovie = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/movie%@.m4v", [DataTool getTimeNow]]];
    NSURL *movieURL = [NSURL fileURLWithPath:pathToMovie];
    
//    movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:movieURL size:CGSizeMake(540.0, 960.0)];
    movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:movieURL size:CGSizeMake(240.0, 320.0)];
//    movieWriter.encodingLiveVideo = YES;
    [filter addTarget:movieWriter];
    videoCamera.audioEncodingTarget = movieWriter;
    [movieWriter startRecording];
}

- (void)finishiRecordVideo {
    
    [filter removeTarget:movieWriter];
    videoCamera.audioEncodingTarget = nil;
    [movieWriter finishRecording];
    
    [self stopRecordAction];
    
    NSURL *movieURL = [NSURL fileURLWithPath:pathToMovie];
    [playerView updatePlayerWith:movieURL];
    
}


#pragma 压缩视频
- (void)compressVideoWithAsset:(AVAsset *)asset callback:(void (^)(NSURL *fileUrl, NSString *errorStr))callback {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *documentDirectory = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask].firstObject;
    NSString *fileName =  [NSString stringWithFormat:@"%@.mp4", NSUUID.UUID.UUIDString];
    
    NSURL *outputURL = [documentDirectory URLByAppendingPathComponent:@"output"];
    NSError *error = nil;
    [fileManager createDirectoryAtURL:outputURL withIntermediateDirectories:YES attributes:nil error:&error];
    outputURL = [outputURL URLByAppendingPathComponent:fileName];
    if (error) {
        callback(nil, error.localizedDescription);
        return;
    }
    [fileManager removeItemAtURL:outputURL error:nil];
    
    //设置视频压缩
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPreset640x480];
    exportSession.outputURL = outputURL;
    exportSession.outputFileType = AVFileTypeMPEG4;
    
    __weak typeof (exportSession) weakSession = exportSession;
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        __weak typeof (weakSession) exportSession = weakSession;
        switch (exportSession.status) {
            case AVAssetExportSessionStatusCompleted:
                callback(outputURL, nil);
                break;
            case AVAssetExportSessionStatusFailed:
                callback(nil, exportSession.error.localizedDescription);
                break;
            case AVAssetExportSessionStatusCancelled:
                callback(nil, exportSession.error.localizedDescription);
                break;
            default:
                break;
        }
    }];
}

#pragma mark - scrollview delegate
/**
手指开始拖动的时候, 就让计时器停止
*/
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self stopTimer];
    self.bannerTimer = nil ;
}
/**
 手指离开屏幕的时候, 就让计时器开始工作
 */
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    [self creatTimer];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    _currentIndex = (scrollView.contentOffset.x + ZoomSize(1) ) / self.view.frame.size.width;
    _pageControl.currentPage = _currentIndex;
}

#pragma Lazy

- (UIView *)navView {
    if (!_navView) {
        _navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];;
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(10), StatusBarHeight + ZoomSize(5), ZoomSize(40), ZoomSize(30))];
        [_navView addSubview:backBt];
        [backBt setImage:GetImageByName(@"common_back") forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(75), StatusBarHeight, ZoomSize(240), ZoomSize(30))];
        titleLb.text = @"Create a Live Video";
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightSemibold];
        [_navView addSubview:titleLb];
        
    }
    return _navView;
}

- (UIView *)contentView {
    if (!_contentView) {
        _contentView = [[UIView alloc] initWithFrame:CGRectMake(0, NavigationHeight, SCREEN_Width,  SCREEN_Height - NavigationHeight)];
        
        UILabel *tipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(20), ZoomSize(5), SCREEN_Width - ZoomSize(40), ZoomSize(40))];
        tipLb.text = @"You are only one live video away from \nyour dream match";
        tipLb.textAlignment = NSTextAlignmentCenter;
        tipLb.textColor = WhiteColor;
        tipLb.font = [UIFont systemFontOfSize:ZoomSize(16)];
        tipLb.numberOfLines = 0;
        [_contentView addSubview:tipLb];
        
        playerView = [[VideoPlayerView alloc] initWithFrame:CGRectMake(ZoomSize(67), ZoomSize(77), ZoomSize(240), ZoomSize(320))];
        [_contentView addSubview:playerView];
        playerView.layer.cornerRadius = ZoomSize(16);
        playerView.layer.masksToBounds = YES;
        [playerView setLookMeProfileState];
        playerView.hidden = YES;
        
        videoCamera = [[GPUImageVideoCamera alloc] initWithSessionPreset:AVCaptureSessionPreset640x480 cameraPosition:AVCaptureDevicePositionFront]; //设置分辨率与前置摄像头
        videoCamera.outputImageOrientation = [UIApplication sharedApplication].statusBarOrientation; //设置镜头向上
        videoCamera.horizontallyMirrorFrontFacingCamera = YES; //启动镜面模式
        
        filter = [[GPUImageSepiaFilter alloc] init];
        [(GPUImageSepiaFilter *)filter setIntensity:0];
        
        filterMaskView = [[UIView alloc] initWithFrame:CGRectMake(ZoomSize(67), ZoomSize(77), ZoomSize(240), ZoomSize(320))];
        [_contentView addSubview:filterMaskView];
        filterMaskView.layer.cornerRadius = ZoomSize(16);
        filterMaskView.layer.masksToBounds = YES;
        filterView = [[GPUImageView alloc] initWithFrame:filterMaskView.bounds];
        [filterMaskView addSubview:filterView];
        
        [videoCamera addTarget:filter];
        [filter addTarget:filterView];
        [videoCamera startCameraCapture];
        
        _infoArray = [NSArray arrayWithObjects:@"Use beauty cam to capture your best moment!", @"Great lighting and proper dress", @"show your face to get more like", nil];
        
        _pageControl = [[NTUIPageControl alloc] initWithFrame:CGRectMake( 0, ZoomSize(468) , SCREEN_Width, ZoomSize(8))];
//        _pageControl.center = CGPointMake(self.view.center.x, ZoomSize(470));
        [_contentView addSubview:_pageControl];
        _pageControl.numberOfPages = _infoArray.count;
        _pageControl.currentPage = 0;
        
        _pageControl.hidesForSinglePage = YES;
        [_pageControl setValue:[UIImage imageNamed:@"pageControl_current"] forKeyPath:@"_currentPageImage"];
        [_pageControl setValue:[UIImage imageNamed:@"pageControl_bg"] forKeyPath:@"_pageImage"];
        
        _infoScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, ZoomSize(438), SCREEN_Width,  ZoomSize(40))];
        [_contentView addSubview:_infoScrollView];
        _infoScrollView.bounces = YES;
        _infoScrollView.pagingEnabled = YES;
        _infoScrollView.showsHorizontalScrollIndicator = NO;
        _infoScrollView.delegate = self;
        _infoScrollView.contentSize = CGSizeMake( SCREEN_Width * _infoArray.count, 0);
        
        for (int i = 0; i < _infoArray.count; i++) {
            
            UILabel *tipLb = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_Width * i, 0, SCREEN_Width, ZoomSize(20))];
            tipLb.text = VipInfoArray[i];
            tipLb.textAlignment = NSTextAlignmentCenter;
            tipLb.textColor = RGBA(249, 249, 249, 0.8);
            tipLb.font = [UIFont systemFontOfSize:ZoomSize(14)];
            tipLb.numberOfLines = 0;
            [_infoScrollView addSubview:tipLb];
        }
        
        startRecordBt = [DataTool createCommomBtWithFrame:CGRectMake(ZoomSize(38), ZoomSize(500), ZoomSize(299), ZoomSize(48)) andTitle:@"Press to Record"];
        [_contentView addSubview:startRecordBt];
        [startRecordBt addTarget:self action:@selector(startRecordAction) forControlEvents:UIControlEventTouchUpInside];
        
#pragma 转圈
        circleView = [[CircleScaleView alloc] initWithFrame:CGRectMake(ZoomSize(161), ZoomSize(500), ZoomSize(55), ZoomSize(55))];
        [_contentView addSubview:circleView];
        circleView.backgroundColor = BGDarkBlueColor;
        circleView.layer.cornerRadius = ZoomSize(27);
        circleView.layer.masksToBounds = YES;
        
        circleView.bgColor = RGB(85, 20, 105);
        circleView.showColor = RGB(200, 40, 200);
        circleView.lineWidth = ZoomSize(4);
        circleView.durationTime = 19.0;
        circleView.hidden = YES;
        
        restartRecordBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(100), ZoomSize(500), ZoomSize(51), ZoomSize(51))];
        [_contentView addSubview:restartRecordBt];
        [restartRecordBt setBackgroundImage:GetImageByName(@"pop_uploadagrain") forState:UIControlStateNormal];
        [restartRecordBt addTarget:self action:@selector(restartRecordAction) forControlEvents:UIControlEventTouchUpInside];
        restartRecordBt.hidden = YES;
        
        ensureBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(224), ZoomSize(500), ZoomSize(51), ZoomSize(51))];
        [_contentView addSubview:ensureBt];
        [ensureBt setBackgroundImage:GetImageByName(@"pop_confirm") forState:UIControlStateNormal];
        [ensureBt addTarget:self action:@selector(ensureAction) forControlEvents:UIControlEventTouchUpInside];
        ensureBt.hidden = YES;
    }
    
    return _contentView;
}

@end

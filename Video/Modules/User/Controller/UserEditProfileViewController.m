//
//  UserEditProfileViewController.m
//  Video
//
//  Created by steve on 2020/4/16.
//  Copyright © 2020 steve. All rights reserved.
//

#import "UserEditProfileViewController.h"
#import "JKPickView.h"
#import "AgePickView.h"
#import "SelectPhotoManager.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <MJExtension/MJExtension.h>
#import "UserUpdateDataModel.h"

#import "VideoPlayerView.h"
#import <IDMPhotoBrowser.h>

#define textLength 100

@interface UserEditProfileViewController ()<UITextViewDelegate> {
    UITextField *nameTF;
    UIButton *birthdayBt;
    NSString *getBirthdayString;
    
    UITextView *aboutTV;
    UILabel *feedbackTipLb;
    UILabel *textNumLb;
    NSString *textPlaceHolder;
    
    UIButton *videoBt;
    VideoPlayerView *player;
    UIButton *videoFunctionBt;
}

@property (nonatomic, strong) UIView *navBgView;
@property (nonatomic, strong) UIScrollView *contentScrollView;
@property (nonatomic, strong) SelectPhotoManager *photoManager;
@property (nonatomic, strong) NSMutableArray *photoArray;

@end

@implementation UserEditProfileViewController

- (void)viewDidLoad {
    [self setupUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [self setdata];
    self.navigationController.tabBarController.tabBar.hidden = YES;
}

- (void)setupUI {
    self.view.backgroundColor = BGDarkBlueColor;
    [self.view addSubview:self.navBgView];
    [self.view addSubview:self.contentScrollView];
}

- (void)setdata {
    _photoManager = [[SelectPhotoManager alloc] init];
    _photoArray = [NSMutableArray array];
    
    textPlaceHolder = @"Enter a minimum of 20 characters...";
    [self setPage];
    
    NSDictionary *paramsDic = @{@"userId":[UserInfo shareInstant].user.userId}; //    //获取用户资料
    
    [[NetTool shareInstance] startRequest:API_User_Profile method:PostMethod params:paramsDic needShowHUD:NO callback:^(id  _Nullable obj) {
        NSDictionary *resultDic = obj;
        UserProfileDataModel *userProfileDataModel = [UserProfileDataModel mj_objectWithKeyValues:resultDic];
        [[UserInfo shareInstant] checkUserDataModel:userProfileDataModel callBackInfo:^(id  _Nullable obj) {
            [self setPage];
        }];
    }];
}

#pragma mark - Function

- (void)setPage {
    
#pragma 设置NameTF 如果状态为 AUDIT_ACCESS 则可以编辑  如果为空则可以编辑
    nameTF.text = [UserInfo shareInstant].user.nickName;
    if ([DataTool isEmptyString:[UserInfo shareInstant].user.nickName_status] || [[UserInfo shareInstant].user.nickName_status isEqualToString:@"AUDIT_ACCESS"] || [DataTool isEmptyString:[UserInfo shareInstant].user.nickName]) {
        nameTF.enabled = YES;
    }
    
#pragma 设置aboutTV 如果状态为 AUDIT_ACCESS 则可以编辑  如果为空则可以编辑 且设置为占位文字
    aboutTV.text = [UserInfo shareInstant].user.aboutMe;
    if ([DataTool isEmptyString:[UserInfo shareInstant].user.aboutMe_status] || [[UserInfo shareInstant].user.aboutMe_status isEqualToString:@"AUDIT_ACCESS"]) {
        aboutTV.editable = YES;
    }
    
    if ([DataTool isEmptyString:[UserInfo shareInstant].user.aboutMe] ) {
        aboutTV.editable = YES;
        aboutTV.text = textPlaceHolder;
        aboutTV.textColor= TextGrayColor;
    } else {
        textNumLb.text = [NSString stringWithFormat:@"( %lu/%d )", + [UserInfo shareInstant].user.aboutMe.length, textLength];
        aboutTV.textColor = BGDarkBlueColor;
    }
    
#pragma 设置生日 无状态设置 如果有数据则显示数据并改变颜色 否则 显示 yyyy-mm-dd
    [self setBirthdayBtWithString:[UserInfo shareInstant].user.birthday];
    
#pragma 设置图片视频信息
    [self setPageImageVideo];
}

- (void)setPageImageVideo {
    
    [_photoArray removeAllObjects];
    
    if ([UserInfo shareInstant].user.videos.count > 0) {
        UserResourceDataModel *model = [UserInfo shareInstant].user.videos.firstObject;
        if (model.state == 1 ) { //视频通过 ，可以重新上传 //        审核状态：审核状态 0.待审核 1.通过 2.拒绝
            videoFunctionBt.hidden = NO;
        }
        player.hidden = NO;
        [player updatePlayerWith:[NSURL URLWithString:model.name]];
    }
    
    UserResourceDataModel *headPortraitDataModel = [[UserResourceDataModel alloc] init]; //添加头像
    if (![DataTool isEmptyString:[UserInfo shareInstant].user.o_avatar]) {
        headPortraitDataModel.name = [UserInfo shareInstant].user.o_avatar;
    }
    if (![DataTool isEmptyString:[UserInfo shareInstant].user.headThumbnailUrl]) {
        headPortraitDataModel.name = [UserInfo shareInstant].user.headThumbnailUrl;
    }
    if (![DataTool isEmptyString:[UserInfo shareInstant].user.headPortraitUrl]) {
        headPortraitDataModel.name = [UserInfo shareInstant].user.headPortraitUrl;
    }
    
    if ([DataTool getStateCodeFromString:[UserInfo shareInstant].user.avatar_status] == 0) { //未审核，不能修改
        headPortraitDataModel.state = 0;
    } else {
        headPortraitDataModel.state = 1;
    }
    [_photoArray addObject:headPortraitDataModel];
    
    if ([UserInfo shareInstant].user.photos.count > 0) { //添加相册
        for (UserResourceDataModel *dataModel in [UserInfo shareInstant].user.photos) {
            [_photoArray addObject:dataModel];
        }
    }
    
    for (int i = 0; i<6; i++) { //刷新界面内容
        UIButton *subBt = [_contentScrollView viewWithTag:100+i];
        UIImageView *photoIV  = [subBt viewWithTag:200];
        UIButton *editPhotoBt = [_contentScrollView viewWithTag:300+i];
        
        if (i< _photoArray.count ) {
            UserResourceDataModel *dataModel = _photoArray[i];
            
            photoIV.hidden = NO;
            [photoIV sd_setImageWithURL:[NSURL URLWithString:dataModel.name] placeholderImage:GetImageByName(@"message_user_nopho")];
            if (dataModel.state == 1) { //显示提示图片
                editPhotoBt.hidden = NO;
            } else {
                editPhotoBt.hidden = YES;
            }
        } else {
            photoIV.hidden = YES;
            editPhotoBt.hidden = YES;
        }
    }
    
}

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)brithdayAction {
    [self.view endEditing:YES];
    
    AgePickView *pickview = [[AgePickView alloc] initWithFrame:self.view.frame];
    pickview.titleString = @"Birthday";
    [pickview show];
    
    pickview.back = ^(NSString *callBackString) {
        [self setBirthdayBtWithString:callBackString];
    };
}

- (void)setBirthdayBtWithString:(NSString *) birthString{
    
    if ([DataTool isEmptyString:birthString] ) {
        self->getBirthdayString = @"";
        [birthdayBt setTitle:@"MM-DD-YYYY" forState:UIControlStateNormal];
        [birthdayBt setTitleColor:TextGrayColor forState:UIControlStateNormal];
    } else {
        self->getBirthdayString = birthString;
        [self->birthdayBt setTitle:birthString forState:UIControlStateNormal];
        [self->birthdayBt setTitleColor:BGDarkBlueColor forState:UIControlStateNormal];
    }
}

- (void)doneAction {
    
    UserUpdateDataModel *dataModel = [[UserUpdateDataModel alloc] init]; // 由于数据存在状态问题，修改之前需要判断可以修改的内容
    dataModel.birthday = getBirthdayString;
    
    if ( ![[UserInfo shareInstant].user.nickName_status isEqualToString:@"UN_AUDIT"] || [DataTool isEmptyString:[UserInfo shareInstant].user.nickName]) {
        dataModel.nickName = nameTF.text;
    }
    
    if ( ![[UserInfo shareInstant].user.aboutMe_status isEqualToString:@"UN_AUDIT"] || [DataTool isEmptyString:[UserInfo shareInstant].user.aboutMe]) {
        if (![aboutTV.text isEqualToString:textPlaceHolder]) {
            dataModel.aboutMe = aboutTV.text;
        }
    }
    
    NSDictionary *paramsDic = [dataModel mj_keyValues];
    
    [[NetTool shareInstance] startRequest:API_User_Update method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
        NSDictionary *resultDic = obj;
        UserProfileDataModel *userProfileDataModel = [UserProfileDataModel mj_objectWithKeyValues:resultDic];
        
        [[UserInfo shareInstant] checkUserDataModel:userProfileDataModel callBackInfo:^(id  _Nullable obj) {
            [self backAction];
        }];
    }];
    
}

- (void)videoAction {
    [DataTool showUploadVideoPage];
}

- (void)photoAction:(UIButton *) sender {
    long tag = sender.tag - 100;
   
    if (tag < _photoArray.count) {
        UserResourceDataModel *dataModel = _photoArray[tag];
        
        if (dataModel.state != 1) { //1 代表图片可以编辑
            NSMutableArray *showPhotoArray = [NSMutableArray array];
            for (UserResourceDataModel *model in _photoArray) {
                IDMPhoto *photo = [IDMPhoto photoWithURL:[NSURL URLWithString:model.name]];
                [showPhotoArray addObject:photo];
            }
            NSArray *photos = [NSArray arrayWithArray:showPhotoArray];
            
            IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photos];
            [browser setInitialPageIndex:tag];
            browser.displayToolbar = NO;
            browser.displayDoneButton = NO;
            browser.dismissOnTouch = YES;
            browser.usePopAnimation = NO;
            [[DataTool getCurrentVC] presentViewController:browser animated:YES completion:nil];
            return;
        }
        
    } else { //无图片则添加
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle: UIAlertControllerStyleActionSheet];
        
        [alertController addAction: [UIAlertAction actionWithTitle: @"Add Photo" style: UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self addPhotoAction];
        }]];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
        [self.navigationController presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)editPhotoAction:(UIButton *) sender {
    long tag = sender.tag - 300;
    
    if (tag < _photoArray.count) {
        
        UserResourceDataModel *dataModel = _photoArray[tag];
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle: UIAlertControllerStyleActionSheet];
        
        if (dataModel.state == 1) { //1 代表图片可以编辑
            if (tag == 0) {
                [alertController addAction: [UIAlertAction actionWithTitle: @"Change" style: UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self changeHeadImg];
                }]];
            } else {
                [alertController addAction: [UIAlertAction actionWithTitle: @"Delete Photo" style: UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self deletePhoto:tag];
                }]];
            }
        }
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
        [self.navigationController presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)changeHeadImg {
    
    [_photoManager startSelectPhotoWithImageName:@"Choose Photo"];
    __weak __typeof (self) weakSelf = self;
    
    _photoManager.successHandle = ^(SelectPhotoManager * _Nonnull manager, UIImage * _Nonnull originalImage, UIImage * _Nonnull resizeImage) {
        
        __strong __typeof(self) strongSelf = weakSelf;
        NSData *originalImageData = UIImageJPEGRepresentation([DataTool compressSizeImage:originalImage], 0.25);
        NSData *resizeImageData = UIImageJPEGRepresentation([DataTool compressSizeImage:resizeImage], 0.25);
        
        NSMutableArray *dataArray = [NSMutableArray arrayWithObjects:originalImageData, resizeImageData, nil];
        
        [[NetTool shareInstance] putDatas:dataArray fileType:@"jpg" needShowHUD:YES callback:^(id  _Nullable obj) {
            NSMutableArray *getArray = obj;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSDictionary *paramsDic = @{ @"headPortraitUrl":getArray[0], @"headThumbnailUrl":getArray[1]};
                [[NetTool shareInstance] startRequest:API_User_Update method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
                    NSDictionary *resultDic = obj;
                    UserProfileDataModel *userProfileDataModel = [UserProfileDataModel mj_objectWithKeyValues:resultDic];
                    
                    [[UserInfo shareInstant] checkUserDataModel:userProfileDataModel callBackInfo:^(id  _Nullable obj) {
                        [strongSelf setPage];
                    }];
                }];
                
            });
            
        }];
    };
    
}

- (void)addPhotoAction {
    
    [_photoManager startSelectPhotoWithImageName:@"Choose Photo"];
    __weak __typeof (self) weakSelf = self;
    
    _photoManager.successHandle = ^(SelectPhotoManager * _Nonnull manager, UIImage * _Nonnull originalImage, UIImage * _Nonnull resizeImage) {
        
        __strong __typeof(self) strongSelf = weakSelf;
        NSData *compressResizeImageData = UIImageJPEGRepresentation([DataTool compressSizeImage:resizeImage], 0.25);
        NSMutableArray *dataArray = [NSMutableArray arrayWithObjects:compressResizeImageData,  nil];
        [[NetTool shareInstance] putDatas:dataArray fileType:@"jpg" needShowHUD:YES callback:^(id  _Nullable obj) {
            NSMutableArray *getArray = obj;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSMutableArray *photoArray = [NSMutableArray arrayWithObject:getArray[0]];
                NSDictionary *paramsDic = @{@"urls":photoArray}; //API_User_PictureAudit
                [[NetTool shareInstance] startRequest:API_Picture_Audit_Save method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
                    
                    NSDictionary *resultDic = obj;
                    UserProfileDataModel *userProfileDataModel = [UserProfileDataModel mj_objectWithKeyValues:resultDic];
                    
                    [[UserInfo shareInstant] checkUserDataModel:userProfileDataModel callBackInfo:^(id  _Nullable obj) {
                        [strongSelf setPageImageVideo];
                    }];
                }];
            });
        }];
    };
    
}

- (void)deletePhoto:(long) photoIndex {
    
    UserResourceDataModel *dataModel = _photoArray[photoIndex];
//    NSDictionary *paramsDic = @{@"id": [NSArray arrayWithObjects:dataModel.resource_id, nil] }; //API_User_PictureAudit
    NSDictionary *paramsDic = @{}; //API_User_PictureAudit
    [[NetTool shareInstance] startRequest:[NSString stringWithFormat:@"%@?id=%@", API_Pictures_Del, dataModel.resource_id] method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
        NSDictionary *resultDic = obj;
        UserProfileDataModel *userProfileDataModel = [UserProfileDataModel mj_objectWithKeyValues:resultDic];
        
        [[UserInfo shareInstant] checkUserDataModel:userProfileDataModel callBackInfo:^(id  _Nullable obj) {
            [self setPageImageVideo];
        }];
    }];
}

- (void) nameTFChange :(UITextField *) textField {
    if (textField.text.length > 20) {
        textField.text = [textField.text substringToIndex:20];
    }
}

#pragma mark - UITextViewDelegate
- (void)textViewDidBeginEditing:(UITextView*)textView {
    if([textView.text isEqualToString:textPlaceHolder]) {
        textView.text = @"";
        textView.textColor = BGDarkBlueColor;
    }
}

- (void)textViewDidEndEditing:(UITextView*)textView {
    if(textView.text.length<1) {
        textView.text = textPlaceHolder;
        textView.textColor= TextGrayColor;
    }
}

- (void)textViewDidChange:(UITextView *)textView {
    NSString *tempStr = textView.text;
    if (tempStr.length > textLength) {
        tempStr = [tempStr substringToIndex:textLength];
        textView.text = tempStr;
    }
    
    textNumLb.text = [NSString stringWithFormat:@"( %lu/%d )", + textView.text.length, textLength];
}

#pragma mark - lazy

- (UIView *)navBgView {
    if (!_navBgView) {
        _navBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];
        [self.view addSubview:_navBgView];
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(10), StatusBarHeight + ZoomSize(5), ZoomSize(40), ZoomSize(30))];
        [_navBgView addSubview:backBt];
        [backBt setImage:GetImageByName(@"common_back") forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(60), StatusBarHeight, ZoomSize(160), ZoomSize(30))];
        [_navBgView addSubview:titleLb];
        titleLb.text = @"Edit profile";
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightSemibold];
        
        UIButton *doneBt = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(76), StatusBarHeight, ZoomSize(60), ZoomSize(30))];
        [_navBgView addSubview:doneBt];
        [doneBt setImage:GetImageByName(@"DoneBt") forState:UIControlStateNormal];
        [doneBt addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _navBgView;
}

- (UIScrollView *)contentScrollView {
    if (!_contentScrollView) {
        _contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, NavigationHeight, SCREEN_Width, SCREEN_Height - NavigationHeight)];
        _contentScrollView.bounces = YES ;
        _contentScrollView.showsHorizontalScrollIndicator = NO ;
        _contentScrollView.showsVerticalScrollIndicator = NO ;
        
        UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, ZoomSize(17), SCREEN_Width, ZoomSize(1180) )];
        [_contentScrollView addSubview:contentView];
        contentView.backgroundColor = UIColor.whiteColor;
        
        UIBezierPath *contentViewMaskPath = [UIBezierPath bezierPathWithRoundedRect:contentView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(ZoomSize(36.5),ZoomSize(36.5))]; //设置部分圆角
        CAShapeLayer *contentViewMaskLayer = [[CAShapeLayer alloc] init];
        contentViewMaskLayer.frame = contentView.bounds;
        contentViewMaskLayer.path = contentViewMaskPath.CGPath;
        contentView.layer.mask = contentViewMaskLayer;
        
#pragma Album
        UIView *albumView = [self createFuntionViewWithFrame:CGRectMake(0, ZoomSize(30) , SCREEN_Width, ZoomSize(40)) andTitleString:@"Album"];
        [_contentScrollView addSubview:albumView];
        
        float photoBtWidth = (SCREEN_Width - ZoomSize(50))/3;
        float photoBtHeigth = (SCREEN_Width - ZoomSize(50))/2.2;
        
        for (int i = 0; i < 6; i++) {
            
            UIButton *photoBt = [[UIButton alloc] initWithFrame:CGRectMake( ZoomSize(5) + ZoomSize(10) * (i%3 + 1) + photoBtWidth * (i%3),  ZoomSize(55) + ZoomSize(10) * (i/3 + 1) + photoBtHeigth * (i/3) , photoBtWidth, photoBtHeigth)];
            photoBt.tag = 100 + i;
            photoBt.backgroundColor = LightGrayColor;
            [photoBt setImage:[UIImage imageNamed:@"profile_add"] forState:UIControlStateNormal];
            [_contentScrollView addSubview:photoBt];
            photoBt.layer.cornerRadius =ZoomSize(21);
            photoBt.layer.masksToBounds = YES;
            [photoBt addTarget:self action:@selector(photoAction:) forControlEvents:UIControlEventTouchUpInside];
            
            UIImageView *photoIV = [[UIImageView alloc] initWithFrame:photoBt.bounds];
            [photoBt addSubview:photoIV];
            photoIV.hidden = YES;
            [photoIV setContentMode:UIViewContentModeScaleAspectFill];
            photoIV.tag = 200;
            
            if (i == 0) {
                UIImageView *mainTipView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, ZoomSize(42), ZoomSize(42))];
                mainTipView.image = [UIImage imageNamed:@"profile_main"];
                [photoBt addSubview:mainTipView];
            }
            
            UIButton *editPhotoBt = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(photoBt.frame) - ZoomSize(22), CGRectGetMaxY(photoBt.frame) - ZoomSize(22), ZoomSize(24), ZoomSize(24))];
            [editPhotoBt addTarget:self action:@selector(editPhotoAction:) forControlEvents:UIControlEventTouchUpInside];
            [_contentScrollView addSubview:editPhotoBt];
            editPhotoBt.tag = 300 + i;
            
            [editPhotoBt setImage:GetImageByName(@"profile_delect") forState:UIControlStateNormal];
            if (i == 0) { //头像提示图片不一样
                [editPhotoBt setImage:GetImageByName(@"profile_edit") forState:UIControlStateNormal];
            }
            editPhotoBt.hidden = YES;
        }
        
#pragma VideoView
        UIView *videoView = [self createFuntionViewWithFrame:CGRectMake(0, ZoomSize(400) , SCREEN_Width, ZoomSize(400)) andTitleString:@"Live Video"];
        [_contentScrollView addSubview:videoView];
        
        videoBt = [[UIButton alloc] initWithFrame:CGRectMake( ZoomSize(15), ZoomSize(40) , ZoomSize(240), ZoomSize(320))];
        videoBt.backgroundColor = LightGrayColor;
        [videoBt setImage:[UIImage imageNamed:@"profile_add"] forState:UIControlStateNormal];
        [videoView addSubview:videoBt];
        videoBt.layer.cornerRadius =ZoomSize(16);
        videoBt.layer.masksToBounds = YES;
        [videoBt addTarget:self action:@selector(videoAction) forControlEvents:UIControlEventTouchUpInside];
        
        player = [[VideoPlayerView alloc]initWithFrame:videoBt.frame];
        player.layer.cornerRadius =ZoomSize(16);
        player.layer.masksToBounds = YES;
        [player setLookMeProfileState];
        [videoView addSubview:player];
        player.hidden = YES;
        
        videoFunctionBt = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(videoBt.frame) - ZoomSize(22), CGRectGetMaxY(videoBt.frame) - ZoomSize(22), ZoomSize(24), ZoomSize(24))];
        [videoFunctionBt addTarget:self action:@selector(videoAction) forControlEvents:UIControlEventTouchUpInside];
        [videoView addSubview:videoFunctionBt];
        [videoFunctionBt setImage:[UIImage imageNamed:@"profile_uploadagain"] forState:UIControlStateNormal];
        videoFunctionBt.hidden = YES;
        
        
#pragma nameView
        UIView *nameView = [self createFuntionViewWithFrame:CGRectMake(0, CGRectGetMaxY(videoView.frame), SCREEN_Width, ZoomSize(105)) andTitleString:@"Name"];
        [_contentScrollView addSubview:nameView];
        
        nameTF = [[UITextField alloc] initWithFrame:CGRectMake(ZoomSize(16), ZoomSize(42), SCREEN_Width - ZoomSize(32), ZoomSize(48))];
        nameTF.enabled = NO;
        [nameTF addTarget:self action:@selector(nameTFChange:) forControlEvents:UIControlEventEditingChanged];
        [nameView addSubview:nameTF];
        nameTF.backgroundColor = LightGrayColor;
        nameTF.layer.cornerRadius = ZoomSize(16);
        nameTF.layer.masksToBounds = YES;
        nameTF.font = [UIFont systemFontOfSize:ZoomSize(16)];
        nameTF.tintColor = BGDarkBlueColor;
        nameTF.textColor = BGDarkBlueColor;
        nameTF.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ZoomSize(13), 0)];
        nameTF.leftViewMode = UITextFieldViewModeAlways;
        
        
        NSMutableDictionary *attrs = [NSMutableDictionary dictionary]; //设置placeholder效果
        attrs[NSForegroundColorAttributeName] = TextGrayColor;
        NSAttributedString *attrStr = [[NSAttributedString alloc] initWithString:@"Add your first name" attributes:attrs];
        nameTF.attributedPlaceholder = attrStr;
        
#pragma birthdayView
        UIView *birthdayView = [self createFuntionViewWithFrame:CGRectMake(0, CGRectGetMaxY(nameView.frame), SCREEN_Width, ZoomSize(105)) andTitleString:@"Birthday"];
        [_contentScrollView addSubview:birthdayView];
        
        birthdayBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(16), ZoomSize(42), SCREEN_Width - ZoomSize(32), ZoomSize(48))];
        [birthdayView addSubview:birthdayBt];
        [birthdayBt addTarget:self action:@selector(brithdayAction) forControlEvents:UIControlEventTouchUpInside];
        birthdayBt.backgroundColor = LightGrayColor;
        birthdayBt.layer.cornerRadius = ZoomSize(16);
        [birthdayBt setTitle:@"MM-DD-YYYY" forState:UIControlStateNormal];
        [birthdayBt setTitleColor:TextGrayColor forState:UIControlStateNormal];
        [birthdayBt setImage:GetImageByName(@"common_arrow_right_blue") forState:UIControlStateNormal];
        birthdayBt.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft; //设置居左
        birthdayBt.titleEdgeInsets = UIEdgeInsetsMake(0, ZoomSize(5), 0, 0);
        birthdayBt.imageEdgeInsets = UIEdgeInsetsMake(0, SCREEN_Width - ZoomSize(55), 0, 0); //设置图片文字距离
        
#pragma genderView
        UIView *aboutView = [self createFuntionViewWithFrame:CGRectMake(0, CGRectGetMaxY(birthdayView.frame), SCREEN_Width, ZoomSize(150)) andTitleString:@"About me"];
        [_contentScrollView addSubview:aboutView];
        
        aboutTV = [[UITextView alloc] initWithFrame:CGRectMake(ZoomSize(15), ZoomSize(40), SCREEN_Width - ZoomSize(30), ZoomSize(100))];
        [aboutView addSubview:aboutTV];
        aboutTV.editable = NO;
        
        aboutTV.backgroundColor = RGB(234, 231, 237);
        aboutTV.tintColor = UIColor.blueColor;
        aboutTV.layer.cornerRadius = ZoomSize(16);
        aboutTV.font = [UIFont systemFontOfSize:ZoomSize(16)];
        aboutTV.textContainerInset = UIEdgeInsetsMake(ZoomSize(15), ZoomSize(15), ZoomSize(15), ZoomSize(15));
        aboutTV.delegate = self;
        
        textNumLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(110), ZoomSize(8), ZoomSize(100), ZoomSize(20))];
        [aboutView addSubview:textNumLb];
        textNumLb.font = [UIFont systemFontOfSize:ZoomSize(16)];
        textNumLb.text = [NSString stringWithFormat:@"( 0/%d )", textLength];
        textNumLb.textColor = TextGrayColor;
        textNumLb.textAlignment = NSTextAlignmentLeft;
        
        _contentScrollView.contentSize = CGSizeMake( SCREEN_Width , ZoomSize(1180));
    }
    return _contentScrollView;;
}

- (UIView *) createFuntionViewWithFrame:(CGRect) frame andTitleString:(NSString *) titleString {
    UIView *getView = [[UIView alloc] initWithFrame:frame];
    
    UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(16), ZoomSize(5), ZoomSize(200), ZoomSize(22))];
    [getView addSubview:titleLb];
    titleLb.text = titleString;
    titleLb.textColor = BGDarkBlueColor;
    titleLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightSemibold];
    
    return getView;
}

@end

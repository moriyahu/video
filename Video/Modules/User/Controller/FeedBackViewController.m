//
//  FeedBackViewController.m
//  Video
//
//  Created by steve on 2020/4/10.
//  Copyright © 2020 steve. All rights reserved.
//

#import "FeedBackViewController.h"

#define textLength 200

@interface FeedBackViewController ()<UITextViewDelegate>{
    UITextView *feedbackTV;
    UILabel *feedbackTipLb;
    UILabel *textNumLb;
    
    UIButton *submitBt;
    NSString *textPlaceHolder;
}

@property (nonatomic, strong) UIView *navView;
@property (nonatomic, strong) UIScrollView *contentScrollView;


@end

@implementation FeedBackViewController

- (void)viewDidLoad {
    
    [self setupUI];
    [self setdata];
}

- (void)setupUI {
    self.view.backgroundColor = BGDarkBlueColor;
    [self.view addSubview:self.navView];
    [self.view addSubview:self.contentScrollView];
}

- (void)setdata {
    
    textPlaceHolder = @"Enter a minimum of 20 characters...";
    feedbackTV.text = textPlaceHolder;
    feedbackTV.textColor= TextGrayColor;
}

#pragma mark - Function

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)submitAction {
    NSLog(@"submitAction");
    
//    //产品说的存枚举 type
//    BUGS
//    IDEAS
//    QUESTIONS
//    BILLING_QUESTIONS
    
    if (feedbackTV.text.length >= 20) {
        NSDictionary *paramsDic1 = @{@"description":feedbackTV.text, @"type":@"QUESTIONS" }; //删除资源
        [[NetTool shareInstance] startRequest:API_Feedbacks_Save method:PostMethod params:paramsDic1 needShowHUD:YES callback:^(id  _Nullable obj) {
            NSLog(@"obj %@", obj);
            [DataTool showToast:@"submit success"];
            [self backAction];
        }];
    }
    
    
}

#pragma mark - UITextViewDelegate
- (void)textViewDidBeginEditing:(UITextView*)textView {
    if([textView.text isEqualToString:textPlaceHolder]) {
        textView.text = @"";
        textView.textColor = BGDarkBlueColor;
    }
}

- (void)textViewDidEndEditing:(UITextView*)textView {
    if(textView.text.length<1) {
        textView.text = textPlaceHolder;
        textView.textColor= TextGrayColor;
    }
}

- (void)textViewDidChange:(UITextView *)textView {
    NSString *tempStr = textView.text;
    if (tempStr.length > textLength) {
        tempStr = [tempStr substringToIndex:textLength];
        textView.text = tempStr;
    }
    
    textNumLb.text = [NSString stringWithFormat:@"( %lu/%d )", + textView.text.length, textLength];
    if (textView.text.length >= 20) {
        [submitBt setBackgroundImage:GetImageByName(@"btBG_enable") forState:UIControlStateNormal];
    } else {
        [submitBt setBackgroundImage:GetImageByName(@"btBG_disable") forState:UIControlStateNormal];
    }
    
}


#pragma Lazy

- (UIView *)navView {
    if (!_navView) {
        _navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];;
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(10), StatusBarHeight + ZoomSize(5), ZoomSize(40), ZoomSize(30))];
        [_navView addSubview:backBt];
        [backBt setImage:GetImageByName(@"common_back") forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(60), StatusBarHeight, ZoomSize(160), ZoomSize(30))];
        titleLb.text = @"Feedback";
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightSemibold];
        [_navView addSubview:titleLb];
        
    }
    return _navView;
}

- (UIScrollView *)contentScrollView {
    if (!_contentScrollView) {
        _contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, NavigationHeight + ZoomSize(17), SCREEN_Width, SCREEN_Height - NavigationHeight - ZoomSize(17))];
//        _contentScrollView.bounces = YES ;
        _contentScrollView.showsHorizontalScrollIndicator = NO ;
        _contentScrollView.showsVerticalScrollIndicator = NO ;
        _contentScrollView.alwaysBounceVertical = YES;// 垂直
//        _contentScrollView.contentSize =
        
        UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, ZoomSize(0), SCREEN_Width, SCREEN_Height - NavigationHeight - ZoomSize(17) )];
        [_contentScrollView addSubview:contentView];
        contentView.backgroundColor = UIColor.whiteColor;
        
        UIBezierPath *contentViewMaskPath = [UIBezierPath bezierPathWithRoundedRect:contentView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(ZoomSize(36.5),ZoomSize(36.5))]; //设置部分圆角
        CAShapeLayer *contentViewMaskLayer = [[CAShapeLayer alloc] init];
        contentViewMaskLayer.frame = contentView.bounds;
        contentViewMaskLayer.path = contentViewMaskPath.CGPath;
        contentView.layer.mask = contentViewMaskLayer;
        
        feedbackTV = [UITextView new];
        [contentView addSubview:feedbackTV];
        feedbackTV.backgroundColor = RGB(234, 231, 237);
        feedbackTV.tintColor = UIColor.blueColor;
        feedbackTV.layer.cornerRadius = ZoomSize(16);
        feedbackTV.font = [UIFont systemFontOfSize:ZoomSize(16)];
        feedbackTV.textContainerInset = UIEdgeInsetsMake(ZoomSize(15), ZoomSize(15), ZoomSize(15), ZoomSize(15));
        feedbackTV.delegate = self;
        [feedbackTV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo( ZoomSize(20));
            make.left.mas_equalTo(ZoomSize(20));
            make.right.mas_equalTo(ZoomSize(-20));
            make.height.mas_equalTo(ZoomSize(207));
        }];
        
        textNumLb = [UILabel new];
        [contentView addSubview:textNumLb];
        textNumLb.font = [UIFont systemFontOfSize:ZoomSize(16)];
        textNumLb.text = [NSString stringWithFormat:@"( 0/%d )", textLength];
        textNumLb.textColor = BGGrayColor;
        textNumLb.textAlignment = NSTextAlignmentRight;
        [textNumLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(feedbackTV.mas_bottom).mas_offset(ZoomSize(10));
            make.right.mas_equalTo(ZoomSize(-20));
            make.width.mas_equalTo(ZoomSize(100));
            make.height.mas_equalTo(ZoomSize(20));
        }];
        
        submitBt = [UIButton new];
        [contentView addSubview:submitBt];
        [submitBt addTarget:self action:@selector(submitAction) forControlEvents:UIControlEventTouchUpInside];
        submitBt.layer.cornerRadius = ZoomSize(24);
        submitBt.layer.masksToBounds = YES;
        //    submitBt.backgroundColor = UIColor.whiteColor;
        [submitBt setTitle:@"Submit" forState:UIControlStateNormal];
        [submitBt setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
        submitBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightSemibold];
        [submitBt setBackgroundImage:GetImageByName(@"btBG_disable") forState:UIControlStateNormal];
        //    [submitBt setBackgroundImage:[DataTool imageWithColor:RGB(170, 162, 170)] forState:UIControlStateNormal];
        [submitBt mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(feedbackTV.mas_bottom).mas_offset(ZoomSize(82));
            make.left.mas_equalTo(ZoomSize(38));
            make.right.mas_equalTo(ZoomSize(-38));
            make.height.mas_equalTo(48);
        }];
        
    }
    return _contentScrollView;
}

@end

//
//  UserGetCoinsPopView.m
//  Video
//
//  Created by steve on 2020/4/20.
//  Copyright © 2020 steve. All rights reserved.
//

#import "UserGetCoinsPopView.h"
#import <MJExtension/MJExtension.h>
#import "PurchaseTool.h"

@interface UserGetCoinsPopView() <UITextViewDelegate> {
    UILabel *userCoinsLb;
    int chooseIndex;
}

@property (nonatomic, strong) UIView *contentView;

@end

@implementation UserGetCoinsPopView

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        [self initSubviews:frame];
    }
    return self;
}

- (void)initSubviews:(CGRect) frame {
    self.frame = frame;
    [self addSubview:self.contentView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setData) name:NeedRefleshProfilePage object:nil]; //编辑用户设置
}

#pragma function
- (void)show {
    
    [[[[[[UIApplication sharedApplication] delegate] window] rootViewController] view] addSubview:self]; //使用window，可以解决隐藏tabbar的问题
    
    [UIView animateWithDuration:0.25 animations:^{
        [self.contentView setFrame:CGRectMake(0, 0 , SCREEN_Width, SCREEN_Height)];
    }];
}

- (void)dismiss {
    [UIView animateWithDuration:0.25 animations:^{
        [self.contentView setFrame:CGRectMake(0, SCREEN_Height  , SCREEN_Width, SCREEN_Height)];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void) setData {
    userCoinsLb.text = [UserInfo shareInstant].user.balance;
}

- (NSAttributedString *)getContentLabelAttributedText:(NSString *)text {
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:text attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:ZoomSize(16)],NSForegroundColorAttributeName:WhiteColor, NSParagraphStyleAttributeName: paragraphStyle}];
    
    NSDictionary *attribtDic1 = @{NSLinkAttributeName: @"mailTo:", NSUnderlineStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
    [attrStr addAttributes:attribtDic1 range:NSMakeRange(text.length - 21, 21)];
    
    return attrStr;
}

- (void)buyAction {
    NSLog(@"buyAction");
    
    NSString *productID = CoinsPIDArray[chooseIndex];
    [[PurchaseTool shareInstance] startCreateOrderWithPID:productID];
    
}

- (void)chooseAction:(UIButton *)sender {
    
    for (int i = 0; i <6; i++) {
        UIButton *chooseBt = [_contentView viewWithTag:100 + i];
        chooseBt.layer.borderColor = UIColor.clearColor.CGColor;
        chooseBt.layer.borderWidth = ZoomSize(0);
    }
    sender.layer.borderColor = RGB(183, 39, 215).CGColor;
    sender.layer.borderWidth = ZoomSize(4);
    
    chooseIndex = (int) sender.tag - 100;
}

#pragma mark - UITextViewDelegate
- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange interaction:(UITextItemInteraction)interaction {
    if ([[URL scheme] isEqualToString:@"mailTo"]) {
        NSLog(@"mailTo");
    }
    return YES;
}

#pragma lazy
- (UIView *)contentView {
    if (!_contentView) {
        
        _contentView = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_Height, SCREEN_Width ,SCREEN_Height)];
        _contentView.layer.masksToBounds = YES;
        
        CAGradientLayer *gl = [CAGradientLayer layer];
        gl.frame = _contentView.bounds;
        gl.startPoint = CGPointMake(0.56, -0.77);
        gl.endPoint = CGPointMake(0.34, 1.34);
        gl.colors = @[(__bridge id)[UIColor colorWithRed:255/255.0 green:243/255.0 blue:119/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:255/255.0 green:174/255.0 blue:93/255.0 alpha:1.0].CGColor];
        gl.locations = @[@(0), @(1.0f)];
        [_contentView.layer addSublayer:gl];
        
        UIButton *closeBt = [[UIButton alloc] initWithFrame:CGRectMake( ZoomSize(12), StatusBarHeight + ZoomSize(12), ZoomSize(30), ZoomSize(30))];
        [closeBt addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
        [closeBt setImage:[UIImage imageNamed:@"coin_close"] forState:UIControlStateNormal];
        [_contentView addSubview:closeBt];
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake( (SCREEN_Width - ZoomSize(140))/2, StatusBarHeight + ZoomSize(12), ZoomSize(140), ZoomSize(30))];
        [_contentView addSubview:titleLb];
        titleLb.text = @"Get Coins";
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.textColor = BGDarkBlueColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightSemibold];
        
        UIImageView *coinIV = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(120), StatusBarHeight + ZoomSize(6), ZoomSize(40), ZoomSize(40))];
        coinIV.contentMode = UIViewContentModeScaleAspectFit;
        coinIV.image = GetImageByName(@"coins_img_0");
        [_contentView addSubview:coinIV];
        
        userCoinsLb = [[UILabel alloc] initWithFrame:CGRectMake( SCREEN_Width - ZoomSize(80), StatusBarHeight + ZoomSize(6), ZoomSize(80), ZoomSize(40))];
        [_contentView addSubview:userCoinsLb];
//        userCoinsLb.text =
        userCoinsLb.text = [UserInfo shareInstant].user.balance;
        userCoinsLb.textAlignment = NSTextAlignmentLeft;
        userCoinsLb.textColor = BGDarkBlueColor;
        userCoinsLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightSemibold];
        userCoinsLb.adjustsFontSizeToFitWidth = YES;
        
#pragma chooseBt
        
//        float photoBtWidth = (SCREEN_Width - ZoomSize(114))/2;
//        float photoBtHeigth = (SCREEN_Width - ZoomSize(114))/2.25;
        
        float photoBtWidth = ZoomSize(132);
        float photoBtHeigth = ZoomSize(117);
        
        
        for (int i = 0; i < 6; i++) {
            
            UIButton *chooseBt = [[UIButton alloc] initWithFrame:CGRectMake( ZoomSize(37) * (i%2 + 1) + photoBtWidth * (i%2), ZoomSize(60) + ZoomSize(45) * (i/2 + 1) + (photoBtHeigth / 1.2) * (i/2) , photoBtWidth, photoBtHeigth)];
            chooseBt.tag = 100 + i;
            chooseBt.backgroundColor = RGB(255, 231, 183);
            [_contentView addSubview:chooseBt];
            chooseBt.layer.cornerRadius =ZoomSize(21);
            chooseBt.layer.masksToBounds = YES;
            [chooseBt addTarget:self action:@selector(chooseAction:) forControlEvents:UIControlEventTouchUpInside];
            
            NSString *tipIVString = [NSString stringWithFormat:@"coins_img_%d", i];
            UIImageView *tipIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(0), ZoomSize(0), ZoomSize(70), ZoomSize(70))];
            tipIV.image = GetImageByName(tipIVString);
            [chooseBt addSubview:tipIV];
            
            UILabel *chooseNumLb = [[UILabel alloc] initWithFrame:CGRectMake( ZoomSize(72), ZoomSize(25), ZoomSize(60), ZoomSize(20))];
            [chooseBt addSubview:chooseNumLb];
            chooseNumLb.text = CoinsNumArray[i];
            chooseNumLb.textAlignment = NSTextAlignmentLeft;
            chooseNumLb.textColor = BGDarkBlueColor;
            chooseNumLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightSemibold];
            chooseNumLb.adjustsFontSizeToFitWidth = YES;
            
            UILabel *feeLb = [[UILabel alloc] initWithFrame:CGRectMake( 0, photoBtHeigth - ZoomSize(36), photoBtWidth, ZoomSize(36))];
            feeLb.backgroundColor = UIColor.whiteColor;
            [chooseBt addSubview:feeLb];
            feeLb.text = CoinsPriceArray[i];
            feeLb.textAlignment = NSTextAlignmentCenter;
            feeLb.textColor = BGDarkBlueColor;
            feeLb.font = [UIFont systemFontOfSize:ZoomSize(16)];
            feeLb.layer.cornerRadius =ZoomSize(18);
            feeLb.layer.masksToBounds = YES;
            
            if (i == 3) { //设置默认数据
                chooseBt.layer.borderColor = RGB(183, 39, 215).CGColor;
                chooseBt.layer.borderWidth = ZoomSize(4);
                chooseIndex = 3;
            }
        }
        
        
#pragma buyBt
        UIButton *buyBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(38), ZoomSize(540), SCREEN_Width - ZoomSize(76), ZoomSize(48))];
        [_contentView addSubview:buyBt];
        [buyBt addTarget:self action:@selector(buyAction) forControlEvents:UIControlEventTouchUpInside];
        buyBt.layer.cornerRadius = ZoomSize(24);
        buyBt.layer.masksToBounds = YES;
        
        CAGradientLayer *buyBtGl = [CAGradientLayer layer];
        buyBtGl.frame = buyBt.bounds;
        buyBtGl.startPoint = CGPointMake(0.56, -0.77);
        buyBtGl.endPoint = CGPointMake(0.46, 1.66);
        buyBtGl.colors = @[(__bridge id)[UIColor colorWithRed:238/255.0 green:48/255.0 blue:202/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:137/255.0 green:36/255.0 blue:244/255.0 alpha:1.0].CGColor];
        buyBtGl.locations = @[@(0), @(1.0f)];
        [buyBt.layer addSublayer:buyBtGl];
        
        UILabel *buyBtTitleLb = [[UILabel alloc] initWithFrame:buyBt.bounds];
        [buyBt addSubview:buyBtTitleLb];
        buyBtTitleLb.text = @"Buy Now";
        buyBtTitleLb.textAlignment = NSTextAlignmentCenter;
        buyBtTitleLb.textColor = WhiteColor;
        buyBtTitleLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightSemibold];
        
#pragma Terms &Privacy
        NSString *content = @"Contact us if you have any questions \n shgiegndnvg@gmail.com";
        UITextView *contentTextView = [[UITextView alloc]  initWithFrame:CGRectMake(ZoomSize(38), ZoomSize(600), SCREEN_Width - ZoomSize(76), ZoomSize(60))];
        [_contentView addSubview:contentTextView];
        contentTextView.attributedText = [self getContentLabelAttributedText:content];
        contentTextView.backgroundColor = UIColor.clearColor;
        contentTextView.linkTextAttributes = @{NSForegroundColorAttributeName:RGB(89, 75, 104)};

        contentTextView.textAlignment = NSTextAlignmentCenter;
        contentTextView.delegate = self;
        contentTextView.editable = NO;
        contentTextView.scrollEnabled = NO;
    }
    return _contentView;
}

@end

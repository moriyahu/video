//
//  BlockListTableViewCell.m
//  Video
//
//  Created by steve on 2020/4/26.
//  Copyright © 2020 steve. All rights reserved.
//

#import "BlockListTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface BlockListTableViewCell()

@property (weak, nonatomic) IBOutlet UIButton *unblockBt;
@property (weak, nonatomic) IBOutlet UILabel *nameLb;
@property (weak, nonatomic) IBOutlet UIImageView *logoIV;

@end

@implementation BlockListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];

    _unblockBt.layer.borderColor = RGB(201, 44, 217).CGColor;
    
}
- (IBAction)unblockAction:(id)sender {
    if (_unblockBlock) {
        _unblockBlock();
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setProfileModel:(UserProfileDataModel *)profileModel {
    
    _profileModel = profileModel;
    
//    //设置名字
    _nameLb.text = [NSString stringWithFormat:@"%@,%ld", profileModel.nickName, [DataTool getUserAgeFromString:profileModel.birthday]];
    [_logoIV sd_setImageWithURL:[NSURL URLWithString:profileModel.headPortraitUrl] placeholderImage:GetImageByName(@"message_system_logo")];
    
}


@end

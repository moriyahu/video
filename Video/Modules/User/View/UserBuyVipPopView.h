//
//  UserBuyVipPopView.h
//  Video
//
//  Created by steve on 2020/4/17.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserBuyVipPopView : UIView

- (instancetype) initWithFrame:(CGRect)frame showPageIndex: (int )showPageIndex;

- (void)show;

- (void)dismiss;

//@property (nonatomic, copy) ClickBlock dismissCallBack;

@end

NS_ASSUME_NONNULL_END

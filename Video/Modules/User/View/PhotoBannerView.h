//
//  PhotoBannerView.h
//  Video
//
//  Created by steve on 2020/4/22.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PhotoBannerView : UIView

- (instancetype) initWithFrame:(CGRect)frame photoArray:(NSMutableArray *)photoArray ;

@end

NS_ASSUME_NONNULL_END

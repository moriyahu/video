//
//  UserGetCoinsPopView.h
//  Video
//
//  Created by steve on 2020/4/20.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserGetCoinsPopView : UIView

- (instancetype) initWithFrame:(CGRect)frame;

- (void)show;

- (void)dismiss;

@property (nonatomic, copy) ClickBlock dismissCallBack;

@end

NS_ASSUME_NONNULL_END

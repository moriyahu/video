//
//  VipInfoView.m
//  Video
//
//  Created by steve on 2020/4/17.
//  Copyright © 2020 steve. All rights reserved.
//

#import "VipInfoView.h"
#import "NTUIPageControl.h"

@interface VipInfoView() <UIScrollViewDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NTUIPageControl *pageControl;

@property (nonatomic, strong) NSArray *photoArray;
@property (nonatomic, assign) int currentIndex;
@property(strong ,nonatomic) NSTimer *bannerTimer;

@end

@implementation VipInfoView

- (instancetype) initWithFrame:(CGRect)frame photoArray:(NSArray *)photoArray currentIndex:(int) currentIndex {
    self = [super initWithFrame:frame];
    if (self) {
        _photoArray = photoArray;
        _currentIndex = currentIndex;
        [self initSubviews:frame];
    }
    return self;
}

- (void)initSubviews: (CGRect) frame {
    self.frame = frame;
    
    [self addSubview:self.pageControl];
    [self addSubview:self.scrollView];
}


- (void) createTimer {
    _bannerTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(changeAction) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:_bannerTimer forMode:NSRunLoopCommonModes];
    
}

- (void) stopTimer {
    [self.bannerTimer invalidate];
    self.bannerTimer = nil;
}

- (void)changeAction {
    
    if (_currentIndex == _photoArray.count - 1 ) {
        _currentIndex = 0;
        [_scrollView setContentOffset:CGPointMake(self.frame.size.width  * -1 , 0) animated:NO];
    } else {
        _currentIndex ++;
    }
    
    [_scrollView setContentOffset:CGPointMake(self.frame.size.width  * _currentIndex , 0) animated:YES];
    _pageControl.currentPage = _currentIndex;
    
}


-(void)tapAction:(id)tap {
    if (_canPop) {
        [DataTool showGetVipViewWithIndex:_currentIndex];
    }
}

#pragma mark - scrollview delegate

/**
 手指开始拖动的时候, 就让计时器停止
 */
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    [self stopTimer];
    self.bannerTimer = nil ;
}
/**
 手指离开屏幕的时候, 就让计时器开始工作
 */
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    [self createTimer];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    _currentIndex = (_scrollView.contentOffset.x + ZoomSize(1) ) / self.frame.size.width;
    
    _pageControl.currentPage = _currentIndex;
    if (_scrollAction) {
        _scrollAction(_currentIndex);
    }
}

//- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
//    pozitionNum = (scrollView.contentOffset.x + 1 ) / _scrollView.frame.size.width ;
//    NSLog(@"scrollViewDidEndScrollingAnimation pozitionNum %d", pozitionNum);
//}

#pragma mark - lazy
- (UIScrollView *) scrollView {
    
    if (!_scrollView) {
        
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width,  self.frame.size.height)];
        _scrollView.bounces = YES;
        _scrollView.pagingEnabled = YES;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake( self.frame.size.width * _photoArray.count, 0);
        
        
        for (int i = 0; i < _photoArray.count; i++) {
            UIImageView *imageIV = [[UIImageView alloc] initWithFrame:CGRectMake( self.frame.size.width * i + (self.frame.size.width - ZoomSize(200))/2.0 , ZoomSize(10), ZoomSize(200) , ZoomSize(116))];
            imageIV.image = GetImageByName(_photoArray[i]);
            imageIV.contentMode = UIViewContentModeScaleAspectFill;
            imageIV.layer.masksToBounds = YES;
            [_scrollView addSubview:imageIV];
            
            UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width * i , ZoomSize(130), self.frame.size.width, ZoomSize(30))];
            titleLb.text = VipTitleArray[i];
            titleLb.textAlignment = NSTextAlignmentCenter;
            titleLb.textColor = BGDarkBlueColor;
            titleLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightSemibold];
            [_scrollView addSubview:titleLb];
            
            UILabel *tipLb = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width * i, ZoomSize(160), self.frame.size.width, ZoomSize(40))];
            tipLb.text = VipInfoArray[i];
            tipLb.textAlignment = NSTextAlignmentCenter;
            tipLb.textColor = TextBlackColor;
            tipLb.font = [UIFont systemFontOfSize:ZoomSize(16)];
            tipLb.numberOfLines = 0;
            [_scrollView addSubview:tipLb];
            
            
            [_scrollView setContentOffset:CGPointMake( _currentIndex * self.frame.size.width, 0) animated:NO];
            
            
            UITapGestureRecognizer *tapGesturRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction:)];
            [_scrollView addGestureRecognizer:tapGesturRecognizer];
            
            
        }
    }
    return _scrollView;
}

- (NTUIPageControl *)pageControl {
    if (!_pageControl) {
        
        _pageControl = [[NTUIPageControl alloc] initWithFrame:CGRectMake(0, self.frame.size.height - ZoomSize(15) ,self.frame.size.width, ZoomSize(8))];
//        _pageControl.center = CGPointMake(self.center.x, self.frame.size.height -  ZoomSize(15));
        _pageControl.numberOfPages = _photoArray.count;
        _pageControl.currentPage = _currentIndex;
        
        _pageControl.hidesForSinglePage = YES;
        [_pageControl setValue:[UIImage imageNamed:@"pageControl_current"] forKeyPath:@"_currentPageImage"];
        [_pageControl setValue:[UIImage imageNamed:@"pageControl_bg"] forKeyPath:@"_pageImage"];
        
    }
    return _pageControl;
}

@end

//
//  VipInfoView.h
//  Video
//
//  Created by steve on 2020/4/17.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VipInfoView : UIView

- (instancetype) initWithFrame:(CGRect)frame photoArray:(NSArray *)photoArray currentIndex:(int) currentIndex;

- (void) createTimer;
- (void) stopTimer;

@property (nonatomic ,strong) void(^scrollAction)(int scrollIndex);
@property (nonatomic ,assign) BOOL canPop;


@end

NS_ASSUME_NONNULL_END

//
//  UserBuyVipPopView.m
//  Video
//
//  Created by steve on 2020/4/17.
//  Copyright © 2020 steve. All rights reserved.
//

#import "UserBuyVipPopView.h"
#import "VipInfoView.h"
#import "WebViewController.h"
#import "PurchaseTool.h"

@interface UserBuyVipPopView()<UIGestureRecognizerDelegate, UITextViewDelegate> {
    CAGradientLayer *gl;
    int vipInfoIndex;
}

@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) VipInfoView *vipInfoView;

@end

@implementation UserBuyVipPopView


- (instancetype) initWithFrame:(CGRect)frame showPageIndex: (int )showPageIndex {
    self = [super initWithFrame:frame];
    if (self) {
        vipInfoIndex = showPageIndex;
        [self initSubviews:frame];
    }
    return self;
}

- (void)initSubviews:(CGRect) frame {
    self.frame = frame;
    [self addSubview:self.contentView];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismiss) name:NeedRefleshProfilePage object:nil]; //购买完消失
}
#pragma function
- (void)show {
    
    [self.vipInfoView createTimer];
    [[[[[[UIApplication sharedApplication] delegate] window] rootViewController] view] addSubview:self]; //使用window，可以解决隐藏tabbar的问题
    
    [UIView animateWithDuration:0.25 animations:^{
        [self.contentView setFrame:CGRectMake(0, 0 , SCREEN_Width, SCREEN_Height)];
    }];
}

- (void)dismiss {
    
    [self.vipInfoView stopTimer];
    [UIView animateWithDuration:0.25 animations:^{
        [self.contentView setFrame:CGRectMake(0, SCREEN_Height  , SCREEN_Width, SCREEN_Height)];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)continueAction {
    NSLog(@"continueAction");
    
    [[PurchaseTool shareInstance] startCreateOrderWithPID:@"vip_1"];
}

- (void)restoreAction {
    [[PurchaseTool shareInstance] restorePurchase:YES];
    NSLog(@"restoreAction");
}

- (NSAttributedString *)getContentLabelAttributedText:(NSString *)text {
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:text attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:ZoomSize(16)],NSForegroundColorAttributeName:TextGrayColor, NSParagraphStyleAttributeName: paragraphStyle}];
    
    NSDictionary *attribtDic1 = @{NSLinkAttributeName: @"Terms://", NSUnderlineStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
    [attrStr addAttributes:attribtDic1 range:NSMakeRange(0, 17)];
    
    NSDictionary *attribtDic2 = @{NSLinkAttributeName: @"Privacy://", NSUnderlineStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
    [attrStr addAttributes:attribtDic2 range:NSMakeRange(text.length - 14, 14)];
    
    return attrStr;
}

- (void)setBGglWithIndex:(int) pageIndex {
    
    switch (pageIndex) {
        case 0:
            gl.colors = @[(__bridge id)[UIColor colorWithRed:255/255.0 green:103/255.0 blue:238/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor];
            break;
            
        case 1:
            gl.colors = @[(__bridge id)[UIColor colorWithRed:131/255.0 green:113/255.0 blue:255/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor];
            break;
            
        case 2:
            gl.colors = @[(__bridge id)[UIColor colorWithRed:255/255.0 green:85/255.0 blue:128/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor];
            break;
            
        case 3:
            gl.colors = @[(__bridge id)[UIColor colorWithRed:155/255.0 green:128/255.0 blue:186/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor];
            break;
            
        default:
            break;
    }
    
    
}


#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange interaction:(UITextItemInteraction)interaction {
    WebViewController *ActivityVC = [[WebViewController alloc] init];
    if ([[URL scheme] isEqualToString:@"Terms"]) {
        ActivityVC.url = Terms_Url;
    } else if ([[URL scheme] isEqualToString:@"Privacy"]) {
        ActivityVC.url = Privacy_Url;
    }
    //    [[DataTool getCurrentVC].navigationController pushViewController:ActivityVC animated:YES];
    [[DataTool getCurrentVC] presentViewController:ActivityVC animated:YES completion:nil];
    return YES;
}


#pragma lazy
- (UIView *)contentView {
    if (!_contentView) {
        
        _contentView = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_Height, SCREEN_Width ,SCREEN_Height)];
        _contentView.backgroundColor = [UIColor whiteColor];
        _contentView.layer.masksToBounds = YES;
        
        // gradient
        gl = [CAGradientLayer layer];
        gl.frame = _contentView.bounds;
        gl.startPoint = CGPointMake(0.5, 0);
        gl.endPoint = CGPointMake(0.5, 0.44);
        gl.colors = @[(__bridge id)[UIColor colorWithRed:255/255.0 green:103/255.0 blue:238/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor];
        gl.locations = @[@(0), @(1.0f)];
        
        [_contentView.layer addSublayer:gl];
        
        
        UIButton *closeBt = [[UIButton alloc] initWithFrame:CGRectMake( ZoomSize(12), StatusBarHeight + ZoomSize(12), ZoomSize(30), ZoomSize(30))];
        [closeBt addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
        [closeBt setImage:[UIImage imageNamed:@"video_close"] forState:UIControlStateNormal];
        [_contentView addSubview:closeBt];
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake( (SCREEN_Width - ZoomSize(100))/2, StatusBarHeight + ZoomSize(12), ZoomSize(100), ZoomSize(30))];
        [_contentView addSubview:titleLb];
        titleLb.text = @"Get Vip";
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightSemibold];
        
#pragma vipInfoView
        [_contentView addSubview:self.vipInfoView];
        
        UIImageView *vipIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(76), ZoomSize(310), SCREEN_Width - ZoomSize(152), ZoomSize(135))];
        vipIV.contentMode = UIViewContentModeScaleAspectFit;
        vipIV.image = GetImageByName(@"vip_one_month");
        [_contentView addSubview:vipIV];
        
#pragma continueBt
        UIButton *continueBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(38), ZoomSize(500), SCREEN_Width - ZoomSize(76), ZoomSize(48))];
        [_contentView addSubview:continueBt];
        [continueBt addTarget:self action:@selector(continueAction) forControlEvents:UIControlEventTouchUpInside];
        continueBt.layer.cornerRadius = ZoomSize(24);
        continueBt.layer.masksToBounds = YES;
        
        CAGradientLayer *gl = [CAGradientLayer layer];
        gl.frame = continueBt.bounds;
        gl.startPoint = CGPointMake(0.56, -0.77);
        gl.endPoint = CGPointMake(0.46, 1.66);
        gl.colors = @[(__bridge id)[UIColor colorWithRed:238/255.0 green:48/255.0 blue:202/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:137/255.0 green:36/255.0 blue:244/255.0 alpha:1.0].CGColor];
        gl.locations = @[@(0), @(1.0f)];
        [continueBt.layer addSublayer:gl];
        
        UILabel *continueBtTitleLb = [[UILabel alloc] initWithFrame:continueBt.bounds];
        [continueBt addSubview:continueBtTitleLb];
        continueBtTitleLb.text = @"Continue";
        continueBtTitleLb.textAlignment = NSTextAlignmentCenter;
        continueBtTitleLb.textColor = WhiteColor;
        continueBtTitleLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightSemibold];
        
#pragma restoreBt
        UIButton *restoreBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(38), ZoomSize(580), SCREEN_Width - ZoomSize(76), ZoomSize(30))];
        [restoreBt setTitle:@"Restore" forState:UIControlStateNormal];
        [restoreBt setTitleColor:TextGrayColor forState:UIControlStateNormal];
        restoreBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(16)];
        [_contentView addSubview:restoreBt];
        [restoreBt addTarget:self action:@selector(restoreAction) forControlEvents:UIControlEventTouchUpInside];
        
#pragma Terms &Privacy
        NSString *content = @"Terms of Service and Privacy Policy";
        UITextView *contentTextView = [[UITextView alloc]  initWithFrame:CGRectMake(ZoomSize(38), ZoomSize(620), SCREEN_Width - ZoomSize(76), ZoomSize(30))];
        [_contentView addSubview:contentTextView];
        contentTextView.attributedText = [self getContentLabelAttributedText:content];
        contentTextView.backgroundColor = UIColor.clearColor;
        contentTextView.linkTextAttributes = @{NSForegroundColorAttributeName:BGDarkBlueColor};
        
        contentTextView.textAlignment = NSTextAlignmentCenter;
        contentTextView.delegate = self;
        contentTextView.editable = NO;
        contentTextView.scrollEnabled = NO;
    }
    
    return _contentView;
}

- (VipInfoView *)vipInfoView {
    if (!_vipInfoView ) {
        
        _vipInfoView = [[VipInfoView alloc] initWithFrame:CGRectMake(0, StatusBarHeight + ZoomSize(42), SCREEN_Width, ZoomSize(220)) photoArray:VipImageArray currentIndex:vipInfoIndex];
//        _vipInfoView.interval = 3;
        [self setBGglWithIndex:vipInfoIndex];
        
        __weak __typeof (self) weakSelf = self;
        _vipInfoView.scrollAction = ^(int scrollIndex) {
            __strong __typeof(self) strongSelf = weakSelf;
            [strongSelf setBGglWithIndex:scrollIndex];
        };
        
    }
    return  _vipInfoView;
}

@end

//
//  OfficialMessageTableViewCell.h
//  Video
//
//  Created by steve on 2020/6/5.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "V2TIMManager+Message.h"

NS_ASSUME_NONNULL_BEGIN

@interface OfficialMessageTableViewCell : UITableViewCell

@property (nonatomic, strong) V2TIMMessage *message;

@end

NS_ASSUME_NONNULL_END

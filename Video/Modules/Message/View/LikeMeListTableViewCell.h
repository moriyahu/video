//
//  LikeMeListTableViewCell.h
//  Video
//
//  Created by steve on 2020/4/8.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserProfileDataModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LikeMeListTableViewCell : UITableViewCell

@property (nonatomic, strong) UserProfileDataModel *profileModel;

@property (nonatomic, copy) ClickBlock likeBlock;

@end

NS_ASSUME_NONNULL_END

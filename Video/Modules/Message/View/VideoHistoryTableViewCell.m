//
//  VideoHistoryTableViewCell.m
//  Video
//
//  Created by steve on 2020/6/12.
//  Copyright © 2020 steve. All rights reserved.
//

#import "VideoHistoryTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface VideoHistoryTableViewCell() {
    
}

@property (weak, nonatomic) IBOutlet UIImageView *logoIV;
@property (weak, nonatomic) IBOutlet UILabel *nameLb;

@property (weak, nonatomic) IBOutlet UIImageView *onlineLogo;
@property (weak, nonatomic) IBOutlet UIImageView *validationLogo;

@property (weak, nonatomic) IBOutlet UIImageView *countryIV;
@property (weak, nonatomic) IBOutlet UILabel *countryLb;
@property (weak, nonatomic) IBOutlet UILabel *timeLb;

@end

@implementation VideoHistoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)setHistoryDataModel:(VideoHistoryDataModel *)historyDataModel {
    
    _historyDataModel = historyDataModel;
    
    _nameLb.text = historyDataModel.userInfo.nickName;
    [_logoIV sd_setImageWithURL:[NSURL URLWithString:historyDataModel.userInfo.headPortraitUrl] placeholderImage:GetImageByName(@"message_system_logo")];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *callTimeDate = [dateFormatter dateFromString:historyDataModel.lastestCallTime];
    
    _timeLb.text = [DataTool getTimTimeFromString:callTimeDate];
    
    
    _countryIV.image = [DataTool getImageForCountryName:historyDataModel.userInfo.country];
    _countryLb.text = historyDataModel.userInfo.country;
    
    if ([historyDataModel.userInfo.onlineState isEqualToString:@"ONLINE"]) {
        _onlineLogo.hidden = NO;
    }
    
    if (historyDataModel.userInfo.videos.count > 0) {
        _validationLogo.hidden = NO;
    }
    
}

@end

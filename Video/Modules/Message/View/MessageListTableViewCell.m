//
//  MessageListTableViewCell.h
//  Video
//
//  Created by steve on 2020/4/8.
//  Copyright © 2020 steve. All rights reserved.
//

#import "MessageListTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface MessageListTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *nameLb;
@property (weak, nonatomic) IBOutlet UILabel *timeLb;
@property (weak, nonatomic) IBOutlet UIImageView *logoIV;


@property (weak, nonatomic) IBOutlet UIImageView *messageNumBgIV;
@property (weak, nonatomic) IBOutlet UILabel *messageNumLb;

@end

@implementation MessageListTableViewCell

+ (MessageListTableViewCell *)initView{
    return  [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([MessageListTableViewCell class]) owner:nil options:nil] lastObject];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    UITapGestureRecognizer *singleFingerOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchAction)];
        singleFingerOne.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:singleFingerOne];
    
    
//    _messageLb.text = [NSString stringWithFormat:@"Hello, %@, welcome to Vido", [UserInfo shareInstant].user.nickName];
    
}


- (void)touchAction {
//    NSLog(@"dissmiss");
    if (_actionBlock) {
        _actionBlock();
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

//- (void)setProfileModel:(UserProfileDataModel *)profileModel {
//
//    _profileModel = profileModel;
//
////    //设置名字
////    _nameLb.text = [NSString stringWithFormat:@"%@,%ld", profileModel.nickName, [DataTool getUserAgeFromString:profileModel.birthday]];
////    [_likeBt setImage:GetImageByName(@"likeme_like_normal") forState:UIControlStateNormal];
//
//}

- (void)setConv:(V2TIMConversation *)conv {
    _conv = conv;
    
    _nameLb.text = conv.showName;
    
    [_logoIV sd_setImageWithURL:[NSURL URLWithString:conv.faceUrl] placeholderImage:GetImageByName(@"message_system_logo")];
    
    if (conv.lastMessage.elemType == V2TIM_ELEM_TYPE_TEXT) {
        _messageLb.text = conv.lastMessage.textElem.text;
    }
    
    _timeLb.text = [DataTool getTimTimeFromString:conv.lastMessage.timestamp];
    
    if (conv.unreadCount == 0) {
        _messageNumBgIV.hidden = YES;
        _messageNumLb.hidden = YES;
    } else {
        _messageNumBgIV.hidden = NO;
        _messageNumLb.hidden = NO;
        
        if (conv.unreadCount > 0 && conv.unreadCount <= 9) {
            _messageNumBgIV.image = GetImageByName(@"message_1");
            _messageNumLb.text = [NSString stringWithFormat:@"%d", conv.unreadCount];
        } else if (conv.unreadCount > 9 && conv.unreadCount <= 99 ) {
            _messageNumBgIV.image = GetImageByName(@"message_2");
            _messageNumLb.text = [NSString stringWithFormat:@"%d", conv.unreadCount];
        } else if (conv.unreadCount > 99) {
            _messageNumBgIV.image = GetImageByName(@"message_2");
            _messageNumBgIV.image = GetImageByName(@"99+");
        }
    }
    
}

- (void)setHistoryDataModel:(VideoHistoryDataModel *)historyDataModel {
    
    _historyDataModel = historyDataModel;
    
    _nameLb.text = historyDataModel.userInfo.nickName;
    [_logoIV sd_setImageWithURL:[NSURL URLWithString:historyDataModel.userInfo.headPortraitUrl] placeholderImage:GetImageByName(@"message_system_logo")];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *callTimeDate = [dateFormatter dateFromString:historyDataModel.lastestCallTime];
    
    _timeLb.text = [DataTool getTimTimeFromString:callTimeDate];
    
    _messageNumLb.hidden = YES;
    
}

@end

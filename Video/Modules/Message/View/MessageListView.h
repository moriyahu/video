//
//  MessageListView.h
//  Video
//
//  Created by steve on 2020/4/8.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MessageListView : UIView

@property (nonatomic, strong) UINavigationController *getNav;

- (instancetype) initWithFrame:(CGRect)frame;

//- (void)getHeaderData;
//- (void)reloadListData;

@end

NS_ASSUME_NONNULL_END

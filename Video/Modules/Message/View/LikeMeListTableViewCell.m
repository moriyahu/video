//
//  LikeMeListTableViewCell.m
//  Video
//
//  Created by steve on 2020/4/8.
//  Copyright © 2020 steve. All rights reserved.
//

#import "LikeMeListTableViewCell.h"

@interface LikeMeListTableViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *logoIV;
@property (weak, nonatomic) IBOutlet UILabel *nameLb;

@property (weak, nonatomic) IBOutlet UIImageView *onlineLogo;
@property (weak, nonatomic) IBOutlet UIImageView *validationLogo;
@property (weak, nonatomic) IBOutlet UIImageView *hotLogo;

@property (weak, nonatomic) IBOutlet UIImageView *countryIV;
@property (weak, nonatomic) IBOutlet UILabel *countryLb;
@property (weak, nonatomic) IBOutlet UIButton *likeBt;

@end

@implementation LikeMeListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (IBAction)likeAction:(id)sender {
    
    if (_likeBlock) {
        _likeBlock();
    }
    
    [_likeBt setImage:GetImageByName(@"likeme_like_select") forState:UIControlStateNormal];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setProfileModel:(UserProfileDataModel *)profileModel {
    
    _profileModel = profileModel;
    
//    //设置名字
    _nameLb.text = [NSString stringWithFormat:@"%@,%ld", profileModel.nickName, [DataTool getUserAgeFromString:profileModel.birthday]];
    [_likeBt setImage:GetImageByName(@"likeme_like_normal") forState:UIControlStateNormal];
    
    _countryIV.image = [DataTool getImageForCountryName:profileModel.country];
    _countryLb.text = profileModel.country;
    
    if ([_profileModel.onlineState isEqualToString:@"ONLINE"]) {
        _onlineLogo.hidden = NO;
    }
    
    if (_profileModel.videos.count > 0) {
        _validationLogo.hidden = NO;
    }
    
    
}

@end

//
//  MessageListTableViewCell.h
//  Video
//
//  Created by steve on 2020/4/8.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "V2TIMManager+Conversation.h"
#import "VideoHistoryDataModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface  MessageListTableViewCell : UITableViewCell

+ (MessageListTableViewCell *)initView;

@property (weak, nonatomic) IBOutlet UILabel *messageLb;
@property (nonatomic, strong) V2TIMConversation *conv;

@property (nonatomic, strong) VideoHistoryDataModel *historyDataModel;

@property (nonatomic, copy) ClickBlock actionBlock;

@end

NS_ASSUME_NONNULL_END

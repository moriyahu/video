//
//  LikeMeListView.m
//  Video
//
//  Created by steve on 2020/4/8.
//  Copyright © 2020 steve. All rights reserved.
//

#import "MJExtension.h"
#import "MJRefresh.h"

#import "LikeMeListView.h"
#import "LikeMeListTableViewCell.h"
#import "UITableView+PlaceHolderView.h"
#import "ListPlaceHoderActionView.h"


static NSString * const reuseIdentifier = @"LikeMeListTableViewCell";

@interface LikeMeListView() <UITableViewDelegate, UITableViewDataSource> {
    int pageIndex;
    bool hasNextPage;
}

@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * array;

@end

@implementation LikeMeListView

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews:frame];
    }
    return self;
}

- (void)initSubviews:(CGRect) frame {
    self.frame = frame;
    self.backgroundColor = BGDarkBlueColor;
    
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, ZoomSize(17), self.frame.size.width , self.frame.size.height - ZoomSize(17))];
    [self addSubview:contentView];
    contentView.backgroundColor = WhiteColor;
    
    UIBezierPath *contentViewMaskPath = [UIBezierPath bezierPathWithRoundedRect:contentView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(ZoomSize(36.5),ZoomSize(36.5))]; //设置部分圆角
    CAShapeLayer *contentViewMaskLayer = [[CAShapeLayer alloc] init];
    contentViewMaskLayer.frame = contentView.bounds;
    contentViewMaskLayer.path = contentViewMaskPath.CGPath;
    contentView.layer.mask = contentViewMaskLayer;
    
    [contentView addSubview:self.tableView];
    self.array = [NSMutableArray array];
    
}

#pragma mark - action

- (void)getHeaderData {
    self->pageIndex = 1;
    [self requestData];
}

- (void)getFooterData {
    if (hasNextPage) {
        [self requestData];
    } else {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    }
}

- (void)requestData {
    NSDictionary *paramsDic = @{@"pageSize": [NSNumber numberWithInt:5], @"pageNum": [NSNumber numberWithInt:pageIndex]};

    [[NetTool shareInstance] startRequest:API_Rel_LikeMe method:PostMethod params:paramsDic needShowHUD:NO callback:^(id  _Nullable obj) {
        
        if (self->pageIndex == 1) {
            [self.array removeAllObjects];
            [self.tableView reloadData];
        }
        
        NSDictionary *resultDic = obj;
        NSArray *listArray = resultDic[@"list"];
        NSArray *userArray = [UserProfileDataModel mj_objectArrayWithKeyValuesArray:listArray];
        [self.array addObjectsFromArray:userArray];
        self->hasNextPage = [resultDic[@"hasNextPage"] boolValue];
        
        self->pageIndex ++;

        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [self.tableView reloadData];
    }];
    
}

- (void)reloadListData {
    [self.tableView reloadData];
}

#pragma mark - delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.array.count;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    LikeMeListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    
    if(cell == nil) {
        cell = [[LikeMeListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }
    
    UserProfileDataModel *userModel = self.array[indexPath.row];
    cell.profileModel = userModel;
    
    cell.likeBlock = ^{
        
        NSDictionary *paramsDic = @{@"beLikedUserId": userModel.userId};
        [[NetTool shareInstance] startRequest:API_Rel_AddLike method:PostMethod params:paramsDic needShowHUD:NO callback:^(id  _Nullable obj) {
            [self.array removeObjectAtIndex:indexPath.row];
            [self.tableView reloadData];
            
            [[UserInfo shareInstant] showMatchPopView:userModel showType:@"LikeMe" roomId:@""];
            
        }];
    };
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    UserProfileDataModel *profileModel = self.array[indexPath.row];
//    [DataTool showUserProfilePage:profileModel];
    
}

#pragma mark - lazy
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width , self.frame.size.height - ZoomSize(17) ) style:UITableViewStylePlain];
        
        [_tableView registerNib:[UINib nibWithNibName:reuseIdentifier bundle:nil] forCellReuseIdentifier:reuseIdentifier];
        _tableView.separatorStyle = UITableViewCellEditingStyleNone;
        
//        _tableView.backgroundView = UIColor.whiteColor;
        _tableView.backgroundColor = UIColor.whiteColor;
        _tableView.rowHeight = ZoomSize(94);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        _tableView.mj_header = [MJRefreshStateHeader headerWithRefreshingBlock:^{
            [self getHeaderData];
        }];
        _tableView.mj_footer = [MJRefreshBackStateFooter footerWithRefreshingBlock:^{
            [self getFooterData];
        }];
        
        ListPlaceHoderActionView *listPlaceHolderActionView = [[ListPlaceHoderActionView alloc] initWithFrame:_tableView.bounds tipImgName:@"common_empty" viewTitle:@"No one likes you yet" tipsInfo:@"" btTitle:@"Start Match"];
        listPlaceHolderActionView.actionBlock = ^{
            [self.getNav.tabBarController setSelectedIndex:1];
        };
        _tableView.enablePlaceHolderView = YES;
        _tableView.yh_PlaceHolderView = listPlaceHolderActionView;
    }
    return _tableView;
}

@end

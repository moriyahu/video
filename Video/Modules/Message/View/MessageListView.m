//
//  MessageListView.m
//  Video
//
//  Created by steve on 2020/4/8.
//  Copyright © 2020 steve. All rights reserved.
//

#import "MJExtension.h"
#import "MJRefresh.h"

#import "MessageListView.h"
#import "MessageListTableViewCell.h"
#import "UITableView+PlaceHolderView.h"
#import "ListPlaceHoderActionView.h"

#import "VideoHistoryViewController.h"
#import "OfficialChatListViewController.h"

#import <TIMManager.h>
#import <TIMConversation.h>
#import "V2TIMManager+Conversation.h"
#import "V2TIMManager+Message.h"

static NSString * const reuseIdentifier = @"MessageListTableViewCell";

@interface MessageListView() <UITableViewDelegate, UITableViewDataSource,V2TIMSDKListener, V2TIMConversationListener> {
    int pageIndex;
    ListPlaceHoderActionView *listPlaceHolderActionView;
    V2TIMConversation *systemConv;
    
}

@property (nonatomic, strong) UIButton * chatHistoryBt;
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UITableView * tableView;
//@property (nonatomic, strong) NSMutableArray * array;
@property (nonatomic, strong) NSMutableArray * uiConvList;

@end

@implementation MessageListView

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews:frame];
        
        self.uiConvList = [NSMutableArray array];
    }
    return self;
}

- (void)initSubviews:(CGRect) frame {
    
    self.frame = frame;
    self.backgroundColor = BGDarkBlueColor;
    
    [self addSubview:self.chatHistoryBt];
    [self addSubview:self.contentView];
    
//    if ([[TIMManager sharedInstance] getLoginStatus] == 1) {
//        [self requestData];
//    } else {
//        [UserInfo shareInstant].timLoginSuccBlock = ^(id  _Nullable obj) {
//            [self requestData];
//        };
//    }
    
    V2TIMSDKConfig *config = [V2TIMSDKConfig alloc]; //需要初始化，否则不能监听
    config.logLevel = V2TIM_LOG_NONE;
    [[V2TIMManager sharedInstance] initSDK:[TXIMSDK_APP_KEY intValue] config:config listener:self];
    
    [[V2TIMManager sharedInstance] setConversationListener:self];//添加消息监听
    
}

#pragma mark - action

- (void)chatHistoryAction {
    VideoHistoryViewController *vc = [[VideoHistoryViewController alloc] init];
    [self.getNav pushViewController:vc animated:YES];
}

- (void)requestData {
    
//    V2TIMSDKConfig *config = [V2TIMSDKConfig alloc]; //需要初始化，否则不能监听
//    config.logLevel = V2TIM_LOG_NONE;
//    [[V2TIMManager sharedInstance] initSDK:[TXIMSDK_APP_KEY intValue] config:config listener:self];
//
//    [[V2TIMManager sharedInstance] setConversationListener:self];//添加消息监听
    
    
//    __weak __typeof(self) weakSelf = self;
//    [[V2TIMManager sharedInstance] getConversationList:0 count:50
//                                                  succ:^(NSArray<V2TIMConversation *> *list, uint64_t nextSeq, BOOL isFinished) {
//        __strong __typeof(weakSelf) strongSelf = weakSelf;
//        [strongSelf updateConversation:list]; // 获取成功
////        NSLog(@"msg %@", list);
//    } fail:^(int code, NSString *msg) {
////        NSLog(@"code %d msg %@", code, msg); // 拉取会话列表失败
//    }];
    
}

// 收到会话新增的回调
- (void)onNewConversation:(NSArray<V2TIMConversation*> *) conversationList {
    [self updateConversation:conversationList];
}

// 收到会话更新的回调
- (void)onConversationChanged:(NSArray<V2TIMConversation*> *) conversationList {
    [self updateConversation:conversationList];
}

// 更新 UI 会话列表
- (void)updateConversation:(NSArray *)convList {

    for (int i = 0 ; i < convList.count ; ++ i) { //     如果 UI 会话列表有更新的会话，就替换，如果没有，就新增
        V2TIMConversation *conv = convList[i];
        BOOL isExit = NO;
        for (int j = 0; j < self.uiConvList.count; ++ j) {
            V2TIMConversation *uiConv = self.uiConvList[j];
            
            if ([uiConv.conversationID isEqualToString:conv.conversationID]) { // UI 会话列表有更新的会话，直接替换
                [self.uiConvList replaceObjectAtIndex:j withObject:conv];
                isExit = YES;
                break;
            }
        }
        
        if (!isExit) { // UI 会话列表没有更新的会话，直接新增
            [self.uiConvList addObject:conv];
        }
    }
    
    if (self.uiConvList.count > 0) {
        
        // 重新按照会话 lastMessage 的 timestamp 对 UI 会话列表做排序
        [self.uiConvList sortUsingComparator:^NSComparisonResult(V2TIMConversation *obj1, V2TIMConversation *obj2) {
            return [obj2.lastMessage.timestamp compare:obj1.lastMessage.timestamp];
        }];
        
       
        systemConv = self.uiConvList.firstObject;
        if (self.uiConvList.count == 1 && [systemConv.showName isEqualToString:@"vido_system"] ) { //判断只有一个数据，且为系统消息的时候。出现placeholder
            
            [listPlaceHolderActionView setSystemConv:systemConv];
            
            [self.uiConvList removeAllObjects];
            [self.tableView reloadData];
            
        } else {
            
             BOOL needShowSystemToFirst = false;
            
            for (V2TIMConversation *obj in self.uiConvList) { //将系统的数据放在第一个
                if ([obj.showName isEqualToString:@"vido_system"]) {
                    needShowSystemToFirst = YES;
                    systemConv = obj;
                }
            }
            
            if (needShowSystemToFirst) {
                [self.uiConvList removeObject:systemConv];
                [self.uiConvList insertObject:systemConv atIndex:0];
            }
            
            [self.tableView reloadData];
        }
        
    }
    
}

#pragma mark - delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.uiConvList.count;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MessageListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if(cell == nil) {
        cell = [[MessageListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }
    
    V2TIMConversation *uiConv = self.uiConvList[indexPath.row];
    cell.conv = uiConv; //设置数据
    
    cell.actionBlock = ^{
        
        if ([uiConv.userID isEqualToString:@"Vido"]) {
            OfficialChatListViewController *vc = [[OfficialChatListViewController alloc] init];
            [[DataTool getCurrentVC].navigationController pushViewController:vc animated:YES];
            
            //设置消息已读
            [[V2TIMManager sharedInstance] markC2CMessageAsRead:uiConv.userID succ:^{
            } fail:^(int code, NSString *desc) {
            }];
            
        } else {
            
            UserProfileDataModel *profileModel = [[UserProfileDataModel alloc] init];
            profileModel.userId = uiConv.userID;
            profileModel.nickName = uiConv.showName;
            profileModel.headPortraitUrl = uiConv.faceUrl;
//            _nameLb.text = conv.showName;
//            [_logoIV sd_setImageWithURL:[NSURL URLWithString:conv.faceUrl] placeholderImage:GetImageByName(@"message_system_logo")];
            
            [[UserInfo shareInstant] showChatViewControllerWithType:TIM_C2C receiver:profileModel.userId receiverProfileModel:profileModel];
        }
    };
    
    return cell;
}


#pragma mark - lazy

- (UIButton *)chatHistoryBt {
    if (!_chatHistoryBt) {
        _chatHistoryBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(16), ZoomSize(17), self.frame.size.width - ZoomSize(32), ZoomSize(68))];
        [_chatHistoryBt addTarget:self action:@selector(chatHistoryAction) forControlEvents:UIControlEventTouchUpInside];
        _chatHistoryBt.backgroundColor = WhiteColor;
        _chatHistoryBt.layer.cornerRadius = ZoomSize(34);
        _chatHistoryBt.layer.masksToBounds = YES;
        
        [_chatHistoryBt setImage:[UIImage imageNamed:@"common_arrow_right_blue"] forState:UIControlStateNormal];
        [_chatHistoryBt setTitle:@"History" forState:UIControlStateNormal];
        [_chatHistoryBt setTitleColor:BGDarkBlueColor forState:UIControlStateNormal];
        _chatHistoryBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(18)];
        _chatHistoryBt.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft; //设置居左
        _chatHistoryBt.titleEdgeInsets = UIEdgeInsetsMake(0, ZoomSize(82), 0, 0);
        _chatHistoryBt.imageEdgeInsets = UIEdgeInsetsMake(0, self.frame.size.width - ZoomSize(60), 0, 0); //设置图片文字距离
        
        UIImageView *tipIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, ZoomSize(68), ZoomSize(68))];
        tipIV.image = GetImageByName(@"message_history");
        [_chatHistoryBt addSubview:tipIV];
        
    }
    return _chatHistoryBt;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:self.contentView.bounds style:UITableViewStylePlain];
        
        [_tableView registerNib:[UINib nibWithNibName:reuseIdentifier bundle:nil] forCellReuseIdentifier:reuseIdentifier];
        _tableView.separatorStyle = UITableViewCellEditingStyleNone;
        
        _tableView.backgroundColor = UIColor.whiteColor;
        _tableView.rowHeight = ZoomSize(94);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        listPlaceHolderActionView = [[ListPlaceHoderActionView alloc] initWithFrame:_tableView.bounds tipImgName:@"common_empty" viewTitle:@"Meet new friends now!" tipsInfo:@"No history of conversations right now. Start chatting with folks!" btTitle:@"Start Match"];
        
         __weak __typeof (self) weakSelf = self;
        listPlaceHolderActionView.actionBlock = ^{
             __strong __typeof(self) strongSelf = weakSelf;
            [strongSelf.getNav.tabBarController setSelectedIndex:1];
        };
        _tableView.enablePlaceHolderView = YES;
        _tableView.yh_PlaceHolderView = listPlaceHolderActionView;
        
    }
    return _tableView;
}

- (UIView *)contentView {
    if (!_contentView) {
        _contentView = [[UIView alloc] initWithFrame:CGRectMake(0, ZoomSize(108), self.frame.size.width , self.frame.size.height - ZoomSize(108))];
        _contentView.backgroundColor = WhiteColor;
        
        UIBezierPath *contentViewMaskPath = [UIBezierPath bezierPathWithRoundedRect:_contentView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(ZoomSize(36.5),ZoomSize(36.5))]; //设置部分圆角
        CAShapeLayer *contentViewMaskLayer = [[CAShapeLayer alloc] init];
        contentViewMaskLayer.frame = _contentView.bounds;
        contentViewMaskLayer.path = contentViewMaskPath.CGPath;
        _contentView.layer.mask = contentViewMaskLayer;
        
        [_contentView addSubview:self.tableView];
    }
    
    return _contentView;
}


@end

//
//  VideoHistoryTableViewCell.h
//  Video
//
//  Created by steve on 2020/6/12.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoHistoryDataModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface VideoHistoryTableViewCell : UITableViewCell

@property (nonatomic, strong) VideoHistoryDataModel *historyDataModel;

@end

NS_ASSUME_NONNULL_END

//
//  MessageViewController.m
//  Video
//
//  Created by steve on 2020/3/23.
//  Copyright © 2020 steve. All rights reserved.
//

#import "MessageViewController.h"

#import "MessageListView.h"
#import "LikeMeListView.h"

@interface MessageViewController ()

@property (nonatomic, strong) UIView *navBgView;
@property (nonatomic, strong) UIButton *messageBt;
@property (nonatomic, strong) UIButton *likeMeBt;

@property (nonatomic, strong) UIScrollView *contentScrollView;
@property (nonatomic, strong) MessageListView *messageListView;
@property (nonatomic, strong) LikeMeListView *likeMeListView;

@end

@implementation MessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//#warning 缺失红点提示逻辑
    [self setupUI];
    [self setData]; //获取数据
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = YES;
    self.navigationController.tabBarController.tabBar.hidden = NO;
}

- (void)setupUI {
    self.view.backgroundColor = BGDarkBlueColor;
    [self.view addSubview:self.navBgView];
    [self.view addSubview:self.contentScrollView];
}

- (void)setData {
    [self.likeMeListView getHeaderData];
}

- (void)showMessageView {
    _messageBt.selected = YES;
    _likeMeBt.selected = NO;
    [self->_contentScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (void)showLikeMeView {
    _messageBt.selected = NO;
    _likeMeBt.selected = YES;
    [self->_contentScrollView setContentOffset:CGPointMake( SCREEN_Width, 0) animated:YES];
}

#pragma mark - lazy
- (UIView *)navBgView {
    if (!_navBgView) {
        
        _navBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, StatusBarHeight + ZoomSize(50))];
        _navBgView.backgroundColor = BGDarkBlueColor;
        
        _messageBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(10), StatusBarHeight + ZoomSize(10), ZoomSize(100), ZoomSize(30))];
        [_messageBt setTitle:@"Message" forState:UIControlStateNormal];
        [_messageBt setTitleColor:RGBA(255, 255, 255, 0.29) forState:UIControlStateNormal];
        [_messageBt setTitleColor:RGBA(255, 255, 255, 1.0) forState:UIControlStateSelected];
        _messageBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(24)];
        [_messageBt addTarget:self action:@selector(showMessageView) forControlEvents:UIControlEventTouchUpInside];
        [self.navBgView addSubview:_messageBt];
        
        _likeMeBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(140), StatusBarHeight + ZoomSize(10), ZoomSize(100), ZoomSize(30))];
        [_likeMeBt setTitle:@"Like Me" forState:UIControlStateNormal];
        [_likeMeBt setTitleColor:RGBA(255, 255, 255, 0.29) forState:UIControlStateNormal];
        [_likeMeBt setTitleColor:RGBA(255, 255, 255, 1.0) forState:UIControlStateSelected];
        _likeMeBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(24)];
        [_likeMeBt addTarget:self action:@selector(showLikeMeView) forControlEvents:UIControlEventTouchUpInside];
        [self.navBgView addSubview:_likeMeBt];
        
        [self showMessageView];
    }
    
    return _navBgView;
}

- (UIScrollView *)contentScrollView {
    if (!_contentScrollView) {
        _contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, StatusBarHeight + ZoomSize(50), SCREEN_Width, SCREEN_Height - StatusBarHeight - ZoomSize(50) - TabBarHeight)];
        _contentScrollView.contentSize = CGSizeMake(SCREEN_Width * 2, SCREEN_Height - NavigationHeight);
        _contentScrollView.bounces = NO ;
        _contentScrollView.alwaysBounceHorizontal = NO ;
        _contentScrollView.alwaysBounceVertical = NO ;
        
        _contentScrollView.showsHorizontalScrollIndicator = NO ;
        _contentScrollView.showsVerticalScrollIndicator = NO ;
        _contentScrollView.pagingEnabled = NO ;
        _contentScrollView.scrollEnabled = NO;
        
        MessageListView *extractedExpr = [MessageListView alloc];
        _messageListView = [extractedExpr initWithFrame:CGRectMake(0, 0, SCREEN_Width, SCREEN_Height - StatusBarHeight - ZoomSize(50) - TabBarHeight)];
        _messageListView.getNav = self.navigationController;
        [self.contentScrollView addSubview:self.messageListView];
        
        _likeMeListView = [[LikeMeListView alloc] initWithFrame:CGRectMake(SCREEN_Width, 0, SCREEN_Width, SCREEN_Height - StatusBarHeight - ZoomSize(50) - TabBarHeight)];
        _likeMeListView.getNav = self.navigationController;
        [self.contentScrollView addSubview:self.likeMeListView];
    }
    return _contentScrollView;
}

@end

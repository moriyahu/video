//
//  OfficialChatListViewController.m
//  Video
//
//  Created by steve on 2020/5/29.
//  Copyright © 2020 steve. All rights reserved.
//

#import "MJExtension.h"
#import "MJRefresh.h"
#import "OfficialChatListViewController.h"
#import "FeedBackViewController.h"

#import "UITableView+PlaceHolderView.h"
#import "ListPlaceHoderView.h"

#import "V2TIMManager+Message.h"
#import "V2TIMManager+Conversation.h"

#import "OfficialMessageTableViewCell.h"

static NSString * const reuseIdentifier = @"OfficialMessageTableViewCell";

@interface OfficialChatListViewController ()<UITableViewDelegate, UITableViewDataSource> {
    int pageIndex;
}

@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * array;

@property (nonatomic, strong) UIView *navView;
@property (nonatomic, strong) UIScrollView *contentScrollView;
@property (nonatomic, strong) UIButton *feedbackBt;

@end

@implementation OfficialChatListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
    [self setData];
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.tabBarController.tabBar.hidden = YES;
}

- (void)setupUI {
    self.view.backgroundColor = BGDarkBlueColor;
    [self.view addSubview:self.navView];
    [self.view addSubview:self.contentScrollView];
}

- (void)setData {
    [self getOfficialMessage];
}

- (void) getOfficialMessage {
    
    self.array = [NSMutableArray array];
    
    [[V2TIMManager sharedInstance] getC2CHistoryMessageList:@"Vido" count:10 lastMsg:nil succ:^(NSArray<V2TIMMessage *> *msgs) {
        
        if (msgs.count > 0) {
            [self.array setArray:msgs];
            [self.tableView reloadData];
        }
        [self.tableView.mj_header endRefreshing];
        
    } fail:^(int code, NSString *desc) {
    }];
    
}

#pragma mark - Function
- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)feedbackAction {
    FeedBackViewController *vc = [[FeedBackViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    V2TIMMessage *getMessage = self.array[indexPath.row];
    CGFloat textHeight = [DataTool getTextHeightWithFont:[UIFont systemFontOfSize:16] getTextString: getMessage.textElem.text getFrameWidth:SCREEN_Width - ZoomSize(52)] + ZoomSize(80);
    
    return textHeight;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OfficialMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];

    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if(cell == nil) {
        cell = [[OfficialMessageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }
    
    V2TIMMessage *getMessage = self.array[indexPath.row];
    cell.message = getMessage;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSLog(@"didSelectRowAtIndexPath");
}

#pragma Lazy
- (UIView *)navView {
    if (!_navView) {
        _navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];;
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(10), StatusBarHeight + ZoomSize(5), ZoomSize(40), ZoomSize(30))];
        [_navView addSubview:backBt];
        [backBt setImage:GetImageByName(@"common_back") forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        UIImageView *logoIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(60), StatusBarHeight, ZoomSize(40), ZoomSize(40))];
        logoIV.image = GetImageByName(@"message_system_logo");
        [_navView addSubview:logoIV];
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(110), StatusBarHeight + ZoomSize(5), ZoomSize(160), ZoomSize(30))];
        titleLb.text = @"Official";
        titleLb.textAlignment = NSTextAlignmentLeft;
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightSemibold];
        [_navView addSubview:titleLb];
        
    }
    return _navView;
}

- (UIScrollView *)contentScrollView {
    if (!_contentScrollView) {
        _contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, NavigationHeight + ZoomSize(17), SCREEN_Width, SCREEN_Height - NavigationHeight - ZoomSize(17))];
        _contentScrollView.bounces = YES ;
        _contentScrollView.showsHorizontalScrollIndicator = NO ;
        _contentScrollView.showsVerticalScrollIndicator = NO ;
        
        UIView *contentView = [[UIView alloc] initWithFrame:_contentScrollView.bounds];
        [_contentScrollView addSubview:contentView];
        contentView.backgroundColor = UIColor.whiteColor;
        
        UIBezierPath *contentViewMaskPath = [UIBezierPath bezierPathWithRoundedRect:contentView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(ZoomSize(36.5),ZoomSize(36.5))]; //设置部分圆角
        CAShapeLayer *contentViewMaskLayer = [[CAShapeLayer alloc] init];
        contentViewMaskLayer.frame = contentView.bounds;
        contentViewMaskLayer.path = contentViewMaskPath.CGPath;
        contentView.layer.mask = contentViewMaskLayer;
        
        [contentView addSubview:self.tableView];
        [contentView addSubview:self.feedbackBt];
    }
    return _contentScrollView;
}

- (UITableView *)tableView {
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:self.contentScrollView.bounds style:UITableViewStylePlain];
        [_tableView registerNib:[UINib nibWithNibName:reuseIdentifier bundle:nil] forCellReuseIdentifier:reuseIdentifier];
        _tableView.separatorStyle = UITableViewCellEditingStyleNone;
        _tableView.backgroundColor = WhiteColor;
        _tableView.rowHeight = ZoomSize(140);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        _tableView.mj_header = [MJRefreshStateHeader headerWithRefreshingBlock:^{
            [self setData];
        }];
        
        _tableView.enablePlaceHolderView = YES;
        _tableView.yh_PlaceHolderView = [[ListPlaceHoderView alloc] initWithFrame:_tableView.frame tipImgName:@"common_empty" tipsInfo:@""];
    }
    return _tableView;
}

- (UIButton *)feedbackBt {
    if (!_feedbackBt) {
        _feedbackBt = [DataTool createCommomBtWithFrame:CGRectMake(ZoomSize(38), SCREEN_Height - NavigationHeight -  BottomSafeAreaHeight - ZoomSize(78), ZoomSize(300), ZoomSize(48)) andTitle:@"Feedback"];
        [_feedbackBt addTarget:self action:@selector(feedbackAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _feedbackBt;
}

@end

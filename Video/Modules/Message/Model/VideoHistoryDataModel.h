//
//  VideoHistoryDataModel.h
//  Video
//
//  Created by steve on 2020/6/11.
//  Copyright © 2020 steve. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface VideoHistoryDataModel : NSObject

@property (nonatomic, strong) NSString *callType;
@property (nonatomic, strong) NSString *lastestCallTime;
@property (nonatomic, strong) NSString *state;
@property (nonatomic, strong) NSString *times;

@property (nonatomic, strong) UserProfileDataModel *userInfo;

@end

NS_ASSUME_NONNULL_END

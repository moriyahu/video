//
//  HomeViewController.m
//  Video
//
//  Created by steve on 2020/3/23.
//  Copyright © 2020 steve. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController () {
    UILabel *numLb;
    UILabel *tipLb;
    UIButton *functionBt;
}

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self setData];
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = YES;
    self.navigationController.tabBarController.tabBar.hidden = NO;
}

- (void) setupUI {
    self.view.backgroundColor = BGDarkBlueColor;
    
    UIImageView *logoBGIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(25), StatusBarHeight + ZoomSize(45), SCREEN_Width - ZoomSize(50), (SCREEN_Width - ZoomSize(50))*0.98 )];
    [self.view addSubview:logoBGIV];
    logoBGIV.image = GetImageByName(@"home_bg");
    logoBGIV.contentMode = UIViewContentModeScaleAspectFit;
    
    numLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(25), CGRectGetMaxY(logoBGIV.frame) + ZoomSize(20), SCREEN_Width - ZoomSize(50), ZoomSize(30) )];
    [self.view addSubview:numLb];
    numLb.text = @"24234";
    numLb.textAlignment = NSTextAlignmentCenter;
    numLb.textColor = WhiteColor;
    numLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightSemibold];
    
    
    tipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(25), CGRectGetMaxY(numLb.frame) + ZoomSize(10), SCREEN_Width - ZoomSize(50), ZoomSize(20) )];
    [self.view addSubview:tipLb];
    tipLb.text = @"Real-time interaction";
    tipLb.textAlignment = NSTextAlignmentCenter;
    tipLb.textColor = RGBA(255, 255, 255, 0.6);
    tipLb.font = [UIFont systemFontOfSize:ZoomSize(16)];
    
    functionBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(38),  CGRectGetMaxY(tipLb.frame) + ZoomSize(40), SCREEN_Width - ZoomSize(76), ZoomSize(48))];
    [self.view addSubview:functionBt];
    [functionBt addTarget:self action:@selector(functionAction) forControlEvents:UIControlEventTouchUpInside];
    functionBt.layer.cornerRadius = ZoomSize(24);
    functionBt.layer.masksToBounds = YES;
    
    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.frame = functionBt.bounds;
    gl.startPoint = CGPointMake(0.56, -0.77);
    gl.endPoint = CGPointMake(0.46, 1.66);
    gl.colors = @[(__bridge id)[UIColor colorWithRed:238/255.0 green:48/255.0 blue:202/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:137/255.0 green:36/255.0 blue:244/255.0 alpha:1.0].CGColor];
    gl.locations = @[@(0), @(1.0f)];
    [functionBt.layer addSublayer:gl];
    
    UILabel *functionBtTitleLb = [[UILabel alloc] initWithFrame:functionBt.bounds];
    [functionBt addSubview:functionBtTitleLb];
    functionBtTitleLb.text = @"Let's start!";
    functionBtTitleLb.textAlignment = NSTextAlignmentCenter;
    functionBtTitleLb.textColor = WhiteColor;
    functionBtTitleLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightSemibold];
    
    
}

- (void) setData {
}

#pragma function
- (void) functionAction {
    NSLog(@"functionAction");
}

//- (void) startAction {
//    startBt.selected = !startBt.selected;
//    if (!startBt.selected) {
//        infoLb.text = @"322 users are chatting right now...";
//        [startBt setTitle:@"Start My Match" forState:UIControlStateNormal];
//    } else {
//        infoLb.text = @"Searching...";
//        [startBt setTitle:@"Cancel" forState:UIControlStateNormal];
//    }
//
//}

@end

//
//  SearchViewController.h
//  Video
//
//  Created by steve on 2020/5/12.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchViewController : UIViewController

@property (nonatomic, strong) NSMutableArray * array; //数据列表

@end

NS_ASSUME_NONNULL_END

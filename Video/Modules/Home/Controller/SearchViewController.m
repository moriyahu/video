//
//  SearchViewController.m
//  Video
//
//  Created by steve on 2020/5/12.
//  Copyright © 2020 steve. All rights reserved.
//

#import "MJExtension.h"
#import "SearchViewController.h"
#import "FilterViewController.h"

#import "SearchFunctionView.h"
#import "SwipeView.h"
#import "UserInfoCardView.h"
#import "UserTipUploadVideoView.h"
#import "UserBuyVipView.h"
#import "UserBuyVipPopView.h"

@interface SearchViewController () <SwipeDelegate, SwipeViewDataSource, UIPopoverPresentationControllerDelegate> {
    FilterViewController *filterVC; //选择弹出视图
    BOOL userHaveVideo;
//    int pageNum;
}
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UserProfileDataModel *currentUserModel;//当前用户数据

@property (nonatomic, strong) SwipeView *swipeView; //滑动视图
@property (nonatomic, strong) SearchFunctionView *functionView; //功能视图

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self setData];
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.tabBarController.tabBar.hidden = YES;
}

- (void)setupUI {
    self.view.backgroundColor = TextGrayColor;
    [self.view addSubview:self.contentView];
}

- (void)setData {
    if (self.array.count > 0) {
        [self->_swipeView resetCurrentCardNumber];
    }
}

- (void)getSpark {
    
    NSDictionary *paramsDic = @{@"pageSize": [NSNumber numberWithInt:1], @"pageNum": [NSNumber numberWithInt:1]};
    if (userHaveVideo) {
        paramsDic = @{@"pageSize": [NSNumber numberWithInt:1], @"pageNum": [NSNumber numberWithInt:1], @"isVideo":@"1"};
    }
    
    [[NetTool shareInstance] startRequest:API_Rel_Spark method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
        
        NSDictionary *responseDic = obj;
        int resultCode = [responseDic[@"code"] intValue];
        NSDictionary *resultDic = [[NSDictionary alloc] init];
        resultDic = responseDic[@"data"];
        
        if (resultCode == 8200) {
            NSArray *listArray = resultDic[@"list"];
            if (listArray.count > 0) {
                self.array = [UserProfileDataModel mj_objectArrayWithKeyValuesArray:listArray];
                [self->_swipeView resetCurrentCardNumber];
            } else {
                [self.navigationController popViewControllerAnimated:YES];
            }
            
        }  else if (resultCode == 8703) {
            
            if (![[UserInfo shareInstant].user.vipState isEqualToString:@"VIP"]) {
                
                if ([UserInfo shareInstant].user.enableAddTimes) {
                    UserTipUploadVideoView *view = [[UserTipUploadVideoView alloc] initWithFrame:self.view.frame];
                    view.dif = [resultDic[@"nextSparkTime"] intValue] ;
                    [view show];
                }
                
            } else {
                [self.navigationController popViewControllerAnimated:YES];
            }
            
        }
        
    }];
}

#pragma mark - Function

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)filterAction:(UIButton *) sender {
    filterVC = [[FilterViewController alloc] init];
    filterVC.modalPresentationStyle = UIModalPresentationPopover; //设置显示方式
    filterVC.preferredContentSize = CGSizeMake(ZoomSize(343), ZoomSize(136)); //设置弹出控制器的尺寸
    filterVC.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp; //设置箭头方向
    filterVC.popoverPresentationController.delegate = self;
    
    filterVC.popoverPresentationController.sourceRect = sender.bounds; // 设置 sourceRect和sourceView属性
    filterVC.popoverPresentationController.sourceView = sender;
    [self presentViewController:filterVC animated:YES completion:nil];
    
    __weak __typeof (self) weakSelf = self;
    filterVC.allActionBlock = ^{
         __strong __typeof(self) strongSelf = weakSelf;
        [strongSelf->filterVC dismissViewControllerAnimated:YES completion:nil];
        strongSelf->userHaveVideo = NO;
        [strongSelf getSpark];
    };
    filterVC.haveactionBlock = ^{
        
        __strong __typeof(self) strongSelf = weakSelf;
        [strongSelf->filterVC dismissViewControllerAnimated:YES completion:nil];
        if ([[UserInfo shareInstant].user.vipState isEqualToString:@"VIP"]) {
            strongSelf->userHaveVideo = YES;
            [strongSelf getSpark];
        } else {
            [DataTool showGetVipViewWithIndex:0];
        }
    };
}

- (void)passAction {
    
    NSDictionary *paramsDic = @{@"beLikedUserId": _currentUserModel.userId, @"userType": _currentUserModel.type};
    [[NetTool shareInstance] startRequest:API_Rel_AddPass method:PostMethod params:paramsDic needShowHUD:NO callback:^(id  _Nullable obj) {
        [self getSpark];
    }];
}

- (void)likeAction {
    
    NSDictionary *paramsDic = @{@"beLikedUserId": _currentUserModel.userId, @"userType": _currentUserModel.type , @"isNotify":@"1"};
    [[NetTool shareInstance] startRequest:API_Rel_AddLike method:PostMethod params:paramsDic needShowHUD:NO callback:^(id  _Nullable obj) {
        
        NSString *addResultString = obj;
        if ([DataTool isEmptyString:addResultString]) {
            return;
        }
        
        if ([addResultString isEqualToString:@"LIKED"]) {
            [[UserInfo shareInstant] showUserLikeOtherPopView:self->_currentUserModel callback:^(id  _Nullable obj) {
                [self getSpark];
            }];
        }
        
    }];
    
}

- (void)recommandReact:(NSUInteger)index inDirection:(SwipeDirection)direction {
    if (direction == SwipeDirectionLeft) {
        [self passAction];
    } else {
        [self likeAction];
    }
}

#pragma mark - swipeView delegate
- (NSUInteger)swipeViewNumberOfCards:(SwipeView *)swipeView {
    return _array.count;
}
- (UIView *)swipeView:(SwipeView *)swipeView cardAtIndex:(NSUInteger)index {
    _currentUserModel = _array[index];
    _functionView.dataModel = _currentUserModel;
    UserInfoCardView *cardView = [[UserInfoCardView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, SCREEN_Height) userProfile:_currentUserModel];
    cardView.blockUserBlock = ^{
        [self performSelector:@selector(getSpark) withObject:nil afterDelay:1];
//        [self getSpark];
    };
    return cardView;
}
- (OverlayView *)swipeView:(SwipeView *)swipeView  cardOverlayAtIndex:(NSUInteger)index {
    return  nil;
}
- (void)swipeView:(SwipeView *)swipeView didSwipeCardAtIndex:(NSUInteger)index inDirection:(SwipeDirection)direction {
    [self recommandReact:index inDirection:direction];
}
- (void)swipeViewDidRunOutOfCards:(SwipeView *)swipeView {
}
- (void)swipeView:(SwipeView *)swipeView didSelectCardAtIndex:(NSUInteger)index {
}
- (BOOL)swipeViewShouldApplyAppearAnimation:(SwipeView *)swipeView {
    return YES;
}
- (BOOL)swipeViewShouldMoveBackgroundCard:(SwipeView *)swipeView {
    return YES;
}
- (BOOL)swipeViewShouldTransparentizeNextCard:(SwipeView *)swipeView {
    return YES;
}
- (POPPropertyAnimation *)swipeViewBackgroundCardAnimation:(SwipeView *)swipeView {
    return nil;
}
#pragma mark - 实现该代理方法,返回UIModalPresentationNone值,可以在iPhone设备实现popover效果
-(UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller{
    return UIModalPresentationNone;//不适配(不区分ipad或iPhone)
}

#pragma mark - lazy
- (UIView *)contentView {
    if (!_contentView) {
        _contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, SCREEN_Height)];
        [_contentView addSubview:self.swipeView];
        [_contentView addSubview:self.functionView];
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(10), StatusBarHeight + ZoomSize(8), ZoomSize(40), ZoomSize(30))];
        [_contentView addSubview:backBt];
        [backBt setImage:GetImageByName(@"match_back") forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *filterBt = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(50), StatusBarHeight + ZoomSize(8), ZoomSize(40), ZoomSize(30))];
        [_contentView addSubview:filterBt];
        [filterBt setImage:GetImageByName(@"common_filter") forState:UIControlStateNormal];
        [filterBt addTarget:self action:@selector(filterAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _contentView;
}

- (SwipeView *)swipeView {
    if (!_swipeView) {
        _swipeView = [[SwipeView alloc] initWithFrame:_contentView.bounds];
        _swipeView.dataSource = self;
        _swipeView.delegate = self;
    }
    return _swipeView;
}

- (SearchFunctionView *)functionView {
    if (!_functionView) {
         _functionView = [[SearchFunctionView alloc] initWithFrame:CGRectMake(0, SCREEN_Height - BottomSafeAreaHeight - ZoomSize(140), SCREEN_Width, ZoomSize(130))];
        __weak __typeof (self) weakSelf = self;
        
        
        _functionView.likeBlock = ^{
            __strong __typeof(self) strongSelf = weakSelf;
            [strongSelf likeAction];
        };
    }
    return _functionView;
}

@end

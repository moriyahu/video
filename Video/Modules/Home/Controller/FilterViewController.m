//
//  FilterViewController.m
//  Video
//
//  Created by steve on 2020/5/21.
//  Copyright © 2020 steve. All rights reserved.
//

#import "FilterViewController.h"

@interface FilterViewController ()

@end

@implementation FilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = WhiteColor;
    
    UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(114), ZoomSize(30), ZoomSize(118), ZoomSize(22))];
    [self.view addSubview:titleLb];
    titleLb.text = @"Private Videos";
    titleLb.textAlignment = NSTextAlignmentCenter;
    titleLb.textColor = BGDarkBlueColor;
    titleLb.font = [UIFont systemFontOfSize:ZoomSize(18)];
    
    UIImageView *vipLogoIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(230), ZoomSize(52), ZoomSize(28), ZoomSize(28))];
    vipLogoIV.image = GetImageByName(@"common_vip");
    [self.view addSubview:vipLogoIV];
    
    UIButton *allBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(50), ZoomSize(82), ZoomSize(116), ZoomSize(32))];
    [allBt addTarget:self action:@selector(allAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:allBt];
    [allBt setTitle:@"All" forState:UIControlStateNormal];
    [allBt setTitleColor:WhiteColor forState:UIControlStateNormal];
    allBt.layer.cornerRadius = ZoomSize(16);
    allBt.backgroundColor = RGB(201, 44, 217);
    
    UIButton *haveBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(185), ZoomSize(82), ZoomSize(116), ZoomSize(32))];
    [haveBt addTarget:self action:@selector(haveAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:haveBt];
    [haveBt setTitle:@"Have videos" forState:UIControlStateNormal];
    [haveBt setTitleColor:TextGrayColor forState:UIControlStateNormal];
    haveBt.backgroundColor = WhiteColor;
    haveBt.layer.cornerRadius = ZoomSize(16);
    haveBt.layer.masksToBounds = YES;
    haveBt.layer.borderColor = TextGrayColor.CGColor;
    haveBt.layer.borderWidth = ZoomSize(1);
    
}

- (void)allAction {
    if (_allActionBlock) {
        _allActionBlock();
        
//       
    }
}

- (void)haveAction {
    if (_haveactionBlock) {
        _haveactionBlock();
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

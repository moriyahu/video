//
//  FilterViewController.h
//  Video
//
//  Created by steve on 2020/5/21.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FilterViewController : UIViewController

@property (nonatomic, copy) ClickBlock allActionBlock;
@property (nonatomic, copy) ClickBlock haveactionBlock;


@end

NS_ASSUME_NONNULL_END

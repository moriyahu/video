//
//  HomeViewController.m
//  Video
//
//  Created by steve on 2020/3/23.
//  Copyright © 2020 steve. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "HomeViewController.h"
#import "MJExtension.h"

#import "UserTipUploadVideoView.h"
#import "UserBuyVipView.h"
#import "SearchViewController.h"

#import "LBXPermission.h"
#import "LBXPermissionSetting.h"

#import "BubbleAnimationView.h"

@interface HomeViewController () {
    UILabel *numLb;
    UILabel *tipLb;
    UIButton *functionBt;
}

@property (nonatomic, strong) BubbleAnimationView *bubbleAnimationView;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self setData];
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = YES;
    self.navigationController.tabBarController.tabBar.hidden = NO;
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:GetPermission]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:GetPermission];
        
        numLb.hidden = YES;
        tipLb.hidden = YES;
        [DataTool showGetPermissionViewDismissBlock:^(id  _Nullable obj) {
            self->numLb.hidden = NO;
            self->tipLb.hidden = NO;
        }];
        
    }
    
    [self addBuubleAnimationView];
    
}

- (void)setupUI {
    
    self.view.backgroundColor = BGDarkBlueColor;
    
    UIImageView *logoBGIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(25), StatusBarHeight + ZoomSize(45), SCREEN_Width - ZoomSize(50), (SCREEN_Width - ZoomSize(50))*0.98 )];
    [self.view addSubview:logoBGIV];
    logoBGIV.image = GetImageByName(@"home_bg");
    logoBGIV.contentMode = UIViewContentModeScaleAspectFit;
    
    numLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(25), CGRectGetMaxY(logoBGIV.frame) + ZoomSize(20), SCREEN_Width - ZoomSize(50), ZoomSize(30) )];
    [self.view addSubview:numLb];
    numLb.textAlignment = NSTextAlignmentCenter;
    numLb.textColor = WhiteColor;
    numLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightSemibold];
    
    
    tipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(25), CGRectGetMaxY(numLb.frame) + ZoomSize(10), SCREEN_Width - ZoomSize(50), ZoomSize(20) )];
    [self.view addSubview:tipLb];
    tipLb.textAlignment = NSTextAlignmentCenter;
    tipLb.textColor = RGBA(255, 255, 255, 0.6);
    tipLb.font = [UIFont systemFontOfSize:ZoomSize(16)];
    
    functionBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(38),  StatusBarHeight + ZoomSize(473), SCREEN_Width - ZoomSize(76), ZoomSize(48))];
    [self.view addSubview:functionBt];
    [functionBt addTarget:self action:@selector(functionAction) forControlEvents:UIControlEventTouchUpInside];
    functionBt.layer.borderColor = RGBA(255, 255, 255, 0.6).CGColor;
    functionBt.layer.cornerRadius = ZoomSize(24);
    functionBt.layer.masksToBounds = YES;
    functionBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightSemibold];
    
}

- (void)setData {
    [self setBtStateNormal];
}

#pragma function

- (void)addBuubleAnimationView {
    [self.bubbleAnimationView removeFromSuperview];
    self.bubbleAnimationView = nil;
    
    self.bubbleAnimationView = [[BubbleAnimationView alloc] initWithFrame:CGRectMake(0, StatusBarHeight, SCREEN_Width, ZoomSize(360))];
    [self.view addSubview:self.bubbleAnimationView];
    [self.bubbleAnimationView animateBuubleViews];
}

- (void) setBtStateNormal {
    
    int x = arc4random() % 100 + 220;
    
    numLb.hidden = NO;
    numLb.text = [NSString stringWithFormat:@"%d",x];
    tipLb.text = @"Real-time interaction";
    [functionBt setTitle:@"Start Match" forState:UIControlStateNormal];
    [functionBt setTitleColor:WhiteColor forState:UIControlStateNormal];
    
    [functionBt setBackgroundImage:GetImageByName(@"btBG_enable") forState:UIControlStateNormal];
    functionBt.layer.borderWidth = 0;
    self->functionBt.enabled = YES;
    
}
- (void) setBtStateSearching {
    
    numLb.hidden = YES;
    tipLb.text = @"Searching...";
    [functionBt setTitle:@"Cancel" forState:UIControlStateNormal];
    [functionBt setTitleColor:RGBA(255, 255, 255, 0.6) forState:UIControlStateNormal];
    [functionBt setBackgroundImage:nil forState:UIControlStateNormal];
    functionBt.layer.borderWidth = ZoomSize(1);
    
}

- (void)functionAction {
    
    //        是否有摄像头权限
    AVAuthorizationStatus statusVideo = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (statusVideo == AVAuthorizationStatusDenied) {
        
        [LBXPermissionSetting showAlertToDislayPrivacySettingWithTitle:@"Tip" msg:@"No permissions,  go to setting？" cancel:@"cancel" setting:@"setting"];
        return;
    }
    
    //是否有麦克风权限
    AVAuthorizationStatus statusAudio = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio];
    if (statusAudio == AVAuthorizationStatusDenied) {
        [LBXPermissionSetting showAlertToDislayPrivacySettingWithTitle:@"Tip" msg:@"No permissions,  go to setting？" cancel:@"cancel" setting:@"setting"];
        return;
    }
    
    functionBt.enabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self->functionBt.enabled = YES;
    });
    
    [self setBtStateSearching];
    NSDictionary *paramsDic = @{@"pageSize": [NSNumber numberWithInt:1], @"pageNum": [NSNumber numberWithInt:1]};
    [[NetTool shareInstance] startRequest:API_Rel_Spark method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
        [self setBtStateNormal];
        
        NSDictionary *responseDic = obj;
        int resultCode = [responseDic[@"code"] intValue];
        NSDictionary *resultDic = [[NSDictionary alloc] init];
        resultDic = responseDic[@"data"];
        
        if (resultCode == 8200) {
            NSArray *listArray = resultDic[@"list"];
            if (listArray.count > 0) {
                SearchViewController *vc = [[SearchViewController alloc] init];
                vc.array = [UserProfileDataModel mj_objectArrayWithKeyValuesArray:listArray];
                [self.navigationController pushViewController:vc animated:YES];
            }
            
        }  else if (resultCode == 8703) {
            
            if (![[UserInfo shareInstant].user.vipState isEqualToString:@"VIP"]) {
                if ([UserInfo shareInstant].user.enableAddTimes) {
                    UserTipUploadVideoView *view = [[UserTipUploadVideoView alloc] initWithFrame:self.view.frame];
                    view.dif = [resultDic[@"nextSparkTime"] intValue] ;
                    [view show];
                }
            }
            
        }
        
        
    }];
}


@end

//
//  UserBuyVipView.h
//  Video
//
//  Created by steve on 2020/4/30.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserBuyVipView : UIView

- (instancetype) initWithFrame:(CGRect)frame;

@property (nonatomic, assign) int dif; //nextSparkTime

- (void)show;

- (void)dismiss;

@end

NS_ASSUME_NONNULL_END

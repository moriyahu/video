//
//  UserInfoCardView.h
//  Video
//
//  Created by steve on 2020/5/12.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserInfoCardView : UIView

@property (nonatomic, copy) ClickBlock blockUserBlock;

- (instancetype) initWithFrame:(CGRect)frame userProfile:(UserProfileDataModel *)profileModel;

@end

NS_ASSUME_NONNULL_END

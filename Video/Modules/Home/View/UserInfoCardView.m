//
//  UserInfoCardView.m
//  Video
//
//  Created by steve on 2020/5/12.
//  Copyright © 2020 steve. All rights reserved.
//

#import "UserInfoCardView.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface UserInfoCardView()

@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UserProfileDataModel *profileModel;
@property (nonatomic, strong) UIView *infoView;

@end

@implementation UserInfoCardView

- (instancetype) initWithFrame:(CGRect)frame userProfile:(UserProfileDataModel *)profileModel {
    self = [super initWithFrame:frame];
    if (self) {
        _profileModel = profileModel;
        [self initSubviews:frame];
    }
    return self;
}

- (void)initSubviews:(CGRect) frame {
    self.frame = frame;
    
    [self addSubview:self.contentView];
    [self addSubview:self.infoView];
}

#pragma mark - lazy

- (UIView *)contentView {
    if (!_contentView) {
        _contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, SCREEN_Height)];
        
        UIImageView *photoIV = [[UIImageView alloc] initWithFrame:_contentView.bounds];
        photoIV.contentMode = UIViewContentModeScaleAspectFill;
        photoIV.layer.cornerRadius = 16;
        photoIV.layer.masksToBounds = YES;
        [_contentView addSubview:photoIV];
        
        NSMutableArray *photoArray = [NSMutableArray array];
        
        UserResourceDataModel *headPortraitDataModel = [[UserResourceDataModel alloc] init]; //添加头像
        
        if (![DataTool isEmptyString:_profileModel.o_avatar]) {
            headPortraitDataModel.name = _profileModel.o_avatar;
        }
        if (![DataTool isEmptyString:_profileModel.headThumbnailUrl]) {
            headPortraitDataModel.name = _profileModel.headThumbnailUrl;
        }
        if (![DataTool isEmptyString:_profileModel.headPortraitUrl]) {
            headPortraitDataModel.name = _profileModel.headPortraitUrl;
        }
        
        if ([DataTool getStateCodeFromString:_profileModel.avatar_status] == 0) { //未审核，不能修改
            headPortraitDataModel.state = 0;
        } else {
            headPortraitDataModel.state = 1;
        }
        [photoArray addObject:headPortraitDataModel];
        
//        if (![DataTool isEmptyString:_profileModel.headPortraitUrl]) { //添加头像
//
//            headPortraitDataModel.name = _profileModel.headPortraitUrl;
//            headPortraitDataModel.state = [DataTool getStateCodeFromString:_profileModel.avatar_status];
//            [photoArray addObject:headPortraitDataModel];
//        }
        
        if (_profileModel.photos.count > 0) { //添加相册
            for (UserResourceDataModel *dataModel in _profileModel.photos) {
                [photoArray addObject:dataModel];
            }
        }
        
        if (photoArray.count > 0) {
            UserResourceDataModel *getResourceModel = photoArray.firstObject;
            [photoIV sd_setImageWithURL:[NSURL URLWithString:getResourceModel.name] placeholderImage:GetImageByName(@"message_user_nopho")];
        }
        
    }
    
    return _contentView;
}

- (void)reportAction {
    [DataTool reportBlockUserName:_profileModel.nickName andBlockUserId:_profileModel.userId reportCallback:^(id  _Nullable obj) {
        NSLog(@"reportSuccess");
    } blockCallback:^(id  _Nullable obj) {
//        [self getSpark];
        if (self.blockUserBlock) {
            self->_blockUserBlock();
        }
    }];
}


- (UIView *)infoView {
    if (!_infoView) {

        _infoView = [[UIView alloc] initWithFrame:CGRectMake(0, StatusBarHeight, SCREEN_Width, SCREEN_Height)];
        
        UILabel *nameLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(51), ZoomSize(8), ZoomSize(160), ZoomSize(30))];
        [_infoView addSubview:nameLb];
        nameLb.text =  [NSString stringWithFormat:@"%@,%ld",  _profileModel.nickName, [DataTool getUserAgeFromString:_profileModel.birthday]];
        nameLb.textAlignment = NSTextAlignmentLeft;
        nameLb.textColor = WhiteColor;
        nameLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightSemibold];

        UIImageView *onlineLogoIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(142), ZoomSize(10), ZoomSize(28), ZoomSize(28))];
        onlineLogoIV.image = GetImageByName(@"common_online");
        [_infoView addSubview:onlineLogoIV];
        onlineLogoIV.hidden = YES;
        if ([_profileModel.onlineState isEqualToString:@"ONLINE"]) {
            onlineLogoIV.hidden = NO;
        }

        UIImageView *verifyLogoIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(170), ZoomSize(10), ZoomSize(28), ZoomSize(28))];
        verifyLogoIV.image = GetImageByName(@"common_validation");
        [_infoView addSubview:verifyLogoIV];
        verifyLogoIV.hidden = YES;
        if (_profileModel.videos.count > 0) {
            verifyLogoIV.hidden = NO;
        }

//        UIImageView *vipLogoIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(200), ZoomSize(10), ZoomSize(28), ZoomSize(28))];
//        vipLogoIV.image = GetImageByName(@"common_vip");
//        [_infoView addSubview:vipLogoIV];
//        vipLogoIV.hidden = YES;
//        if ([_profileModel.vipState isEqualToString:@"VIP"]) {
//            vipLogoIV.hidden = NO;
//        }
        
        UIButton *reportBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(280),  ZoomSize(8) , ZoomSize(40), ZoomSize(30))];
        [_infoView addSubview:reportBt];
        [reportBt setImage:GetImageByName(@"common_report") forState:UIControlStateNormal];
        [reportBt addTarget:self action:@selector(reportAction) forControlEvents:UIControlEventTouchUpInside];
        

        UIImageView *nationIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(51), ZoomSize(46), ZoomSize(20), ZoomSize(16))];
        nationIV.contentMode = UIViewContentModeScaleAspectFit;
        nationIV.image = [DataTool getImageForCountryName:_profileModel.country];
        [_infoView addSubview:nationIV];

        UILabel *nationLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(70), ZoomSize(46), ZoomSize(150), ZoomSize(16))];
        [_infoView addSubview:nationLb];
        nationLb.text = _profileModel.country;
        nationLb.textColor = WhiteColor;
        nationLb.font = [UIFont systemFontOfSize:ZoomSize(14)];

        UILabel *aboutLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(50), ZoomSize(70), SCREEN_Width - ZoomSize(50), ZoomSize(50))];
        [_infoView addSubview:aboutLb];
        aboutLb.text = _profileModel.aboutMe;
        aboutLb.textAlignment = NSTextAlignmentLeft;
        aboutLb.textColor = WhiteColor;
        aboutLb.font = [UIFont systemFontOfSize:ZoomSize(16)];
        aboutLb.numberOfLines = 0;

    }

    return _infoView;
}

@end

//
//  SearchFunctionView.h
//  Video
//
//  Created by steve on 2020/5/18.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchFunctionView : UIView

- (instancetype) initWithFrame:(CGRect)frame;

@property (nonatomic, strong) UserProfileDataModel *dataModel;

@property (nonatomic, copy) ClickBlock likeBlock;

@end

NS_ASSUME_NONNULL_END

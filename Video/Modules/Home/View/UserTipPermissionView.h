//
//  UserTipPermissionView.h
//  Video
//
//  Created by steve on 2020/6/10.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserTipPermissionView : UIView

- (instancetype) initWithFrame:(CGRect)frame;

@property (nonatomic, copy) ClickBlock dismissBLock;

- (void)show;

- (void)dismiss;

@end

NS_ASSUME_NONNULL_END

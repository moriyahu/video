//
//  SearchFunctionView.m
//  Video
//
//  Created by steve on 2020/5/18.
//  Copyright © 2020 steve. All rights reserved.
//

#import "SearchFunctionView.h"
#import "VideoPlayerView.h"
#import <GPUImage/GPUImage.h>

@interface SearchFunctionView() {
    UIButton *likeBt;
    VideoPlayerView *player;
    
    UIView *filterMaskView; //视频视图
    GPUImageVideoCamera *videoCamera; //摄像头
    GPUImageOutput<GPUImageInput> *filter; //输出
    GPUImageView *filterView; //输出视图
    GPUImageMovieWriter *movieWriter; //写入
    
}

@property (nonatomic, strong) UIView *contentView;

@end

@implementation SearchFunctionView

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews:frame];
    }
    return self;
}

- (void)initSubviews:(CGRect) frame {
    self.frame = frame;
    [self addSubview:self.contentView];
}

- (void)setDataModel:(UserProfileDataModel *)dataModel {
    
    [likeBt setImage:GetImageByName(@"match_like_normal") forState:UIControlStateNormal];
    
    if (dataModel.videos.count > 0) {
        UserResourceDataModel *model = dataModel.videos.firstObject;
        
        [player updatePlayerWith:[NSURL URLWithString:model.name]];
    } else {
        [player updatePlayerWith:[NSURL URLWithString:@""]];
    }
    
}

#pragma function

- (void)drawLineOfDashByCAShapeLayer:(UIView *)lineView lineLength:(int)lineLength lineSpacing:(int)lineSpacing lineColor:(UIColor *)lineColor lineDirection:(BOOL)isHorizonal {

    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    [shapeLayer setBounds:lineView.bounds];
    if (isHorizonal) {
        [shapeLayer setPosition:CGPointMake(CGRectGetWidth(lineView.frame) / 2, CGRectGetHeight(lineView.frame))];
    } else{
        [shapeLayer setPosition:CGPointMake(CGRectGetWidth(lineView.frame) / 2, CGRectGetHeight(lineView.frame)/2)];
    }
    
    [shapeLayer setFillColor:[UIColor clearColor].CGColor]; //  设置虚线颜色为blackColor
    [shapeLayer setStrokeColor:lineColor.CGColor];
    if (isHorizonal) { //  设置虚线宽度
        [shapeLayer setLineWidth:CGRectGetHeight(lineView.frame)];
    } else {
        [shapeLayer setLineWidth:CGRectGetWidth(lineView.frame)];
    }
    [shapeLayer setLineJoin:kCALineJoinRound]; //  设置线宽，线间距
    [shapeLayer setLineDashPattern:[NSArray arrayWithObjects:[NSNumber numberWithInt:lineLength], [NSNumber numberWithInt:lineSpacing], nil]];
    CGMutablePathRef path = CGPathCreateMutable(); //  设置路径
    CGPathMoveToPoint(path, NULL, 0, 0);

    if (isHorizonal) {
        CGPathAddLineToPoint(path, NULL,CGRectGetWidth(lineView.frame), 0);
    } else {
        CGPathAddLineToPoint(path, NULL, 0, CGRectGetHeight(lineView.frame));
    }
    [shapeLayer setPath:path];
    CGPathRelease(path);
    
    [lineView.layer addSublayer:shapeLayer]; //  把绘制好的虚线添加上来
}

- (void)likeAction {
    NSLog(@"likeAction");
    
    [likeBt setImage:GetImageByName(@"match_like_select") forState:UIControlStateNormal];
    
    if (_likeBlock) {
        _likeBlock();
    }
}

#pragma lazy

- (UIView *)contentView {
    if (!_contentView) {
        
        _contentView = [[UIView alloc] initWithFrame:self.bounds];
//        _contentView.backgroundColor = [UIColor lightGrayColor];
        
        player = [[VideoPlayerView alloc]initWithFrame:CGRectMake( ZoomSize(16), ZoomSize(0) , ZoomSize(90), ZoomSize(120))];
        player.layer.cornerRadius =ZoomSize(16);
        player.layer.masksToBounds = YES;
        [player setSparkState];
        [_contentView addSubview:player];
        
        
        UIView *leftLineView = [[UIView alloc] initWithFrame:CGRectMake(ZoomSize(115), ZoomSize(60), ZoomSize(40), ZoomSize(2))];
        [_contentView addSubview:leftLineView];
        [self drawLineOfDashByCAShapeLayer:leftLineView lineLength:ZoomSize(10) lineSpacing:ZoomSize(5) lineColor:BGDarkBlueColor lineDirection:YES];
        
        UIView *rightLineView = [[UIView alloc] initWithFrame:CGRectMake(ZoomSize(220), ZoomSize(60), ZoomSize(40), ZoomSize(2))];
        [_contentView addSubview:rightLineView];
        [self drawLineOfDashByCAShapeLayer:rightLineView lineLength:ZoomSize(10) lineSpacing:ZoomSize(5) lineColor:BGDarkBlueColor lineDirection:YES];
        
        UIView *likeBtBGView = [[UIView alloc] initWithFrame:CGRectMake((SCREEN_Width - ZoomSize(76))/2.0, ZoomSize(24), ZoomSize(76), ZoomSize(76))];
//        likeBtBGView.backgroundColor = UIColor.whiteColor;
        likeBtBGView.layer.cornerRadius = ZoomSize(38);
        likeBtBGView.layer.masksToBounds = YES;
        likeBtBGView.layer.borderColor = RGB(180, 170, 190).CGColor;
        likeBtBGView.layer.borderWidth = ZoomSize(4);
        
        [_contentView addSubview:likeBtBGView];
        likeBt = [[UIButton alloc] initWithFrame:CGRectMake((SCREEN_Width - ZoomSize(76))/2.0, ZoomSize(25), ZoomSize(76), ZoomSize(76))];
        [likeBt setImage:GetImageByName(@"match_like_normal") forState:UIControlStateNormal];
        [likeBt addTarget:self action:@selector(likeAction) forControlEvents:UIControlEventTouchUpInside];
        [_contentView addSubview:likeBt];
        
        
        UIImageView *tipIV = [[UIImageView alloc]initWithFrame:CGRectMake( (SCREEN_Width - ZoomSize(20))/2.0, ZoomSize(100) , ZoomSize(20), ZoomSize(20))];
        tipIV.image = GetImageByName(@"match_vioce");
        [_contentView addSubview:tipIV];
        
#warning 需要添加人脸验证
        videoCamera = [[GPUImageVideoCamera alloc] initWithSessionPreset:AVCaptureSessionPreset640x480 cameraPosition:AVCaptureDevicePositionFront]; //设置分辨率与前置摄像头
        videoCamera.outputImageOrientation = [UIApplication sharedApplication].statusBarOrientation; //设置镜头向上
        videoCamera.horizontallyMirrorFrontFacingCamera = YES; //启动镜面模式
        
        filter = [[GPUImageSepiaFilter alloc] init];
        [(GPUImageSepiaFilter *)filter setIntensity:0];
        
        filterMaskView = [[UIView alloc] initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(106), ZoomSize(0), ZoomSize(90), ZoomSize(120))];
        [_contentView addSubview:filterMaskView];
        filterMaskView.layer.cornerRadius = ZoomSize(16);
        filterMaskView.layer.masksToBounds = YES;
        filterView = [[GPUImageView alloc] initWithFrame:filterMaskView.bounds];
        [filterMaskView addSubview:filterView];
        
        
        [videoCamera addTarget:filter];
        [filter addTarget:filterView];
        [videoCamera startCameraCapture];
        
        
    }
    return _contentView;
}

@end

//
//  UserBuyVipView.m
//  Video
//
//  Created by steve on 2020/4/30.
//  Copyright © 2020 steve. All rights reserved.
//

#import "UserBuyVipView.h"
#import "VipInfoView.h"

@interface UserBuyVipView() <UIGestureRecognizerDelegate> {
    UILabel *timeLb;
    NSTimer *codeTimer;
    int getDif;
}

@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIVisualEffectView *visualView;
@property (nonatomic, strong) VipInfoView *vipInfoView;

@end

@implementation UserBuyVipView

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews:frame];
    }
    return self;
}

- (void)initSubviews:(CGRect) frame {
    self.frame = frame;
    
    [self addSubview:self.visualView];
    [self addSubview:self.contentView];
    
    UITapGestureRecognizer *singleFingerOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
    singleFingerOne.numberOfTouchesRequired = 1;
    singleFingerOne.delegate = self;
    [self addGestureRecognizer:singleFingerOne];
}

#pragma function

- (void)setDif:(int)dif {
    getDif = dif;
    [self startTimer];
}

- (void)startTimer {
    codeTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(codeShow) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:codeTimer forMode:NSRunLoopCommonModes];
}

- (void)stopTimer {
    [codeTimer invalidate];
    codeTimer = nil;
}

- (void)codeShow {
    getDif--;
    if (getDif <= 0) {
        [self stopTimer];
    } else {
        timeLb.text = [NSString stringWithFormat:@"%02d:%02d:%02d",(getDif/3600)%24, (getDif/60)%60, getDif%60];
    }
}

- (void)show {
    [[[[[[UIApplication sharedApplication] delegate] window] rootViewController] view] addSubview:self]; //使用window，可以解决隐藏tabbar的问题
    [self.vipInfoView createTimer];
    [UIView animateWithDuration:0.25 animations:^{
        [self.contentView setFrame:CGRectMake( ZoomSize(38), (SCREEN_Height - ZoomSize(440) )/2.0, ZoomSize(300) ,ZoomSize(440))];
    }];
}

- (void)dismiss {
    
    [self.vipInfoView stopTimer];
    [UIView animateWithDuration:0.25 animations:^{
        [self.contentView setFrame:CGRectMake( ZoomSize(38), (SCREEN_Height - ZoomSize(440) )/2.0 + SCREEN_Height, ZoomSize(300) ,ZoomSize(440))];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)getVipAction {
    [DataTool showGetVipViewWithIndex:0];
}

#pragma delegate
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if ([touch.view isDescendantOfView:_contentView]) {
        return NO;
    }
    return YES;
}

#pragma lazy
- (UIVisualEffectView *)visualView {
    if (!_visualView) {
        UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        
        _visualView = [[UIVisualEffectView alloc] initWithEffect:blur];
        _visualView.frame = self.frame;
        _visualView.alpha = 0.5;
    }
    
    return _visualView;
}

- (UIView *)contentView {
    if (!_contentView) {
        
        _contentView = [[UIView alloc] initWithFrame:CGRectMake( ZoomSize(38), (SCREEN_Height - ZoomSize(440) )/2.0 + SCREEN_Height, ZoomSize(300) ,ZoomSize(440))];
        _contentView.backgroundColor = [UIColor whiteColor];
        _contentView.layer.cornerRadius = ZoomSize(16);
        _contentView.layer.masksToBounds = YES;
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(0, ZoomSize(35), ZoomSize(300), ZoomSize(20))];
        titleLb.text = @"Free matches are used up today";
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(15)];
        titleLb.textColor = RGBA(89, 75, 104, 1);
        [_contentView addSubview:titleLb];
        
        timeLb = [[UILabel alloc] initWithFrame:CGRectMake(0, ZoomSize(61), ZoomSize(300), ZoomSize(20))];
        timeLb.text = @"";
        timeLb.textAlignment = NSTextAlignmentCenter;
        timeLb.font = [UIFont systemFontOfSize:ZoomSize(12) weight:UIFontWeightSemibold];
        timeLb.textColor = TextRedColor;
        [_contentView addSubview:timeLb];
        
        [_contentView addSubview:self.vipInfoView];
        
        UIButton *getVIPBt = [DataTool createCommomBtWithFrame:CGRectMake(ZoomSize(22), ZoomSize(359), ZoomSize(255), ZoomSize(48)) andTitle:@"Get VIP"];
        [_contentView addSubview:getVIPBt];
        [getVIPBt addTarget:self action:@selector(getVipAction) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _contentView;
}

- (VipInfoView *)vipInfoView {
    if (!_vipInfoView ) {
        _vipInfoView = [[VipInfoView alloc] initWithFrame:CGRectMake(0, ZoomSize(100), ZoomSize(300), ZoomSize(220)) photoArray:VipImageArray currentIndex:0];
        _vipInfoView.canPop = YES;
//        _vipInfoView.interval = 3;
    }
    return  _vipInfoView;
}

@end

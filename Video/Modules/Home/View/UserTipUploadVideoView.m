//
//  UserTipUploadVideoView.m
//  Video
//
//  Created by steve on 2020/4/30.
//  Copyright © 2020 steve. All rights reserved.
//

#import "UserTipUploadVideoView.h"


@interface UserTipUploadVideoView() <UIGestureRecognizerDelegate> {
    UILabel *timeLb;
    NSTimer *codeTimer;
    int getDif;
}

@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIVisualEffectView *visualView;

@end

@implementation UserTipUploadVideoView

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews:frame];
    }
    return self;
}

- (void)initSubviews:(CGRect) frame {
    self.frame = frame;
    
    [self addSubview:self.visualView];
    [self addSubview:self.contentView];
    
    UITapGestureRecognizer *singleFingerOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
    singleFingerOne.numberOfTouchesRequired = 1;
    singleFingerOne.delegate = self;
    [self addGestureRecognizer:singleFingerOne];
    
    
}
    
#pragma function

- (void)setDif:(int)dif {
    
    getDif = dif;
    [self startTimer];
}

- (void)startTimer {
    codeTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(codeShow) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:codeTimer forMode:NSRunLoopCommonModes];
}

- (void)stopTimer {
    [codeTimer invalidate];
    codeTimer = nil;
}

- (void)codeShow {
    getDif--;
    
    if (getDif <= 0) {
        [self stopTimer];
    } else {
        timeLb.text = [NSString stringWithFormat:@"%02d:%02d:%02d",(getDif/3600)%24, (getDif/60)%60, getDif%60];
    }
}

- (void)recordAction {
    NSLog(@"recordAction");
    [self dismiss];
    [DataTool showUploadVideoPage];
}

- (void)getVipAction {
    [DataTool showGetVipViewWithIndex:0];
}

- (void)show {
    [[[[[[UIApplication sharedApplication] delegate] window] rootViewController] view] addSubview:self]; //使用window，可以解决隐藏tabbar的问题
    [UIView animateWithDuration:0.25 animations:^{
        [self.contentView setFrame:CGRectMake( ZoomSize(38), (SCREEN_Height - ZoomSize(440) )/2.0, ZoomSize(300) ,ZoomSize(440))];
    }];
}

- (void)dismiss {
    [UIView animateWithDuration:0.25 animations:^{
        [self.contentView setFrame:CGRectMake( ZoomSize(38), (SCREEN_Height - ZoomSize(440) )/2.0 + SCREEN_Height, ZoomSize(300) ,ZoomSize(440))];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma delegate
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if ([touch.view isDescendantOfView:_contentView]) {
        return NO;
    }
    return YES;
}

#pragma lazy
- (UIVisualEffectView *)visualView {
    if (!_visualView) {
        UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        
        _visualView = [[UIVisualEffectView alloc] initWithEffect:blur];
        _visualView.frame = self.frame;
        _visualView.alpha = 0.5;
    }
    
    return _visualView;
}

- (UIView *)contentView {
    if (!_contentView) {
        
        _contentView = [[UIView alloc] initWithFrame:CGRectMake( ZoomSize(38), (SCREEN_Height - ZoomSize(440) )/2.0 + SCREEN_Height, ZoomSize(300) ,ZoomSize(440))];
        _contentView.backgroundColor = [UIColor whiteColor];
        _contentView.layer.cornerRadius = ZoomSize(16);
        _contentView.layer.masksToBounds = YES;
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(0, ZoomSize(35), ZoomSize(300), ZoomSize(20))];
        titleLb.text = @"Free matches are used up today";
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(15)];
        titleLb.textColor = RGBA(89, 75, 104, 1);
        [_contentView addSubview:titleLb];
        
        timeLb = [[UILabel alloc] initWithFrame:CGRectMake(0, ZoomSize(61), ZoomSize(300), ZoomSize(20))];
        timeLb.text = @"";
        timeLb.textAlignment = NSTextAlignmentCenter;
        timeLb.font = [UIFont systemFontOfSize:ZoomSize(12) weight:UIFontWeightSemibold];
        timeLb.textColor = TextRedColor;
        [_contentView addSubview:timeLb];
        
        UILabel *tipLb = [[UILabel alloc] initWithFrame:CGRectMake(0, ZoomSize(105), ZoomSize(300), ZoomSize(62))];
        tipLb.numberOfLines = 0;
        tipLb.text = @"Upload 15s of video for\n5 free matches";
        tipLb.textAlignment = NSTextAlignmentCenter;
        tipLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightSemibold];
        tipLb.textColor = BGDarkBlueColor;
        [_contentView addSubview:tipLb];
        
        NSArray *infoArray = [NSArray arrayWithObjects:@"Certificate Badge", @"5 Free Matchs", @"Profile Boost", nil];
        for (int i = 0; i < 3; i ++) {
            UIImageView *tipIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(78), ZoomSize(195) + ZoomSize(24) * i, ZoomSize(13), ZoomSize(9))];
            [_contentView addSubview:tipIV];
            tipIV.contentMode = UIViewContentModeScaleAspectFit;
            tipIV.image = GetImageByName(@"pop_choose");
            
            UILabel *infoLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(100), ZoomSize(189) + ZoomSize(24) * i, ZoomSize(140), ZoomSize(20))];
            infoLb.text = infoArray[i];
            infoLb.textAlignment = NSTextAlignmentLeft;
            infoLb.font = [UIFont systemFontOfSize:ZoomSize(16)];
            infoLb.textColor = RGBA(56, 47, 65, 1);
            [_contentView addSubview:infoLb];
        }
        
        UIButton *recordBt = [DataTool createCommomBtWithFrame:CGRectMake(ZoomSize(22), ZoomSize(289), ZoomSize(255), ZoomSize(48)) andTitle:@"Record Now"];
        [_contentView addSubview:recordBt];
        [recordBt addTarget:self action:@selector(recordAction) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *getVipBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(22), ZoomSize(359), ZoomSize(255), ZoomSize(48))];
        [_contentView addSubview:getVipBt];
        [getVipBt addTarget:self action:@selector(getVipAction) forControlEvents:UIControlEventTouchUpInside];
        [getVipBt setTitle:@"Get VIP to unblock more\n feature" forState:UIControlStateNormal];
        [getVipBt setTitleColor:BGPurPleColor forState:UIControlStateNormal];
        getVipBt.titleLabel.numberOfLines = 0;
        getVipBt.titleLabel.textAlignment = NSTextAlignmentCenter;
        getVipBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(16) weight:UIFontWeightSemibold];
        getVipBt.layer.borderColor = BGPurPleColor.CGColor;
        getVipBt.layer.borderWidth = ZoomSize(2);
        getVipBt.layer.cornerRadius = ZoomSize(24);
        
    }
    return _contentView;
}

@end

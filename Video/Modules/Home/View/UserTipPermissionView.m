//
//  UserTipPermissionView.m
//  Video
//
//  Created by steve on 2020/6/10.
//  Copyright © 2020 steve. All rights reserved.
//

#import "UserTipPermissionView.h"

#import "LBXPermission.h"
#import "LBXPermissionSetting.h"

@interface UserTipPermissionView() {
    UISwitch *cameraSwitch; //视频开关
    UISwitch *microphoneSwitch; //麦克风开关
}

@property (nonatomic, strong) UIView *contentView;

@end

@implementation UserTipPermissionView

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        [self initSubviews:frame];
    }
    return self;
}

- (void)initSubviews:(CGRect) frame {
    self.frame = frame;
    [self addSubview:self.contentView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshStatus) name:UIApplicationDidBecomeActiveNotification object:nil];
    
}

#pragma function
- (void)show {
    
    [[[[[[UIApplication sharedApplication] delegate] window] rootViewController] view] addSubview:self]; //使用window，可以解决隐藏tabbar的问题
    
    [UIView animateWithDuration:0.25 animations:^{
        [self.contentView setFrame:CGRectMake(0, 0 , SCREEN_Width, SCREEN_Height)];
    }];
}

- (void)dismiss {
    
    if (_dismissBLock) {
        _dismissBLock();
    }
    
    [UIView animateWithDuration:0.25 animations:^{
        [self.contentView setFrame:CGRectMake(0, SCREEN_Height  , SCREEN_Width, SCREEN_Height)];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)swithValueChange:(id)sender {
    
    UISwitch *_switch = sender;
    if (sender == cameraSwitch) { //相册
        [LBXPermission authorizeWithType:LBXPermissionType_Camera completion:^(BOOL granted, BOOL firstTime) {
            _switch.on = granted;
            [self handCompletionWithGranted:granted firstTime:firstTime];
        }];
    }
    else if (sender == microphoneSwitch) { //相机
        [LBXPermission authorizeWithType:LBXPermissionType_Microphone completion:^(BOOL granted, BOOL firstTime) {
            _switch.on = granted;
            [self handCompletionWithGranted:granted firstTime:firstTime];
        }];
    }
    
}

- (void)handCompletionWithGranted:(BOOL)granted firstTime:(BOOL)firstTime {
    //没有权限，且不是第一次获取权限
    if ( !granted && !firstTime ) {
        [LBXPermissionSetting showAlertToDislayPrivacySettingWithTitle:@"Tip" msg:@"No permissions,  go to setting？" cancel:@"cancel" setting:@"setting"];
    }
    
    //增加所有switch的值变化监听
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self refreshStatus];
    });
    
}

- (void)refreshStatus {
    
    cameraSwitch.on = [LBXPermission authorizedWithType:LBXPermissionType_Camera];
    microphoneSwitch.on = [LBXPermission authorizedWithType:LBXPermissionType_Microphone];
    
    cameraSwitch.enabled = !cameraSwitch.on;
    microphoneSwitch.enabled = !microphoneSwitch.on;
}


#pragma lazy
- (UIView *)contentView {
    if (!_contentView) {
        
        _contentView = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_Height, SCREEN_Width ,SCREEN_Height)];
        
        UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVisualEffectView *visualView = [[UIVisualEffectView alloc] initWithEffect:blur];
        visualView.frame = self.frame;
        visualView.alpha = 0.88;
        [_contentView addSubview:visualView];
        
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake( (SCREEN_Width - ZoomSize(300))/2, StatusBarHeight + ZoomSize(51), ZoomSize(300), ZoomSize(30))];
        [_contentView addSubview:titleLb];
        titleLb.text = @"Vido would like to access";
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightSemibold];
        
        UILabel *infoLb = [[UILabel alloc] initWithFrame:CGRectMake( (SCREEN_Width - ZoomSize(300))/2, StatusBarHeight + ZoomSize(95), ZoomSize(300), ZoomSize(40))];
        [_contentView addSubview:infoLb];
        infoLb.numberOfLines = 0;
        infoLb.text = @"We need these permissions to get you \ninto the entertaining video chat sessions";
        infoLb.textAlignment = NSTextAlignmentCenter;
        infoLb.textColor = WhiteColor;
        infoLb.font = [UIFont systemFontOfSize:ZoomSize(16)];
        
        UIButton *startBt = [DataTool createCommomBtWithFrame:CGRectMake(ZoomSize(38), StatusBarHeight + ZoomSize(473), ZoomSize(300), ZoomSize(48)) andTitle:@"Let's start!"];
        [_contentView addSubview:startBt];
        [startBt addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
        
#pragma camera
        UIView *cameraBGView = [[UIView alloc] initWithFrame:CGRectMake(ZoomSize(68),StatusBarHeight + ZoomSize(166), ZoomSize(240), ZoomSize(122))];
        cameraBGView.layer.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:0.1].CGColor;
        cameraBGView.layer.cornerRadius = 16;
        [_contentView addSubview:cameraBGView];
        
        UILabel *cameraTitleLb = [[UILabel alloc] initWithFrame:CGRectMake( 0,ZoomSize(10), ZoomSize(240), ZoomSize(20))];
        [cameraBGView addSubview:cameraTitleLb];
        cameraTitleLb.text = @"Camera";
        cameraTitleLb.textAlignment = NSTextAlignmentCenter;
        cameraTitleLb.textColor = WhiteColor;
        cameraTitleLb.font = [UIFont systemFontOfSize:ZoomSize(18)];
        
        UILabel *cameraInfoLb = [[UILabel alloc] initWithFrame:CGRectMake( 0, ZoomSize(40), ZoomSize(240), ZoomSize(20))];
        [cameraBGView addSubview:cameraInfoLb];
        cameraInfoLb.text = @"Required for live video calls";
        cameraInfoLb.textAlignment = NSTextAlignmentCenter;
        cameraInfoLb.textColor = WhiteColor;
        cameraInfoLb.font = [UIFont systemFontOfSize:ZoomSize(16)];
        
        cameraSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(  (ZoomSize(240) - 60)/2 , ZoomSize(76), 57, 32 )];
        [cameraSwitch addTarget:self action:@selector(swithValueChange:) forControlEvents:UIControlEventValueChanged];
        cameraSwitch.on = NO;
        [cameraBGView addSubview:cameraSwitch];
        
#pragma microphone
        UIView *microphoneBGView = [[UIView alloc] initWithFrame:CGRectMake(ZoomSize(68), StatusBarHeight + ZoomSize(308), ZoomSize(240), ZoomSize(122))];
        microphoneBGView.layer.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:0.1].CGColor;
        microphoneBGView.layer.cornerRadius = 16;
        [_contentView addSubview:microphoneBGView];
        
        UILabel *microphoneTitleLb = [[UILabel alloc] initWithFrame:CGRectMake( 0,ZoomSize(10), ZoomSize(240), ZoomSize(20))];
        [microphoneBGView addSubview:microphoneTitleLb];
        microphoneTitleLb.text = @"Microphone";
        microphoneTitleLb.textAlignment = NSTextAlignmentCenter;
        microphoneTitleLb.textColor = WhiteColor;
        microphoneTitleLb.font = [UIFont systemFontOfSize:ZoomSize(18)];
        
        UILabel *microphoneInfoLb = [[UILabel alloc] initWithFrame:CGRectMake( 0, ZoomSize(40), ZoomSize(240), ZoomSize(20))];
        [microphoneBGView addSubview:microphoneInfoLb];
        microphoneInfoLb.text = @"Required for audio calls";
        microphoneInfoLb.textAlignment = NSTextAlignmentCenter;
        microphoneInfoLb.textColor = WhiteColor;
        microphoneInfoLb.font = [UIFont systemFontOfSize:ZoomSize(16)];
        
        microphoneSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(  (ZoomSize(240) - 60)/2 , ZoomSize(76), 57, 32 )];
        [microphoneSwitch addTarget:self action:@selector(swithValueChange:) forControlEvents:UIControlEventValueChanged];
        microphoneSwitch.on = NO;
        [microphoneBGView addSubview:microphoneSwitch];
        
        [self refreshStatus];
        
    }
    
    return _contentView;
}

@end

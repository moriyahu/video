//
//  AppDelegate.m
//  Video
//
//  Created by steve on 2020/3/19.
//  Copyright © 2020 steve. All rights reserved.
//

#import "AppDelegate.h"
#import "IQKeyboardManager.h"
#import "TUIKit.h"

#import <Bugly/Bugly.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import "UserInfo.h"
#import "SocketTool.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions]; //facebook
    [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
    [FBSDKSettings setAppID:FaceBook_APP_KEY];
    
    [[IQKeyboardManager sharedManager] setEnable:YES]; //keyboard
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    [[IQKeyboardManager sharedManager] setShouldShowToolbarPlaceholder:NO];
    
    BuglyConfig *config = [[BuglyConfig alloc] init]; //设置bug监测
    config.unexpectedTerminatingDetectionEnable = YES;
    config.reportLogLevel = BuglyLogLevelError;
    [Bugly startWithAppId:BuglyID developmentDevice:YES config:config];
    
    [[TUIKit sharedInstance] setupWithAppId:[TXIMSDK_APP_KEY longLongValue]]; // SDKAppID 可以在 即时通信 IM 控制台中获取
    
    if ([UserInfo shareInstant].user ) { //homePage
        [[UserInfo shareInstant] checkUserDataModel:[UserInfo shareInstant].user callBackInfo:^(id  _Nullable obj) {
            
            if ([DataTool isEmptyString:[UserInfo shareInstant].user.headPortraitUrl] && [DataTool isEmptyString:[UserInfo shareInstant].user.headThumbnailUrl] && [DataTool isEmptyString:[UserInfo shareInstant].user.o_avatar] ) {
                 [DataTool showUploadPhotoPage];
            } else {
                [[UserInfo shareInstant] userLogin];
            }
            
        }];
    } else {
        [[UserInfo shareInstant] userLogout];
    }
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey] annotation:options[UIApplicationOpenURLOptionsAnnotationKey] ];
    return handled;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    [SocketTool shareInstance].reConnectTime = 5;
    [[SocketTool shareInstance] closeSocket];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [SocketTool shareInstance].reConnectTime = 0;
    [[SocketTool shareInstance] reconnectSocket];
}

@end


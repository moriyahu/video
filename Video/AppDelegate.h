//
//  AppDelegate.h
//  Video
//
//  Created by steve on 2020/3/19.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


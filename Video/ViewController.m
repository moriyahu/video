//
//  ViewController.m
//  Video
//
//  Created by steve on 2020/3/19.
//  Copyright © 2020 steve. All rights reserved.
//

#import "ViewController.h"

//#import <GoogleSignIn/GoogleSignIn.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#import <AuthenticationServices/AuthenticationServices.h>

//#import <AgoraRtcEngineKit/AgoraRtcEngineKit.h>
//#import <AgoraRtcKit/AgoraRtcEngineKit.h>

#import <AliyunOSSiOS/OSSService.h>
#import <AliyunOSSiOS/OSSCompat.h>
//#import <SDWebImage/SDWebImage.h>

#import "TUIChatController.h"
#import "TUIContactController.h"
#import "TUIConversationListController.h"

NSString * const endpoint = @"https://oss-cn-hangzhou.aliyuncs.com";
NSString * const bucketName = @"videoproject4test";

NSString * const AccessKey = @"LTAI4Fj7dYQVN6fwMnbEfu47";
NSString * const SecretKey = @"N8VJykbZPJnSCNsIqU1inns8omZtdq";


@interface ViewController () <ASAuthorizationControllerDelegate,ASAuthorizationControllerPresentationContextProviding, UINavigationControllerDelegate,UIImagePickerControllerDelegate> {
//    OSSClient * client;
    
    OSSClient * client;
    UIImageView *getImg;
}

//@property (strong, nonatomic) AgoraRtcEngineKit *agoraKit;
//@property (weak, nonatomic) IBOutlet UIView *localVideo;
//@property (weak, nonatomic) IBOutlet UIView *remoteVideo;

@property (strong, nonatomic) NSData *imageData;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (@available(iOS 13.0, *)) {//13.0的新属性
        
        ASAuthorizationAppleIDButton *appleIDSignInButton = [ASAuthorizationAppleIDButton buttonWithType:ASAuthorizationAppleIDButtonTypeDefault style:ASAuthorizationAppleIDButtonStyleWhiteOutline];
        appleIDSignInButton.frame = CGRectMake(50, 150, 200, 50);
        [appleIDSignInButton addTarget:self action:@selector(userAppIDLogin:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:appleIDSignInButton];
        
        ASAuthorizationAppleIDButton *appleIDContinueButton = [ASAuthorizationAppleIDButton buttonWithType:ASAuthorizationAppleIDButtonTypeContinue style:ASAuthorizationAppleIDButtonStyleBlack];
        appleIDContinueButton.frame = CGRectMake(50, 220, 200, 50);
        [appleIDContinueButton addTarget:self action:@selector(userAppIDLogin:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:appleIDContinueButton];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(monitorSignInWithAppleStateChanged:) name:ASAuthorizationAppleIDProviderCredentialRevokedNotification object:nil]; //判断授权是否失效
    }
    
    
    getImg = [[UIImageView alloc] initWithFrame:CGRectMake(50, 300, 100, 100)];
    getImg.backgroundColor =[UIColor greenColor];
    [self.view addSubview:getImg];
    
//    2020-04-09 16:50:33.812259+0800 Video[6640:285947] responseObject {
//        code = 8200;
//        data =     {
//            aboutMe = "";
//            age = "<null>";
//            avatar = "";
//            birthday = "1950-01-01";
//            blance = "<null>";
//            country = "<null>";
//            email = "504495404@qq.com";
//            logonType = "";
//            nickName = "";
//            sex = "";
//            token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJCRkYiLCJtdWx0aVBsYXRmb3JtIjpmYWxzZSwibGFzdFNpZ25pblRpbWUiOjE1ODY0MjIyMzk3MDYsImlkIjo0NTUwNDg0NDQyNDQ5MjIzNjgsImV4cCI6MTU4Nzg5MzQ2OH0.gId8dBcX8PgniAOOKq_Xizco1JsP6bXEU9wWlHMaJQs";
//            vipEndtime = "<null>";
//            vipState = "";
//        };
//        msg = Success;
//        timestamp = 1586422239775;
//    }
    
    
    
//    id<OSSCredentialProvider> credential = [[OSSPlainTextAKSKPairCredentialProvider alloc] initWithPlainTextAccessKey:AccessKey secretKey:SecretKey];
//
//    client = [[OSSClient alloc] initWithEndpoint:endpoint credentialProvider:credential];
    
//    [self loadImagePicker];
//    [self getImgAddress:@"User/20200403020235605-6070.jpg"];
//    [self getImageData:@"User/20200403020235605-6070.jpg"];
    
//    [self uiFunction];
    
    
//    [GIDSignIn sharedInstance].clientID = @"800269223004-tb7qnek2sfbli1batsmblbqh3am4v9h9.apps.googleusercontent.com";
//    [GIDSignIn sharedInstance].delegate = self;
//    [GIDSignIn sharedInstance].presentingViewController = self;
//    [[GIDSignIn sharedInstance] signIn];
    
    
    
//    // 判断是否有摄像头
//    if(![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
//        [self showInfo:@"您的设备没有摄像头或者相关的驱动, 不能进行直播"];
//        return NO;
//    }
//
//    // 判断是否有摄像头权限
//    AVAuthorizationStatus  authorizationStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
//    if (authorizationStatus == AVAuthorizationStatusRestricted|| authorizationStatus == AVAuthorizationStatusDenied) {
//        [self showInfo:@"app需要访问您的摄像头。\\n请启用摄像头-设置/隐私/摄像头"];
//        return NO;
//    }
//
//    // 开启麦克风权限
//    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
//    if ([audioSession respondsToSelector:@selector(requestRecordPermission:)]) {
//        [audioSession performSelector:@selector(requestRecordPermission:) withObject:^(BOOL granted) {
//            if (granted) {
//                return YES;
//            }
//            else {
//                [self showInfo:@"app需要访问您的麦克风。\\n请启用麦克风-设置/隐私/麦克风"];
//                return NO;
//            }
//        }];
//    }
    
    
    
}

//#pragma   delegate - GIDSignInDelegate -GIDSignIn
//- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
////    user.profile.name
////    user.profile.email
//    NSLog(@"user %@",user.profile.name);
//}

#pragma mark - 处理授权
- (void)userAppIDLogin:(ASAuthorizationAppleIDButton *)button  API_AVAILABLE(ios(13.0)){
    
    ASAuthorizationAppleIDProvider *appleIdProvider = [[ASAuthorizationAppleIDProvider alloc] init]; // 创建新的AppleID 授权请求
    ASAuthorizationAppleIDRequest *request = appleIdProvider.createRequest; // 在用户授权期间请求的联系信息
    request.requestedScopes = @[ASAuthorizationScopeEmail,ASAuthorizationScopeFullName];
    
    ASAuthorizationController *controller = [[ASAuthorizationController alloc] initWithAuthorizationRequests:@[request]];
    controller.delegate = self;
    controller.presentationContextProvider = self;
    [controller performRequests];
}

//#pragma mark - 授权成功的回调
//- (void)authorizationController:(ASAuthorizationController *)controller didCompleteWithAuthorization:(ASAuthorization *)authorization  API_AVAILABLE(ios(13.0)) {
//    
//    NSString * appleIDString = [NTDataTool getContentFromKeyChain:@"AppleID"];
//    if (![NTDataTool isEmptyString:appleIDString]) {
//        [self checkEmail:appleIDString];
//    } else {
//        
//        if ([authorization.credential isKindOfClass:[ASAuthorizationAppleIDCredential class]]) {
//            
//            ASAuthorizationAppleIDCredential *credential = authorization.credential; // 用户登录使用ASAuthorizationAppleIDCredential
//            if (![NTDataTool isEmptyString:credential.email]) {
//                [NTDataTool getContentFromKeyChain:@"AppleID"];
//                
//                [NTDataTool saveContentToKeyChain:credential.email forKey:@"AppleID" service:@"DeviceUUID"];
//                [self checkEmail:credential.email];
//            } else {
//                [SVProgressHUD showInfoWithStatus:@"please retry later"];
//                [SVProgressHUD dismissWithDelay:2.0f];
//            }
//        } else {
//            [SVProgressHUD showInfoWithStatus:@"please retry later"];
//            [SVProgressHUD dismissWithDelay:2.0f];
//        }
//    }
//}


#pragma mark - 判断授权是否失效
- (void)monitorSignInWithAppleStateChanged:(NSNotification *)notification {
    NSLog(@"state CHANGE -  %@",notification);
}

#pragma mark - 授权成功的回调
- (void)authorizationController:(ASAuthorizationController *)controller didCompleteWithAuthorization:(ASAuthorization *)authorization  API_AVAILABLE(ios(13.0)) {
    
    if ([authorization.credential isKindOfClass:[ASAuthorizationAppleIDCredential class]]) {
        
        ASAuthorizationAppleIDCredential *credential = authorization.credential; // 用户登录使用ASAuthorizationAppleIDCredential
        NSString *user = credential.user;
        NSData *identityToken = credential.identityToken;
        NSLog(@"fullName -     %@",credential.fullName); //授权成功后，你可以拿到苹果返回的全部数据，根据需要和后台交互。
        NSLog(@"user   -   %@  %@ %@",user,identityToken, credential.email);
        
//        2020-03-19 18:07:40.950287+0800 Video[31272:617802] state CHANGE -  NSConcreteNotification 0x60000072e250 {name = ASAuthorizationAppleIDCredentialRevokedNotification}
//        2020-03-19 18:07:40.956167+0800 Video[31272:613516] fullName -     <NSPersonNameComponents: 0x600000b8c400> {givenName = May, familyName = Lee, middleName = (null), namePrefix = (null), nameSuffix = (null), nickname = (null) phoneticRepresentation = (null) }
//        2020-03-19 18:07:40.956330+0800 Video[31272:613516] user   -   001623.24ebe5ff9dd8404b94ec16511f36c602.0724  {length = 758, bytes = 0x65794a72 61575169 4f694934 4e6b5134 ... 78375742 61326451 } 504495404@qq.com
        
        [[NSUserDefaults standardUserDefaults] setObject:user forKey:@"userIdentifier"]; //保存apple返回的唯一标识符
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    } else if ([authorization.credential isKindOfClass:[ASPasswordCredential class]]) {
        
        ASPasswordCredential *psdCredential = authorization.credential; // 用户登录使用现有的密码凭证
        NSString *user = psdCredential.user;
        NSString *psd = psdCredential.password;
        NSLog(@"psduser -  %@   %@",psd,user);
        
    } else {
        NSLog(@"授权信息不符");
    }
}

#pragma mark - 授权回调失败
- (void)authorizationController:(ASAuthorizationController *)controller didCompleteWithError:(NSError *)error  API_AVAILABLE(ios(13.0)){
    
    NSLog(@"授权回调失败");
    
    NSLog(@"错误信息：%@", error);
    NSString *errorMsg;
    switch (error.code) {
        case ASAuthorizationErrorCanceled:
            errorMsg = @"用户取消了授权请求";
            NSLog(@"errorMsg -   %@",errorMsg);
            break;

        case ASAuthorizationErrorFailed:
            errorMsg = @"授权请求失败";
            NSLog(@"errorMsg -   %@",errorMsg);
            break;

        case ASAuthorizationErrorInvalidResponse:
            errorMsg = @"授权请求响应无效";
            NSLog(@"errorMsg -   %@",errorMsg);
            break;

        case ASAuthorizationErrorNotHandled:
            errorMsg = @"未能处理授权请求";
            NSLog(@"errorMsg -   %@",errorMsg);
            break;

        case ASAuthorizationErrorUnknown:
            errorMsg = @"授权请求失败未知原因";
            NSLog(@"errorMsg -   %@",errorMsg);
            break;

        default:
            break;
    }
}

//presentationAnchorForAuthorizationController
- (ASPresentationAnchor)presentationAnchorForAuthorizationController:(ASAuthorizationController *)controller API_AVAILABLE(ios(13.0)){
    return self.view.window;
}


#pragma IM

//创建会话列表界面

// 创建会话列表
//TUIConversationListController *vc = [[TUIConversationListController alloc] init];
//vc.delegate = self;
//[self.navigationController pushViewController:vc animated:YES];
//
//- (void)conversationListController:(TUIConversationListController *)conversationController didSelectConversation:(TUIConversationCell *)conversation
//{
//    // 会话列表点击事件，通常是打开聊天界面
//}

//打开聊天界面
//TIMConversation *conv = [[TIMManager sharedInstance] getConversation:TIM_C2C receiver:@"abc"];
//TUIChatController *vc = [[TUIChatController alloc] initWithConversation:conv];
//[self.navigationController pushViewController:vc animated:YES];

//添加通讯录界面
//TUIContactController *vc = [[TUIContactController alloc] init];
//[self.navigationController pushViewController:vc animated:YES];

#pragma facebook

//- (void)facebookLoginAction {
//
//    [SVProgressHUD dismissWithDelay:60.0f];
//    [SVProgressHUD show];
//
////    [Flurry logEvent:@"login_with_facebook"];
//
//    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
//
//    [login logInWithPermissions: @[@"public_profile"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
//
//        if (result.isCancelled) {
//            [SVProgressHUD dismiss];
//            return;
//        }
//
//        NSDictionary * params = @{@"fields": @"id, name, email, age_range, first_name, last_name, link, gender, locale, picture, timezone, updated_time, verified"};
//        FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:result.token.userID parameters:params HTTPMethod:GetMethod];
//
//        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
//
////            NSDictionary *resultDic = result;
////            self->facebookUserId = resultDic[@"id"];
////
////            NTLoginDataModel* dataModel = [[NTLoginDataModel alloc] init];
////
////            dataModel.type = @"facebook";
////            dataModel.facebook_user_id = self->facebookUserId;
////            dataModel.telephone = @"";
////            dataModel.accountkit_user_id = @"";
////            dataModel.password = @"";
////            dataModel.email = @"";
////            dataModel.google = @"";
////
////            NSDictionary *paramsDic = [dataModel mj_keyValues];
////
////            [self loginWithInfo:paramsDic];
//
//        }];
//    }];
//
//
//}


//    // Add this to the header of your file, e.g. in ViewController.m
//    // after #import "ViewController.h"
//    #import <FBSDKCoreKit/FBSDKCoreKit.h>
//    #import <FBSDKLoginKit/FBSDKLoginKit.h>
//
//    // Add this to the body
//    @implementation ViewController
    
//    - (void)viewDidLoad
//    {
//      [super viewDidLoad];
//      if ([FBSDKAccessToken currentAccessToken]) {
//       // User is logged in, do work such as go to next view controller.
//      }
//    }
    
//    // Extend the code sample from 6a. Add Facebook Login to Your Code
//    // Add to your viewDidLoad method:
//    loginButton.readPermissions = @[@"public_profile", @"email"];

#pragma video

//- (void)initializeAgoraEngine {
//    self.agoraKit = [AgoraRtcEngineKit sharedEngineWithAppId:appID delegate:self];
//}

//- (void)setupVideo {
//    [self.agoraKit enableVideo];
//    // Default mode is disableVideo
//
//    AgoraVideoEncoderConfiguration *encoderConfiguration =
//    [[AgoraVideoEncoderConfiguration alloc] initWithSize:AgoraVideoDimension640x360
//                                               frameRate:AgoraVideoFrameRateFps15
//                                                 bitrate:AgoraVideoBitrateStandard
//                                         orientationMode:AgoraVideoOutputOrientationModeAdaptative];
//    [self.agoraKit setVideoEncoderConfiguration:encoderConfiguration];
//}
//
//- (void)setupLocalVideo {
//    AgoraRtcVideoCanvas *videoCanvas = [[AgoraRtcVideoCanvas alloc] init];
//    videoCanvas.uid = 0;
//    // UID = 0 means we let Agora pick a UID for us
//
//    videoCanvas.view = self.localVideo;
//    videoCanvas.renderMode = AgoraVideoRenderModeHidden;
//    [self.agoraKit setupLocalVideo:videoCanvas];
//    // Bind local video stream to view
//}
//
//- (void)joinChannel {
//    [self.agoraKit joinChannelByToken:token channelId:@"demoChannel1" info:nil uid:0 joinSuccess:^(NSString *channel, NSUInteger uid, NSInteger elapsed) {
//        // Join channel "demoChannel1"
//    }];
//    // The UID database is maintained by your app to track which users joined which channels. If not assigned (or set to 0), the SDK will allocate one and returns it in joinSuccessBlock callback. The App needs to record and maintain the returned value as the SDK does not maintain it.
//
//    [self.agoraKit setEnableSpeakerphone:YES];
//    [UIApplication sharedApplication].idleTimerDisabled = YES;
//}
//
//- (void)rtcEngine:(AgoraRtcEngineKit *)engine firstRemoteVideoDecodedOfUid:(NSUInteger)uid size: (CGSize)size elapsed:(NSInteger)elapsed {
//    if (self.remoteVideo.hidden) {
//        self.remoteVideo.hidden = NO;
//    }
//
//    AgoraRtcVideoCanvas *videoCanvas = [[AgoraRtcVideoCanvas alloc] init];
//    videoCanvas.uid = uid;
//    // Since we are making a simple 1:1 video chat app, for simplicity sake, we are not storing the UIDs. You could use a mechanism such as an array to store the UIDs in a channel.
//
//    videoCanvas.view = self.remoteVideo;
//    videoCanvas.renderMode = AgoraVideoRenderModeHidden;
//    [self.agoraKit setupRemoteVideo:videoCanvas];
//    // Bind remote video stream to view
//}

#pragma aliyun
///**
// *  进入相册方法
// */
-(void)loadImagePicker {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate                 = self;
    picker.sourceType               = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.allowsEditing            = YES;
    [self presentViewController:picker animated:YES completion:NULL];

}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {

    UIImage *avatar = info[UIImagePickerControllerOriginalImage];
    //处理完毕，回到个人信息页面
    [picker dismissViewControllerAnimated:YES completion:NULL];
    //判断图片是不是png格式的文件
    if (UIImagePNGRepresentation(avatar)) {
        //返回为png图像。
        UIImage *imagenew = [self imageWithImageSimple:avatar scaledToSize:CGSizeMake(200, 200)];
        self.imageData = UIImagePNGRepresentation(imagenew);
    }else {
        //返回为JPEG图像。
        UIImage *imagenew = [self imageWithImageSimple:avatar scaledToSize:CGSizeMake(200, 200)];
        self.imageData = UIImageJPEGRepresentation(imagenew, 0.1);
    }
    
    [self putData];

}
//
///*
// 处理图片
// */
//
- (UIImage *)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
//
///**
// *  返回当前时间
// *
// *  @return return value description
// *
// */

- (NSString *)getTimeNow {
    NSString* date;
    NSDateFormatter * formatter = [[NSDateFormatter alloc ] init];
    [formatter setDateFormat:@"YYYYMMddhhmmssSSS"];
    date = [formatter stringFromDate:[NSDate date]];
    
    int last = arc4random() % 10000;//取出个随机数
    NSString *timeNow = [[NSString alloc] initWithFormat:@"%@-%i", date,last];
    NSLog(@"%@", timeNow);
    return timeNow;
}

- (NSString *)getImgAddress:(NSString *)imgFileName{
    
    NSString *objectKey = [NSString stringWithFormat:@"%@",imgFileName];
    NSString * publicURL = nil;
    OSSTask *task = [self->client presignPublicURLWithBucketName:bucketName withObjectKey:objectKey]; // sign public url
    
    if (!task.error) {
        publicURL = task.result;
//        https://videoproject4test.oss-cn-hangzhou.aliyuncs.com/User/20200403020235605-6070.jpg
//        [self->getImg sd_setImageWithURL:[NSURL URLWithString:publicURL]];
    } else {
        NSLog(@"sign url error: %@", task.error);
    }
    
    return publicURL;
}


- (void) putData {
    
    OSSPutObjectRequest * put = [OSSPutObjectRequest new];
    put.bucketName = bucketName;
    NSString *objectKeys = [NSString stringWithFormat:@"User/%@.jpg",[self getTimeNow]];
    put.objectKey = objectKeys;
    put.uploadingData = self.imageData;
    put.uploadProgress = ^(int64_t bytesSent, int64_t totalByteSent, int64_t totalBytesExpectedToSend) {
        NSLog(@"%lld, %lld, %lld", bytesSent, totalByteSent, totalBytesExpectedToSend);
    };
    
    OSSTask * putTask = [client putObject:put];
    [putTask continueWithBlock:^id(OSSTask *task) {
        task = [self->client presignPublicURLWithBucketName:@"videoproject4test"
                                              withObjectKey:objectKeys];
        NSLog(@"objectKey: %@", put.objectKey);
        if (!task.error) {
            NSLog(@"upload object success!");
        } else {
            NSLog(@"upload object failed, error: %@" , task.error);
        }
        return nil;
    }];
    
}

- (void)getImageData:(NSString *)imgFileName{
    
    OSSGetObjectRequest * request = [OSSGetObjectRequest new];
    request.bucketName = bucketName;
    request.objectKey = imgFileName;
    request.downloadProgress = ^(int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite) {
        NSLog(@"%lld, %lld, %lld", bytesWritten, totalBytesWritten, totalBytesExpectedToWrite);
    };
    OSSTask * getTask = [client getObject:request];
    
    NSLog(@"getTask %@",getTask.result);
    
    
    
    [getTask continueWithBlock:^id(OSSTask *task) {
        if (!task.error) {
            NSLog(@"download object success!");
            OSSGetObjectResult * getResult = task.result;
            NSLog(@"download result: %@", getResult.downloadedData);
            
            //获取数据，并更新数据
            dispatch_sync(dispatch_get_main_queue(), ^{
                self->getImg.image = [UIImage imageWithData:getResult.downloadedData];
            });
            
        } else {
            NSLog(@"download object failed, error: %@" ,task.error);
            
        }
        
        return nil;
    }];
    
}



#pragma 接口调试

    //数据转模型方法
    //    NSDictionary *paramsDic = @{@"type":@"REGISTER", @"email":@"504495410@qq.com", @"password":@"123456", @"code":@"123456" };
    //    LoginDataModel *dataModel = [LoginDataModel mj_objectWithKeyValues:paramsDic];
    
//    LoginDataModel *dataModel = [[LoginDataModel alloc] init];
    //    dataModel.email = @"504495409@qq.com";
    //    dataModel.type = @"REGISTER";
    //    dataModel.password = @"123456";
    //    dataModel.code = @"123456";
    //    NSDictionary *paramsDic = [dataModel mj_keyValues];
    
    
    //注册
    //    NSDictionary *paramsDic = @{@"type":@"REGISTER", @"email":@"504495410@qq.com", @"password":@"123456", @"code":@"123456" };
    //    [[NetTool shareInstance] startRequest:API_Login method:Post params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
    //    }];
    
    //    //登录
    //    NSDictionary *paramsDic = @{@"type":@"LOGIN", @"email":@"504495410@qq.com", @"password":@"123456" };
    //    [[NetTool shareInstance] startRequest:API_Login method:Post params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
    //    }];
    
    //    action = "<null>";
    //    code = 200;
    //    data = "eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiIxOTg2NTkiLCJzdWIiOiJ7XCJpZFwiOjE5ODY1OX0iLCJleHAiOjE1ODY0MjQxODQsIm5iZiI6MTU4NTEyODE4NH0.4RzY6x9xCg5soqVv_tD0ROiSunrjAaor1_iPY9k5vilpDZhy8bzPTqeeoYdk7pEJrwAJwresMa5iFoindbH6aw";
    //    msg = Success;
    //    pages = "<null>";
    
    //发送验证码
    //    NSDictionary *paramsDic = @{@"phoneCode":@"86", @"phoneNumber":@"15201923055", @"register":[NSNumber numberWithBool:YES] };
    //    [[NetTool shareInstance] startRequest:API_User_Phone_Code method:Post params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
    //    }];
    
    
    //国家的手机区号
//    NSDictionary *paramsDic = @{};
//    [[NetTool shareInstance] startRequest:API_System_Area_Code method:GetMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
//    }];
//    2020-03-25 18:45:48.188381+0800 Video[14800:377276] responseObject {
//    action = "<null>";
//    code = 200;
//    data =     (
//                {
//            code = 1;
//            contry = "United\U00a0States";
//        },
//                {
//            code = 1;
//            contry = Canada;
//        },
    
//    https://cloud.tencent.com/document/product/269/37065 界面样式


//    //敏感词
//    NSDictionary *paramsDic1 = @{};
//    [[NetTool shareInstance] startRequest:API_Sensitive_words method:PostMethod params:paramsDic1 needShowHUD:YES callback:^(id  _Nullable obj) {
//        NSLog(@"obj %@", obj);
//
//    }];

@end

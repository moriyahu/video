//
//  HomeTabbarViewController.h
//  Video
//
//  Created by steve on 2020/3/23.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeTabbarViewController : UITabBarController

@end

NS_ASSUME_NONNULL_END

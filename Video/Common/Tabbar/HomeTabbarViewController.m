//
//  HomeTabbarViewController.m
//  Video
//
//  Created by steve on 2020/3/23.
//  Copyright © 2020 steve. All rights reserved.
//

#import "HomeTabbarViewController.h"

#import "MessageViewController.h"
#import "HomeViewController.h"
#import "UserViewController.h"

@interface HomeTabbarViewController () {
}

@end

@implementation HomeTabbarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
    [self setData];
}

- (void)setupUI  {
    MessageViewController *messageVC = [[MessageViewController alloc]init];
    HomeViewController *homeVC = [[HomeViewController alloc] init];
    UserViewController *userVC = [[UserViewController alloc] init];
    
    [self setUpOneChildVcWithVc:messageVC Image:@"tabbar_message_default" selectedImage:@"tabbar_message_selected" title:@"Chat"];
    [self setUpOneChildVcWithVc:homeVC Image:@"tabbar_home_default" selectedImage:@"tabbar_home_selected" title:@"Home"];
    [self setUpOneChildVcWithVc:userVC Image:@"tabbar_user_default" selectedImage:@"tabbar_user_selected" title:@"User"];
    
    [UITabBar appearance].backgroundImage = [DataTool imageWithColor:RGBA(35, 5, 67, 0.3)];
}

- (void)setData {
    
    //设置tabbar的预加载
    for(UINavigationController *nav in  self.viewControllers){
        if ([self.viewControllers indexOfObject:nav] == 0 || [self.viewControllers indexOfObject:nav] == 1 || [self.viewControllers indexOfObject:nav] == 2 ) { // 指定需要加载的vc
            UIViewController *viewController = nav.viewControllers.firstObject;
            if (@available(iOS 9.0, *)) {
                [viewController loadViewIfNeeded];
            } else {
                [viewController loadView];
            }
        }
    }
    
    
    [self setSelectedIndex:1];
}

#pragma mark - function

- (void)setUpOneChildVcWithVc:(UIViewController *)Vc Image:(NSString *)image selectedImage:(NSString *)selectedImage title:(NSString *)title {
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:Vc];
    nav.navigationBar.hidden = YES;

    UIImage *myImage = [UIImage imageNamed:image];
    myImage = [myImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage *mySelectedImage = [UIImage imageNamed:selectedImage];
    mySelectedImage = [mySelectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    Vc.tabBarItem.image = myImage;
    Vc.tabBarItem.selectedImage = mySelectedImage;
    Vc.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0); //设置图片偏移，注 这里需要对称，否则会出现连点变小的问题
    
//    Vc.tabBarItem.title = title;
//    Vc.navigationItem.title = title;
    
//    NSDictionary *dictHome = [NSDictionary dictionaryWithObject:[UIColor colorWithRed:0.0f/255.0f green:53.0f/255.0f blue:111.0f/255.0f alpha:1.0f] forKey:NSForegroundColorAttributeName]; // //       设置 tabbarItem 选中状态下的文字颜色(不被系统默认渲染,显示文字自定义颜色)
//    [Vc.tabBarItem setTitleTextAttributes:dictHome forState:UIControlStateSelected];
//    [Vc.tabBarItem setTitlePositionAdjustment:UIOffsetMake(0, -3)];
    
    [self addChildViewController:nav];
    
}

- (void) showHomeTab {
    [self setSelectedIndex:1];
}

@end

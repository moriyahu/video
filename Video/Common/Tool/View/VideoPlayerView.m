//
//  VideoPlayerView.m
//  Video
//
//  Created by steve on 2020/5/13.
//  Copyright © 2020 steve. All rights reserved.
//

#import "VideoPlayerView.h"
#import <AVFoundation/AVFoundation.h>

@interface VideoPlayerView() <NSURLSessionDelegate> {
    BOOL isLookMe;
//    BOOL isSpark;
}

@property (nonatomic, strong) AVPlayerItem *playerItem;
@property (nonatomic, assign) BOOL isLoadFinish;
@property (nonatomic, assign) BOOL isPlaying;
@property (nonatomic, assign) BOOL isLoopPlay; //是否循环播放标志

//@property (nonatomic, assign) BOOL isIntoBackground;
//@property (nonatomic, strong) UIView *contentView;
//@property (nonatomic, strong) UIView *playerView;

@property (nonatomic, strong) AVPlayerLayer *playerLayer;
@property (nonatomic, strong) AVPlayer *player;
@property (nonatomic, strong) UIActivityIndicatorView *loadActivity;
@property (nonatomic, strong) UIButton *playBtn;

@property (nonatomic, copy) NSURL *movieURL;//视频的URL
@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSURLSessionTask *task;
@property (nonatomic, copy) NSURL *playerURL;

@property (nonatomic, strong) UIView *connectTipView; //连接中视频，spark提示
@property (nonatomic, strong) UIView *vipTipView; //vip提示视图

@end

@implementation VideoPlayerView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews:frame];
    }
    return self;
}

- (void)initSubviews:(CGRect) frame {
    self.frame = frame;
    
    UIView *contentView = [[UIView alloc] initWithFrame:self.bounds];
    [self addSubview:contentView];
    contentView.backgroundColor = RGB(100, 100, 100);
    
    self.playerItem = [AVPlayerItem playerItemWithURL:[NSURL URLWithString:@""]];
    self.player = [AVPlayer playerWithPlayerItem:self.playerItem];
    self.playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.player];
    
    self.playerLayer.frame = self.bounds;
    [contentView.layer addSublayer:self.playerLayer];
    
    self.loadActivity = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    self.loadActivity.center = contentView.center;
    [contentView addSubview:self.loadActivity];
    
    self.playBtn = [[UIButton alloc] initWithFrame:self.bounds];
    [self.playBtn addTarget:self action:@selector(playORPause) forControlEvents:UIControlEventTouchUpInside];
    [contentView addSubview:self.playBtn];
    
#pragma vipTipView
    self.connectTipView = [[UIView alloc] initWithFrame:self.bounds];
    [contentView addSubview:self.connectTipView];
    self.connectTipView.hidden = YES;
    
    UIBlurEffect *connectBlur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *connectVisualView = [[UIVisualEffectView alloc] initWithEffect:connectBlur];
    connectVisualView.frame = self.bounds;
    connectVisualView.alpha = 0.9;
    [self.vipTipView addSubview:connectVisualView];
    
    UIImageView *connectTipIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, ZoomSize(62), ZoomSize(59))];
    connectTipIV.center = self.connectTipView.center;
    [self.connectTipView addSubview:connectTipIV];
    connectTipIV.image = GetImageByName(@"playerConnectTip");
    
#pragma vipTipView
    
    self.vipTipView = [[UIView alloc] initWithFrame:self.bounds];
    [contentView addSubview:self.vipTipView];
    self.vipTipView.hidden = YES;
    
    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *visualView = [[UIVisualEffectView alloc] initWithEffect:blur];
    visualView.frame = self.bounds;
    visualView.alpha = 0.9;
    [self.vipTipView addSubview:visualView];
    
    UIImageView *vipTipIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, ZoomSize(32), ZoomSize(58))];
    vipTipIV.center = self.vipTipView.center;
    [self.vipTipView addSubview:vipTipIV];
    vipTipIV.image = GetImageByName(@"playerVipTip");
    
    UIButton *vipTipBt = [[UIButton alloc] initWithFrame:self.bounds];
    [self.vipTipView addSubview:vipTipBt];
    [vipTipBt addTarget:self action:@selector(vipAction) forControlEvents:UIControlEventTouchUpInside];
    
}

#pragma function

- (void)updatePlayerWith:(NSURL *)url{
    
   if ([DataTool isEmptyString:[url absoluteString]]) { //如果连接为空，则不继续执行
       return;
   }
    
    self.movieURL = url;
    [self loadData];
    [self addNotification];
    
     [_loadActivity startAnimating];
}


- (void)play{
    _isPlaying = YES;
    [_player play];
    [self.playBtn setImage:nil forState:UIControlStateNormal];
}

- (void)pause{
    _isPlaying = NO;
    [_player pause];
    [self.playBtn setImage:[UIImage imageNamed:@"userprofile_play"] forState:UIControlStateNormal];
}

- (void)setMute {
    _player.muted = YES;
}
- (void)setLoopPay {
    _isLoopPlay = YES;
}
- (void) setLookMeProfileState {
    isLookMe = YES;
}
- (void) setLookOtherProfileState {
    isLookMe = NO;
    _isLoopPlay = YES;
}
- (void) setSparkState {
    _player.muted = YES;
    _isLoopPlay = YES;
//    isSpark = YES;
    
    self.connectTipView.hidden = NO;
    self.playBtn.hidden = YES;
}

- (void)vipAction {
    [DataTool showGetVipViewWithIndex:0];
}
- (void)playORPause {
    if (_isLoadFinish) {
        if (_isPlaying) {
            [self pause];
        }else{
            [self play];
        }
    }
}

//设置在连接中
- (void)setPlayerConnecting {
}

-(void)addNotification{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playbackFinished:) name:AVPlayerItemDidPlayToEndTimeNotification object:nil]; //给AVPlayerItem添加播放完成通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enterForegroundNotification) name:UIApplicationWillEnterForegroundNotification object:nil]; // 后台&前台通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resignActiveNotification) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
}

-(void)playbackFinished:(NSNotification *)notification{
    _playerItem = [notification object];
    [_playerItem seekToTime:kCMTimeZero];
    
    if (_isLoopPlay) {
        [self play];
    } else {
        [self pause];
    }
    
}

- (void)resignActiveNotification{ // 后台
    //    _isIntoBackground = YES;
    [self pause];
}

- (void)enterForegroundNotification { // 前台
    //    _isIntoBackground = NO;
    [self play];
}

- (void)loadData {
    
    NSString *name = [[NSFileManager defaultManager] displayNameAtPath:self.movieURL.path];
    NSString *filePath = [[[self class] filePath] stringByAppendingPathComponent:name];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    if (fileExists) {
        self.playerURL = [NSURL fileURLWithPath:filePath];
        [self startPlay];
    } else {
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        queue.maxConcurrentOperationCount = 1;
        self.session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:self delegateQueue:queue];
        
        self.task = [self.session downloadTaskWithURL:self.movieURL];
        [self.task resume];
    }
}

- (void)startPlay {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self->_isLoadFinish = YES;
        [self->_loadActivity stopAnimating];
        self.playerItem = [AVPlayerItem playerItemWithURL:self.playerURL];
        [self.player replaceCurrentItemWithPlayerItem:self.playerItem];
        
        self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspect; //设置视频显示模式、
        
        self.connectTipView.hidden = YES;
        
        if ([[UserInfo shareInstant].user.vipState isEqualToString:@"VIP"]) {
            self.vipTipView.hidden = YES;
        } else {
            self.vipTipView.hidden = NO;
        }
        if (self->isLookMe) {
            self.vipTipView.hidden = YES;
        }
        
        if (self->_isLoopPlay) {
            [self play];
        } else {
            [self pause];
        }
        
    });
}

#pragma mark NSURLSessionDelegate
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location {
    NSString *name = [[NSFileManager defaultManager] displayNameAtPath:downloadTask.currentRequest.URL.path];
    NSString *filePath = [[[self class] filePath] stringByAppendingPathComponent:name];
    [[NSFileManager defaultManager] moveItemAtPath:location.path toPath:filePath error:nil];
    
    self.playerURL = [NSURL fileURLWithPath:filePath];
    
    [self startPlay];
    
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(nullable NSError *)error {
    NSString *name = [[NSFileManager defaultManager] displayNameAtPath:task.currentRequest.URL.path];
    NSString *filePath = [[[self class] filePath] stringByAppendingPathComponent:name];
    BOOL isExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    
    if (isExists) {
        self.playerURL = [NSURL fileURLWithPath:filePath];
        [self startPlay];
    }
}

#pragma mark 文件管理
+ (NSString *)filePath {
    NSString *path = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES).firstObject;
    NSString *filePath = [path stringByAppendingPathComponent:@"wj_movie_file"];
    BOOL isExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    if (!isExists) {
        [[NSFileManager defaultManager] createDirectoryAtPath:filePath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return filePath;
}

@end

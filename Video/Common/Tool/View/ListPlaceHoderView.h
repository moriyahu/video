//
//  ListPlaceHoderView.h
//  Video
//
//  Created by steve on 2020/4/26.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ListPlaceHoderView : UIView

- (instancetype) initWithFrame:(CGRect)frame tipImgName:(NSString *)tipImgName tipsInfo:(NSString *)tipsInfo ;

@end

NS_ASSUME_NONNULL_END

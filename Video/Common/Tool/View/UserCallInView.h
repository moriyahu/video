//
//  UserCallInView.h
//  Video
//
//  Created by steve on 2020/5/21.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserCallInView : UIView

@property (nonatomic, copy) ClickBlock hungUpBlock;
@property (nonatomic, copy) ClickBlock getThoughBlock;
@property (nonatomic, strong) UserProfileDataModel *profileModel;

@end

NS_ASSUME_NONNULL_END

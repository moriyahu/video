//
//  NTUIPageControl.m
//  newTindar
//
//  Created by steve on 2019/4/14.
//  Copyright © 2019 handu. All rights reserved.
//

#import "NTUIPageControl.h"

#define dotW ZoomSize(10)
#define dotH ZoomSize(3)
#define magrin ZoomSize(3)

@interface NTUIPageControl()

@property (nonatomic, assign) CGRect viewFrame;

@end

@implementation NTUIPageControl

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _viewFrame = frame;
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat marginX = dotW + magrin;
    CGFloat newW = (self.subviews.count - 1 ) * marginX;
    self.frame = CGRectMake( _viewFrame.size.width/2-(newW + dotW)/2, _viewFrame.origin.y, newW + dotW, _viewFrame.size.height);
    
    for (int i=0; i<[self.subviews count]; i++) {
        UIImageView* dot = [self.subviews objectAtIndex:i];
        dot.contentMode = UIViewContentModeScaleAspectFit;
        
        if (i == self.currentPage) {
            [dot setFrame:CGRectMake(i * marginX, dot.frame.origin.y, dotW, dotH)];
        }else {
            [dot setFrame:CGRectMake(i * marginX, dot.frame.origin.y, dotW, dotH)];
        }
    }
}

//- (void)layoutSubviews {
//    [super layoutSubviews];
//
//
//
//}

@end

//
//  ListPlaceHoderActionView.h
//  Video
//
//  Created by steve on 2020/4/26.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TIMConversation.h>
#import "V2TIMManager+Conversation.h"

NS_ASSUME_NONNULL_BEGIN

@interface ListPlaceHoderActionView : UIView

- (instancetype) initWithFrame:(CGRect)frame tipImgName:(NSString *)tipImgName viewTitle:(NSString *)viewTitle tipsInfo:(NSString *)tipsInfo btTitle:(NSString *)btTitle ;

@property (nonatomic, copy) ClickBlock actionBlock;

@property (nonatomic, strong ) V2TIMConversation *systemConv;

@end

NS_ASSUME_NONNULL_END

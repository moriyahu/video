//
//  UserCallOutView.h
//  Video
//
//  Created by steve on 2020/6/13.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserCallOutView : UIView

@property (nonatomic, strong) UserProfileDataModel *profileModel;

- (instancetype) initWithFrame:(CGRect)frame;
- (void)show;
- (void)dismiss;

@end

NS_ASSUME_NONNULL_END

//
//  UserChooseGiftView.m
//  Video
//
//  Created by steve on 2020/6/8.
//  Copyright © 2020 steve. All rights reserved.
//

#import "UserChooseGiftView.h"
#import <MJExtension/MJExtension.h>

@interface UserChooseGiftView() {
    UILabel *userCoinsLb;
}

@end

@implementation UserChooseGiftView

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews:frame];
        [self setData];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setData) name:NeedRefleshProfilePage object:nil]; //编辑用户设置
    }
    return self;
}

- (void)initSubviews:(CGRect) frame {
    self.frame = frame;
    
    float giftBtWidth = SCREEN_Width / 4;
    float giftBtHeigth = ZoomSize(110);
    
    for (int i = 0; i < 8; i++) {
        
        UIButton *giftBt = [[UIButton alloc] initWithFrame:CGRectMake( giftBtWidth * (i%4), giftBtHeigth * (i/4) , giftBtWidth, giftBtHeigth)];
        giftBt.tag = 100 + i;
        [self addSubview:giftBt];
        [giftBt addTarget:self action:@selector(giftAction:) forControlEvents:UIControlEventTouchUpInside];
        
        UIImageView *giftIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(5), ZoomSize(0), giftBtWidth - ZoomSize(10), ZoomSize(73))];
        giftIV.contentMode = UIViewContentModeScaleAspectFit;
        giftIV.image = GetImageByName(GiftImgArray[i]);
        [giftBt addSubview:giftIV];
        //
        UIImageView *coinIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(16), ZoomSize(76), ZoomSize(20), ZoomSize(20))];
        coinIV.image = GetImageByName(@"gift_mycoins");
        [giftBt addSubview:coinIV];
        
        UILabel *coinNumLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(40), ZoomSize(76), ZoomSize(100), ZoomSize(20))];
        coinNumLb.textAlignment = NSTextAlignmentLeft;
        coinNumLb.textColor = WhiteColor;
        coinNumLb.font = [UIFont systemFontOfSize:ZoomSize(14)];
        [giftBt addSubview:coinNumLb];
        coinNumLb.text = GiftCoinNumArray[i];
        
    }
    
    UIImageView *coinIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(16), ZoomSize(254), ZoomSize(30), ZoomSize(30))];
    coinIV.image = GetImageByName(@"gift_mycoins");
    [self addSubview:coinIV];
    
    userCoinsLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(52), ZoomSize(256), ZoomSize(100), ZoomSize(22))];
    userCoinsLb.textAlignment = NSTextAlignmentLeft;
    userCoinsLb.textColor = WhiteColor;
    userCoinsLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightSemibold];
    [self addSubview:userCoinsLb];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, ZoomSize(229), SCREEN_Width, ZoomSize(1))];
    lineView.backgroundColor = TextGrayColor;
    [self addSubview:lineView];
    
    UIButton *buyBt = [DataTool createCommomBtWithFrame:CGRectMake(ZoomSize(220), ZoomSize(242), ZoomSize(137), ZoomSize(48)) andTitle:@"Buy Now"];
    [self addSubview:buyBt];
    [buyBt addTarget:self action:@selector(buyAction) forControlEvents:UIControlEventTouchUpInside];
    
}

#pragma function


- (void) setData {
    userCoinsLb.text = [UserInfo shareInstant].user.balance;
}

- (void) buyAction {
    [DataTool showGetCoinsView];
}

- (void) giftAction:(UIButton *)sender  {
    NSLog(@"gift %ld", (long)sender.tag);
    long giftIndex = sender.tag - 100;
    NSString *giftIndexString = [NSString stringWithFormat:@"%ld", giftIndex];
    
    if (self->_sendGiftBlock) {
        self->_sendGiftBlock(giftIndexString);
    }
    
}

@end

//
//  TRTCShowView.m
//  Video
//
//  Created by steve on 2020/5/11.
//  Copyright © 2020 steve. All rights reserved.
//

#import "TRTCShowView.h"

@interface TRTCShowView() {
}

//@property (nonatomic, strong) UIView *contentView;

@end

@implementation TRTCShowView

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews:frame];
    }
    return self;
}

- (void)initSubviews:(CGRect) frame {
    self.frame = frame;
    self.backgroundColor = BGDarkBlueColor;
}

#pragma function

- (void)show {
    
    [[[[[[UIApplication sharedApplication] delegate] window] rootViewController] view] addSubview:self]; //使用window，可以解决隐藏tabbar的问题
    
    [UIView animateWithDuration:0.25 animations:^{
//        [self.contentView setFrame:CGRectMake( ZoomSize(38), (SCREEN_Height - ZoomSize(440) )/2.0, ZoomSize(300) ,ZoomSize(440))];
    }];
}

- (void)dismiss {
    [UIView animateWithDuration:0.25 animations:^{
//        [self.contentView setFrame:CGRectMake( ZoomSize(38), (SCREEN_Height - ZoomSize(440) )/2.0 + SCREEN_Height, ZoomSize(300) ,ZoomSize(440))];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma lazy

//- (UIView *)contentView {
//    if (!_contentView) {
//
//        _contentView = [[UIView alloc] initWithFrame:CGRectMake( ZoomSize(38), (SCREEN_Height - ZoomSize(440) )/2.0 + SCREEN_Height, ZoomSize(300) ,ZoomSize(440))];
//        _contentView.backgroundColor = [UIColor whiteColor];
//        _contentView.layer.cornerRadius = ZoomSize(16);
//        _contentView.layer.masksToBounds = YES;
//
//    }
//    return _contentView;
//}

@end

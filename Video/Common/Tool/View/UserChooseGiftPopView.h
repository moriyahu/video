//
//  UserChooseGiftPopView.h
//  Video
//
//  Created by steve on 2020/6/8.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserChooseGiftPopView : UIView

@property (nonatomic, copy) CallBack sendGiftBlock;

- (void)show;

- (void)dismiss;

@end

NS_ASSUME_NONNULL_END

//
//  UserMatchPopView.h
//  Video
//
//  Created by steve on 2020/5/22.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserMatchPopView : UIView

@property (nonatomic, strong) NSString *roomId;
@property (nonatomic, strong) UserProfileDataModel *dataModel;
@property (nonatomic, strong) NSString *showType; // CallOut, CallIn, LikeMe

- (void)show;
- (void)dismiss;

@end

NS_ASSUME_NONNULL_END

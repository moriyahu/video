//
//  AgePickView.h
//  Video
//
//  Created by steve on 2020/4/15.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AgePickView : UIView

@property(nonatomic,strong) void(^back)(NSString *callBackString);
@property(nonatomic,strong) NSString *titleString;

- (void)show;

- (void)dismiss;

@end

NS_ASSUME_NONNULL_END

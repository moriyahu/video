//
//  NTUIPageControl.h
//  newTindar
//
//  Created by steve on 2019/4/14.
//  Copyright © 2019 handu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NTUIPageControl : UIPageControl

- (instancetype) initWithFrame:(CGRect)frame ;


@end

NS_ASSUME_NONNULL_END

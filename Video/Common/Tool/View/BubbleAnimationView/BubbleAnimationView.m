//
//  BubbleAnimationView.m
//  BubbleAnimation
//
//  Created by Brammanand Soni on 9/8/15.
//  Copyright (c) 2015 Brammanand Soni. All rights reserved.
//

#import "BubbleAnimationView.h"
#import "BubbleConfig.h"
#import "BubbleInfo.h"

#define ZOOM_ANIMATION_DURATION 0.3

@interface BubbleAnimationView () {
    NSInteger previousIndex;
}

@property(nonatomic, strong) NSArray *bubbleInfoArray;
@property(nonatomic, strong) NSMutableArray *bubbleViewsArray;
@property (nonatomic ,strong) NSTimer *animationTimer;

//@property (nonatomic, strong) UIView *selectedBubbleView;
//@property (nonatomic, strong) UIView *overlayBubbleView;
@property (strong, nonatomic) UIView *bubbleContainerView;
//@property (nonatomic, strong) UITapGestureRecognizer *tapOutsideDismiss;

@end

@implementation BubbleAnimationView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

#pragma mark Private Methods

- (void)animateBuubleViews {
    
    self.bubbleContainerView = [[UIView alloc] initWithFrame:self.frame];
    self.bubbleContainerView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.bubbleContainerView];
    
    self.bubbleInfoArray = [BubbleConfig getBubbleInfoArray];
    self.bubbleViewsArray = [NSMutableArray array];
    NSArray *frameArray = [self noRepeatRandomArrayWithMinNum:0 maxNum:9 count:10];
    NSArray *imageArray = [self noRepeatRandomArrayWithMinNum:0 maxNum:9 count:10];
    
    for (int i = 0; i < 10; i++) {
        
        int imageIndex = [imageArray[i] intValue];
        NSString *headIVString = [NSString stringWithFormat:@"woman%d", imageIndex];
        if ([[UserInfo shareInstant].user.sex isEqualToString:@"FEMALE"]) {
            headIVString = [NSString stringWithFormat:@"man%d", imageIndex];
        }
        
        int frameIndex = [frameArray[i] intValue];
        BubbleInfo *info = self.bubbleInfoArray[frameIndex];
        
        UIImageView *headIV = [[UIImageView alloc] initWithFrame:info.frame];
        headIV.layer.cornerRadius = headIV.frame.size.width / 2;
        headIV.clipsToBounds = YES;
        headIV.hidden = YES;
        headIV.image = GetImageByName(headIVString);
        headIV.contentMode = UIViewContentModeScaleAspectFill;
        
        [self.bubbleContainerView addSubview:headIV];
        [self.bubbleViewsArray addObject:headIV];
        
    }
    
    [self performSelector:@selector(animateBubbleViews:) withObject:nil afterDelay:1];
}

- (void)animateBubbleViews:(id)sender {
    self.animationTimer = [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(animateSingleBubble:) userInfo:nil repeats:YES];
}

- (void)animateSingleBubble:(id)snder {
    if (previousIndex < self.bubbleViewsArray.count) {
        UIView *view = [self.bubbleViewsArray objectAtIndex:previousIndex ++];
        view.hidden = NO;
        [self animateView:view];
    }
}

- (void)animateView:(UIView *)view {
    view.transform = CGAffineTransformMakeScale(0.3, 0.3);
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.30 initialSpringVelocity:5.0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        view.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
    }];
}

- (NSArray *)noRepeatRandomArrayWithMinNum:(NSInteger)min maxNum:(NSInteger )max count:(NSInteger)count{
    
    NSMutableArray <NSNumber *>*randomValues = [NSMutableArray array];//保存随机数
    NSInteger value;
    for (int i = 0; i < count; i++) {
        do {
            value = arc4random() % (max-min+1) + min;
        } while ([randomValues containsObject:[NSNumber numberWithInteger:value]]);
        
        [randomValues addObject:[NSNumber numberWithInteger:value]];
    }
    return randomValues;
}


@end

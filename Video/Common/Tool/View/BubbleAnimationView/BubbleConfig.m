//
//  BubbleConfig.m
//  BubbleAnimation
//
//  Created by Brammanand Soni on 9/2/15.
//  Copyright (c) 2015 Brammanand Soni. All rights reserved.
//

#import "BubbleConfig.h"

#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height

#define X_PROP 1
#define Y_PROP 1

#define SIZE_PROP SCREEN_WIDTH / 320

@implementation BubbleConfig

+(NSArray *)getBubbleInfoArray {
    NSMutableArray *createProDataArray = [NSMutableArray array];
    
    {
        BubbleInfo *info = [[BubbleInfo alloc] init];
        info.frame = CGRectMake(ZoomSize(135), ZoomSize(9), ZoomSize(31), ZoomSize(31));
        [createProDataArray addObject:info];
    }
    
    {
        BubbleInfo *info = [[BubbleInfo alloc] init];
        info.frame = CGRectMake(ZoomSize(241), ZoomSize(32) , ZoomSize(45), ZoomSize(45));
        [createProDataArray addObject:info];
    }
    
    {
        BubbleInfo *info = [[BubbleInfo alloc] init];
        info.frame = CGRectMake( ZoomSize(76) , ZoomSize(78) , ZoomSize(64), ZoomSize(64));
        [createProDataArray addObject:info];
    }
    
    {
        BubbleInfo *info = [[BubbleInfo alloc] init];
        info.frame = CGRectMake( ZoomSize(173), ZoomSize(105), ZoomSize(74), ZoomSize(74));
        
        [createProDataArray addObject:info];
    }
    
    {
        BubbleInfo *info = [[BubbleInfo alloc] init];
        info.frame = CGRectMake( ZoomSize(325), ZoomSize(153), ZoomSize(26), ZoomSize(26));
        [createProDataArray addObject:info];
    }
    
    {
        BubbleInfo *info = [[BubbleInfo alloc] init];
        info.frame = CGRectMake(ZoomSize(51), ZoomSize(171), ZoomSize(33) , ZoomSize(33));
        [createProDataArray addObject:info];
    }
    
    {
        BubbleInfo *info = [[BubbleInfo alloc] init];
        info.frame = CGRectMake( ZoomSize(258) , ZoomSize(191), ZoomSize(41), ZoomSize(41));
        [createProDataArray addObject:info];
    }
    
    {
        BubbleInfo *info = [[BubbleInfo alloc] init];
        info.frame = CGRectMake(ZoomSize(123), ZoomSize(212), ZoomSize(56), ZoomSize(56));
        [createProDataArray addObject:info];
    }
    
    {
        BubbleInfo *info = [[BubbleInfo alloc] init];
        info.frame = CGRectMake( ZoomSize(272), ZoomSize(261), ZoomSize(35), ZoomSize(35));
        [createProDataArray addObject:info];
    }
    
    {
        BubbleInfo *info = [[BubbleInfo alloc] init];
        info.frame = CGRectMake( ZoomSize(71), ZoomSize(271), ZoomSize(31), ZoomSize(31));
        [createProDataArray addObject:info];
    }
    
    return createProDataArray;
}


@end

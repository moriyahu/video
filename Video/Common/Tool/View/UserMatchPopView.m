//
//  UserMatchPopView.m
//  Video
//
//  Created by steve on 2020/5/22.
//  Copyright © 2020 steve. All rights reserved.
//

#import "TUIKitConfig.h"
#import "TUIKit.h"
#import "UserMatchPopView.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface UserMatchPopView() {
    UILabel *titleLb;
    UIImageView *leftIV;
    UIImageView *rightIV;
    UILabel *tipInfoLb;
    
    UIView *callOutFuncView;
    UIView *callInFuncView;
    UIView *likeMeFuncView;
}

@property (nonatomic, strong) UIView *contentView;

@end

@implementation UserMatchPopView

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews:frame];
    }
    return self;
}

- (void)initSubviews:(CGRect) frame {
    self.frame = frame;
    [self addSubview:self.contentView];
    [self performSelector:@selector(dismiss) withObject:nil afterDelay:30];
}

#pragma function

- (void)show {
    
    [[[[[[UIApplication sharedApplication] delegate] window] rootViewController] view] addSubview:self]; //使用window，可以解决隐藏tabbar的问题
    [UIView animateWithDuration:0.25 animations:^{
        [self.contentView setFrame:CGRectMake(0, 0 , SCREEN_Width, SCREEN_Height)];
    }];
}

- (void)dismiss {
    [UIView animateWithDuration:0.25 animations:^{
        [self.contentView setFrame:CGRectMake( 0, SCREEN_Height, SCREEN_Width ,SCREEN_Height)];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)callOutHangUpAction {
    [self dismiss];
    [[UserInfo shareInstant] sendImMessageToOther:_dataModel.userId messgeTextString:@"[VideoCall_Cancel]" roomId:_roomId];
}

- (void)callInHangUpAction {
    [self dismiss];
    [[UserInfo shareInstant] sendImMessageToOther:_dataModel.userId messgeTextString:@"[VideoCall_Denny]" roomId:_roomId];
}

- (void)callInAnswerAction {
    [self dismiss];
    
    [[UserInfo shareInstant] sendImMessageToOther:_dataModel.userId messgeTextString:@"[VideoCall_Accept]" roomId:_roomId];
    [[UserInfo shareInstant] showTrtcPage:_dataModel roomId:_roomId callType:@"CallIn"];
}


- (void)setDataModel:(UserProfileDataModel *)dataModel {
    _dataModel = dataModel;
    
    titleLb.text = [NSString stringWithFormat:@"You and %@ liked each other",dataModel.nickName];
    [leftIV sd_setImageWithURL:[NSURL URLWithString:[UserInfo shareInstant].user.headPortraitUrl] placeholderImage:GetImageByName(@"message_vido")];
    [rightIV sd_setImageWithURL:[NSURL URLWithString:dataModel.headPortraitUrl] placeholderImage:GetImageByName(@"message_vido")];
    
}

- (void)setRoomId:(NSString *)roomId {
    _roomId = roomId;
    
}

- (void)setShowType:(NSString *)showType {
    if ([showType isEqualToString:@"CallOut"]) {
        callOutFuncView.hidden = NO;
        tipInfoLb.text = @"calling ...";
    } else if ([showType isEqualToString:@"CallIn"]) {
        callInFuncView.hidden = NO;
        tipInfoLb.text =[NSString stringWithFormat:@"%@ invites you to a video call",_dataModel.nickName];
    } else {
        likeMeFuncView.hidden = NO;
        tipInfoLb.hidden = YES;
    }
}

#pragma lazy
- (UIView *)contentView {
    if (!_contentView) {
        
        _contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width ,SCREEN_Height)];
        
        UIVisualEffectView * visualView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleDark]];
        visualView.frame = self.contentView.bounds;
        visualView.alpha = 0.9;
        [self addSubview:visualView];
        
        UIButton *closeBt = [[UIButton alloc] initWithFrame:CGRectMake( ZoomSize(12), StatusBarHeight + ZoomSize(12), ZoomSize(30), ZoomSize(30))];
        [closeBt addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
        [closeBt setImage:[UIImage imageNamed:@"video_close"] forState:UIControlStateNormal];
        [_contentView addSubview:closeBt];
        
        UIImageView *matchTipIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(64), StatusBarHeight + ZoomSize(20), ZoomSize(313), ZoomSize(157))];
        matchTipIV.contentMode = UIViewContentModeScaleAspectFit;
        matchTipIV.image = GetImageByName(@"pop_match_img");
        [_contentView addSubview:matchTipIV];
        
        titleLb = [[UILabel alloc] initWithFrame:CGRectMake( ZoomSize(50), CGRectGetMaxY(matchTipIV.frame) + ZoomSize(25), ZoomSize(275), ZoomSize(22))];
        [_contentView addSubview:titleLb];
        //        titleLb.text = @"You and Dareen liked each other";
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(18)];
        
        leftIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(72), CGRectGetMaxY(titleLb.frame) + ZoomSize(44), ZoomSize(116), ZoomSize(116))];
        [_contentView addSubview:leftIV];
        leftIV.layer.cornerRadius = ZoomSize(58);
        leftIV.layer.masksToBounds = YES;
        leftIV.contentMode = UIViewContentModeScaleAspectFill;
        
        rightIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(188), CGRectGetMaxY(titleLb.frame) + ZoomSize(44), ZoomSize(116), ZoomSize(116))];
        [_contentView addSubview:rightIV];
        rightIV.layer.cornerRadius = ZoomSize(58);
        rightIV.layer.masksToBounds = YES;
        rightIV.contentMode = UIViewContentModeScaleAspectFill;
        //        rightIV.image = GetImageByName(@"testUser2");
        
        UIImageView *likeTipIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(147), CGRectGetMaxY(titleLb.frame) + ZoomSize(61), ZoomSize(80), ZoomSize(80))];
        [_contentView addSubview:likeTipIV];
        likeTipIV.layer.cornerRadius = ZoomSize(58);
        likeTipIV.image = GetImageByName(@"pop_match_heart");
        
        tipInfoLb = [[UILabel alloc] initWithFrame:CGRectMake( ZoomSize(50), CGRectGetMaxY(leftIV.frame) + ZoomSize(38), ZoomSize(275), ZoomSize(22))];
        [_contentView addSubview:tipInfoLb];
        //        tipInfoLb.text = @"Dareen is calling you…";
        tipInfoLb.textAlignment = NSTextAlignmentCenter;
        tipInfoLb.textColor = WhiteColor;
        tipInfoLb.font = [UIFont systemFontOfSize:ZoomSize(16)];
        
#pragma callOutFuncView
        callOutFuncView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(tipInfoLb.frame) + ZoomSize(70), SCREEN_Width, ZoomSize(120))];
        [_contentView addSubview:callOutFuncView];
        callOutFuncView.hidden = YES;
        
        UIButton *callOutHangUpBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(159), 0, ZoomSize(66), ZoomSize(66))];
        [callOutFuncView addSubview:callOutHangUpBt];
        [callOutHangUpBt setImage:GetImageByName(@"pop_hangup") forState:UIControlStateNormal];
        [callOutHangUpBt addTarget:self action:@selector(callOutHangUpAction) forControlEvents:UIControlEventTouchUpInside];
        
#pragma callInFuncView
        callInFuncView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(tipInfoLb.frame) + ZoomSize(70), SCREEN_Width, ZoomSize(120))];
        [_contentView addSubview:callInFuncView];
        callInFuncView.hidden = YES;
        
        UIButton *callInHangUpBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(72), 0, ZoomSize(66), ZoomSize(66))];
        [callInFuncView addSubview:callInHangUpBt];
        [callInHangUpBt setImage:GetImageByName(@"pop_hangup") forState:UIControlStateNormal];
        [callInHangUpBt addTarget:self action:@selector(callInHangUpAction) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *callInGetThoughBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(235), 0, ZoomSize(66), ZoomSize(66))];
        [callInFuncView addSubview:callInGetThoughBt];
        [callInGetThoughBt setImage:GetImageByName(@"pop_answer") forState:UIControlStateNormal];
        [callInGetThoughBt addTarget:self action:@selector(callInAnswerAction) forControlEvents:UIControlEventTouchUpInside];
        
        
        //旋转动画
        CAKeyframeAnimation * keyAnimaion = [CAKeyframeAnimation animation];
        keyAnimaion.keyPath = @"transform.rotation";
        keyAnimaion.values = @[@(-10 / 180.0 * M_PI),@(10 /180.0 * M_PI),@(-10/ 180.0 * M_PI)];//度数转弧度
        
        keyAnimaion.removedOnCompletion = NO;
        keyAnimaion.fillMode = kCAFillModeForwards;
        keyAnimaion.duration = 0.3;
        keyAnimaion.repeatCount = MAXFLOAT;
        [callInGetThoughBt.layer addAnimation:keyAnimaion forKey:nil];
        
        //    上下动画
        CALayer *viewLayer = callInGetThoughBt.layer;
        CGPoint position = viewLayer.position;
        CGPoint startPoint = CGPointMake(position.x , position.y + ZoomSize(5));
        CGPoint endPoint = CGPointMake(position.x , position.y - ZoomSize(5));
        
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
        [animation setFromValue:[NSValue valueWithCGPoint:startPoint]];
        [animation setToValue:[NSValue valueWithCGPoint:endPoint]];
        [animation setAutoreverses:YES];
        [animation setDuration:.3];
        [animation setRepeatCount:MAXFLOAT];
        [viewLayer addAnimation:animation forKey:nil];
        
        
#pragma likeMeFuncView
        likeMeFuncView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(tipInfoLb.frame) + ZoomSize(70), SCREEN_Width, ZoomSize(120))];
        [_contentView addSubview:likeMeFuncView];
        likeMeFuncView.hidden = YES;
        
        UIButton *inviteBt = [DataTool createCommomBtWithFrame:CGRectMake(ZoomSize(30), 0, ZoomSize(315), ZoomSize(48)) andTitle:@"Invite to Video Chat"];
        [likeMeFuncView addSubview:inviteBt];
        //        [inviteBt addTarget:self action:@selector(recordAction) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *messageBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(30), ZoomSize(70), ZoomSize(315), ZoomSize(48))];
        messageBt.layer.cornerRadius = ZoomSize(24);
        messageBt.layer.masksToBounds = YES;
        [likeMeFuncView addSubview:messageBt];
        [messageBt setTitle:@"Send a message" forState:UIControlStateNormal];
        messageBt.titleLabel.textColor = WhiteColor;
        messageBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightSemibold];
        messageBt.layer.borderColor = WhiteColor.CGColor;
        messageBt.layer.borderWidth = ZoomSize(2);
        
    }
    return _contentView;
}


@end

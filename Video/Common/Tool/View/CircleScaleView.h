//
//  CircleScaleView.h
//  ChartCollection
//
//  Created by anyongxue on 2016/12/28.
//  Copyright © 2016年 cc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CircleScaleView : UIView

@property (nonatomic,assign)CGContextRef ctx;

@property (nonatomic,strong)UIColor* bgColor;//背景颜色
@property (nonatomic,strong)UIColor* showColor;//显示颜色

@property (nonatomic,assign)float lineWidth; //线的粗细
@property (nonatomic,assign)float durationTime;//持续时间

- (void)startAnimation;
- (void)stopAnimation ;
//- (void)startCountDown;
//- (void)startRecordCount;

@end

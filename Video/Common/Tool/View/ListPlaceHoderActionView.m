//
//  ListPlaceHoderActionView.m
//  Video
//
//  Created by steve on 2020/4/26.
//  Copyright © 2020 steve. All rights reserved.
//

#import "ListPlaceHoderActionView.h"
#import "MessageListTableViewCell.h"
#import "OfficialChatListViewController.h"

#import "TUIConversationListController.h"
#import "TUIChatController.h"


@interface ListPlaceHoderActionView()<TUIConversationListControllerDelegagte>{
    NSString *tipImgNameString;
    NSString *viewTitleString;
    NSString *tipsInfoString;
    NSString *btTitleString;
    
    MessageListTableViewCell *officialCell;
}

@end

@implementation ListPlaceHoderActionView

- (instancetype) initWithFrame:(CGRect)frame tipImgName:(NSString *)tipImgName viewTitle:(NSString *)viewTitle tipsInfo:(NSString *)tipsInfo btTitle:(NSString *)btTitle {
    
    self = [super initWithFrame:frame];
    if (self) {
        tipImgNameString = tipImgName;
        viewTitleString = viewTitle;
        tipsInfoString = tipsInfo;
        btTitleString = btTitle;
        [self initSubviews:frame];
    }
    return self;
    
}

- (void)initSubviews:(CGRect) frame {
    
    self.backgroundColor = UIColor.whiteColor;
    
    if ([viewTitleString isEqualToString:@"Meet new friends now!"]) {
        
        officialCell = [MessageListTableViewCell initView];
        officialCell.frame = CGRectMake(0, 0, SCREEN_Width, ZoomSize(94));
        officialCell.actionBlock = ^{
            
            OfficialChatListViewController *vc = [[OfficialChatListViewController alloc] init];
            [[DataTool getCurrentVC].navigationController pushViewController:vc animated:YES];
            
        };
        officialCell.messageLb.text = [NSString stringWithFormat:@"Hello, %@, welcome to Vido", [UserInfo shareInstant].user.nickName];
        [self addSubview:officialCell];
    }
    
    UIImageView *centerImgV = [[UIImageView alloc] initWithFrame:CGRectMake(( frame.size.width - ZoomSize(50))/2.0, (frame.size.height)/2.0 - ZoomSize(80), ZoomSize(50), ZoomSize(50))];
    [self addSubview:centerImgV];
    centerImgV.image = [UIImage imageNamed:tipImgNameString];
    centerImgV.contentMode = UIViewContentModeScaleAspectFill;
    
    
    UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(60), CGRectGetMaxY(centerImgV.frame) + ZoomSize(15), SCREEN_Width - ZoomSize(120), ZoomSize(21))];
    titleLb.text = viewTitleString;
    titleLb.textAlignment = NSTextAlignmentCenter;
    titleLb.textColor = BGDarkBlueColor;
    titleLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightSemibold];
    [self addSubview:titleLb];
    
    
    UILabel *infoLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(20), CGRectGetMaxY(titleLb.frame), SCREEN_Width - ZoomSize(40), ZoomSize(45))];
    [self addSubview:infoLb];
    infoLb.text = tipsInfoString;
    infoLb.numberOfLines = 0;
    infoLb.textAlignment = NSTextAlignmentLeft;
    infoLb.font = [UIFont systemFontOfSize:ZoomSize(15)];
    infoLb.textAlignment = NSTextAlignmentCenter;
    infoLb.textColor = RGBA(126, 116, 136, 1);
    
    UIButton *actionBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(20), CGRectGetMaxY(infoLb.frame) + ZoomSize(20), SCREEN_Width - ZoomSize(40), ZoomSize(48))];
    [self addSubview:actionBt];
    [actionBt addTarget:self action:@selector(submitAction) forControlEvents:UIControlEventTouchUpInside];
    //    submitBt.backgroundColor = UIColor.whiteColor;
    [actionBt setTitle:btTitleString forState:UIControlStateNormal];
    [actionBt setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    actionBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightSemibold];
    [actionBt setBackgroundImage:GetImageByName(@"btBG_enable") forState:UIControlStateNormal];

}

- (void)setSystemConv:(V2TIMConversation *)systemConv {
    
    officialCell.conv = systemConv;
    
    
}

- (void)submitAction {
//    NSLog(@"submitAction");
    
    if (self.actionBlock) {
        self.actionBlock();
    }
}

- (void)conversationListController:(TUIConversationListController *)conversationController didSelectConversation:(TUIConversationCell *)conversation {
    // 会话列表点击事件，通常是打开聊天界面
    
}


@end

//
//  UserCallOutView.m
//  Video
//
//  Created by steve on 2020/6/13.
//  Copyright © 2020 steve. All rights reserved.
//

#import "UserCallOutView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "TUIKitConfig.h"
#import "TUIKit.h"

@interface UserCallOutView() {
    
    UIImageView *logoBGIV;
    UIImageView *logoIV;
    UILabel *nameLb;
    UIImageView *onlineLogoIV;
    UIImageView *verifyLogoIV;
    
    UIImageView *nationIV;
    UILabel *nationLb;
    UILabel *aboutLb;
    UILabel *timeLb;
    
    NSTimer *codeTimer;
    int callCheckTime;
}

@property (nonatomic, strong) UIView *contentView;

@end

@implementation UserCallOutView


- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews:frame];
        [self addSubview:self.contentView];
    }
    return self;
}

- (void)initSubviews:(CGRect) frame {
    self.frame = frame;
    [self performSelector:@selector(dismiss) withObject:nil afterDelay:33];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismiss) name:NeedDismissCallOutView object:nil]; //编辑用户设置
}

- (void)setProfileModel:(UserProfileDataModel *)profileModel {
    _profileModel = profileModel;
    
    [logoBGIV sd_setImageWithURL:[NSURL URLWithString:profileModel.headPortraitUrl] placeholderImage:GetImageByName(@"message_vido")];
    [logoIV sd_setImageWithURL:[NSURL URLWithString:profileModel.headPortraitUrl] placeholderImage:GetImageByName(@"message_vido")];
    nameLb.text = [NSString stringWithFormat:@"%@,%ld", profileModel.nickName, [DataTool getUserAgeFromString:profileModel.birthday]];
    
    if ([_profileModel.onlineState isEqualToString:@"ONLINE"]) {
        onlineLogoIV.hidden = NO;
    }
    if (_profileModel.videos.count > 0) {
        verifyLogoIV.hidden = NO;
    }
    
    nationIV.image = [DataTool getImageForCountryName:_profileModel.country];
    nationLb.text = _profileModel.country;
    aboutLb.text = _profileModel.aboutMe;
    
    NSDictionary *paramsDic = @{@"receiverId": _profileModel.userId};
    [[NetTool shareInstance] startRequest:API_Room_Create method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
        
        
        NSDictionary *responseDic = obj;
        int resultCode = [responseDic[@"code"] intValue];
        NSDictionary *resultDic = [[NSDictionary alloc] init];
        resultDic = responseDic[@"data"];
        
        
        if (resultCode == 8200) {
            NSString *roomIdString = resultDic[@"roomId"];
            [[UserInfo shareInstant] sendImMessageToOther:profileModel.userId messgeTextString:@"[direct_VideoCall]" roomId:roomIdString];
            [[UserInfo shareInstant] sendTextMessageToOther:profileModel.userId messgeTextString:@"[VideoCall]"];
            [self startTimer];

        }  else {
            
            [[NetTool shareInstance] startRequest:API_Room_Clean method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
                
            }];
            
            [DataTool showHUDWithString:@"user may not be online, please retry later"];
            [[UserInfo shareInstant] sendTextMessageToOther:profileModel.userId messgeTextString:@"[CallFail]"];
            [self dismiss];
        }
        
    }];
    
}


#pragma function
- (void)show {
    
    [[[[[[UIApplication sharedApplication] delegate] window] rootViewController] view] addSubview:self];
    [UIView animateWithDuration:0.25 animations:^{
        [self.contentView setFrame:CGRectMake( 0, 0, SCREEN_Width ,SCREEN_Height)];
    }];
}

- (void)dismiss {
    [self stopTimer];
    [UIView animateWithDuration:0.25 animations:^{
        [self.contentView setFrame:CGRectMake( 0, SCREEN_Height, SCREEN_Width ,SCREEN_Height)];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void) hangUpAction{
    NSLog(@"hangUpAction");
    [self dismiss];
    [[UserInfo shareInstant] sendTextMessageToOther:_profileModel.userId messgeTextString:@"[CallHangUp]"];
    
}

- (void)startTimer {
    
    NSDate *senddate = [NSDate date];
    int date = [senddate timeIntervalSince1970];
    callCheckTime = date;
    
    codeTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countDownAction) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:codeTimer forMode:NSRunLoopCommonModes];
}

- (void)stopTimer {
    [codeTimer invalidate];
    codeTimer = nil;
}

- (void)countDownAction {
    NSDate *senddate = [NSDate date];
    int date = [senddate timeIntervalSince1970];
    int lastTime = callCheckTime + 30 - date;
    
    timeLb.text = [NSString stringWithFormat:@"%d s",lastTime];
}

#pragma lazy

- (UIView *)contentView {
    if (!_contentView) {
        _contentView = [[UIView alloc] initWithFrame:CGRectMake( 0, SCREEN_Height, SCREEN_Width, SCREEN_Height)];
        _contentView.backgroundColor = [UIColor whiteColor];
        
        logoBGIV = [[UIImageView alloc] initWithFrame:_contentView.bounds];
        logoBGIV.contentMode = UIViewContentModeScaleAspectFill;
        [_contentView addSubview:logoBGIV];
        
        UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVisualEffectView *visualView = [[UIVisualEffectView alloc] initWithEffect:blur];
        [_contentView addSubview:visualView];
        visualView.frame = _contentView.bounds;
        visualView.alpha = 0.5;
        
        logoIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(130), ZoomSize(121), ZoomSize(116), ZoomSize(116))];
        logoIV.layer.cornerRadius = ZoomSize(58);
        logoIV.layer.masksToBounds = YES;
        logoIV.contentMode = UIViewContentModeScaleAspectFill;
        [_contentView addSubview:logoIV];
        
        nameLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(106), ZoomSize(260), ZoomSize(120), ZoomSize(29))];
        [_contentView addSubview:nameLb];
        nameLb.textAlignment = NSTextAlignmentLeft;
        nameLb.textColor = WhiteColor;
        nameLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightSemibold];
        
        onlineLogoIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(230), ZoomSize(260), ZoomSize(28), ZoomSize(28))];
        onlineLogoIV.image = GetImageByName(@"common_online");
        [_contentView addSubview:onlineLogoIV];
        onlineLogoIV.hidden = YES;
        
        verifyLogoIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(260), ZoomSize(260), ZoomSize(28), ZoomSize(28))];
        verifyLogoIV.image = GetImageByName(@"common_validation");
        [_contentView addSubview:verifyLogoIV];
        verifyLogoIV.hidden = YES;
        
        nationIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(163), ZoomSize(300), ZoomSize(20), ZoomSize(16))];
        [_contentView addSubview:nationIV];
        nationIV.contentMode = UIViewContentModeScaleAspectFit;
        
        nationLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(184), ZoomSize(300), ZoomSize(150), ZoomSize(16))];
        [_contentView addSubview:nationLb];
        nationLb.textColor = WhiteColor;
        nationLb.font = [UIFont systemFontOfSize:ZoomSize(14)];
        
        aboutLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(23), ZoomSize(336), ZoomSize(330), ZoomSize(50))];
        [_contentView addSubview:aboutLb];
        aboutLb.textAlignment = NSTextAlignmentLeft;
        aboutLb.textColor = WhiteColor;
        aboutLb.font = [UIFont systemFontOfSize:ZoomSize(16)];
        aboutLb.numberOfLines = 0;
        
        timeLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(137), ZoomSize(480), ZoomSize(100), ZoomSize(29))];
        [_contentView addSubview:timeLb];
        timeLb.text = @"";
        timeLb.textAlignment = NSTextAlignmentCenter;
        timeLb.textColor = WhiteColor;
        timeLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightSemibold];
        
        UIButton *callOutHangUpBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(154), ZoomSize(528), ZoomSize(66), ZoomSize(66))];
        [_contentView addSubview:callOutHangUpBt];
        [callOutHangUpBt setImage:GetImageByName(@"pop_hangup") forState:UIControlStateNormal];
        [callOutHangUpBt addTarget:self action:@selector(hangUpAction) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _contentView;
}


@end

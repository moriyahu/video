//
//  VideoPlayerView.h
//  Video
//
//  Created by steve on 2020/5/13.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VideoPlayerView : UIView

- (void)updatePlayerWith:(NSURL *)url;

- (void)play;
- (void)pause;
- (void)setMute; //设置静音
- (void)setLoopPay;

- (void) setLookMeProfileState; //设置为查看信息模式（未加载完视频时，提示转圈。加载完成给出播放按钮。查看其他人有vip蒙板）
- (void) setLookOtherProfileState; //设置为查看信息模式（未加载完视频时，提示转圈。加载完成给出播放按钮。查看自己无vip 蒙板）
- (void) setSparkState; //设置为spark模式（未加载完视频时，出现连接界面。加载完成循环播放。有vip蒙板）

@end

NS_ASSUME_NONNULL_END

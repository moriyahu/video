//
//  UserLikeOtherPopView.h
//  Video
//
//  Created by steve on 2020/5/22.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserLikeOtherPopView : UIView

@property (nonatomic, strong) UserProfileDataModel *dataModel;

@property (nonatomic, copy) ClickBlock closeBlock;

- (void)show;

@end

NS_ASSUME_NONNULL_END

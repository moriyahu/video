//
//  UserLikeOtherPopView.m
//  Video
//
//  Created by steve on 2020/5/22.
//  Copyright © 2020 steve. All rights reserved.
//

#import "UserLikeOtherPopView.h"
#import "CircleScaleView.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface UserLikeOtherPopView() {
    UIImageView *logoIV;
    UILabel *nameLb;
    UILabel *tipLb;
}

@property (nonatomic, strong) UIView *contentView;

@end

@implementation UserLikeOtherPopView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews:frame];
    }
    return self;
}

- (void)initSubviews:(CGRect) frame {
    self.frame = frame;
    [self addSubview:self.contentView];
    
    [self performSelector:@selector(dismiss) withObject:nil afterDelay:10];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismiss) name:NeedDismissLikeOtherView object:nil]; //编辑用户设置
}

- (void)setDataModel:(UserProfileDataModel *)dataModel {
    
    if (![DataTool isEmptyString:dataModel.o_avatar]) {
        [logoIV sd_setImageWithURL:[NSURL URLWithString:dataModel.o_avatar] placeholderImage:GetImageByName(@"message_user_nopho")];
    }
    if (![DataTool isEmptyString:dataModel.headThumbnailUrl]) {
        [logoIV sd_setImageWithURL:[NSURL URLWithString:dataModel.headThumbnailUrl] placeholderImage:GetImageByName(@"message_user_nopho")];
    }
    if (![DataTool isEmptyString:dataModel.headPortraitUrl]) {
        [logoIV sd_setImageWithURL:[NSURL URLWithString:dataModel.headThumbnailUrl] placeholderImage:GetImageByName(@"message_user_nopho")];
    }
    
//    [logoIV sd_setImageWithURL:[NSURL URLWithString:dataModel.headPortraitUrl] placeholderImage:GetImageByName(@"message_user_nopho")];
    nameLb.text = dataModel.nickName;
    tipLb.text = @"You liked him. Waiting for him response...";
    if ([dataModel.sex isEqualToString:@"FEMALE"]) {
        tipLb.text = @"You liked her. Waiting for her response...";
    }
}

- (void)show {
    
    [[[[[[UIApplication sharedApplication] delegate] window] rootViewController] view] addSubview:self]; //使用window，可以解决隐藏tabbar的问题
    
    [UIView animateWithDuration:0.25 animations:^{
        [self.contentView setFrame:CGRectMake(ZoomSize(38), ( SCREEN_Height- ZoomSize(211) )/2.0 , ZoomSize(300), ZoomSize(211))];
    }];
}

- (void)dismiss {
    
    if (_closeBlock) {
        _closeBlock();
    }
    
    [UIView animateWithDuration:0.25 animations:^{
        [self.contentView setFrame:CGRectMake(ZoomSize(38), SCREEN_Height, ZoomSize(300), ZoomSize(211))];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}


#pragma lazy
- (UIView *)contentView {
    if (!_contentView) {
        _contentView = [[UIView alloc] initWithFrame:CGRectMake(ZoomSize(38), SCREEN_Height, ZoomSize(300), ZoomSize(211))];
        _contentView.layer.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor;
        _contentView.layer.cornerRadius = ZoomSize(16);
        _contentView.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5].CGColor;
        _contentView.layer.shadowOffset = CGSizeMake(0, ZoomSize(2));
        _contentView.layer.shadowOpacity = 1;
        _contentView.layer.shadowRadius = ZoomSize(4);
        
        UIImageView *bgIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(0), ZoomSize(40), ZoomSize(300), ZoomSize(45))];
        bgIV.image = GetImageByName(@"match_PopView_line");
        [_contentView addSubview:bgIV];
        
        CAShapeLayer *bgLayer = [CAShapeLayer layer];
        [_contentView.layer addSublayer:bgLayer];
        bgLayer.lineCap = kCALineCapRound;
        bgLayer.lineWidth = ZoomSize(4);
        bgLayer.fillColor = [UIColor clearColor].CGColor;
        bgLayer.strokeColor = RGB(244, 205, 243).CGColor;
        UIBezierPath *bgPath = [UIBezierPath bezierPathWithArcCenter:CGPointMake( ZoomSize(150) ,ZoomSize(63)) radius:ZoomSize(40) startAngle:-M_PI_2 endAngle:( 2.0 * M_PI) clockwise:YES];
        bgLayer.path = bgPath.CGPath;
        [bgPath stroke];
        
        UIBezierPath *scalePath = [UIBezierPath bezierPathWithArcCenter:CGPointMake( ZoomSize(150) ,ZoomSize(63)) radius:ZoomSize(40) startAngle:-M_PI_2 endAngle:( 2.0 * M_PI) clockwise:YES];
        CAShapeLayer *circleLayer = [CAShapeLayer layer];
        [_contentView.layer addSublayer:circleLayer];
        circleLayer.path = scalePath.CGPath;
        circleLayer.lineCap = kCALineCapRound;
        circleLayer.lineWidth = ZoomSize(4);
        circleLayer.fillColor = [UIColor clearColor].CGColor;
        circleLayer.strokeColor = RGB(202, 41, 207).CGColor;
        
        CABasicAnimation *baseAnima = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
        baseAnima.removedOnCompletion = NO;
        baseAnima.duration = 15;
        baseAnima.fromValue = [NSNumber numberWithInteger:0];
        baseAnima.toValue = [NSNumber numberWithInteger:1];
        [circleLayer addAnimation:baseAnima forKey:@"strokeEndAnimation"];
        
        logoIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(112), ZoomSize(26), ZoomSize(76), ZoomSize(76))];
        logoIV.layer.cornerRadius = ZoomSize(38);
        logoIV.layer.masksToBounds = YES;
        logoIV.image = GetImageByName(@"message_user_nopho");
        logoIV.contentMode = UIViewContentModeScaleAspectFill;
        [_contentView addSubview:logoIV];
        
        nameLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(0), ZoomSize(107), ZoomSize(300), ZoomSize(28))];
        [_contentView addSubview:nameLb];
//        nameLb.text = @"Alice";
        nameLb.textAlignment = NSTextAlignmentCenter;
        nameLb.textColor = BlackColor;
        nameLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightSemibold];
        
        tipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(20), ZoomSize(150), ZoomSize(260), ZoomSize(50))];
        tipLb.numberOfLines = 0;
        [_contentView addSubview:tipLb];
//        tipLb.text = @"You liked her. Waiting for her response...";
        tipLb.font = [UIFont systemFontOfSize:ZoomSize(18)];
        tipLb.textColor = BGDarkBlueColor;
        tipLb.textAlignment = NSTextAlignmentCenter;
        
    }
    
    return _contentView;
}

@end

//
//  UserCallInView.m
//  Video
//
//  Created by steve on 2020/5/21.
//  Copyright © 2020 steve. All rights reserved.
//

#import "UserCallInView.h"
#import <SDWebImage/UIImageView+WebCache.h>


@interface UserCallInView() {
    UIImageView *logoIV;
    UILabel *nameLb;
    UIImageView *nationIV;
    UILabel *nationLb;
}

@end

@implementation UserCallInView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews:frame];
    }
    return self;
}


- (void)initSubviews:(CGRect) frame {
    self.frame = frame;
    
    //    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(ZoomSize(16), StatusBarHeight + ZoomSize(10), ZoomSize(343), ZoomSize(80))];
    
    self.layer.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor;
    self.layer.cornerRadius = ZoomSize(16);
    self.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5].CGColor;
    self.layer.shadowOffset = CGSizeMake(0, ZoomSize(2));
    self.layer.shadowOpacity = 1;
    self.layer.shadowRadius = ZoomSize(4);
    
    
    UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(17), ZoomSize(9), ZoomSize(80), ZoomSize(14))];
    titleLb.text = @"Incoming Call";
    titleLb.textColor = TextGrayColor;
    titleLb.font = [UIFont systemFontOfSize:ZoomSize(12)];
    [self addSubview:titleLb];
    
    logoIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(17), ZoomSize(40), ZoomSize(45), ZoomSize(45))];
    logoIV.layer.cornerRadius = ZoomSize(22.5);
    logoIV.layer.masksToBounds = YES;
    [self addSubview:logoIV];
    
    nameLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(70), ZoomSize(40), ZoomSize(160), ZoomSize(21))];
    [self addSubview:nameLb];
    nameLb.textAlignment = NSTextAlignmentLeft;
    nameLb.textColor = BlackColor;
    nameLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightSemibold];
    
    nationIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(70), ZoomSize(70), ZoomSize(20), ZoomSize(16))];
    nationIV.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:nationIV];
    
    nationLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(95), ZoomSize(70), ZoomSize(150), ZoomSize(16))];
    [self addSubview:nationLb];
    nationLb.textColor = BGDarkBlueColor;
    nationLb.font = [UIFont systemFontOfSize:ZoomSize(14)];
    
    UIButton *hungUpBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(218), ZoomSize(46), ZoomSize(38), ZoomSize(38))];
    [self addSubview:hungUpBt];
    [hungUpBt setImage:GetImageByName(@"pop_hangup") forState:UIControlStateNormal];
    [hungUpBt addTarget:self action:@selector(hungUpAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *getThoughBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(280), ZoomSize(46), ZoomSize(38), ZoomSize(38))];
    [self addSubview:getThoughBt];
    [getThoughBt setImage:GetImageByName(@"pop_answer") forState:UIControlStateNormal];
    [getThoughBt addTarget:self action:@selector(getThoughAction) forControlEvents:UIControlEventTouchUpInside];
    
    
    //旋转动画
    CAKeyframeAnimation * keyAnimaion = [CAKeyframeAnimation animation];
    keyAnimaion.keyPath = @"transform.rotation";
    keyAnimaion.values = @[@(-10 / 180.0 * M_PI),@(10 /180.0 * M_PI),@(-10/ 180.0 * M_PI)];//度数转弧度
    
    keyAnimaion.removedOnCompletion = NO;
    keyAnimaion.fillMode = kCAFillModeForwards;
    keyAnimaion.duration = 0.3;
    keyAnimaion.repeatCount = MAXFLOAT;
    [getThoughBt.layer addAnimation:keyAnimaion forKey:nil];
    
    //    上下动画
    CALayer *viewLayer = getThoughBt.layer;
    CGPoint position = viewLayer.position;
    CGPoint startPoint = CGPointMake(position.x , position.y + ZoomSize(5));
    CGPoint endPoint = CGPointMake(position.x , position.y - ZoomSize(5));
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
    [animation setFromValue:[NSValue valueWithCGPoint:startPoint]];
    [animation setToValue:[NSValue valueWithCGPoint:endPoint]];
    [animation setAutoreverses:YES];
    [animation setDuration:.3];
    [animation setRepeatCount:MAXFLOAT];
    [viewLayer addAnimation:animation forKey:nil];
    
}


- (void)setProfileModel:(UserProfileDataModel *)profileModel {
    _profileModel = profileModel;
    
    [logoIV sd_setImageWithURL:[NSURL URLWithString:_profileModel.headPortraitUrl] placeholderImage:GetImageByName(@"message_system_logo")];
    nameLb.text = [NSString stringWithFormat:@"%@,%ld", _profileModel.nickName, [DataTool getUserAgeFromString: _profileModel.birthday]];
    nationIV.image = [DataTool getImageForCountryName:[UserInfo shareInstant].user.country];
    nationLb.text = [UserInfo shareInstant].user.country;
    
}

- (void)hungUpAction {
    NSLog(@"hungUpAction");
    
    if (_hungUpBlock) {
        _hungUpBlock();
    }
    
}

- (void)getThoughAction {
    NSLog(@"getThoughAction");
    
    if (_getThoughBlock) {
        _getThoughBlock();
    }
}


@end

//
//  JKPickView.m
//  JKOCPickView
//
//  Created by 王冲 on 2018/5/15.
//  Copyright © 2018年 希艾欧科技有限公司. All rights reserved.
//

#import "JKPickView.h"

@interface JKPickView ()<UIPickerViewDelegate, UIPickerViewDataSource, UIGestureRecognizerDelegate> {
    UIPickerView *pickview;
    UIButton *backView;
    UILabel *titleLb;
    NSInteger selectRow;
}

@property (nonatomic, strong) UIView *contentView;

@end

@implementation JKPickView

+(instancetype)setPickViewDate {
    return [[self alloc]init];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews:frame];
    }
    return self;
}

- (void)initSubviews:(CGRect) frame {
    self.frame = frame;
    [self addSubview:self.contentView];
    
    UITapGestureRecognizer *singleFingerOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
    singleFingerOne.numberOfTouchesRequired = 1;
    singleFingerOne.delegate = self;
    
    [self addGestureRecognizer:singleFingerOne];
}

- (void)show {
    
    [[[[[[UIApplication sharedApplication] delegate] window] rootViewController] view] addSubview:self]; //使用window，可以解决隐藏tabbar的问题
    
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundColor = [ RGB(50, 50, 50) colorWithAlphaComponent:0.3f];
        [self.contentView setFrame:CGRectMake(0, SCREEN_Height - (ZoomSize(300) + BottomSafeAreaHeight)  , SCREEN_Width, ZoomSize(300) + BottomSafeAreaHeight)];
    }];
}

- (void)dismiss {
    
    if (self.dismissBlock) {
        self.dismissBlock();
    }
    
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundColor = [ RGB(50, 50, 50) colorWithAlphaComponent:0.0f];
        [self.contentView setFrame:CGRectMake(0, SCREEN_Height  , SCREEN_Width, ZoomSize(300) + BottomSafeAreaHeight)];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark  确定的方法
-(void)clicksure {
    
    NSInteger row= [pickview selectedRowInComponent:0]; //获取选中的列中的所在的行
    NSString *value= [_dataArray objectAtIndex:row]; //然后是获取这个行中的值，就是数组中的值
    
    if (self.back) {
        self.back(value);
        [self dismiss];
    }
}

//UIPickerViewDataSource中定义的方法，该方法的返回值决定该控件包含的列数
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView*)pickerView {
    return 1; // 返回1表明该控件只包含1列
}

//UIPickerViewDataSource中定义的方法，该方法的返回值决定该控件指定列包含多少个列表项
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.dataArray.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return self.dataArray[row];
}

-(UIView*)pickerView:(UIPickerView*)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView*)view {
    
    for(UIView *singleLine in pickerView.subviews) {
        if (singleLine.frame.size.height < 1) //设置分割线的颜色
        {
            singleLine.backgroundColor = UIColor.lightGrayColor;
        }
    }
    
    UILabel *genderLabel = (UILabel *)view;
    if (!genderLabel) {
        genderLabel = [[UILabel alloc] init];
        genderLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    genderLabel.text = self.dataArray[row];
    genderLabel.font = [UIFont boldSystemFontOfSize:18];
    genderLabel.textColor = BlackColor;
    
    return genderLabel;
}


-(CGFloat)pickerView:(UIPickerView*)pickerView rowHeightForComponent:(NSInteger)component {
    return 40; // 自定义行高
}

-(CGFloat)pickerView:(UIPickerView*)pickerView widthForComponent:(NSInteger)component {
    return SCREEN_Width; // 自定义列宽
}

// 当用户选中UIPickerViewDataSource中指定列和列表项时激发该方法
- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    selectRow = row;
    [pickview reloadComponent:component];
}

-(NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

-(void)setSelectedRow:(NSString *)selectedRow{
    [pickview reloadAllComponents];
    _selectedRow = selectedRow;
    [pickview selectRow:[selectedRow integerValue] inComponent:0 animated:NO];
}

- (void)setTitleString:(NSString *)titleString {
    titleLb.text = titleString;
}

#pragma lazy
- (UIView *)contentView {
    if (!_contentView) {
        _contentView = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_Height, SCREEN_Width , ZoomSize(300) + BottomSafeAreaHeight)];
        _contentView.backgroundColor = [UIColor whiteColor];
        
        UIBezierPath *contentViewMaskPath = [UIBezierPath bezierPathWithRoundedRect:_contentView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(ZoomSize(15),ZoomSize(15))]; //设置部分圆角
        CAShapeLayer *contentViewMaskLayer = [[CAShapeLayer alloc] init];
        contentViewMaskLayer.frame = _contentView.bounds;
        contentViewMaskLayer.path = contentViewMaskPath.CGPath;
        _contentView.layer.mask = contentViewMaskLayer;
        _contentView.layer.masksToBounds = YES;
        
        titleLb = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_Width /2 - ZoomSize(80), ZoomSize(15), ZoomSize(160), ZoomSize(20))];
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.textColor = BGDarkBlueColor;
        [_contentView addSubview: titleLb];
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightSemibold];
        
        
        UIButton *buttonCancle = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(40),ZoomSize(13), ZoomSize(24), ZoomSize(24))];
//        [buttonCancle setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
        [buttonCancle setImage:GetImageByName(@"pop_close") forState:UIControlStateNormal];
        [buttonCancle addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
        [_contentView addSubview: buttonCancle];
        
        
        pickview = [[UIPickerView alloc]initWithFrame:CGRectMake(0, ZoomSize(40), _contentView.frame.size.width, ZoomSize(200))];
        pickview.backgroundColor = [UIColor whiteColor];
        pickview.delegate = self;
        pickview.dataSource = self;
        [_contentView addSubview:pickview];
        
        UIImageView *tipIV = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(45), ZoomSize(132), ZoomSize(26), ZoomSize(18))];
        [_contentView addSubview:tipIV];
        tipIV.image = GetImageByName(@"pop_choose");
        tipIV.contentMode = UIViewContentModeScaleAspectFit;
        
        
        UIButton *addPhotoButton = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(38), ZoomSize(240), SCREEN_Width - ZoomSize(76), ZoomSize(48))];
        [_contentView addSubview:addPhotoButton];
        [addPhotoButton addTarget:self action:@selector(clicksure) forControlEvents:UIControlEventTouchUpInside];
        addPhotoButton.layer.cornerRadius = ZoomSize(24);
        addPhotoButton.layer.masksToBounds = YES;
        
        CAGradientLayer *gl = [CAGradientLayer layer];
        gl.frame = addPhotoButton.bounds;
        gl.startPoint = CGPointMake(0.56, -0.77);
        gl.endPoint = CGPointMake(0.46, 1.66);
        gl.colors = @[(__bridge id)[UIColor colorWithRed:238/255.0 green:48/255.0 blue:202/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:137/255.0 green:36/255.0 blue:244/255.0 alpha:1.0].CGColor];
        gl.locations = @[@(0), @(1.0f)];
        [addPhotoButton.layer addSublayer:gl];
        
        UILabel *addPhotoLb = [[UILabel alloc] initWithFrame:addPhotoButton.bounds];
        [addPhotoButton addSubview:addPhotoLb];
        addPhotoLb.text = @"Submit";
        addPhotoLb.textAlignment = NSTextAlignmentCenter;
        addPhotoLb.textColor = WhiteColor;
        addPhotoLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightSemibold];
        
    }
    return _contentView;
    
}

@end



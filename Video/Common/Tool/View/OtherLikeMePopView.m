//
//  OtherLikeMePopView.m
//  Video
//
//  Created by steve on 2020/5/22.
//  Copyright © 2020 steve. All rights reserved.
//

#import "OtherLikeMePopView.h"
#import "CircleScaleView.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface OtherLikeMePopView() {
    UIImageView *logoIV;
    UILabel *nameLb;
    UILabel *tipLb;
}

@property (nonatomic, strong) UIView *contentView;

@end

@implementation OtherLikeMePopView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews:frame];
    }
    return self;
}

- (void)initSubviews:(CGRect) frame {
    self.frame = frame;
    [self addSubview:self.contentView];
    [self performSelector:@selector(dismiss) withObject:nil afterDelay:8];
}

#pragma function

- (void)show {
    
    [[[[[[UIApplication sharedApplication] delegate] window] rootViewController] view] addSubview:self]; //使用window，可以解决隐藏tabbar的问题
    
    [UIView animateWithDuration:0.25 animations:^{
        [self.contentView setFrame:CGRectMake(ZoomSize(38), ( SCREEN_Height- ZoomSize(342) )/2.0 , ZoomSize(300), ZoomSize(342))];
    }];
    
}

- (void)dismiss {
    
    [UIView animateWithDuration:0.25 animations:^{
        [self.contentView setFrame:CGRectMake(ZoomSize(38), SCREEN_Height, ZoomSize(300), ZoomSize(342))];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)likeAction {
    
     [self dismiss];
    
//    if (_likeBlock) {
//        _likeBlock();
//    }
    
    NSDictionary *paramsDic = @{@"beLikedUserId": _dataModel.userId, @"userType": _dataModel.type , @"isNotify":@"1"};
    [[NetTool shareInstance] startRequest:API_Rel_AddLike method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
        
        
    }];
    
}

- (void)setDataModel:(UserProfileDataModel *)dataModel {
    _dataModel = dataModel;
    [logoIV sd_setImageWithURL:[NSURL URLWithString:dataModel.headPortraitUrl] placeholderImage:GetImageByName(@"message_vido")];
    nameLb.text = dataModel.nickName;
    tipLb.text = @"He liked you. Waiting for your response!";
    if ([dataModel.sex isEqualToString:@"FEMALE"]) {
        tipLb.text = @"She liked you. Waiting for your response!";
    }
}

- (void)closeAction {
//    if (_closeBlock) {
//        self.closeBlock();
//    }
}

#pragma lazy
- (UIView *)contentView {
    if (!_contentView) {
        _contentView = [[UIView alloc] initWithFrame:CGRectMake(ZoomSize(38), SCREEN_Height, ZoomSize(300), ZoomSize(342))];
        _contentView.layer.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor;
        _contentView.layer.cornerRadius = ZoomSize(16);
        _contentView.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5].CGColor;
        _contentView.layer.shadowOffset = CGSizeMake(0, ZoomSize(2));
        _contentView.layer.shadowOpacity = 1;
        _contentView.layer.shadowRadius = ZoomSize(4);
        
        UIButton *buttonCancle = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(40),ZoomSize(13), ZoomSize(24), ZoomSize(24))];
        [buttonCancle setImage:GetImageByName(@"pop_close") forState:UIControlStateNormal];
        [buttonCancle addTarget:self action:@selector(closeAction) forControlEvents:UIControlEventTouchUpInside];
        [_contentView addSubview: buttonCancle];
        
        
        UIImageView *bgIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(0), ZoomSize(40), ZoomSize(300), ZoomSize(45))];
        bgIV.image = GetImageByName(@"match_PopView_line");
        [_contentView addSubview:bgIV];
        
        CAShapeLayer *bgLayer = [CAShapeLayer layer];
        [_contentView.layer addSublayer:bgLayer];
        bgLayer.lineCap = kCALineCapRound;
        bgLayer.lineWidth = ZoomSize(4);
        bgLayer.fillColor = [UIColor clearColor].CGColor;
        bgLayer.strokeColor = RGB(244, 205, 243).CGColor;
        UIBezierPath *bgPath = [UIBezierPath bezierPathWithArcCenter:CGPointMake( ZoomSize(150) ,ZoomSize(63)) radius:ZoomSize(40) startAngle:-M_PI_2 endAngle:( 2.0 * M_PI) clockwise:YES];
        bgLayer.path = bgPath.CGPath;
        [bgPath stroke];
        
        UIBezierPath *scalePath = [UIBezierPath bezierPathWithArcCenter:CGPointMake( ZoomSize(150) ,ZoomSize(63)) radius:ZoomSize(40) startAngle:-M_PI_2 endAngle:( 2.0 * M_PI) clockwise:YES];
        CAShapeLayer *circleLayer = [CAShapeLayer layer];
        [_contentView.layer addSublayer:circleLayer];
        circleLayer.path = scalePath.CGPath;
        circleLayer.lineCap = kCALineCapRound;
        circleLayer.lineWidth = ZoomSize(4);
        circleLayer.fillColor = [UIColor clearColor].CGColor;
        circleLayer.strokeColor = RGB(202, 41, 207).CGColor;
        
        CABasicAnimation *baseAnima = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
        baseAnima.removedOnCompletion = NO;
        baseAnima.duration = 15;
        baseAnima.fromValue = [NSNumber numberWithInteger:0];
        baseAnima.toValue = [NSNumber numberWithInteger:1];
        [circleLayer addAnimation:baseAnima forKey:@"strokeEndAnimation"];
        
        logoIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(112), ZoomSize(27), ZoomSize(76), ZoomSize(76))];
        logoIV.layer.cornerRadius = ZoomSize(38);
        logoIV.layer.masksToBounds = YES;
        logoIV.contentMode = UIViewContentModeScaleAspectFill;
        //        logoIV.image = GetImageByName(@"message_vido");
        [_contentView addSubview:logoIV];
        
        nameLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(0), ZoomSize(107), ZoomSize(300), ZoomSize(28))];
        [_contentView addSubview:nameLb];
        //        nameLb.text = @"Alice";
        nameLb.textAlignment = NSTextAlignmentCenter;
        nameLb.textColor = BlackColor;
        nameLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightSemibold];
        
        tipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(20), ZoomSize(150), ZoomSize(260), ZoomSize(50))];
        tipLb.numberOfLines = 0;
        [_contentView addSubview:tipLb];
        //        tipLb.text = @"He liked you. Waiting for your response!";
        tipLb.font = [UIFont systemFontOfSize:ZoomSize(18)];
        tipLb.textColor = BGDarkBlueColor;
        tipLb.textAlignment = NSTextAlignmentCenter;
        
        UIButton *likeBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(20), ZoomSize(220), ZoomSize(260), ZoomSize(85))];
        [likeBt setImage:GetImageByName(@"pop_button_like_normal") forState:UIControlStateNormal];
        [_contentView addSubview:likeBt];
        [likeBt addTarget:self action:@selector(likeAction) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    return _contentView;
}

@end

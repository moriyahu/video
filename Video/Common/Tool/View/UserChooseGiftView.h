//
//  UserChooseGiftView.h
//  Video
//
//  Created by steve on 2020/6/8.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserChooseGiftView : UIView

@property (nonatomic, copy) CallBack sendGiftBlock;

- (instancetype) initWithFrame:(CGRect)frame;

@end

NS_ASSUME_NONNULL_END

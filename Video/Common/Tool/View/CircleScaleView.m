//
//  CircleScaleView.m
//  ChartCollection
//
//  Created by anyongxue on 2016/12/28.
//  Copyright © 2016年 cc. All rights reserved.
//

#import "CircleScaleView.h"

@interface CircleScaleView ()<CAAnimationDelegate> {
    CAShapeLayer *circleLayer;
    UILabel *tipTimeLb;
    NSTimer *tipTimer;
    int codeCheckTime;
}

@end

@implementation CircleScaleView


- (void)drawRect:(CGRect)rect {
    
    UIImageView *bgIV = [[UIImageView alloc] initWithFrame:self.bounds];
    bgIV.image = GetImageByName(@"pop_fuzzy");
    [self addSubview:bgIV];
    
    UIBezierPath *bgPath = [UIBezierPath bezierPathWithArcCenter:CGPointMake(self.bounds.size.width / 2,self.bounds.size.height / 2) radius:self.bounds.size.height / 2 - 9 / 2 startAngle:-M_PI_2 endAngle:2.0* M_PI clockwise:YES]; //贝塞尔曲线
    UIColor *storkeColor = self.bgColor;
    [storkeColor setStroke];
    bgPath.lineWidth = self.lineWidth;
    [bgPath stroke];
    
    UIBezierPath *scalePath = [UIBezierPath bezierPathWithArcCenter:CGPointMake(self.bounds.size.width / 2,self.bounds.size.height / 2) radius:self.bounds.size.height / 2 - 9 / 2 startAngle:-M_PI_2 endAngle:( 2.0 * M_PI) clockwise:YES];
    circleLayer = [CAShapeLayer layer];
    circleLayer.path = scalePath.CGPath;
    circleLayer.lineCap = kCALineCapRound;
    circleLayer.lineWidth = self.lineWidth;
    circleLayer.frame = self.bounds;
    circleLayer.fillColor = [UIColor clearColor].CGColor;
    circleLayer.strokeColor = self.bgColor.CGColor;
    [self.layer addSublayer:circleLayer];
    
    tipTimeLb = [[UILabel alloc] initWithFrame:self.bounds];
    [self addSubview:tipTimeLb];
    tipTimeLb.textColor = WhiteColor;
    tipTimeLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightSemibold];
    tipTimeLb.text = @"";
    tipTimeLb.textAlignment = NSTextAlignmentCenter;
    
}

- (void)startAnimation {
    circleLayer.strokeColor = self.bgColor.CGColor;
    [self startCountDown];
}

- (void)starRotation {
    
    [self stopAnimation];
    
    circleLayer.strokeColor = self.showColor.CGColor;
    CABasicAnimation *baseAnima = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    baseAnima.removedOnCompletion = NO;
    baseAnima.duration = self.durationTime;
    baseAnima.delegate = self;
    baseAnima.fromValue = [NSNumber numberWithInteger:0];
    baseAnima.toValue = [NSNumber numberWithInteger:1];
    [circleLayer addAnimation:baseAnima forKey:@"strokeEndAnimation"];
}

- (void)stopAnimation {
    [circleLayer removeAnimationForKey:@"strokeEndAnimation"];
}

- (void)startCountDown {
    
    NSDate *senddate = [NSDate date];
    int date = [senddate timeIntervalSince1970];
    codeCheckTime = date;
    
    tipTimeLb.text = @"3";
    tipTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countDownAction) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:tipTimer forMode:NSRunLoopCommonModes];
}

- (void)countDownAction {
    NSDate *senddate = [NSDate date];
    int date = [senddate timeIntervalSince1970];
    int lastTime = codeCheckTime + 3 - date;
    
    if (lastTime <= 0) { //需求先倒计时3秒， 然后再开始旋转， 并计时15秒
        if (lastTime == 0) {
            [self starRotation];
        }
        
        int recordTime = 0 - lastTime;
        if (recordTime > 15) {
            [tipTimer invalidate];
            tipTimer = nil;
        } else {
            tipTimeLb.text = [NSString stringWithFormat:@"%d",recordTime];
        }
    } else {
        tipTimeLb.text = [NSString stringWithFormat:@"%d",lastTime];
    }
}

@end

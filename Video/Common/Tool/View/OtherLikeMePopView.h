//
//  OtherLikeMePopView.h
//  Video
//
//  Created by steve on 2020/5/22.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OtherLikeMePopView : UIView

//@property (nonatomic, copy) ClickBlock likeBlock;
//@property (nonatomic, copy) ClickBlock closeBlock;

@property (nonatomic, strong) UserProfileDataModel *dataModel;

- (void)show;

@end

NS_ASSUME_NONNULL_END

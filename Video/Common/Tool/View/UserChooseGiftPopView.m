//
//  UserChooseGiftPopView.m
//  Video
//
//  Created by steve on 2020/6/8.
//  Copyright © 2020 steve. All rights reserved.
//

#import "UserChooseGiftPopView.h"
#import "UserChooseGiftView.h"

@interface UserChooseGiftPopView()<UIGestureRecognizerDelegate> {
    
}

@property (nonatomic, strong) UIView *contentView;

@end

@implementation UserChooseGiftPopView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews:frame];
    }
    return self;
}

- (void)initSubviews:(CGRect) frame {
    self.frame = frame;
    [self addSubview:self.contentView];
    
    UITapGestureRecognizer *singleFingerOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
    singleFingerOne.numberOfTouchesRequired = 1;
    singleFingerOne.delegate = self;
    [self addGestureRecognizer:singleFingerOne];
    
}

- (void)show {
    
    [[[[[[UIApplication sharedApplication] delegate] window] rootViewController] view] addSubview:self]; //使用window，可以解决隐藏tabbar的问题
    
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundColor = [ RGB(50, 50, 50) colorWithAlphaComponent:0.3f];
        [self.contentView setFrame:CGRectMake(0, SCREEN_Height - (ZoomSize(300) + BottomSafeAreaHeight)  , SCREEN_Width, ZoomSize(300) + BottomSafeAreaHeight)];
    }];
}

- (void)dismiss {
    
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundColor = [ RGB(50, 50, 50) colorWithAlphaComponent:0.0f];
        [self.contentView setFrame:CGRectMake(0, SCREEN_Height  , SCREEN_Width, ZoomSize(300) + BottomSafeAreaHeight)];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}


#pragma delegate
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if ([touch.view isDescendantOfView:_contentView]) {
        return NO;
    }
    return YES;
}


#pragma lazy
- (UIView *)contentView {
    if (!_contentView) {
        _contentView = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_Height, SCREEN_Width , ZoomSize(300) + BottomSafeAreaHeight)];
        
        UIBezierPath *contentViewMaskPath = [UIBezierPath bezierPathWithRoundedRect:_contentView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(ZoomSize(36),ZoomSize(36))]; //设置部分圆角
        CAShapeLayer *contentViewMaskLayer = [[CAShapeLayer alloc] init];
        contentViewMaskLayer.frame = _contentView.bounds;
        contentViewMaskLayer.path = contentViewMaskPath.CGPath;
        _contentView.layer.mask = contentViewMaskLayer;
        _contentView.layer.masksToBounds = YES;
        
        
        UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVisualEffectView *visualView = [[UIVisualEffectView alloc] initWithEffect:blur];
        visualView.frame = _contentView.bounds;
        visualView.alpha = 0.8;
        [_contentView addSubview:visualView];
        
        UserChooseGiftView *giftView = [[UserChooseGiftView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, ZoomSize(300))];
        
        giftView.sendGiftBlock = ^(id  _Nullable obj) {
            if (self.sendGiftBlock) {
                self.sendGiftBlock(obj);
            }
        };
        
        [_contentView addSubview:giftView];
    }
    return _contentView;
    
}
@end

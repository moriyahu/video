//
//  SelectPhotoManager.m
//  Video
//
//  Created by steve on 2020/3/23.
//  Copyright © 2020 steve. All rights reserved.
//

#import "SelectPhotoManager.h"
#import "EditPhotoPageViewController.h"

@interface SelectPhotoManager() {
    UIImagePickerController *ipVC;
}

@end

@implementation SelectPhotoManager

- (void)startSelectPhotoWithImageName:(NSString *)imageName{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle: UIAlertControllerStyleActionSheet];
    
    [alertController addAction: [UIAlertAction actionWithTitle: @"Take a Photo" style: UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self selectPhotoWithType:0];
    }]];
    [alertController addAction: [UIAlertAction actionWithTitle: @"Choose from Library" style: UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self selectPhotoWithType:1];
    }]];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:cancelAction];
    
    [[DataTool getCurrentVC] presentViewController:alertController animated:YES completion:nil];
}

- (void)openPhoto {
    [self selectPhotoWithType:0];
}

- (void)chooseLibrary {
    [self selectPhotoWithType:1];
}

#pragma mark function
-(void)selectPhotoWithType:(int)type {
    
    ipVC = [[UIImagePickerController alloc] init];
    ipVC.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    ipVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    ipVC.preferredContentSize = CGSizeMake(SCREEN_Width,  SCREEN_Width);
    ipVC.delegate = self;
    
    if (type == 0) {
        BOOL isCamera = [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear];
        if (!isCamera) {
            return ;
        }else{
            ipVC.sourceType = UIImagePickerControllerSourceTypeCamera;
            ipVC.cameraDevice = UIImagePickerControllerCameraDeviceFront;// 此处是关键。
        }
    }else{
        ipVC.sourceType = UIImagePickerControllerCameraCaptureModePhoto;
        
    }
    [self presentAction:ipVC];
}

- (void)presentAction:(id)vc{
    
    
    [[DataTool getCurrentVC] presentViewController:vc animated:YES completion:nil];
}

#pragma mark
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    NSURL* url;
    UIImage * image;
    NSString* mediaType = [info objectForKey:UIImagePickerControllerMediaType]; //如果返回的type等于kUTTypeImage，代表返回的是照片,并且需要判断当前相机使用的sourcetype是拍照还是相册
    
    if ([mediaType isEqualToString:@"public.image"]) {
        if (@available(iOS 11.0, *)) {
            url = [info objectForKey:UIImagePickerControllerImageURL];
            if (url != nil) { //解决ios 13相册选择错误
                NSData *imageData = [NSData dataWithContentsOfURL:url];
                image = [UIImage imageWithData:imageData];
            } else {
                image = [info objectForKey:UIImagePickerControllerOriginalImage];
            }
        } else {
            image = [info objectForKey:UIImagePickerControllerOriginalImage];
        }
    }
    
    EditPhotoPageViewController *VC = [[EditPhotoPageViewController alloc] init];
    VC.getImage = image;
//    VC.resizeWHScale = self.resizeWHScale;
    [ipVC pushViewController:VC animated:YES];
    
    VC.callbackImg = ^(id  _Nullable obj) {
        UIImage *getImage = obj;
        if (getImage == nil) {
            getImage = image;
        }
        if (self->_successHandle) {
            //            self->_successHandle(self, getImage);
            self->_successHandle(self, image, getImage);
        }
    };
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [[DataTool getCurrentVC] dismissViewControllerAnimated:YES completion:nil];
}

@end

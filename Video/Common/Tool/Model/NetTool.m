//
//  NetTool.m
//  Video
//
//  Created by steve on 2020/3/23.
//  Copyright © 2020 steve. All rights reserved.
//

#import "NetTool.h"

#import <AFNetworking.h>
#import <AliyunOSSiOS/OSSService.h>
//#import "AFHTTPSessionManager+RetryPolicy.h"

static id _shareInstance = nil;

@implementation NetTool {
    AFHTTPSessionManager *manager;
    OSSClient * client;
}

+ (instancetype)shareInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shareInstance = [[NetTool alloc] init];
    });
    return _shareInstance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup {
    
    manager = [[AFHTTPSessionManager alloc] init];
    manager.requestSerializer = [AFJSONRequestSerializer serializer]; //request 格式
    manager.responseSerializer = [AFJSONResponseSerializer serializer]; //response 格式
    manager.requestSerializer.timeoutInterval = 30.f;
    
    //    manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(200, 201)]; //格式校验 200 - 400
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html",nil];
    
    //    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    //    NSString *app_version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    ////    NSString *system_Version = [[UIDevice currentDevice] systemVersion];
    
    //    [manager.requestSerializer setValue:@"1.0" forHTTPHeaderField:@"api-version"];
    //    [manager.requestSerializer setValue:app_version forHTTPHeaderField:@"app-version"];
        [manager.requestSerializer setValue:@"IOS" forHTTPHeaderField:@"platform"];
    //    [manager.requestSerializer setValue:@"EN_US" forHTTPHeaderField:@"language"];
    //    [manager.requestSerializer setValue:@"" forHTTPHeaderField:@"sign"];
    //    [manager.requestSerializer setValue:@"" forHTTPHeaderField:@"subToken"];
    //    [manager.requestSerializer setValue:[DataTool getTimeStampString] forHTTPHeaderField:@"timeStamp"];
    
//    // 推荐使用OSSAuthCredentialProvider，token过期后会自动刷新。
//    id<OSSCredentialProvider> credential = [[OSSAuthCredentialProvider alloc] initWithAuthServerUrl:@"应用服务器地址，例如http://abc.com:8080"];
//    client = [[OSSClient alloc] initWithEndpoint:OSS_Endpoint credentialProvider:credential];
    
    id<OSSCredentialProvider> credential = [[OSSPlainTextAKSKPairCredentialProvider alloc] initWithPlainTextAccessKey:OSS_AccessKey secretKey:OSS_SecretKey];
    client = [[OSSClient alloc] initWithEndpoint:OSS_Endpoint credentialProvider:credential];
    
}

#pragma 图片上传
- (void)putDatas:(NSMutableArray *)Datas fileType:(NSString *)fileTypeString needShowHUD:(BOOL)needShowHUD callback:(CallBack) callback {
    
    if (needShowHUD) {
        [DataTool startShowHUD];
    }
    
    NSMutableArray *resultArray = [NSMutableArray array]; //结果集合
    dispatch_queue_t q = dispatch_queue_create("fs", DISPATCH_QUEUE_CONCURRENT);
    for (NSData *imageData in Datas) {
        
        dispatch_async(q, ^{
            
            OSSPutObjectRequest * put = [OSSPutObjectRequest new];
            put.bucketName = OSS_BucketName;
            NSString *objectKeys = [NSString stringWithFormat:@"User/%@.%@",[DataTool getTimeNow], fileTypeString];
            put.objectKey = objectKeys;
            put.uploadingData = imageData;
            put.uploadProgress = ^(int64_t bytesSent, int64_t totalByteSent, int64_t totalBytesExpectedToSend) {
            };
            
            OSSTask * putTask = [self->client putObject:put];
            [putTask continueWithBlock:^id(OSSTask *task) {
                task = [self->client presignPublicURLWithBucketName:OSS_BucketName withObjectKey:objectKeys];
                [resultArray addObject:[NSString stringWithFormat:@"%@/%@", OSS_ImageHeader, put.objectKey]];
                if (resultArray.count == Datas.count) { //判断满足条件就返回
                    
                    if (needShowHUD) {
                        [DataTool dismissHUD];
                    }
                    
                    callback(resultArray);
                }
                return nil;
            }];
        });
    }
}


//- (NSString *)getImgAddress:(NSString *)imgFileName {
//
//    NSString *objectKey = [NSString stringWithFormat:@"%@",imgFileName];
//    NSString * publicURL = nil;
//    OSSTask *task = [self->client presignPublicURLWithBucketName:OSS_BucketName withObjectKey:objectKey]; // sign public url
//
//    if (!task.error) {
//        publicURL = task.result;
//        //        https://videoproject4test.oss-cn-hangzhou.aliyuncs.com/User/20200403020235605-6070.jpg
//        //        [self->getImg sd_setImageWithURL:[NSURL URLWithString:publicURL]];
//    } else {
//        NSLog(@"sign url error: %@", task.error);
//    }
//
//    return publicURL;
//}


- (void)startRequest:(NSString *)url method:(NSString *)method params:(id) params needShowHUD:(BOOL)needShowHUD callback:(CallBack) callback {
    
    NSLog(@"startRequest %@ params %@   [UserInfo shareInstant].user.token %@", url, params, [UserInfo shareInstant].user.token);
    
    if (![DataTool isEmptyString:[UserInfo shareInstant].user.token]) { //由于退出后需要重设，这里每次都重设一次
        [manager.requestSerializer setValue:[UserInfo shareInstant].user.token forHTTPHeaderField:@"USER-TOKEN"]; // USER-TOKEN
//        [manager.requestSerializer setValue:UserToken forHTTPHeaderField:@"USER-TOKEN"]; // USER-TOKEN
    }
    
    if (needShowHUD) {
        [DataTool startShowHUD];
    }
    
    if ([method isEqualToString:PostMethod]) {
        
        [manager POST:url parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [self checkDataSuccess:responseObject url:url needShowHUD:needShowHUD callback:^(id  _Nullable obj) {
                callback(obj);
            }];
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self checkDatafailure:error callback:^(id  _Nullable obj) {
                callback(obj);
            }];
        }];
    } else if ([method isEqualToString:GetMethod]) {
        
        [manager GET:url parameters:nil headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [self checkDataSuccess:responseObject url:url needShowHUD:needShowHUD callback:^(id  _Nullable obj) {
                callback(obj);
            }];
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self checkDatafailure:error callback:^(id  _Nullable obj) {
                callback(obj);
            }];
        }];
    }
}

- (void)checkDataSuccess:(id)responseObject url:(NSString *)url needShowHUD:(BOOL)needShowHUD callback:(CallBack) callback {
    
    NSLog(@"responseObject %@  url %@", responseObject, url);
    if (needShowHUD) {
        [DataTool dismissHUD];
    }
    
    NSDictionary *responseDic = [[NSDictionary alloc] init];
    responseDic = responseObject;
    
    int resultCode = [responseDic[@"code"] intValue];
    NSString *msgString = responseDic[@"msg"];
    NSDictionary *resultDic = [[NSDictionary alloc] init];
    
    if ([url isEqualToString:API_Rel_Spark]) { //获取spark需要重新判断状态码
        callback(responseDic);
        return;
    }
    
    if ([url isEqualToString:API_Room_Create]) { //获取spark需要重新判断状态码
        callback(responseDic);
        return;
    }
    
    
    if (resultCode == 8200 ) { //8200业务走通，8424业务不通 || resultCode == 8424
        resultDic = responseDic[@"data"];
        callback(resultDic);
    } else if (resultCode == 8424) {
        [DataTool showHUDWithString:@"please retry later"];
    } else if (resultCode == 8701) {
        [[UserInfo shareInstant] userLogout]; //token 过期
    } else if (resultCode == 8703) {
        resultDic = responseDic[@"data"]; //spark无数据
        callback(resultDic); //滑动无数据
    } else {
        [DataTool showHUDWithString:msgString];
//        callback(resultDic);
    }
}

- (void)checkDatafailure:(id)responseObject  callback:(CallBack) callback {
    NSLog(@"responseObject %@", responseObject);
    [DataTool showHUDWithString:@"please retry later"];
    
//    NSDictionary *resultDic = [[NSDictionary alloc] init];
//    callback(resultDic);
}


///**
//     * 成功响应码
//     */
//    SUC("8200","Success!"),
//    /**
//     * 通用失败响应码,84开头表示业务无异常但没有获得理想数据的异常
//     */
//    FAIL("8424", "Faild!"),
//    /**
//     * 内部错误,85开头表示系统内部异常
//     */
//    INTERAL("8500", "Internal Error!"),
//    /**
//     * token非法, 87开头为业务不合法,参数错误等异常
//     */
//    TOKEN_ERR("8701","Invalid token!"),
//    /**
//     * apple认证token非法
//     */
//    APPLE_TOKEN_ERR("8702","Invalid identity token for Apple!"),
//    /**
//     * 滑动次数不足
//     */
//    SPARK_TIME_ERR("8703","You have not enough spark times!"),
//    /**
//     * 礼物状态错误
//     */
//    GIFT_STATUS_ERR("8704","Gift have not been putdown. cannot send it!"),
//    /**
//     * 余额不足提醒
//     */
//    BALANCE_NOT_ENOUGH("8705","You have not enough balance!");


#pragma mark - send notification

- (void) sendNetWorkNotification {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:NetWorkNotification object:nil userInfo:nil];
    });
}

- (void) sendRefleshProfileInfoNotification {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:NeedRefleshProfilePage object:nil userInfo:nil];
    });
}

//关闭likeOtherView
//- (void) sendDismissUserLikeOtherPopViewNotification {
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [[NSNotificationCenter defaultCenter] postNotificationName:NeedDismissLikeOtherView object:nil userInfo:nil];
//    });
//}

////关闭likeOtherView
//- (void) sendDismissMatchViewNotification {
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [[NSNotificationCenter defaultCenter] postNotificationName:NeedDismissMatchView object:nil userInfo:nil];
//    });
//}
//
////关闭callOutView
//- (void) sendDismissCallOutViewNotification {
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [[NSNotificationCenter defaultCenter] postNotificationName:NeedDismissCallOutView object:nil userInfo:nil];
//    });
//}

@end


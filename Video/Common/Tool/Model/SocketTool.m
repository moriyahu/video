//
//  SocketTool.m
//  Video
//
//  Created by steve on 2020/3/23.
//  Copyright © 2020 steve. All rights reserved.
//

#import "SocketTool.h"
#import <MJExtension.h>
#import "SocketRecieveModel.h"

//#import "NTSocketSendModel.h"
//#import "NTSocketRecieveModel.h"
//#import <MJExtension.h>
//#import "EBBannerView.h"

#import "TUITextMessageCellData.h"
#import "TUIKitConfig.h"
#import "TUIKit.h"


static id _shareInstance = nil;

@interface SocketTool() <SRWebSocketDelegate> {
    //    int heartBeatCheckTime;
    NSTimer *heartBeatTimer;
    CallBack socketCallback;
}

///**
//     * 连接事件
//     */
//    CONNECTED,
//    /**
//     * 心跳
//     */
//    HEARTBEAT,
//    /**
//     * 喜欢
//     */
//    LIKE,
//    /**
//     * 匹配
//     */
//    MATCH,
//    /**
//     * 剔除
//     */
//    ELIMINATE,
//    /**
//     * 礼物
//     */
//    GIFT;


@end

@implementation SocketTool

+ (instancetype)shareInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shareInstance = [[SocketTool alloc] init];
    });
    return _shareInstance;
}

- (instancetype) init {
    self = [super init];
    if (self) {
    }
    return self;
}

#pragma mark - webSocket

- (void)webSocketDidOpen:(SRWebSocket *)webSocket {
    NSLog(@"--------webSocketDidOpen");
    
    _reConnectTime = 0; //重设尝试次数
    [self startHeartBeatTimer]; //发送心跳
    [self sendSocketHeartBeat];
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message {
    NSLog(@"--------didReceiveMessage message %@",message);
    
    SocketRecieveModel *recieveModel = [SocketRecieveModel mj_objectWithKeyValues:message];
    if ([recieveModel.msgType isEqualToString:@"MATCH"]) {
        
        //注，最先 match 的用户弹起 UserMatchPopView 页面
        //        然后create room ,再给对面发送 进房邀请
        //        对方收到后，再弹起match界面
        //        对面弹起match界面后，点击接受，进入trtc界面
        
        UserProfileDataModel *otherUserModel = [UserProfileDataModel mj_objectWithKeyValues:recieveModel.msg]; //对方信息
        BOOL isFirst = [recieveModel.msg[@"isFirst"] boolValue];
        if (isFirst) {
            
            NSDictionary *paramsDic = @{@"receiverId": otherUserModel.userId};
            [[NetTool shareInstance] startRequest:API_Room_Create method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
                
                
                NSDictionary *responseDic = obj;
                int resultCode = [responseDic[@"code"] intValue];
                NSDictionary *resultDic = [[NSDictionary alloc] init];
                resultDic = responseDic[@"data"];
                
                
                if (resultCode == 8200) {
                    NSString *roomIdString = resultDic[@"roomId"];
                    
                    [[UserInfo shareInstant] showMatchPopView:otherUserModel showType:@"CallOut" roomId:roomIdString];
                    [[UserInfo shareInstant] sendImMessageToOther:otherUserModel.userId messgeTextString:@"[match_VideoCall]" roomId:roomIdString];
                    [[UserInfo shareInstant] sendTextMessageToOther:otherUserModel.userId messgeTextString:@"[VideoCall]"];
                }  else {
                    
                    [[NetTool shareInstance] startRequest:API_Room_Clean method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
                    }];
                    [DataTool showHUDWithString:@"user may not be online, please retry later"];
                    [[UserInfo shareInstant] sendTextMessageToOther:otherUserModel.userId messgeTextString:@"[CallFail]"];
                }
                
            }];
            
        }
        
    }  else if ([recieveModel.msgType isEqualToString:@"LIKE"]) {
        
        //注，最后 like 的用户弹起 OtherLikeMe 页面
        BOOL isFirst = [recieveModel.msg[@"isFirst"] boolValue];
        if (!isFirst) {
            UserProfileDataModel *userModel = [UserProfileDataModel mj_objectWithKeyValues:recieveModel.msg];
            [[UserInfo shareInstant] showOtherLikeMePopView:userModel];
        }
    } else if ([recieveModel.msgType isEqualToString:@"GIFT"]) {
        UserProfileDataModel *otherUserModel = [UserProfileDataModel mj_objectWithKeyValues:recieveModel.msg]; //对方信息
        
        NSUInteger giftIndexNum = [GiftNameArray indexOfObject:otherUserModel.giftName];
        NSString *giftNameString = [GiftImgShowArray objectAtIndex:giftIndexNum];
        [DataTool showGiftViewWithTitle:giftNameString];
        
        [[UserInfo shareInstant] sendTextMessageToOther:otherUserModel.userId messgeTextString:[NSString stringWithFormat:@"[%@]", giftNameString]];
        
    }
    
    
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessageWithString:(NSString *)string {
    NSLog(@"--------didReceiveMessageWithString string %@",string);
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessageWithData:(NSData *)data {
    NSLog(@"--------didReceiveMessageWithData data %@",data);
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error {
    NSLog(@"--------didFailWithError error %@",error);
    [self stopHeartBeatTimer];
    [self reconnectSocket];
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(nullable NSString *)reason wasClean:(BOOL)wasClean {
    NSLog(@"--------didCloseWithCode code %ld reaseon %@",(long)code,reason);
    [self stopHeartBeatTimer];
    [self reconnectSocket];
}

#pragma socketFunction

//- (void)setup {
//
//    if ([DataTool isEmptyString:[UserInfo shareInstant].user.token] ) { //判断，当用户没有登录，或者socket没有初始化成功，不执行登录
//        return;
//    }
//
////    self.webSocket = [[SRWebSocket alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", Socket_URL, [DataTool jwtActionString]]]];
////    self.webSocket.delegate = self;
//}

- (void)openSocket {
    
    if (self.webSocket.readyState == SR_OPEN) { //已连上就返回
        return;
    }
    
    if ([DataTool isEmptyString:[UserInfo shareInstant].user.token] ) { //判断，当用户没有登录，或者socket没有初始化成功，不执行登录
        return;
    }
    
    self.webSocket = [[SRWebSocket alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?token=%@", Socket_URL, [UserInfo shareInstant].user.token]]];
    
    //    self.webSocket = [[SRWebSocket alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?token=%@", Socket_URL, UserToken]]];
    
    
    self.webSocket.delegate = self;
    [self.webSocket open];
    
}

- (void)reconnectSocket {
    
    if ([DataTool isEmptyString:[UserInfo shareInstant].user.token] ) { //判断，当用户没有登录，或者socket没有初始化成功，不执行登录
        return;
    }
    
    if (_reConnectTime < 5) {
        _reConnectTime ++;
        [self performSelector:@selector(openSocket) withObject:nil afterDelay:5];
    }
}

- (void)closeSocket {
    if (self.webSocket.readyState == SR_OPEN) {
        [self.webSocket close];
    }
    [self stopHeartBeatTimer];
}

- (void)startHeartBeatTimer {
    if (heartBeatTimer == nil) {
        //        heartBeatCheckTime = 0;
        heartBeatTimer = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(sendSocketHeartBeat) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:heartBeatTimer forMode:NSRunLoopCommonModes];
    }
}

- (void)stopHeartBeatTimer {
    [heartBeatTimer invalidate];
    heartBeatTimer = nil;
}

- (void)sendSocketHeartBeat {
    NSDictionary *dic = [[NSDictionary alloc] init];
    dic = @{@"msg":@{@"time":[DataTool getTimeStampString]}, @"msgType": @"HEARTBEAT" };
    [self sendInfoToSocket:dic];
}

- (void)sendInfoToSocket :(NSDictionary *)infoDic {
    if (self.webSocket.readyState == SR_OPEN) {
        [self.webSocket send:[infoDic mj_JSONString]];
    }
}

- (void)sendAPIRequest:(NSDictionary *)payloadDic {
}

@end

//
//  DataTool.h
//  Video
//
//  Created by steve on 2020/3/23.
//  Copyright © 2020 steve. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DataTool : NSObject

+ (void) showHomePage;
+ (void) showLoginPage;
+ (void) showUploadPhotoPage;
+ (void) showUploadVideoPage;
+ (void) showUserProfilePage:(UserProfileDataModel *)profileModel;

//+ (void) showGetPermissionView;
+ (void) showGetPermissionViewDismissBlock:(CallBack) viewDismissBlock;
+ (void) showGetVipViewWithIndex:(int) pageIndex;
+ (void) showGetCoinsView;
+ (void) showSendGiftView:(CallBack) sendGiftBlock;

+ (void) showBannerViewWithTitle:(NSString *) titleString;
+ (void) showGiftViewWithTitle:(NSString *) giftTitleString;


+ (void) startShowHUD;
+ (void) dismissHUD;
+ (void) showHUDWithString:(NSString *)string;
+ (void) showToast:(NSString *)getString;
+ (void) reportBlockUserName:(NSString *) userName andBlockUserId:(NSString *) BlockUserId reportCallback:(CallBack) reportCallback blockCallback:(CallBack) blockCallback;

+ (UIViewController *) getCurrentVC;

+ (NSString *) getDeviceUUID;
+ (NSString *) getTrimmingString:(NSString *) textString; //字符串去掉首位空格
+ (NSString *) getTimeNow;
+ (NSString *) getTimeStampString;
+ (NSString *) getTimTimeFromString :(NSDate *)timDate; 
+ (NSString *) getVersionString;
//+ (NSString *) jwtActionString;
+ (NSString *) getContentFromKeyChain:(NSString *) key ;

+ (BOOL) saveContentToKeyChain: (NSString *) content forKey: (NSString *) key service: (NSString *) service ;
+ (BOOL) isValidateEmail:(NSString *)email ;
+ (BOOL) isEmptyString:(NSString *)string;

+ (UIImage *) compressSizeImage:(UIImage *)image;
+ (UIImage *) imageWithColor:(UIColor *)color; //颜色转图形
+ (UIImage *) getVideoPreViewImage:(NSString *)path;
+ (UIImage *) videoImageWithvideoURL:(NSURL *)videoURL atTime:(NSTimeInterval)time;
+ (UIImage *) getImageForCountryName:(NSString *)countryName;

+ (UIButton *) createCommomBtWithFrame:(CGRect )frame andTitle:(NSString *) titleString;

+ (int ) getStateCodeFromString :(NSString *) stateString;
+ (long ) getUserAgeFromString :(NSString *) ageString;
+ (CGFloat) getTextHeightWithFont :(UIFont *) textFont getTextString:(NSString *) textString getFrameWidth:(CGFloat) frameWidth;

@end

NS_ASSUME_NONNULL_END

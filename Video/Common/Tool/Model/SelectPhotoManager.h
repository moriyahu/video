//
//  SelectPhotoManager.h
//  Video
//
//  Created by steve on 2020/3/23.
//  Copyright © 2020 steve. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JPImageresizerView.h"

typedef enum {
    PhotoCamera = 0,
    PhotoAlbum,
}SelectPhotoType;

NS_ASSUME_NONNULL_BEGIN

@interface SelectPhotoManager : NSObject<UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property(nonatomic, strong)void (^successHandle)(SelectPhotoManager *manager, UIImage *originalImage, UIImage *resizeImage);
@property(nonatomic, strong)void (^errorHandle)(NSString *error);
@property(nonatomic, strong)void (^clickHandle)(void);

@property (nonatomic, assign) UIStatusBarStyle statusBarStyle;
@property (nonatomic, strong) JPImageresizerConfigure *configure;

@property (nonatomic, weak) JPImageresizerView *imageresizerView;
@property (nonatomic, assign) JPImageresizerFrameType frameType;

//@property (nonatomic, assign) float resizeWHScale; // 设置长宽比例


- (void)startSelectPhotoWithImageName:(NSString *)imageName;

- (void)openPhoto;

- (void)chooseLibrary;

@end

NS_ASSUME_NONNULL_END

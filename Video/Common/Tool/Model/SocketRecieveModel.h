//
//  SocketRecieveModel.h
//  Video
//
//  Created by steve on 2020/5/28.
//  Copyright © 2020 steve. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SocketRecieveModel : NSObject

@property (nonatomic, strong) NSDictionary *msg;
@property (nonatomic, strong) NSString *msgType;

@end

NS_ASSUME_NONNULL_END

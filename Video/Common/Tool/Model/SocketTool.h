//
//  SocketTool.h
//  Video
//
//  Created by steve on 2020/3/23.
//  Copyright © 2020 steve. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SocketRocket.h>

NS_ASSUME_NONNULL_BEGIN

@interface SocketTool : NSObject

@property (nonatomic, assign) int reConnectTime;
@property (nonatomic, strong) SRWebSocket *webSocket;
//@property (nonatomic, strong) NSMutableDictionary *allBlockDic;

+ (instancetype)shareInstance;

#pragma socket
- (void)openSocket;
- (void)closeSocket;
- (void)reconnectSocket;

//- (void)sendInfoToSocket :(NSDictionary *)infoDic callback:(CallBack) callback;
- (void)sendInfoToSocket :(NSDictionary *)infoDic;

@end

NS_ASSUME_NONNULL_END

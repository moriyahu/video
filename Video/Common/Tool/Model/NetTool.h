//
//  NetTool.h
//  Video
//
//  Created by steve on 2020/3/23.
//  Copyright © 2020 steve. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NetTool : NSObject

//typedef void (^ClickBlock)(void);
//typedef void(^CallBack)(id _Nullable obj);
//typedef id _Nullable(^DataParse)(id _Nullable retData, NSError * _Nullable error);

+ (instancetype)shareInstance;

//- (void)putDatas:(NSMutableArray *)Datas callback:(CallBack) callback;
//- (void)putDatas:(NSMutableArray *)Datas fileType:(NSString *)fileTypeString callback:(CallBack) callback;
- (void)putDatas:(NSMutableArray *)Datas fileType:(NSString *)fileTypeString needShowHUD:(BOOL)needShowHUD callback:(CallBack) callback;

- (void)startRequest:(NSString *)url method:(NSString *)method params:(id) params needShowHUD:(BOOL)needShowHUD callback:(CallBack) callback;

- (void) sendRefleshProfileInfoNotification; //重新刷新profile的通知，用于coin，vip购买
//- (void) sendDismissUserLikeOtherPopViewNotification; //关闭likeOtherPop界面
//- (void) sendDismissMatchViewNotification; //关闭matchPop界面
//- (void) sendDismissCallOutViewNotification; //关闭likeOtherPop界面

@end

NS_ASSUME_NONNULL_END

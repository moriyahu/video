//
//  DataTool.m
//  Video
//
//  Created by steve on 2020/3/23.
//  Copyright © 2020 steve. All rights reserved.
//

//#import <Toast.h>
#import "Toast.h"
#import <SVProgressHUD.h>
#import <SAMKeychain/SAMKeychain.h>
#import <JWT/JWT.h>
#import <JWTAlgorithm.h>
#import <AVKit/AVKit.h>
#import <SDWebImage/SDImageCache.h>
#import <EBCustomBannerView.h>

#import "DataTool.h"
#import "MJExtension.h"

#import "HomeTabbarViewController.h"
#import "LoginViewController.h"
#import "SignUpAddPhotoViewController.h"
#import "CreateLiveVideoViewController.h"
#import "UserProfileViewController.h"

#import "UserTipPermissionView.h"
#import "JKPickView.h"
#import "UserBuyVipPopView.h"
#import "UserGetCoinsPopView.h"
#import "UserChooseGiftPopView.h"
#import "UserReportTypeDataModel.h"

@implementation DataTool

+ (void) showHomePage {
    HomeTabbarViewController *vc = [[HomeTabbarViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    nav.navigationBar.hidden = YES;
    [UIApplication sharedApplication].delegate.window.rootViewController = nav;
}

+ (void) showLoginPage {
    LoginViewController *vc = [[LoginViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [UIApplication sharedApplication].delegate.window.rootViewController = nav;
}

+ (void) showUploadPhotoPage {
    SignUpAddPhotoViewController *vc = [[SignUpAddPhotoViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [UIApplication sharedApplication].delegate.window.rootViewController = nav;
}

+ (void) showUploadVideoPage {
    CreateLiveVideoViewController *vc = [[CreateLiveVideoViewController alloc] init];
    [[DataTool getCurrentVC].navigationController pushViewController:vc animated:YES];
}

+ (void) showUserProfilePage:(UserProfileDataModel *)profileModel {
    UserProfileViewController *vc = [[UserProfileViewController alloc] init];
    vc.profileModel = profileModel;
    [[DataTool getCurrentVC].navigationController pushViewController:vc animated:YES];
}

+ (void) showGetPermissionViewDismissBlock:(CallBack) viewDismissBlock {
    
    UserTipPermissionView *view = [[UserTipPermissionView alloc] initWithFrame:[DataTool getCurrentVC].view.frame ];
    view.dismissBLock = ^{
        if (viewDismissBlock) {
            viewDismissBlock(@"dissmissBlock");
        }
    };
    
    [view show];
    
}

+ (void) showGetVipViewWithIndex:(int) pageIndex {
    
    if ([[UserInfo shareInstant].user.vipState isEqualToString:@"VIP"]) {
        return;
    }
    UserBuyVipPopView *view = [[UserBuyVipPopView alloc] initWithFrame:[[[[[UIApplication sharedApplication] delegate] window] rootViewController] view].frame showPageIndex:pageIndex];
    [view show];
}

+ (void) showGetCoinsView {
    UserGetCoinsPopView *view = [[UserGetCoinsPopView alloc] initWithFrame:[DataTool getCurrentVC].view.frame ];
    [view show];
}

+ (void) showSendGiftView:(CallBack) sendGiftBlock {
    UserChooseGiftPopView *view = [[UserChooseGiftPopView alloc] initWithFrame:[DataTool getCurrentVC].view.frame ];
    view.sendGiftBlock = ^(id  _Nullable obj) {
        sendGiftBlock(obj);
    };
    [view show];
}

+ (void) showBannerViewWithTitle:(NSString *) titleString {
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(ZoomSize(16), StatusBarHeight + ZoomSize(10), ZoomSize(343), ZoomSize(80))];
    
    UILabel *tipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(16), ZoomSize(17), ZoomSize(311), ZoomSize(50))];
    tipLb.numberOfLines = 0;
    [view addSubview:tipLb];
    
    tipLb.text = titleString;
    tipLb.font = [UIFont systemFontOfSize:ZoomSize(18)];
    tipLb.textColor = TextRedColor;
    tipLb.textAlignment = NSTextAlignmentCenter;
    
    view.layer.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor;
    view.layer.cornerRadius = ZoomSize(16);
    view.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5].CGColor;
    view.layer.shadowOffset = CGSizeMake(0, ZoomSize(2));
    view.layer.shadowOpacity = 1;
    view.layer.shadowRadius = ZoomSize(4);
    
    EBCustomBannerView *customView = [EBCustomBannerView customView:view block:^(EBCustomBannerViewMaker *make) {
        make.portraitMode = EBCustomViewAppearModeTop;
        make.stayDuration = 3.0 ;
    }];
    
    [customView show];
}

+ (void) showGiftViewWithTitle:(NSString *) giftTitleString {
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, (SCREEN_Height - SCREEN_Width)/2 - ZoomSize(40) , SCREEN_Width, SCREEN_Width)];
    UIImageView *giftIV = [[UIImageView alloc] initWithFrame:view.bounds];
    giftIV.contentMode = UIViewContentModeScaleAspectFit;
    giftIV.image = GetImageByName(giftTitleString);
    [view addSubview:giftIV];
    
    EBCustomBannerView *customView = [EBCustomBannerView customView:view block:^(EBCustomBannerViewMaker *make) {
        make.portraitMode = EBCustomViewAppearModeCenter;
        make.stayDuration = 1.0 ;
    }];
    
    [customView show];
}


#pragma HUD
+ (void) startShowHUD {
    [SVProgressHUD dismissWithDelay:60.0f];
    [SVProgressHUD show];
}

+ (void) dismissHUD {
    [SVProgressHUD dismiss];
}

+ (void) showHUDWithString:(NSString *)string {
    [SVProgressHUD showInfoWithStatus:string];
    [SVProgressHUD dismissWithDelay:2.0f];
}

+ (void) showToast:(NSString *)getString {
    [[UIApplication sharedApplication].delegate.window makeToast:getString duration:2.0 position:CSToastPositionTop];
}

+ (void) reportBlockUserName:(NSString *) userName andBlockUserId:(NSString *) BlockUserId reportCallback:(CallBack) reportCallback blockCallback:(CallBack) blockCallback {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle: UIAlertControllerStyleActionSheet];
    
    [alertController addAction: [UIAlertAction actionWithTitle: @"Report" style: UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        NSMutableArray *reportDataArray = [NSMutableArray arrayWithObjects:@"STOLEN_PHOTO",@"INAPPROPRIATE_CONTENT",@" RUDE_OR_ABUSIVE",@"SENDING_SPAM",@"SCAMMER", nil];
        NSMutableArray *reportShowArray = [NSMutableArray arrayWithObjects:@"Stolen photo",@"Inappropriate content",@" Rude or abusive",@"Sending spam",@"Scammer", nil];
        
        JKPickView *pickview = [[JKPickView alloc] initWithFrame:[DataTool getCurrentVC].view.frame];
        pickview.dataArray = reportShowArray;
        pickview.titleString = [NSString stringWithFormat:@"Report %@", userName];
        [pickview show];
        
        pickview.back = ^(NSString *callBackString) {
            
            NSUInteger index = [reportShowArray indexOfObject:callBackString];
            NSString *reportDataString = reportDataArray[index];
            
            NSDictionary *paramsDic = @{ @"beReportUserId":BlockUserId, @"type":reportDataString, @"description":callBackString, @"platform":@"iOS"};
            [[NetTool shareInstance] startRequest:API_Reports_Save method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
                [DataTool showBannerViewWithTitle:@"Your report has been submited successfully"];
                reportCallback(@"report success");
            }];
        };
        
    }]];
    
    [alertController addAction: [UIAlertAction actionWithTitle: @"Block" style: UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:@"Are you sure you wanna block %@?",userName] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAlert = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *sureAlert = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

            NSDictionary *paramsDic1 = @{@"beBlockUserId":BlockUserId};
            [[NetTool shareInstance] startRequest:API_Block_Save method:PostMethod params:paramsDic1 needShowHUD:YES callback:^(id  _Nullable obj) {
                
                [DataTool showBannerViewWithTitle:[NSString stringWithFormat:@"You have blocked %@ successfully",userName]];
                blockCallback(@"block success");
            }];
        }];
        
        [alertController addAction:cancelAlert];
        [alertController addAction:sureAlert];
        
        [[DataTool getCurrentVC] presentViewController:alertController animated:YES completion:nil];
        
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [[DataTool getCurrentVC].navigationController presentViewController:alertController animated:YES completion:nil];
}

+ (UIViewController *)getCurrentVC {
    UIViewController *vc = [UIApplication sharedApplication].keyWindow.rootViewController;
    UIViewController *currentShowingVC = [self findCurrentShowingViewControllerFrom:vc];
    return currentShowingVC;
}

+ (UIViewController *)findCurrentShowingViewControllerFrom:(UIViewController *)vc {
    UIViewController *currentShowingVC; //方法1：递归方法 Recursive method
    if ([vc presentedViewController]) { //注要优先判断vc是否有弹出其他视图，如有则当前显示的视图肯定是在那上面
        UIViewController *nextRootVC = [vc presentedViewController]; // 当前视图是被presented出来的
        currentShowingVC = [self findCurrentShowingViewControllerFrom:nextRootVC];
    } else if ([vc isKindOfClass:[UITabBarController class]]) {
        UIViewController *nextRootVC = [(UITabBarController *)vc selectedViewController];  // 根视图为UITabBarController
        currentShowingVC = [self findCurrentShowingViewControllerFrom:nextRootVC];
    } else if ([vc isKindOfClass:[UINavigationController class]]){
        UIViewController *nextRootVC = [(UINavigationController *)vc visibleViewController]; // 根视图为UINavigationController
        currentShowingVC = [self findCurrentShowingViewControllerFrom:nextRootVC];
    } else {
        currentShowingVC = vc; // 根视图为非导航类
    }
    return currentShowingVC;
}

+ (NSString *) getDeviceUUID {
    NSString * deviceUUID = [self getContentFromKeyChain:@"DeviceUUID"];
    if ([DataTool isEmptyString:deviceUUID ] || [deviceUUID isEqualToString:@"00000000000000000000000000000000"]) {
        NSString *getAdID = [[NSUUID UUID] UUIDString];
        NSString *adId = [getAdID stringByReplacingOccurrencesOfString:@"-" withString:@""];
        [self saveContentToKeyChain:adId forKey:@"DeviceUUID" service:@"DeviceUUID"];
        deviceUUID = adId;
    }
    NSLog(@"deviceUUID %@", deviceUUID);
    return deviceUUID;
}

+ (NSString *) getTrimmingString:(NSString *) textString {
    return [textString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

+ (NSString *)getTimeNow {
    NSString* date;
    NSDateFormatter * formatter = [[NSDateFormatter alloc ] init];
    [formatter setDateFormat:@"YYYYMMddhhmmssSSS"];
    date = [formatter stringFromDate:[NSDate date]];
    
    int last = arc4random() % 10000;//取出个随机数
    NSString *timeNow = [[NSString alloc] initWithFormat:@"%@-%i", date,last];
    return timeNow;
}

+ (NSString *) getTimeStampString {
    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval timeInterval = [date timeIntervalSince1970];
    NSString *timeStampString  = [NSString stringWithFormat:@"%0.f", timeInterval];
    return timeStampString;
}

+ (NSString *) getTimTimeFromString :(NSDate *)timDate {
    
//    当天显示时间，昨天显示yesterday
//    昨天之前就显示日期02/03
//    去年  2019/02/23
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc ] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString* dateString = @"";
    
    NSDateComponents *dateComponentYear = [[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:timDate toDate:[NSDate date] options:0];
    NSDateComponents *dateComponentDay = [[NSCalendar currentCalendar] components:NSCalendarUnitDay fromDate:timDate toDate:[NSDate date] options:0];
    
    if (dateComponentYear.year > 1 ) {
        [formatter setDateFormat:@"yyyy-MM-dd"];
    } else {
        if (dateComponentDay.day > 1){
            [formatter setDateFormat:@"MM-dd"];
        } else {
            [formatter setDateFormat:@"hh:mm"];
        }
    }
    
    dateString = [formatter stringFromDate:timDate];
    if (dateComponentDay.day == 1) {
        dateString = @"yesterday";
    }
    
    return dateString;
}

+ (NSString *) getVersionString {
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *app_version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    return app_version;
}

//+ (NSString *) jwtActionString {
//    NSString *jwtToken = [UserInfo shareInstant].user.token;
//    NSString *secret = @"78cIYsXMF9TLDCPy";
//    NSString *algorithmName = @"HS256"; //Must specify an algorithm to use
//
//    NSDictionary *payload = [JWTBuilder decodeMessage:jwtToken].secret(secret).algorithmName(algorithmName).decode;
//    NSString *getID = [ payload[@"payload"][@"id"] stringValue];
//
//    return getID;
//}


//+ (NSString *) lowercaseInputString:(NSString *) inputString {
//    return [inputString lowercaseString];
//}
//
//+ (NSString *) uppercaseInputString:(NSString *) inputString {
//    return [inputString lowercaseString];
//}

#pragma keyChain
+ (NSString *) getContentFromKeyChain:(NSString *) key {
    return [SAMKeychain passwordForService:@"DeviceUUID" account:key];
}

+ (BOOL) saveContentToKeyChain: (NSString *) content forKey: (NSString *) key service: (NSString *) service {
    return [SAMKeychain setPassword:content forService:service account:key];
}

//+ (BOOL) removeContentFromKeyChain: (NSString *) key service: (NSString *) service {
//    return [SAMKeychain deletePasswordForService:service account:key];
//}

+ (BOOL) isValidateEmail:(NSString *)email {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

+ (BOOL) isEmptyString:(NSString *)string {
    if (string == nil || string == NULL || [string isEqual:[NSNull null]] ) {
        return YES;
    }
    NSString * newSelf = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
    if(nil == string
       || string.length ==0
       || [string isEqualToString:@""]
       || [string isEqualToString:@"<null>"]
       || [string isEqualToString:@"(null)"]
       || [string isEqualToString:@"null"]
       || newSelf.length ==0
       || [newSelf isEqualToString:@""]
       || [newSelf isEqualToString:@"<null>"]
       || [newSelf isEqualToString:@"(null)"]
       || [newSelf isEqualToString:@"null"]
       || [self isKindOfClass:[NSNull class]] ){
        return YES;
    }else{
        NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        NSString *trimedString = [string stringByTrimmingCharactersInSet:set];
        return [trimedString isEqualToString: @""];
    }
    return NO;
}

+ (UIImage *)compressSizeImage:(UIImage *)image {
    
    CGSize size = image.size;
    UIImage *resultImage = image;
    if (size.width >= size.height) {
        
        if (size.height > 1080) { //横屏图片，目标为: w 1920, h 1080
            int newWidth = (size.width/size.height) * 1080; //设置，当图片为横屏图片，且图片的高度大于 1080，则图片需要被压缩
            int newHeight = 1080;
            
            UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
            [resultImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
            resultImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            return resultImage;
        } else {
            return image;
        }
    } else {
        
        if (size.width > 1080) { //竖屏图片，目标为: w 1080, h 1920
            int newWidth = 1080;
            int newHeight = (size.height / size.width) * 1080; //设置，当图片为竖屏图片，且图片的宽度大于1080，则图片需要被压缩
            
            UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
            [resultImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
            resultImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            return resultImage;
        } else {
            return image;
        }
    }
    return image;
}

+ (UIImage *) imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f,0.0f, 1.0f,1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context =UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image =UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

+ (UIImage* )videoImageWithvideoURL:(NSURL *)videoURL atTime:(NSTimeInterval)time {

    SDImageCache *cache =  [SDImageCache sharedImageCache]; //先从缓存中查找是否有图片
    UIImage *memoryImage =  [cache imageFromMemoryCacheForKey:videoURL.absoluteString];
    if (memoryImage) {
        return memoryImage;
    }else{ //再从磁盘中查找是否有图片
        UIImage *diskImage =  [cache imageFromDiskCacheForKey:videoURL.absoluteString];
        if (diskImage) {
            return diskImage;
        }
    }

    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
    NSParameterAssert(asset);
    AVAssetImageGenerator *assetImageGenerator =[[AVAssetImageGenerator alloc] initWithAsset:asset];
    assetImageGenerator.appliesPreferredTrackTransform = YES;
    assetImageGenerator.apertureMode = AVAssetImageGeneratorApertureModeEncodedPixels;

    CGImageRef thumbnailImageRef = NULL;
    CFTimeInterval thumbnailImageTime = time;
    NSError *thumbnailImageGenerationError = nil;
    thumbnailImageRef = [assetImageGenerator copyCGImageAtTime:CMTimeMake(thumbnailImageTime, 60)actualTime:NULL error:&thumbnailImageGenerationError];

    if(!thumbnailImageRef) {
        NSLog(@"thumbnailImageGenerationError %@",thumbnailImageGenerationError);
    }

    UIImage*thumbnailImage = thumbnailImageRef ? [[UIImage alloc]initWithCGImage: thumbnailImageRef] : nil;
    [cache storeImage:thumbnailImage forKey:videoURL.absoluteString toDisk:YES completion:nil];
    return thumbnailImage;

}

+ (UIImage*) getVideoPreViewImage:(NSString *)path{
    
    //    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:[NSURL fileURLWithPath:path] options:nil]; //本地视频
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:[NSURL URLWithString:path] options:nil]; //网络视频
    AVAssetImageGenerator *assetGen = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    
    assetGen.appliesPreferredTrackTransform = YES;
    CMTime time = CMTimeMakeWithSeconds(0.0, 0);
    NSError *error = nil;
    CMTime actualTime;
    CGImageRef image = [assetGen copyCGImageAtTime:time actualTime:&actualTime error:&error];
    UIImage *videoImage = [[UIImage alloc] initWithCGImage:image];
    CGImageRelease(image);
    if (videoImage == nil) {
        videoImage = [UIImage imageNamed:@"default_video.png"];
    }
    return videoImage;
}

//    logoIV.image = [DataTool getImageForCountryName:@"Barbados"];
+ (UIImage *)getImageForCountryName:(NSString *)countryName{
    
    if ([ DataTool isEmptyString:countryName]) {
//        countryName = @"United States";
        return nil;
    }
    
    NSBundle *resourceBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"Phone-Country-Code-and-Flags" ofType:@"bundle"]];
    
    NSData *jasonData = [[[NSString alloc] initWithContentsOfFile:[resourceBundle pathForResource:@"flag_indices" ofType:@"json"] encoding:NSUTF8StringEncoding error:NULL] dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *flagIndices = [NSJSONSerialization JSONObjectWithData:jasonData options:kNilOptions error:nil];
    
    NSNumber * y = flagIndices[countryName];
    
    if (!y) {
        y = @0;
    }
    CGImageRef cgImage = CGImageCreateWithImageInRect([[UIImage imageWithContentsOfFile:[resourceBundle pathForResource:@"flags" ofType:@"png"]] CGImage], CGRectMake(0, y.integerValue * 2, 32, 32));
    UIImage * result = [UIImage imageWithCGImage:cgImage scale:2.0 orientation:UIImageOrientationUp];
    CGImageRelease(cgImage);
    
    return result;
}

+ (UIButton *) createCommomBtWithFrame:(CGRect )frame andTitle:(NSString *) titleString {
    
    UIButton *addPhotoButton = [[UIButton alloc] initWithFrame:frame];
    addPhotoButton.layer.cornerRadius = frame.size.height/2.0;
    addPhotoButton.layer.masksToBounds = YES;
    
    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.frame = addPhotoButton.bounds;
    gl.startPoint = CGPointMake(0.56, -0.77);
    gl.endPoint = CGPointMake(0.46, 1.66);
    gl.colors = @[(__bridge id)[UIColor colorWithRed:238/255.0 green:48/255.0 blue:202/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:137/255.0 green:36/255.0 blue:244/255.0 alpha:1.0].CGColor];
    gl.locations = @[@(0), @(1.0f)];
    [addPhotoButton.layer addSublayer:gl];
    
    UILabel *addPhotoLb = [[UILabel alloc] initWithFrame:addPhotoButton.bounds];
    [addPhotoButton addSubview:addPhotoLb];
    addPhotoLb.text = titleString;
    addPhotoLb.textAlignment = NSTextAlignmentCenter;
    addPhotoLb.textColor = WhiteColor;
    addPhotoLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightSemibold];
    
    return addPhotoButton;
}

+ (int ) getStateCodeFromString :(NSString *) stateString {
    
//    UN_AUDIT-未审核,AUDIT_ACCESS-审核通过,AUDIT_REJECT-审核拒绝
    
    int code = 0;
    if ([stateString isEqualToString:@"AUDIT_ACCESS"]) {
        code = 1;
    } else if ([stateString isEqualToString:@"AUDIT_REJECT"]) {
        code = 2;
    }
    return code;
}

+ (long ) getUserAgeFromString :(NSString *) ageString {
    long age = 0;
    if (![DataTool isEmptyString:ageString]) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *birdayDate =[dateFormatter dateFromString:ageString];
        
        NSDateComponents *date = [[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:birdayDate toDate:[NSDate date] options:0];
        age = date.year;
    }
    return age;
}


+ (CGFloat) getTextHeightWithFont :(UIFont *) textFont getTextString:(NSString *) textString getFrameWidth:(CGFloat) frameWidth {
    
    if ([DataTool isEmptyString:textString]) {
        return 0;
    }
    
    NSDictionary *attributes = @{NSFontAttributeName:textFont};
    CGSize textSize = [textString boundingRectWithSize:CGSizeMake(frameWidth, SCREEN_Height) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;;
    
    return textSize.height;
}



//- (TIMMessage *)callRequestMessageWithRoomId:(NSString *)roomId userId:(NSString *)userId sendType:(LVMessageSenderType)sendType {
//    TIMMessage *imMsg = [[TIMMessage alloc] init];
//    TIMCustomElem * custom_elem = [[TIMCustomElem alloc] init];
//    LVIMCustomData *data = [LVIMCustomData new];
//    data.type = LVIMCustomDataTypeVideo;
//    LVIMVideoInfo *info = [LVIMVideoInfo new];
//    info.roomId = roomId;
//    info.userId = userId;
//    info.sendType = sendType;
//    data.msgObject = info;
//    NSString *dataStr = [NSString JSONString:data.zl_dictionary];
//    custom_elem.data = [dataStr dataUsingEncoding:NSUTF8StringEncoding];
//    NSString *desc = @"[Video Call]";
//    TIMOfflinePushInfo *pushInfo = [TIMOfflinePushInfo new];
//    pushInfo.desc = desc;
//    TIMIOSOfflinePushConfig *config = [TIMIOSOfflinePushConfig new];
//    config.ignoreBadge = NO;
//    config.sound = @"video_chat_responder.mp3";
//    pushInfo.iosConfig = config;
//    pushInfo.pushFlag = TIM_OFFLINE_PUSH_DEFAULT;
//    [imMsg setOfflinePushInfo:pushInfo];
//    [imMsg addElem:custom_elem];
//    return imMsg;
//}


@end

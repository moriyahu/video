//
//  EditPhotoPageViewController.h
//  Video
//
//  Created by steve on 2020/3/23.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EditPhotoPageViewController : UIViewController

@property (nonatomic, strong) UIImage *getImage;
//@property (nonatomic, assign) float resizeWHScale; // 设置长宽比例

@property (nonatomic, copy) CallBack callbackImg;


@end

NS_ASSUME_NONNULL_END

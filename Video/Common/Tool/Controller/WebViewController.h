//
//  WebViewController.h
//  Video
//
//  Created by steve on 2020/4/3.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WebViewController : UIViewController

@property (nonatomic, strong) NSString *url;

@end

NS_ASSUME_NONNULL_END

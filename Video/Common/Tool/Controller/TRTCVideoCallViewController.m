//
//  TRTCVideoCallViewController.m
//  Video
//
//  Created by steve on 2020/5/29.
//  Copyright © 2020 steve. All rights reserved.
//

#import "TRTCVideoCallViewController.h"

#import "TUIKitConfig.h"
#import "TUIKit.h"
#import <TRTCCloud.h>
#import <TRTCCloudDelegate.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <MJExtension/MJExtension.h>
#import "TUIKitConfig.h"
#import "TUIKit.h"

@interface TRTCVideoCallViewController ()<TRTCCloudDelegate> {
    UIView *localView;
    UIView *remoteView;
    NSDictionary *roomDic;
}

//@property (nonatomic, strong) UIView *callOutTipView;
//@property (nonatomic, strong) UIView *trtcShowView;
//
//@property (nonatomic, strong) UIImageView *logoIV;
//@property (nonatomic, strong) UILabel *nameLb;
//@property (nonatomic, strong) UIImageView *onlineLogoIV;


@end

@implementation TRTCVideoCallViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self setdata];
    
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.tabBarController.tabBar.hidden = YES;
}

- (void)setupUI {
    
    remoteView = [[UIView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:remoteView];
    remoteView.backgroundColor = BGGrayColor;
    
    localView = [[UIView alloc] initWithFrame:CGRectMake(ZoomSize(277), StatusBarHeight + ZoomSize(61) , ZoomSize(82), ZoomSize(122))];
    [self.view addSubview:localView];
    localView.layer.cornerRadius = ZoomSize(16);
    localView.layer.masksToBounds = YES;
    localView.backgroundColor = WhiteColor;
    
    UIImageView* logoIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(0), StatusBarHeight + ZoomSize(0), ZoomSize(40), ZoomSize(40))];
    [logoIV sd_setImageWithURL:[NSURL URLWithString:_profileModel.headPortraitUrl] placeholderImage:GetImageByName(@"message_user_nopho")];
    logoIV.layer.cornerRadius = ZoomSize(20);
    logoIV.contentMode = UIViewContentModeScaleAspectFill;
    logoIV.layer.masksToBounds = YES;
    [self.view addSubview:logoIV];
    
    UILabel *nameLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(45), StatusBarHeight, ZoomSize(160), ZoomSize(40))];
    nameLb.text =  [NSString stringWithFormat:@"%@,%ld",  _profileModel.nickName, [DataTool getUserAgeFromString:_profileModel.birthday]];
    nameLb.textColor = WhiteColor;
    nameLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightSemibold];
    [self.view addSubview:nameLb];
    
    UIImageView *onlineLogoIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(140), StatusBarHeight + ZoomSize(8), ZoomSize(28), ZoomSize(28))];
    onlineLogoIV.image = GetImageByName(@"common_online");
    [self.view addSubview:onlineLogoIV];
    onlineLogoIV.hidden = YES;
    if ([_profileModel.onlineState isEqualToString:@"ONLINE"]) {
        onlineLogoIV.hidden = NO;
    }
    
    UIImageView * verifyLogoIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(170), StatusBarHeight + ZoomSize(8), ZoomSize(28), ZoomSize(28))];
    verifyLogoIV.image = GetImageByName(@"common_validation");
    [self.view addSubview:verifyLogoIV];
    verifyLogoIV.hidden = YES;
    if (_profileModel.videos.count > 0) {
        verifyLogoIV.hidden = NO;
    }
    
    UIButton *reportBt = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(100), StatusBarHeight + ZoomSize(8) , ZoomSize(40), ZoomSize(30))];
    [self.view addSubview:reportBt];
    [reportBt setImage:GetImageByName(@"common_report") forState:UIControlStateNormal];
    [reportBt addTarget:self action:@selector(reportAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *closeBt = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(60), StatusBarHeight + ZoomSize(8), ZoomSize(60), ZoomSize(30))];
    [self.view addSubview:closeBt];
    [closeBt setImage:GetImageByName(@"video_close") forState:UIControlStateNormal];
    [closeBt addTarget:self action:@selector(hangUpAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *nationIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(51),StatusBarHeight + ZoomSize(46), ZoomSize(20), ZoomSize(16))];
    nationIV.contentMode = UIViewContentModeScaleAspectFit;
    nationIV.image = [DataTool getImageForCountryName:_profileModel.country];
    [self.view addSubview:nationIV];
    
    UILabel *nationLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(70),StatusBarHeight + ZoomSize(46), ZoomSize(150), ZoomSize(16))];
    [self.view addSubview:nationLb];
    nationLb.text = _profileModel.country;
    nationLb.textColor = WhiteColor;
    nationLb.font = [UIFont systemFontOfSize:ZoomSize(14)];
    
    UIButton *giftButton = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(154), ZoomSize(600), ZoomSize(66), ZoomSize(66))];
    [giftButton addTarget:self action:@selector(giftAction) forControlEvents:UIControlEventTouchUpInside];
    [giftButton setBackgroundImage:GetImageByName(@"message_gift") forState:UIControlStateNormal];
    [self.view addSubview:giftButton];
    
}

- (void)setdata {
    [TRTCCloud sharedInstance].delegate = self;
    
    /// 设置视频通话的画质（帧率 15fps，码率550, 分辨率 360*640）
    TRTCVideoEncParam *videoEncParam = [[TRTCVideoEncParam alloc] init];
    videoEncParam.videoResolution = TRTCVideoResolution_640_360;
    videoEncParam.resMode = TRTCVideoResolutionModePortrait;
    [[TRTCCloud sharedInstance] setVideoEncoderParam:videoEncParam];
    
    //    设置默认美颜效果（美颜效果：自然，美颜级别：5, 美白级别：1）
    TXBeautyManager *beautyManager = [[TRTCCloud sharedInstance] getBeautyManager];
    [beautyManager setBeautyStyle:TXBeautyStyleSmooth];
    [beautyManager setBeautyLevel:5]; //美颜
    [beautyManager setWhitenessLevel:5]; //美白
    [beautyManager setRuddyLevel:1]; //红润
    
    //设置本地
    [[TRTCCloud sharedInstance] setLocalViewFillMode:TRTCVideoFillMode_Fill];
    [[TRTCCloud sharedInstance] startLocalPreview:YES view:localView];
    [[TRTCCloud sharedInstance] startLocalAudio];
    
}

- (void)setProfileModel:(UserProfileDataModel *)profileModel {
    _profileModel = profileModel;
}

- (void)setRoomId:(NSString *)roomId {
    _roomId = roomId;
    [self enterTRTCRoom:[roomId intValue]];
}

- (void)setCallType:(NSString *)callType {
    _callType = callType;
}

#pragma function


- (void)reportAction {
    [DataTool reportBlockUserName:_profileModel.nickName andBlockUserId:_profileModel.userId reportCallback:^(id  _Nullable obj) {
        NSLog(@"reportSuccess");
    } blockCallback:^(id  _Nullable obj) {
        //        [self getSpark];
        [self hangUpAction];
    }];
}


- (void)hangUpAction {
    
    [[TRTCCloud sharedInstance] exitRoom];
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void) giftAction {
    NSLog(@"giftAction");
    [DataTool showSendGiftView:^(id  _Nullable obj) {
        NSLog(@"obj %@", obj);
        
        int giftIndexNum = [obj intValue];
        NSString *giftPId = GiftPIDArray[giftIndexNum];
        NSString *giftCoinNum = GiftCoinNumArray[giftIndexNum];
        
        if ([[UserInfo shareInstant].user.balance intValue ] < [giftCoinNum intValue]) {
            [DataTool showGetCoinsView];
            return;
        }
        
        NSDictionary *paramsDic = @{@"giftId":giftPId, @"receiverId":self->_profileModel.userId}; //    //获取用户资料
        
        [[NetTool shareInstance] startRequest:API_Gift_Send method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
            
            NSString *giftNameString = [GiftImgShowArray objectAtIndex:giftIndexNum];
            [DataTool showGiftViewWithTitle:giftNameString];
            [[UserInfo shareInstant] sendTextMessageToOther:self->_profileModel.userId messgeTextString:[NSString stringWithFormat:@"[%@]", giftNameString]];
            
            NSDictionary *resultDic = obj;
            UserProfileDataModel *userProfileDataModel = [UserProfileDataModel mj_objectWithKeyValues:resultDic];
            [[UserInfo shareInstant] checkUserDataModel:userProfileDataModel callBackInfo:^(id  _Nullable obj) {
                [[NetTool shareInstance] sendRefleshProfileInfoNotification];
            }];
            
        }];
    }];
    
}

- (void)enterTRTCRoom:(int ) getRoomId {
    TRTCParams *params = [[TRTCParams alloc] init];
    params.sdkAppId = [TXIMSDK_APP_KEY intValue];
    params.userId = [UserInfo shareInstant].user.userId;
    params.userSig = [UserInfo shareInstant].user.sig;
    
    //    params.userId = TestUserId;
    //    params.userSig = TestUserSig;
    params.roomId = getRoomId;
    
    [[TRTCCloud sharedInstance] enterRoom:params appScene:TRTCAppSceneVideoCall];
}

- (void) startRoomAction {
}

- (void) endRoomAction {
}

#pragma TRTCCloud delegate

- (void)onEnterRoom:(NSInteger)result {
    
    if ( result > 0 ) {
        NSLog(@"进房成功 result %ld", (long)result);
        
        if ([_callType isEqualToString: @"CallOut"]) {
            
            roomDic = @{@"callId": [UserInfo shareInstant].user.userId, @"receiverId":_profileModel.userId, @"roomId":_roomId};
            [[NetTool shareInstance] startRequest:API_Room_Start method:PostMethod params:roomDic needShowHUD:NO callback:^(id  _Nullable obj) {
                
            }];
        }
        
    } else {
        NSLog(@"进房失败");
    }
}

- (void)onError:(TXLiteAVError)errCode errMsg:(NSString *)errMsg extInfo:(NSDictionary *)extInfo {
    
}

- (void)onUserVideoAvailable:(NSString *)userId available:(BOOL)available {
    
    if (available) {
        [[TRTCCloud sharedInstance] startRemoteView:userId view:remoteView];
        [[TRTCCloud sharedInstance] setRemoteViewFillMode:userId mode:TRTCVideoFillMode_Fit];
        
    } else {
        [[TRTCCloud sharedInstance] stopRemoteView:userId];
    }
    
}

- (void)onExitRoom:(NSInteger)reason {
//    [[TRTCCloud sharedInstance] exitRoom];
}

- (void)onRemoteUserLeaveRoom:(NSString *)userId reason:(NSInteger)reason {
    
    [[TRTCCloud sharedInstance] exitRoom];
    [self.navigationController popViewControllerAnimated:YES];
    
    if ([_callType isEqualToString: @"CallOut"]) {
        [[NetTool shareInstance] startRequest:API_Room_End method:PostMethod params:roomDic needShowHUD:NO callback:^(id  _Nullable obj) {
        }];
    }
}


@end

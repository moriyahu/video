//
//  ChatViewController.h
//  Video
//
//  Created by steve on 2020/5/8.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TUIChatController.h"
NS_ASSUME_NONNULL_BEGIN

@interface ChatViewController : TUIChatController

@property (nonatomic, strong) UserProfileDataModel *profileModel;
@property (nonatomic, assign) TIMConversationType type;
@property (nonatomic, strong) NSString *receiverId;


@end

NS_ASSUME_NONNULL_END

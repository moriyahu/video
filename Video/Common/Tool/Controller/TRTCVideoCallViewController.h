//
//  TRTCVideoCallViewController.h
//  Video
//
//  Created by steve on 2020/5/29.
//  Copyright © 2020 steve. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TRTCVideoCallViewController : UIViewController

@property (nonatomic, strong) UserProfileDataModel *profileModel;
@property (nonatomic, strong) NSString *roomId;
@property (nonatomic, strong) NSString *callType; // CallOut, CallIn

@end

NS_ASSUME_NONNULL_END

//
//  ChatViewController.m
//  Video
//
//  Created by steve on 2020/5/8.
//  Copyright © 2020 steve. All rights reserved.
//

#import "ChatViewController.h"
#import "FeedBackViewController.h"
#import "TUITextMessageCellData.h"

#import <MJExtension/MJExtension.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface ChatViewController() {
    UIButton *infoBt;
    UIImageView *logoIV;
    UILabel *titleLb;
    UIImageView *onlineLogoIV;
    UIImageView *verifyLogoIV;
    
    UIButton *moreBt;
}

@property (nonatomic, strong) UIView *navView;

@end

@implementation ChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self setdata];
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.tabBarController.tabBar.hidden = YES;
}

- (void)setupUI {
    //    self.view.backgroundColor = BGDarkBlueColor;
    [self.view addSubview:self.navView];
#warning 需要显示自定义消息，gift消息
    
}

- (void)setdata {
    
    if (_profileModel != nil) {
        [logoIV sd_setImageWithURL:[NSURL URLWithString:_profileModel.headPortraitUrl] placeholderImage:GetImageByName(@"message_vido")];
        //        titleLb.text = _profileModel.nickName;
        titleLb.text =  [NSString stringWithFormat:@"%@,%ld",  _profileModel.nickName, [DataTool getUserAgeFromString:_profileModel.birthday]];
        
        if ([_profileModel.onlineState isEqualToString:@"ONLINE"]) {
            onlineLogoIV.hidden = NO;
        }
        
        if (_profileModel.videos.count > 0) {
            verifyLogoIV.hidden = NO;
        }
    }
    
}

- (void)setType:(TIMConversationType)type {
    _type = type;
}

- (void)setReceiverId:(NSString *)receiverId {
    _receiverId = receiverId;
    
    NSDictionary *paramsDic = @{@"userId":_receiverId}; //    //获取用户资料
    [[NetTool shareInstance] startRequest:API_User_Profile method:PostMethod params:paramsDic needShowHUD:NO callback:^(id  _Nullable obj) {
        NSDictionary *resultDic = obj;
        self->_profileModel = [UserProfileDataModel mj_objectWithKeyValues:resultDic];
        [self setdata];
    }];
    
}

#pragma mark - Function
- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)infoAction {
    NSLog(@"infoAction");
    //    [DataTool showUserProfilePage];
    
    if (_profileModel != nil) {
        [DataTool showUserProfilePage:_profileModel];
    }
}

//- (void)moreAction {
//
////    [DataTool reportBlockUserName:@"Alice" andBlockUserId:@"460402390944796672" reportCallback:^(id  _Nullable obj) {
////        NSLog(@"reportSuccess");
////    } blockCallback:^(id  _Nullable obj) {
////        NSLog(@"blockSuccess");
////    }];
//
//}

- (void)moreAction {
    NSLog(@"more");
    
    [DataTool reportBlockUserName:_profileModel.nickName andBlockUserId:_profileModel.userId reportCallback:^(id  _Nullable obj) {
        NSLog(@"reportSuccess");
    } blockCallback:^(id  _Nullable obj) {
        NSLog(@"blockSuccess");
    }];
}

- (void)inputControllerSelectVideoCall:(TUIInputController *)inputController {    
    [[UserInfo shareInstant] showCallOutView:_profileModel];
    
}

- (void)inputControllerSelectSendGift:(TUIInputController *)inputController selectGiftIndex:(NSString *)giftIndex {
    
    int giftIndexNum = [giftIndex intValue];
    NSString *giftPId = GiftPIDArray[giftIndexNum];
    NSString *giftCoinNum = GiftCoinNumArray[giftIndexNum];
    
    if ([[UserInfo shareInstant].user.balance intValue ] < [giftCoinNum intValue]) {
        [DataTool showGetCoinsView];
        return;
    }
    
    NSDictionary *paramsDic = @{@"giftId":giftPId, @"receiverId":_receiverId}; //    //获取用户资料
    
    [[NetTool shareInstance] startRequest:API_Gift_Send method:PostMethod params:paramsDic needShowHUD:YES callback:^(id  _Nullable obj) {
        
        
        NSString *giftNameString = [GiftImgShowArray objectAtIndex:giftIndexNum];
        
        [DataTool showGiftViewWithTitle:giftNameString];
        [[UserInfo shareInstant] sendTextMessageToOther:self->_profileModel.userId messgeTextString:[NSString stringWithFormat:@"[%@]", giftNameString]];
        
        NSDictionary *resultDic = obj;
        UserProfileDataModel *userProfileDataModel = [UserProfileDataModel mj_objectWithKeyValues:resultDic];
        [[UserInfo shareInstant] checkUserDataModel:userProfileDataModel callBackInfo:^(id  _Nullable obj) {
            [[NetTool shareInstance] sendRefleshProfileInfoNotification];
        }];
        
    }];
    
}

#pragma Lazy
- (UIView *)navView {
    if (!_navView) {
        _navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];
        _navView.backgroundColor = BGDarkBlueColor;
        
        UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(10), StatusBarHeight + ZoomSize(5), ZoomSize(40), ZoomSize(30))];
        [_navView addSubview:backBt];
        [backBt setImage:GetImageByName(@"common_back") forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        infoBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(60), StatusBarHeight, SCREEN_Width - ZoomSize(120), NavigationHeight - StatusBarHeight)];
        [_navView addSubview:infoBt];
        [infoBt addTarget:self action:@selector(infoAction) forControlEvents:UIControlEventTouchUpInside];
        
        logoIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(0), ZoomSize(0), ZoomSize(40), ZoomSize(40))];
        logoIV.layer.cornerRadius = ZoomSize(20);
        logoIV.layer.masksToBounds = YES;
        logoIV.image = GetImageByName(@"message_vido");
        logoIV.contentMode = UIViewContentModeScaleAspectFill;
        [infoBt addSubview:logoIV];
        
        titleLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(45), 0, ZoomSize(160), ZoomSize(40))];
        titleLb.text = @"";
        titleLb.textColor = WhiteColor;
        titleLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightSemibold];
        [infoBt addSubview:titleLb];
        
        UIButton *moreBt = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(60), StatusBarHeight, ZoomSize(60), ZoomSize(30))];
        [_navView addSubview:moreBt];
        [moreBt setImage:GetImageByName(@"profile_more") forState:UIControlStateNormal];
        [moreBt addTarget:self action:@selector(moreAction) forControlEvents:UIControlEventTouchUpInside];
        
        onlineLogoIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(140), ZoomSize(8), ZoomSize(28), ZoomSize(28))];
        onlineLogoIV.image = GetImageByName(@"common_online");
        [infoBt addSubview:onlineLogoIV];
        onlineLogoIV.hidden = YES;
        
        verifyLogoIV = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(170), ZoomSize(8), ZoomSize(28), ZoomSize(28))];
        verifyLogoIV.image = GetImageByName(@"common_validation");
        [infoBt addSubview:verifyLogoIV];
        verifyLogoIV.hidden = YES;
        
    }
    return _navView;
}


@end
